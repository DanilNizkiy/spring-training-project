package com.nizkiyd.backend.example.event.listener;

import com.nizkiyd.backend.example.event.SignalToContentManagerStatusChangedEvent;
import com.nizkiyd.backend.example.service.RegisteredUserNotifacationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotifyRegisteredUserOnFixedSignalListener {

    @Autowired
    private RegisteredUserNotifacationService registeredUserNotifacationService;

    @Async
    @EventListener(
            condition = "#event.newStatus == T(com.nizkiyd.backend.example.domain.SignalToContentManagerStatus).FIXED")
    public void onEvent(SignalToContentManagerStatusChangedEvent event) {
        log.info("handline {}", event);
        registeredUserNotifacationService.notifyOnSignalToContentMangerStatusChangeToFixed(
                event.getSignalToContentManagerId());
    }
}
