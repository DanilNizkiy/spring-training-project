package com.nizkiyd.backend.example.event;

import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class SignalToContentManagerStatusChangedEvent {

    private UUID signalToContentManagerId;

    private SignalToContentManagerStatus newStatus;
}
