package com.nizkiyd.backend.example.job;

import com.nizkiyd.backend.example.repository.ActorRepository;
import com.nizkiyd.backend.example.service.ActorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingsOfActorsJob {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ActorService actorService;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.average.ratings.of.actors.job.cron}")
    public void updateAverageRoleRatingOfActors() {
        log.info("Job started");

        actorRepository.getIdsOfActors().forEach(actorId -> {
            try {
                actorService.updateAverageRatingsOfActor(actorId);
            } catch (Exception e) {
                log.error("Failed to update average ratings for actor: {}", actorId, e);
            }
        });

        log.info("Job finished");
    }
}
