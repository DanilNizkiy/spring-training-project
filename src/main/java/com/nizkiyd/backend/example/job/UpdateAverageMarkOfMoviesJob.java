package com.nizkiyd.backend.example.job;

import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.service.MovieService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageMarkOfMoviesJob {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.average.mark.of.movies.job.cron}")
    public void updateAverageMarkOfMovies() {
        log.info("Job started");

        movieRepository.getIdsOfMovies().forEach(movieId -> {
            try {
                movieService.updateAverageMarkOfMovie(movieId);
            } catch (Exception e) {
                log.error("Failed to update average mark for movie: {}", movieId, e);
            }
        });

        log.info("Job finished");
    }


}
