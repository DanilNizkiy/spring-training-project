package com.nizkiyd.backend.example.exception.handler;

import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorInfo {

    private HttpStatus status;
    private Class exceptionClass;
    private String message;


}
