package com.nizkiyd.backend.example.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(String.format(message));
    }

    public EntityNotFoundException(Class entityClass, UUID id) {
        super(String.format("Entity %s with id=%s is not found!", entityClass.getSimpleName(), id));
    }

    public EntityNotFoundException(
            Class entityInternalClass, UUID internalId, Class entityClass, UUID entityId) {
        super(String.format("Entity %s with id=%s is not related to entity %s with id=%s!",
                entityInternalClass.getSimpleName(), internalId, entityClass.getSimpleName(), entityId));
    }

}
