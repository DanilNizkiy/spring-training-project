package com.nizkiyd.backend.example.exception;

import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class SignalToContentManagerWrongStatusException extends RuntimeException {

    public SignalToContentManagerWrongStatusException(UUID id, SignalToContentManagerStatus signalStatus) {
        super(String.format("Entity SignalToContentManager with id=%s"
                + " has status %s while must have NEED_TO_FIX", id, signalStatus));
    }
}
