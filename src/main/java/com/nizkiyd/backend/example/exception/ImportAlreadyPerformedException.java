package com.nizkiyd.backend.example.exception;

import com.nizkiyd.backend.example.domain.ExternalSystemImport;
import lombok.Getter;

@Getter
public class ImportAlreadyPerformedException extends Exception {

    private ExternalSystemImport externalSystemImport;

    public ImportAlreadyPerformedException(ExternalSystemImport esi) {
        super(String.format("Already performed import of %s and id in external system = %s",
                esi.getEntityType(), esi.getEntityId(), esi.getIdInExternalSystem()));
        this.externalSystemImport = esi;
    }
}
