package com.nizkiyd.backend.example.config;

import com.nizkiyd.backend.example.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/health").anonymous()
                .antMatchers(HttpMethod.POST, "/api/v1/moderators").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/moderators/invalid-account").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/moderators/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movie-marks").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movie-marks/{id}").anonymous()
                .antMatchers(HttpMethod.POST, "/api/v1/content-managers").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/content-managers/invalid-account").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/content-managers/{id}").anonymous()
                .antMatchers(HttpMethod.POST, "/api/v1/registered-users").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/registered-users/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/person/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/actors/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/actors").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/{movieId}/crew-members").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/news/{newsId}/crew-members").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/{movieId}/genres").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/{movieId}/movie-reviews").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/leader-board").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/{movieId}/roles/{roleMarkId}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/movies/{movieId}/roles").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/crew-members/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/crew-members").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/news/{newsId}/actors").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/news/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/news").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/news/{newsId}/movies").anonymous()
                .antMatchers("/v2/api-docs").anonymous()
                .antMatchers("/swagger-ui.html", "/webjars/springfox-swagger-ui/**", "/swagger-resources/**",
                        "/", "/csrf").anonymous()
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().csrf().disable();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
        http.authenticationProvider(authProvider());
    }
}
