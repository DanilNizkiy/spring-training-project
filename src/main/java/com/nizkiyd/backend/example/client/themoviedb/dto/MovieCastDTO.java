package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

@Data
public class MovieCastDTO {

    private String id;

    private String character;

    private Integer gender;
}
