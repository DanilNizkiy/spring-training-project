package com.nizkiyd.backend.example.client.themoviedb;

import com.nizkiyd.backend.example.client.themoviedb.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "api.themoviedb.org", url = "${themoviedb.api.url}", configuration = TheMovieDbClientConfig.class)
public interface TheMovieDbClient {

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/top_rated")
    MoviesPageDTO getTopRatedMovies();

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}")
    PersonReadDTO getPerson(@PathVariable("personId") String personId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}")
    PersonReadDTO getPerson(@PathVariable("personId") String personId);

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}/movie_credits")
    PersonCreditsReadDTO getPersonCredits(@PathVariable("personId") String personId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/credits")
    MovieCreditsReadDTO getMovieCredits(@PathVariable("movieId") String movieId);
}
