package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

@Data
public class MovieCrewDTO {

    private String id;

    private String job;
}
