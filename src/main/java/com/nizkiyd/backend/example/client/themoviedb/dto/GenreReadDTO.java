package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

@Data
public class GenreReadDTO {

    private String name;

    public GenreReadDTO(String name) {
        this.name = name;
    }

    public GenreReadDTO() {
    }
}
