package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonReadDTO {

    private String id;

    private String name;

    private Integer gender;

    private String biography;

    private Boolean adult;

    private LocalDate birthday;

    private String placeOfBirth;
}
