package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

@Data
public class PersonCrewDTO {

    private String id;

    private String job;
}
