package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PersonCreditsReadDTO {

    private List<PersonCastDTO> cast = new ArrayList<>();

    private List<PersonCrewDTO> crew = new ArrayList<>();
}
