package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

@Data
public class MovieReadShortDTO {

    private String id;

    private String title;
}
