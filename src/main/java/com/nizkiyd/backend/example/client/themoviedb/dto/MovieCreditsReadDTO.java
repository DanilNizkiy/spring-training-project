package com.nizkiyd.backend.example.client.themoviedb.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MovieCreditsReadDTO {

    private List<MovieCastDTO> cast = new ArrayList<>();

    private List<MovieCrewDTO> crew = new ArrayList<>();
}
