package com.nizkiyd.backend.example.client.themoviedb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class MovieReadDTO {

    private String id;

    private String originalTitle;

    private String title;

    private Boolean adult;

    private Integer budget;

    private List<GenreReadDTO> genres = new ArrayList<>();

    private String originalLanguage;

    private String overview;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate releaseDate;

    private Integer revenue;

    private Double voteAverage;

    private String status;
}
