package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.*;
import com.nizkiyd.backend.example.service.SignalToContentManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/signals-to-content-manager")
public class SignalToContentManagerController {

    @Autowired
    private SignalToContentManagerService signalToContentManagerService;

    @HasAnyAuthorityContentManager
    @GetMapping("/{id}")
    public SignalToContentManagerReadDTO getSignalToContentManager(@PathVariable UUID id) {
        return signalToContentManagerService.getSignalToContentManager(id);
    }

    @HasAnyAuthorityContentManager
    @ApiPageable
    @GetMapping
    public PageResult<SignalToContentManagerReadDTO> getSignalToContentManager(
            SignalToContentManagerFilter filter, @ApiIgnore Pageable pageable) {
        return signalToContentManagerService.getSignalToContentManager(filter, pageable);
    }

    @PatchMapping("/{id}")
    public SignalToContentManagerReadDTO patchSignalToContentManager(
            @PathVariable UUID id,
            @RequestBody SignalToContentManagerPatchDTO patch) {
        return signalToContentManagerService.patchSignalToContentManager(id, patch);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/{id}/fix-content")
    public SignalToContentManagerReadDTO fixContent(@PathVariable UUID id,
                                                    @RequestBody SignalToContentManagerFixContent signalDTO) {
        return signalToContentManagerService.fixContent(id, signalDTO);
    }


    @PutMapping("/{id}")
    public void putSignalToContentManager(@PathVariable UUID id, @RequestBody SignalToContentManagerPutDTO put) {
        signalToContentManagerService.updateSignalToContentManager(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteSignalToContentManager(@PathVariable UUID id) {
        signalToContentManagerService.deleteSignalToContentManager(id);
    }
}
