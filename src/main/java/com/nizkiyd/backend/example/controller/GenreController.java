package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.CurrentContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.genre.GenreTypeDTO;
import com.nizkiyd.backend.example.dto.genre.GenreReadDTO;
import com.nizkiyd.backend.example.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @HasAnyAuthorityContentManager
    @PostMapping("/movies/{movieId}/genres/{id}")
    public List<GenreReadDTO> addGenreToMovie(@PathVariable UUID movieId, @PathVariable UUID id) {
        return genreService.addGenreToMovie(movieId, id);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/genres/type")
    public GenreReadDTO getStaticGenreIdByType(@RequestBody GenreTypeDTO genreTypeDTO) {
        return genreService.getStaticGenreIdByType(genreTypeDTO);
    }

    @GetMapping("/movies/{movieId}/genres")
    public List<GenreReadDTO> getMovieGenres(@PathVariable UUID movieId) {
        return genreService.getMovieGenres(movieId);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("users/{userId}/movies/{movieId}/genres")
    public List<GenreReadDTO> getMovieGenres(@PathVariable UUID userId, @PathVariable UUID movieId) {
        return genreService.getMovieGenres(movieId);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/movies/{movieId}/genres/{id}")
    public List<GenreReadDTO> removeGenreFromMovie(@PathVariable UUID movieId, @PathVariable UUID id) {
        return genreService.removeGenreFromMovie(movieId, id);
    }
}
