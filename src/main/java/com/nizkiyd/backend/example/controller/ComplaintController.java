package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityModerator;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityModeratorOrAdmin;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.complaint.*;
import com.nizkiyd.backend.example.service.ComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/complaints")
public class ComplaintController {

    @Autowired
    private ComplaintService complaintService;

    @HasAnyAuthorityModeratorOrAdmin
    @GetMapping("/{id}")
    public ComplaintReadDTO getComplaint(@PathVariable UUID id) {
        return complaintService.getComplaint(id);
    }

    @ApiPageable
    @HasAnyAuthorityModeratorOrAdmin
    @GetMapping
    public PageResult<ComplaintReadDTO> getComplaint(ComplaintFilter filter, @ApiIgnore Pageable pageable) {
        return complaintService.getComplaint(filter, pageable);
    }

    @HasAnyAuthorityModeratorOrAdmin
    @PatchMapping("/{id}")
    public ComplaintReadDTO patchComplaint(@PathVariable UUID id, @RequestBody ComplaintPatchDTO patch) {
        return complaintService.patchComplaint(id, patch);
    }

    @HasAnyAuthorityModeratorOrAdmin
    @PutMapping("/{id}")
    public void putComplaint(@PathVariable UUID id, @RequestBody ComplaintPutDTO put) {
        complaintService.updateComplaint(id, put);
    }

    @HasAnyAuthorityModerator
    @PostMapping("/{id}/processing")
    public ComplaintReadDTO processingComplaint(@PathVariable UUID id,
                                                @RequestBody ComplaintProcessingDTO processingDTO) {
        return complaintService.processingComplaint(id, processingDTO);
    }

    @HasAnyAuthorityModeratorOrAdmin
    @DeleteMapping("/{id}")
    public void deleteComplaint(@PathVariable UUID id) {
        complaintService.deleteComplaint(id);
    }
}
