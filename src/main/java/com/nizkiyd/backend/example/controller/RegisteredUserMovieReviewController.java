package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.moviereview.MovieReviewCreateDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPutDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserMovieReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/movie-reviews")
public class RegisteredUserMovieReviewController {

    @Autowired
    private RegisteredUserMovieReviewService service;

    @GetMapping("/{movieReviewId}")
    public MovieReviewReadDTO getMovieReview(@PathVariable UUID registeredUserId, @PathVariable UUID movieReviewId) {
        return service.getMovieReview(registeredUserId, movieReviewId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public MovieReviewReadDTO createMovieReview(@PathVariable UUID registeredUserId,
                                                @RequestBody @Valid MovieReviewCreateDTO createDTO) {
        return service.createMovieReview(registeredUserId, createDTO);
    }

    @PatchMapping("/{movieReviewId}")
    public MovieReviewReadDTO patchMovieReview(@PathVariable UUID registeredUserId,
                                               @PathVariable UUID movieReviewId,
                                               @RequestBody MovieReviewPatchDTO patch) {
        return service.patchMovieReview(registeredUserId, movieReviewId, patch);
    }

    @PutMapping("/{movieReviewId}")
    public void putMovieReview(@PathVariable UUID registeredUserId,
                               @PathVariable UUID movieReviewId,
                               @RequestBody MovieReviewPutDTO put) {
        service.updateMovieReview(registeredUserId, movieReviewId, put);
    }

    @DeleteMapping("/{movieReviewId}")
    public void deleteMovieReview(@PathVariable UUID registeredUserId, @PathVariable UUID movieReviewId) {
        service.deleteMovieReview(registeredUserId, movieReviewId);
    }

    @GetMapping
    public List<MovieReviewReadDTO> getRegisteredUserMovieReviews(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserMovieReviews(registeredUserId);
    }
}
