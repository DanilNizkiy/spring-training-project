package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.CurrentContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.actor.ActorCreateDTO;
import com.nizkiyd.backend.example.dto.actor.ActorPatchDTO;
import com.nizkiyd.backend.example.dto.actor.ActorReadDTO;
import com.nizkiyd.backend.example.dto.actor.ActorFilter;
import com.nizkiyd.backend.example.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @GetMapping("/actors/{id}")
    public ActorReadDTO getActor(@PathVariable UUID id) {
        return actorService.getActor(id);
    }

    @ApiPageable
    @GetMapping("/actors")
    public PageResult<ActorReadDTO> getActor(ActorFilter filter, @ApiIgnore Pageable pageable) {
        return actorService.getActor(filter, pageable);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @ApiPageable
    @GetMapping("/users/{userId}/actors")
    public PageResult<ActorReadDTO> getActor(@PathVariable UUID userId, ActorFilter filter,
                                             @ApiIgnore Pageable pageable) {
        return actorService.getActor(filter, pageable);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/news/{newsId}/actors/{actorId}")
    public List<ActorReadDTO> addActorToNews(@PathVariable UUID newsId, @PathVariable UUID actorId) {
        return actorService.addActorToNews(newsId, actorId);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/news/{newsId}/actors/{actorId}")
    public List<ActorReadDTO> removeActorFromNews(@PathVariable UUID newsId, @PathVariable UUID actorId) {
        return actorService.removeActorFromNews(newsId, actorId);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/actors")
    public ActorReadDTO createActor(@RequestBody @Valid ActorCreateDTO createDTO) {
        return actorService.createActor(createDTO);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/actors/{id}")
    public ActorReadDTO patchActor(
            @PathVariable UUID id, @RequestBody ActorPatchDTO patch) {
        return actorService.patchActor(id, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/actors/{id}")
    public void deleteActor(@PathVariable UUID id) {
        actorService.deleteActor(id);
    }

    @GetMapping("/news/{newsId}/actors")
    public List<ActorReadDTO> getActorsNews(@PathVariable UUID newsId) {
        return actorService.getActorsNews(newsId);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("/users/{userId}/news/{newsId}/actors")
    public List<ActorReadDTO> getActorsNews(@PathVariable UUID userId, @PathVariable UUID newsId) {
        return actorService.getActorsNews(newsId);
    }
}
