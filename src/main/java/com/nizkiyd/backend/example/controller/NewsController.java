package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.news.*;
import com.nizkiyd.backend.example.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @GetMapping("/news/{id}")
    public NewsReadDTO getNews(@PathVariable UUID id) {
        return newsService.getNews(id);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @GetMapping("/registered-users/{registeredUserId}/news/{id}")
    public NewsReadDTO getNews(@PathVariable UUID registeredUserId, @PathVariable UUID id) {
        return newsService.getNews(id);
    }

    @ApiPageable
    @GetMapping("/news")
    public PageResult<NewsReadDTO> getNews(NewsFilter filter, @ApiIgnore Pageable pageable) {
        return newsService.getNews(filter, pageable);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/news")
    public NewsReadDTO createNews(@RequestBody @Valid NewsCreateDTO createDTO) {
        return newsService.createNews(createDTO);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/news/{id}")
    public NewsReadDTO patchNews(@PathVariable UUID id, @RequestBody NewsPatchDTO patch) {
        return newsService.patchNews(id, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/news/{id}")
    public void deleteNews(@PathVariable UUID id) {
        newsService.deleteNews(id);
    }

    @HasAnyAuthorityContentManager
    @PutMapping("/news/{id}")
    public void putNews(@PathVariable UUID id, @RequestBody NewsPutDTO put) {
        newsService.updateNews(id, put);
    }
}
