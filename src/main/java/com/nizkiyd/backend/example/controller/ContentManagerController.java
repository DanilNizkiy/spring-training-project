package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityAdmin;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerStatusDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerReadDTO;
import com.nizkiyd.backend.example.service.ContentManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/content-managers")
public class ContentManagerController {

    @Autowired
    private ContentManagerService contentManagerService;

    @GetMapping("/{id}")
    public ContentManagerReadDTO getContentManager(@PathVariable UUID id) {
        return contentManagerService.getContentManager(id);
    }

    @HasAnyAuthorityAdmin
    @GetMapping("/invalid-account")
    public List<ContentManagerReadDTO> getInvalidContentManagers() {
        return contentManagerService.getInvalidContentManagers();
    }

    @PostMapping
    public ContentManagerReadDTO createContentManager(@RequestBody @Valid ContentManagerCreateDTO createDTO) {
        return contentManagerService.createContentManager(createDTO);
    }

    @PreAuthorize("hasAuthority('ContentManager') and authentication.principal.id == #id")
    @PatchMapping("/{id}")
    public ContentManagerReadDTO patchContentManager(@PathVariable UUID id, @RequestBody ContentManagerPatchDTO patch) {
        return contentManagerService.patchContentManager(id, patch);
    }

    @HasAnyAuthorityAdmin
    @PatchMapping("/{id}/account-status")
    public ContentManagerReadDTO changeContentManagerStatus(@PathVariable UUID id,
                                                            @RequestBody ContentManagerStatusDTO statusDTO) {
        return contentManagerService.changeContentManagerStatus(id, statusDTO);
    }

    @PreAuthorize("hasAuthority('ContentManager') and authentication.principal.id == #id")
    @DeleteMapping("/{id}")
    public void deleteContentManager(@PathVariable UUID id) {
        contentManagerService.deleteContentManager(id);
    }
}
