package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.role.RoleFilter;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/roles/{id}")
    public RoleReadDTO getRole(@PathVariable UUID id) {
        return roleService.getRole(id);
    }

    @ApiPageable
    @GetMapping("/roles")
    public PageResult<RoleReadDTO> getRole(RoleFilter filter, @ApiIgnore Pageable pageable) {
        return roleService.getRole(filter, pageable);
    }

    @PatchMapping("/roles/{id}")
    public RoleReadDTO patchRole(@PathVariable UUID id, @RequestBody RolePatchDTO patch) {
        return roleService.patchRole(id, patch);
    }

    @DeleteMapping("/roles/{id}")
    public void deleteRole(@PathVariable UUID id) {
        roleService.deleteRole(id);
    }

    @PutMapping("/roles/{id}")
    public void putRole(@PathVariable UUID id, @RequestBody RolePutDTO put) {
        roleService.updateRole(id, put);
    }
}
