package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityAdmin;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityModerator;
import com.nizkiyd.backend.example.dto.registereduser.*;
import com.nizkiyd.backend.example.service.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/registered-users")
public class RegisteredUserController {

    @Autowired
    private RegisteredUserService registeredUserService;

    @GetMapping("/{id}")
    public RegisteredUserReadDTO getRegisteredUser(@PathVariable UUID id) {
        return registeredUserService.getRegisteredUser(id);
    }

    @PostMapping
    public RegisteredUserReadDTO createRegisteredUser(@RequestBody @Valid RegisteredUserCreateDTO createDTO) {
        return registeredUserService.createRegisteredUser(createDTO);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #id")
    @PatchMapping("/{id}")
    public RegisteredUserReadDTO patchRegisteredUser(
            @PathVariable UUID id, @RequestBody RegisteredUserPatchDTO patch) {
        return registeredUserService.patchRegisteredUser(id, patch);
    }

    @HasAnyAuthorityModerator
    @PatchMapping("/{id}/trust")
    public RegisteredUserReadDTO changeTrustRegisteredUser(
            @PathVariable UUID id, @RequestBody RegisteredUserTrustPatchDTO patch) {
        return registeredUserService.changeTrustRegisteredUser(id, patch);
    }

    @HasAnyAuthorityAdmin
    @PatchMapping("/{id}/account-status")
    public RegisteredUserReadDTO changeAccountStatusRegisteredUser(
            @PathVariable UUID id, @RequestBody RegisteredUserAccountStatusPatchDTO statusDTO) {
        return registeredUserService.changeAccountStatusRegisteredUser(id, statusDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteRegisteredUser(@PathVariable UUID id) {
        registeredUserService.deleteRegisteredUser(id);
    }

    @PutMapping("/{id}")
    public void putRegisteredUser(@PathVariable UUID id, @RequestBody RegisteredUserPutDTO put) {
        registeredUserService.updateRegisteredUser(id, put);
    }
}
