package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.complaint.ComplaintCreateDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPatchDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPutDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/complaints")
public class RegisteredUserComplaintController {

    @Autowired
    private RegisteredUserComplaintService service;

    @GetMapping("/{complaintId}")
    public ComplaintReadDTO getComplaint(@PathVariable UUID registeredUserId, @PathVariable UUID complaintId) {
        return service.getComplaint(registeredUserId, complaintId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public ComplaintReadDTO createComplaint(@PathVariable UUID registeredUserId,
                                            @RequestBody @Valid ComplaintCreateDTO createDTO) {
        return service.createComplaint(registeredUserId, createDTO);
    }

    @PatchMapping("/{complaintId}")
    public ComplaintReadDTO patchComplaint(@PathVariable UUID registeredUserId,
                                           @PathVariable UUID complaintId,
                                           @RequestBody ComplaintPatchDTO patch) {
        return service.patchComplaint(registeredUserId, complaintId, patch);
    }

    @PutMapping("/{complaintId}")
    public void putComplaint(@PathVariable UUID registeredUserId,
                             @PathVariable UUID complaintId,
                             @RequestBody ComplaintPutDTO put) {
        service.updateComplaint(registeredUserId, complaintId, put);
    }

    @DeleteMapping("/{complaintId}")
    public void deleteComplaint(@PathVariable UUID registeredUserId, @PathVariable UUID complaintId) {
        service.deleteComplaint(registeredUserId, complaintId);
    }

    @GetMapping
    public List<ComplaintReadDTO> getRegisteredUserComplaints(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserComplaints(registeredUserId);
    }
}
