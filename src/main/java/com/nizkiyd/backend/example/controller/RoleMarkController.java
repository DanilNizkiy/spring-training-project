package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkFilter;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.service.RoleMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/role-marks")
public class RoleMarkController {

    @Autowired
    private RoleMarkService roleMarkService;

    @GetMapping("/{id}")
    public RoleMarkReadDTO getRoleMark(@PathVariable UUID id) {
        return roleMarkService.getRoleMark(id);
    }

    @ApiPageable
    @GetMapping
    public PageResult<RoleMarkReadDTO> getRoleMark(RoleMarkFilter filter, @ApiIgnore Pageable pageable) {
        return roleMarkService.getRoleMark(filter, pageable);
    }

    @PatchMapping("/{id}")
    public RoleMarkReadDTO patchRoleMark(@PathVariable UUID id, @RequestBody RoleMarkPatchDTO patch) {
        return roleMarkService.patchRoleMark(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteRoleMark(@PathVariable UUID id) {
        roleMarkService.deleteRoleMark(id);
    }
}
