package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityAdmin;
import com.nizkiyd.backend.example.dto.admin.AdminCreateDTO;
import com.nizkiyd.backend.example.dto.admin.AdminPatchDTO;
import com.nizkiyd.backend.example.dto.admin.AdminReadDTO;
import com.nizkiyd.backend.example.dto.admin.AdminStatusDTO;
import com.nizkiyd.backend.example.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/admins")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @HasAnyAuthorityAdmin
    @GetMapping("/{id}")
    public AdminReadDTO getAdmin(@PathVariable UUID id) {
        return adminService.getAdmin(id);
    }

    @HasAnyAuthorityAdmin
    @PostMapping
    public AdminReadDTO createAdmin(@RequestBody @Valid AdminCreateDTO createDTO) {
        return adminService.createAdmin(createDTO);
    }

    @PreAuthorize("hasAuthority('Admin') and authentication.principal.id == #id")
    @PatchMapping("/{id}")
    public AdminReadDTO patchAdmin(@PathVariable UUID id, @RequestBody AdminPatchDTO patch) {
        return adminService.patchAdmin(id, patch);
    }

    @HasAnyAuthorityAdmin
    @PatchMapping("/{id}/account-status")
    public AdminReadDTO changeAdminStatus(@PathVariable UUID id,
                                          @RequestBody AdminStatusDTO statusDTO) {
        return adminService.changeAdminStatus(id, statusDTO);
    }

    @HasAnyAuthorityAdmin
    @DeleteMapping("/{id}")
    public void deleteAdmin(@PathVariable UUID id) {
        adminService.deleteAdmin(id);
    }
}