package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.ContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeCreateDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePutDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserNewsLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/news-likes")
public class RegisteredUserNewsLikeController {

    @Autowired
    private RegisteredUserNewsLikeService service;

    @ContentManagerOrCurrentRegisteredUser
    @GetMapping("/{newsLikeId}")
    public NewsLikeReadDTO getNewsLike(@PathVariable UUID registeredUserId,
                                       @PathVariable UUID newsLikeId) {
        return service.getNewsLike(registeredUserId, newsLikeId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public NewsLikeReadDTO createNewsLike(@PathVariable UUID registeredUserId,
                                          @RequestBody @Valid NewsLikeCreateDTO createDTO) {
        return service.createNewsLike(registeredUserId, createDTO);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PatchMapping("/{newsLikeId}")
    public NewsLikeReadDTO patchNewsLike(@PathVariable UUID registeredUserId,
                                         @PathVariable UUID newsLikeId,
                                         @RequestBody NewsLikePatchDTO patch) {
        return service.patchNewsLike(registeredUserId, newsLikeId, patch);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @DeleteMapping("/{newsLikeId}")
    public void deleteNewsLike(@PathVariable UUID registeredUserId, @PathVariable UUID newsLikeId) {
        service.deleteNewsLike(registeredUserId, newsLikeId);
    }

    @ContentManagerOrCurrentRegisteredUser
    @GetMapping
    public List<NewsLikeReadDTO> getRegisteredUserNewsLikes(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserNewsLikes(registeredUserId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PutMapping("/{newsLikeId}")
    public void putNewsLike(@PathVariable UUID registeredUserId,
                            @PathVariable UUID newsLikeId,
                            @RequestBody NewsLikePutDTO put) {
        service.updateNewsLike(registeredUserId, newsLikeId, put);
    }
}

