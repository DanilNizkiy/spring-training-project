package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeFilter;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.service.RoleReviewLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/role-review-likes")
public class RoleReviewLikeController {

    @Autowired
    private RoleReviewLikeService roleReviewLikeService;

    @GetMapping("/{id}")
    public RoleReviewLikeReadDTO getRoleReviewLike(@PathVariable UUID id) {
        return roleReviewLikeService.getRoleReviewLike(id);
    }

    @GetMapping
    public PageResult<RoleReviewLikeReadDTO> getRoleReviewLike(
            RoleReviewLikeFilter filter, Pageable pageable) {
        return roleReviewLikeService.getRoleReviewLike(filter, pageable);
    }

    @PatchMapping("/{id}")
    public RoleReviewLikeReadDTO patchRoleReviewLike(
            @PathVariable UUID id, @RequestBody RoleReviewLikePatchDTO patch) {
        return roleReviewLikeService.patchRoleReviewLike(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteRoleReviewLike(@PathVariable UUID id) {
        roleReviewLikeService.deleteRoleReviewLike(id);
    }
}
