package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.CurrentContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.role.RoleCreateDTO;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.service.MovieRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class MovieRoleController {

    @Autowired
    private MovieRoleService service;

    @GetMapping("/movies/{movieId}/roles/{roleId}")
    public RoleReadDTO getRole(@PathVariable UUID movieId, @PathVariable UUID roleId) {
        return service.getRole(movieId, roleId);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/movies/{movieId}/roles")
    public RoleReadDTO createRole(@PathVariable UUID movieId,
                                  @RequestBody @Valid RoleCreateDTO createDTO) {
        return service.createRole(movieId, createDTO);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/movies/{movieId}/roles/{roleId}")
    public RoleReadDTO patchRole(@PathVariable UUID movieId,
                                 @PathVariable UUID roleId,
                                 @RequestBody RolePatchDTO patch) {
        return service.patchRole(movieId, roleId, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/movies/{movieId}/roles/{roleId}")
    public void deleteRole(@PathVariable UUID movieId, @PathVariable UUID roleId) {
        service.deleteRole(movieId, roleId);
    }

    @GetMapping("/movies/{movieId}/roles")
    public List<RoleReadDTO> getMovieRoles(@PathVariable UUID movieId) {
        return service.getMovieRoles(movieId);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("/users/{userId}/movies/{movieId}/roles")
    public List<RoleReadDTO> getMovieRoles(@PathVariable UUID userId, @PathVariable UUID movieId) {
        return service.getMovieRoles(movieId);
    }

    @HasAnyAuthorityContentManager
    @PutMapping("/movies/{movieId}/roles/{roleId}")
    public void putRole(@PathVariable UUID movieId,
                        @PathVariable UUID roleId,
                        @RequestBody RolePutDTO put) {
        service.updateRole(movieId, roleId, put);
    }
}
