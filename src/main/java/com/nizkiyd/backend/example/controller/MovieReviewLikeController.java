package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeFilter;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.service.MovieReviewLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movie-review-likes")
public class MovieReviewLikeController {

    @Autowired
    private MovieReviewLikeService movieReviewLikeService;

    @GetMapping("/{id}")
    public MovieReviewLikeReadDTO getMovieReviewLike(@PathVariable UUID id) {
        return movieReviewLikeService.getMovieReviewLike(id);
    }

    @ApiPageable
    @GetMapping
    public PageResult<MovieReviewLikeReadDTO> getMovieReviewLike(
            MovieReviewLikeFilter filter, @ApiIgnore Pageable pageable) {
        return movieReviewLikeService.getMovieReviewLike(filter, pageable);
    }

    @PatchMapping("/{id}")
    public MovieReviewLikeReadDTO patchMovieReviewLike(@PathVariable UUID id,
                                                       @RequestBody MovieReviewLikePatchDTO patch) {
        return movieReviewLikeService.patchMovieReviewLike(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteMovieReviewLike(@PathVariable UUID id) {
        movieReviewLikeService.deleteMovieReviewLike(id);
    }
}
