package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.person.PersonCreateDTO;
import com.nizkiyd.backend.example.dto.person.PersonPatchDTO;
import com.nizkiyd.backend.example.dto.person.PersonPutDTO;
import com.nizkiyd.backend.example.dto.person.PersonReadDTO;
import com.nizkiyd.backend.example.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/{id}")
    public PersonReadDTO getPerson(@PathVariable UUID id) {
        return personService.getPerson(id);
    }

    @HasAnyAuthorityContentManager
    @PostMapping
    public PersonReadDTO createPerson(@RequestBody @Valid PersonCreateDTO createDTO) {
        return personService.createPerson(createDTO);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/{id}")
    public PersonReadDTO patchPerson(
            @PathVariable UUID id, @RequestBody PersonPatchDTO patch) {
        return personService.patchPerson(id, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable UUID id) {
        personService.deletePerson(id);
    }

    @HasAnyAuthorityContentManager
    @PutMapping("/{id}")
    public void putPerson(@PathVariable UUID id, @RequestBody PersonPutDTO put) {
        personService.updatePerson(id, put);
    }
}