package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.CurrentContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkFilter;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.service.MovieMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class MovieMarkController {

    @Autowired
    private MovieMarkService movieMarkService;

    @GetMapping("/movie-marks/{id}")
    public MovieMarkReadDTO getMovieMark(@PathVariable UUID id) {
        return movieMarkService.getMovieMark(id);
    }

    @ApiPageable
    @GetMapping("/movie-marks")
    public PageResult<MovieMarkReadDTO> getMovieMark(MovieMarkFilter filter, @ApiIgnore Pageable pageable) {
        return movieMarkService.getMovieMark(filter, pageable);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @ApiPageable
    @GetMapping("/users/{userId}/movie-marks")
    public PageResult<MovieMarkReadDTO> getMovieMark(@PathVariable UUID userId, MovieMarkFilter filter,
                                                     @ApiIgnore Pageable pageable) {
        return movieMarkService.getMovieMark(filter, pageable);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/movie-marks/{id}")
    public MovieMarkReadDTO patchMovieMark(@PathVariable UUID id, @RequestBody MovieMarkPatchDTO patch) {
        return movieMarkService.patchMovieMark(id, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/movie-marks/{id}")
    public void deleteMovieMark(@PathVariable UUID id) {
        movieMarkService.deleteMovieMark(id);
    }
}