package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityAdmin;
import com.nizkiyd.backend.example.dto.moderator.ModeratorStatusDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorCreateDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorPatchDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorReadDTO;
import com.nizkiyd.backend.example.service.ModeratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/moderators")
public class ModeratorController {

    @Autowired
    private ModeratorService moderatorService;

    @GetMapping("/{id}")
    public ModeratorReadDTO getModerator(@PathVariable UUID id) {
        return moderatorService.getModerator(id);
    }

    @HasAnyAuthorityAdmin
    @GetMapping("/invalid-account")
    public List<ModeratorReadDTO> getInvalidModerators() {
        return moderatorService.getInvalidModerators();
    }

    @PostMapping
    public ModeratorReadDTO createModerator(@RequestBody @Valid ModeratorCreateDTO createDTO) {
        return moderatorService.createModerator(createDTO);
    }

    @PreAuthorize("hasAuthority('Moderator') and authentication.principal.id == #id")
    @PatchMapping("/{id}")
    public ModeratorReadDTO patchModerator(@PathVariable UUID id, @RequestBody ModeratorPatchDTO patch) {
        return moderatorService.patchModerator(id, patch);
    }

    @HasAnyAuthorityAdmin
    @PatchMapping("/{id}/account-status")
    public ModeratorReadDTO changeModeratorStatus(@PathVariable UUID id,
                                                  @RequestBody ModeratorStatusDTO statusDTO) {
        return moderatorService.changeModeratorStatus(id, statusDTO);
    }

    @PreAuthorize("hasAuthority('Moderator') and authentication.principal.id == #id")
    @DeleteMapping("/{id}")
    public void deleteModerator(@PathVariable UUID id) {
        moderatorService.deleteModerator(id);
    }
}
