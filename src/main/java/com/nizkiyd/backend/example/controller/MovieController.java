package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.CurrentContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.movie.*;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.exception.ImportedEntityAlreadyExistException;
import com.nizkiyd.backend.example.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/movies/{id}")
    public MovieReadDTO getMovie(@PathVariable UUID id) {
        return movieService.getMovie(id);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("/users/{userId}/movies/{id}")
    public MovieReadDTO getMovie(@PathVariable UUID userId, @PathVariable UUID id) {
        return movieService.getMovie(id);
    }

    @ApiPageable
    @GetMapping("/movies")
    public PageResult<MovieReadDTO> getMovie(MovieFilter filter, @ApiIgnore Pageable pageable) {
        return movieService.getMovie(filter, pageable);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @ApiPageable
    @GetMapping("/users/{userId}/movies")
    public PageResult<MovieReadDTO> getMovie(@PathVariable UUID userId,
                                             MovieFilter filter,
                                             @ApiIgnore Pageable pageable) {
        return movieService.getMovie(filter, pageable);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/news/{newsId}/movies/{movieId}")
    public List<MovieReadDTO> addMovieToNews(@PathVariable UUID newsId, @PathVariable UUID movieId) {
        return movieService.addMovieToNews(newsId, movieId);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/news/{newsId}/movies/{movieId}")
    public List<MovieReadDTO> removeMovieFromNews(@PathVariable UUID newsId, @PathVariable UUID movieId) {
        return movieService.removeMovieFromNews(newsId, movieId);
    }

    @GetMapping("/news/{newsId}/movies")
    public List<MovieReadDTO> getMoviesNews(@PathVariable UUID newsId) {
        return movieService.getMoviesNews(newsId);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("/users/{userId}/news/{newsId}/movies")
    public List<MovieReadDTO> getMoviesNews(@PathVariable UUID userId, @PathVariable UUID newsId) {
        return movieService.getMoviesNews(newsId);
    }

    @HasAnyAuthorityContentManager
    @GetMapping("/movies/{movieExternalId}/import")
    public MovieReadDTO importMovie(@PathVariable String movieExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        return movieService.importMovie(movieExternalId);
    }

    @GetMapping("/movies/leader-board")
    public List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard() {
        return movieService.getMoviesLeaderBoard();
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/movies")
    public MovieReadDTO createMovie(@RequestBody @Valid MovieCreateDTO createDTO) {
        return movieService.createMovie(createDTO);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/movies/{id}")
    public MovieReadDTO patchMovie(@PathVariable UUID id, @RequestBody MoviePatchDTO patch) {
        return movieService.patchMovie(id, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/movies/{id}")
    public void deleteMovie(@PathVariable UUID id) {
        movieService.deleteMovie(id);
    }

    @HasAnyAuthorityContentManager
    @PutMapping("/movies/{id}")
    public void putMovie(@PathVariable UUID id, @RequestBody MoviePutDTO put) {
        movieService.updateMovie(id, put);
    }
}
