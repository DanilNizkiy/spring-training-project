package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserMovieReviewLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/movie-review-likes")
public class RegisteredUserMovieReviewLikeController {

    @Autowired
    private RegisteredUserMovieReviewLikeService service;

    @GetMapping("/{movieReviewLikeId}")
    public MovieReviewLikeReadDTO getMovieReviewLike(@PathVariable UUID registeredUserId,
                                                     @PathVariable UUID movieReviewLikeId) {
        return service.getMovieReviewLike(registeredUserId, movieReviewLikeId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public MovieReviewLikeReadDTO createMovieReviewLike(@PathVariable UUID registeredUserId,
                                                        @RequestBody @Valid MovieReviewLikeCreateDTO createDTO) {
        return service.createMovieReviewLike(registeredUserId, createDTO);
    }

    @PatchMapping("/{movieReviewLikeId}")
    public MovieReviewLikeReadDTO patchMovieReviewLike(@PathVariable UUID registeredUserId,
                                                       @PathVariable UUID movieReviewLikeId,
                                                       @RequestBody MovieReviewLikePatchDTO patch) {
        return service.patchMovieReviewLike(registeredUserId, movieReviewLikeId, patch);
    }

    @DeleteMapping("/{movieReviewLikeId}")
    public void deleteMovieReviewLike(@PathVariable UUID registeredUserId, @PathVariable UUID movieReviewLikeId) {
        service.deleteMovieReviewLike(registeredUserId, movieReviewLikeId);
    }

    @GetMapping
    public List<MovieReviewLikeReadDTO> getRegisteredUserMovieReviewLikes(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserMovieReviewLikes(registeredUserId);
    }

    @PutMapping("/{movieReviewLikeId}")
    public void putMovieReviewLike(@PathVariable UUID registeredUserId,
                                   @PathVariable UUID movieReviewLikeId,
                                   @RequestBody MovieReviewLikePutDTO put) {
        service.updateMovieReviewLike(registeredUserId, movieReviewLikeId, put);
    }
}
