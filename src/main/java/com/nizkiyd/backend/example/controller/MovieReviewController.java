package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.CurrentModeratorOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityModeratorOrContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviereview.*;
import com.nizkiyd.backend.example.service.MovieReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class MovieReviewController {

    @Autowired
    private MovieReviewService movieReviewService;

    @GetMapping("/movie-reviews/{id}")
    public MovieReviewReadDTO getMovieReview(@PathVariable UUID id) {
        return movieReviewService.getMovieReview(id);
    }

    @CurrentModeratorOrCurrentRegisteredUser
    @ApiPageable
    @GetMapping("/users/{userId}/movie-reviews")
    public PageResult<MovieReviewReadDTO> getMovieReview(@PathVariable UUID userId,
                                                         MovieReviewFilter filter,
                                                         @ApiIgnore Pageable pageable) {
        return movieReviewService.getMovieReview(filter, pageable);
    }

    @HasAnyAuthorityModeratorOrContentManager
    @ApiPageable
    @GetMapping("/movie-reviews")
    public PageResult<MovieReviewReadDTO> getMovieReview(MovieReviewFilter filter, @ApiIgnore Pageable pageable) {
        return movieReviewService.getMovieReview(filter, pageable);
    }

    @GetMapping("/movies/{movieId}/movie-reviews")
    public List<MovieReviewReadDTO> getReviewsMovie(@PathVariable UUID movieId) {
        return movieReviewService.getReviewsMovie(movieId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #userId")
    @GetMapping("/users/{userId}/movies/{movieId}/movie-reviews")
    public List<MovieReviewReadDTO> getReviewsMovie(@PathVariable UUID userId, @PathVariable UUID movieId) {
        return movieReviewService.getReviewsMovie(movieId);
    }

    @HasAnyAuthorityModeratorOrContentManager
    @PatchMapping("/movie-reviews/{id}")
    public MovieReviewReadDTO patchMovieReview(@PathVariable UUID id, @RequestBody MovieReviewPatchDTO patch) {
        return movieReviewService.patchMovieReview(id, patch);
    }

    @HasAnyAuthorityModeratorOrContentManager
    @PutMapping("/movie-reviews/{id}")
    public void putMovieReview(@PathVariable UUID id, @RequestBody MovieReviewPutDTO put) {
        movieReviewService.updateMovieReview(id, put);
    }

    @HasAnyAuthorityModeratorOrContentManager
    @DeleteMapping("/movie-reviews/{id}")
    public void deleteMovieReview(@PathVariable UUID id) {
        movieReviewService.deleteMovieReview(id);
    }
}
