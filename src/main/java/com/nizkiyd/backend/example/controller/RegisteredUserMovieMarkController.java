package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.moviemark.MovieMarkCreateDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserMovieMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/movie-marks")
public class RegisteredUserMovieMarkController {

    @Autowired
    private RegisteredUserMovieMarkService service;

    @GetMapping("/{roleMarkId}")
    public MovieMarkReadDTO getMovieMark(@PathVariable UUID registeredUserId, @PathVariable UUID roleMarkId) {
        return service.getMovieMark(registeredUserId, roleMarkId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public MovieMarkReadDTO createMovieMark(@PathVariable UUID registeredUserId,
                                            @RequestBody @Valid MovieMarkCreateDTO createDTO) {
        return service.createMovieMark(registeredUserId, createDTO);
    }

    @PatchMapping("/{roleMarkId}")
    public MovieMarkReadDTO patchMovieMark(@PathVariable UUID registeredUserId,
                                           @PathVariable UUID roleMarkId,
                                           @RequestBody MovieMarkPatchDTO patch) {
        return service.patchMovieMark(registeredUserId, roleMarkId, patch);
    }

    @DeleteMapping("/{roleMarkId}")
    public void deleteMovieMark(@PathVariable UUID registeredUserId, @PathVariable UUID roleMarkId) {
        service.deleteMovieMark(registeredUserId, roleMarkId);
    }

    @GetMapping
    public List<MovieMarkReadDTO> getRegisteredUserMovieMarks(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserMovieMarks(registeredUserId);
    }
}
