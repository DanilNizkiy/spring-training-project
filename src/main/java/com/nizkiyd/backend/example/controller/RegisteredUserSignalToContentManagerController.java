package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.security.ContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.validation.ControllerValidationUtil;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserSignalToContentManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/signals-to-content-manager")
public class RegisteredUserSignalToContentManagerController {

    @Autowired
    private RegisteredUserSignalToContentManagerService service;

    @ContentManagerOrCurrentRegisteredUser
    @GetMapping("/{signalToContentManagerId}")
    public SignalToContentManagerReadDTO getSignalToContentManager(@PathVariable UUID registeredUserId,
                                                                   @PathVariable UUID signalToContentManagerId) {
        return service.getSignalToContentManager(registeredUserId, signalToContentManagerId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public SignalToContentManagerReadDTO createSignalToContentManager(
            @PathVariable UUID registeredUserId,
            @RequestBody @Valid SignalToContentManagerCreateDTO createDTO) {
        ControllerValidationUtil.validateLessThen(createDTO.getFromIndex(), createDTO.getToIndex(),
                "fromIndex", "toIndex");
        return service.createSignalToContentManager(registeredUserId, createDTO);
    }

    @ContentManagerOrCurrentRegisteredUser
    @PatchMapping("/{signalToContentManagerId}")
    public SignalToContentManagerReadDTO patchSignalToContentManager(
            @PathVariable UUID registeredUserId,
            @PathVariable UUID signalToContentManagerId,
            @RequestBody SignalToContentManagerPatchDTO patch) {
        return service.patchSignalToContentManager(registeredUserId, signalToContentManagerId, patch);
    }

    @ContentManagerOrCurrentRegisteredUser
    @PutMapping("/{signalToContentManagerId}")
    public void putSignalToContentManager(@PathVariable UUID registeredUserId,
                                          @PathVariable UUID signalToContentManagerId,
                                          @RequestBody SignalToContentManagerPutDTO put) {
        service.updateSignalToContentManager(registeredUserId, signalToContentManagerId, put);
    }

    @ContentManagerOrCurrentRegisteredUser
    @DeleteMapping("/{signalToContentManagerId}")
    public void deleteSignalToContentManager(@PathVariable UUID registeredUserId,
                                             @PathVariable UUID signalToContentManagerId) {
        service.deleteSignalToContentManager(registeredUserId, signalToContentManagerId);
    }

    @ContentManagerOrCurrentRegisteredUser
    @GetMapping
    public List<SignalToContentManagerReadDTO> getRegisteredUserSignalToContentManagers(
            @PathVariable UUID registeredUserId) {
        return service.getRegisteredUserSignalToContentManagers(registeredUserId);
    }
}