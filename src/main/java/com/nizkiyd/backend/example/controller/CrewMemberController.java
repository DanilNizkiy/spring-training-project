package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.controller.security.CurrentContentManagerOrCurrentRegisteredUser;
import com.nizkiyd.backend.example.controller.security.HasAnyAuthorityContentManager;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberCreateDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberFilter;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberPatchDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberReadDTO;
import com.nizkiyd.backend.example.service.CrewMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class CrewMemberController {

    @Autowired
    private CrewMemberService crewMemberService;

    @HasAnyAuthorityContentManager
    @PostMapping("/movies/{movieId}/crew-members/{id}")
    public List<CrewMemberReadDTO> addCrewMemberToMovie(@PathVariable UUID movieId, @PathVariable UUID id) {
        return crewMemberService.addCrewMemberToMovie(movieId, id);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/crew-members")
    public CrewMemberReadDTO createCrewMember(@RequestBody @Valid CrewMemberCreateDTO createDTO) {
        return crewMemberService.createCrewMember(createDTO);
    }

    @HasAnyAuthorityContentManager
    @PatchMapping("/crew-members/{id}")
    public CrewMemberReadDTO patchCrewMember(@PathVariable UUID id, @RequestBody CrewMemberPatchDTO patch) {
        return crewMemberService.patchCrewMember(id, patch);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/movies/{movieId}/crew-members/{id}")
    public List<CrewMemberReadDTO> removeCrewMemberFromMovie(@PathVariable UUID movieId, @PathVariable UUID id) {
        return crewMemberService.removeCrewMemberFromMovie(movieId, id);
    }

    @GetMapping("/movies/{movieId}/crew-members")
    public List<CrewMemberReadDTO> getCrewMemberMovie(@PathVariable UUID movieId) {
        return crewMemberService.getCrewMemberMovies(movieId);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("/users/{userId}/movies/{movieId}/crew-members")
    public List<CrewMemberReadDTO> getCrewMemberMovie(@PathVariable UUID userId, @PathVariable UUID movieId) {
        return crewMemberService.getCrewMemberMovies(movieId);
    }

    @GetMapping("/crew-members/{id}")
    public CrewMemberReadDTO getCrewMember(@PathVariable UUID id) {
        return crewMemberService.getCrewMember(id);
    }

    @ApiPageable
    @GetMapping("/crew-members")
    public PageResult<CrewMemberReadDTO> getCrewMember(CrewMemberFilter filter, @ApiIgnore Pageable pageable) {
        return crewMemberService.getCrewMember(filter, pageable);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @ApiPageable
    @GetMapping("/users/{userId}/crew-members")
    public PageResult<CrewMemberReadDTO> getCrewMember(@PathVariable UUID userId,
                                                       CrewMemberFilter filter,
                                                       @ApiIgnore Pageable pageable) {
        return crewMemberService.getCrewMember(filter, pageable);
    }

    @HasAnyAuthorityContentManager
    @PostMapping("/news/{newsId}/crew-members/{crewMemberId}")
    public List<CrewMemberReadDTO> addCrewMemberToNews(@PathVariable UUID newsId, @PathVariable UUID crewMemberId) {
        return crewMemberService.addCrewMemberToNews(newsId, crewMemberId);
    }

    @HasAnyAuthorityContentManager
    @DeleteMapping("/news/{newsId}/crew-members/{crewMemberId}")
    public List<CrewMemberReadDTO> removeCrewMemberFromNews(
            @PathVariable UUID newsId, @PathVariable UUID crewMemberId) {
        return crewMemberService.removeCrewMemberFromNews(newsId, crewMemberId);
    }

    @GetMapping("/news/{newsId}/crew-members")
    public List<CrewMemberReadDTO> getCrewMembersNews(@PathVariable UUID newsId) {
        return crewMemberService.getCrewMembersNews(newsId);
    }

    @CurrentContentManagerOrCurrentRegisteredUser
    @GetMapping("/users/{userId}/news/{newsId}/crew-members")
    public List<CrewMemberReadDTO> getCrewMembersNews(@PathVariable UUID userId, @PathVariable UUID newsId) {
        return crewMemberService.getCrewMembersNews(newsId);
    }
}