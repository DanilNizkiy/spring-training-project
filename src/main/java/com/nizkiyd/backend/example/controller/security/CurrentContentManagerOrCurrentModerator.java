package com.nizkiyd.backend.example.controller.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAuthority('Moderator') and authentication.principal.id == #userId or "
        + "(hasAuthority('ContentManager') and authentication.principal.id == #userId)")
public @interface CurrentContentManagerOrCurrentModerator {
}
