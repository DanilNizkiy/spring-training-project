package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolereview.*;
import com.nizkiyd.backend.example.service.RoleReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/role-reviews")
public class RoleReviewController {

    @Autowired
    private RoleReviewService roleReviewService;

    @GetMapping("/{id}")
    public RoleReviewReadDTO getRoleReview(@PathVariable UUID id) {
        return roleReviewService.getRoleReview(id);
    }

    @ApiPageable
    @GetMapping
    public PageResult<RoleReviewReadDTO> getRoleReview(RoleReviewFilter filter, @ApiIgnore Pageable pageable) {
        return roleReviewService.getRoleReview(filter, pageable);
    }

    @PatchMapping("/{id}")
    public RoleReviewReadDTO patchRoleReview(@PathVariable UUID id, @RequestBody RoleReviewPatchDTO patch) {
        return roleReviewService.patchRoleReview(id, patch);
    }

    @PutMapping("/{id}")
    public void putRoleReview(@PathVariable UUID id, @RequestBody RoleReviewPutDTO put) {
        roleReviewService.updateRoleReview(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteRoleReview(@PathVariable UUID id) {
        roleReviewService.deleteRoleReview(id);
    }
}
