package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserRoleReviewLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/role-review-likes")
public class RegisteredUserRoleReviewLikeController {

    @Autowired
    private RegisteredUserRoleReviewLikeService service;

    @GetMapping("/{roleReviewLikeId}")
    public RoleReviewLikeReadDTO getRoleReviewLike(@PathVariable UUID registeredUserId,
                                                   @PathVariable UUID roleReviewLikeId) {
        return service.getRoleReviewLike(registeredUserId, roleReviewLikeId);
    }

    @PostMapping
    public RoleReviewLikeReadDTO createRoleReviewLike(@PathVariable UUID registeredUserId,
                                                      @RequestBody @Valid RoleReviewLikeCreateDTO createDTO) {
        return service.createRoleReviewLike(registeredUserId, createDTO);
    }

    @PatchMapping("/{roleReviewLikeId}")
    public RoleReviewLikeReadDTO patchRoleReviewLike(@PathVariable UUID registeredUserId,
                                                     @PathVariable UUID roleReviewLikeId,
                                                     @RequestBody RoleReviewLikePatchDTO patch) {
        return service.patchRoleReviewLike(registeredUserId, roleReviewLikeId, patch);
    }

    @DeleteMapping("/{roleReviewLikeId}")
    public void deleteRoleReviewLike(@PathVariable UUID registeredUserId, @PathVariable UUID roleReviewLikeId) {
        service.deleteRoleReviewLike(registeredUserId, roleReviewLikeId);
    }

    @GetMapping
    public List<RoleReviewLikeReadDTO> getRegisteredUserRoleReviewLikes(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserRoleReviewLikes(registeredUserId);
    }

    @PutMapping("/{roleReviewLikeId}")
    public void putRoleReviewLike(@PathVariable UUID registeredUserId,
                                  @PathVariable UUID roleReviewLikeId,
                                  @RequestBody RoleReviewLikePutDTO put) {
        service.updateRoleReviewLike(registeredUserId, roleReviewLikeId, put);
    }
}