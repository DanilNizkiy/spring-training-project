package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.rolemark.RoleMarkCreateDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserRoleMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/role-marks")
public class RegisteredUserRoleMarkController {

    @Autowired
    private RegisteredUserRoleMarkService service;

    @GetMapping("/{roleMarkId}")
    public RoleMarkReadDTO getRoleMark(@PathVariable UUID registeredUserId, @PathVariable UUID roleMarkId) {
        return service.getRoleMark(registeredUserId, roleMarkId);
    }

    @PreAuthorize("hasAuthority('RegisteredUser') and authentication.principal.id == #registeredUserId")
    @PostMapping
    public RoleMarkReadDTO createRoleMark(@PathVariable UUID registeredUserId,
                                          @RequestBody @Valid RoleMarkCreateDTO createDTO) {
        return service.createRoleMark(registeredUserId, createDTO);
    }

    @PatchMapping("/{roleMarkId}")
    public RoleMarkReadDTO patchRoleMark(@PathVariable UUID registeredUserId,
                                         @PathVariable UUID roleMarkId, @RequestBody RoleMarkPatchDTO patch) {
        return service.patchRoleMark(registeredUserId, roleMarkId, patch);
    }

    @DeleteMapping("/{roleMarkId}")
    public void deleteRoleMark(@PathVariable UUID registeredUserId, @PathVariable UUID roleMarkId) {
        service.deleteRoleMark(registeredUserId, roleMarkId);
    }

    @GetMapping
    public List<RoleMarkReadDTO> getRegisteredUserRoleMarks(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserRoleMarks(registeredUserId);
    }
}
