package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.controller.documentation.ApiPageable;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeFilter;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.service.NewsLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/news-likes")
public class NewsLikeController {

    @Autowired
    private NewsLikeService newLikeService;

    @GetMapping("/{id}")
    public NewsLikeReadDTO getNewsLike(@PathVariable UUID id) {
        return newLikeService.getNewsLike(id);
    }

    @ApiPageable
    @GetMapping
    public PageResult<NewsLikeReadDTO> getNewsLike(
            NewsLikeFilter filter, @ApiIgnore Pageable pageable) {
        return newLikeService.getNewsLike(filter, pageable);
    }

    @PatchMapping("/{id}")
    public NewsLikeReadDTO patchNewsLike(@PathVariable UUID id,
                                         @RequestBody NewsLikePatchDTO patch) {
        return newLikeService.patchNewsLike(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteNewsLike(@PathVariable UUID id) {
        newLikeService.deleteNewsLike(id);
    }
}
