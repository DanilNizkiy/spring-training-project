package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.rolereview.RoleReviewCreateDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPatchDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPutDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.service.RegisteredUserRoleReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-users/{registeredUserId}/role-reviews")
public class RegisteredUserRoleReviewController {

    @Autowired
    private RegisteredUserRoleReviewService service;

    @GetMapping("/{roleReviewId}")
    public RoleReviewReadDTO getRoleReview(@PathVariable UUID registeredUserId, @PathVariable UUID roleReviewId) {
        return service.getRoleReview(registeredUserId, roleReviewId);
    }

    @PostMapping
    public RoleReviewReadDTO createRoleReview(@PathVariable UUID registeredUserId,
                                              @RequestBody @Valid RoleReviewCreateDTO createDTO) {
        return service.createRoleReview(registeredUserId, createDTO);
    }

    @PatchMapping("/{roleReviewId}")
    public RoleReviewReadDTO patchRoleReview(@PathVariable UUID registeredUserId,
                                             @PathVariable UUID roleReviewId, @RequestBody RoleReviewPatchDTO patch) {
        return service.patchRoleReview(registeredUserId, roleReviewId, patch);
    }

    @PutMapping("/{roleReviewId}")
    public void putRoleReview(@PathVariable UUID registeredUserId,
                              @PathVariable UUID roleReviewId,
                              @RequestBody RoleReviewPutDTO put) {
        service.updateRoleReview(registeredUserId, roleReviewId, put);
    }

    @DeleteMapping("/{roleReviewId}")
    public void deleteRoleReview(@PathVariable UUID registeredUserId, @PathVariable UUID roleReviewId) {
        service.deleteRoleReview(registeredUserId, roleReviewId);
    }

    @GetMapping
    public List<RoleReviewReadDTO> getRegisteredUserRoleReviews(@PathVariable UUID registeredUserId) {
        return service.getRegisteredUserRoleReviews(registeredUserId);
    }
}
