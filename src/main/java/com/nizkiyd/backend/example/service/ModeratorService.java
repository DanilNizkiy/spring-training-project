package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.Moderator;
import com.nizkiyd.backend.example.dto.moderator.ModeratorStatusDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorCreateDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorPatchDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorReadDTO;
import com.nizkiyd.backend.example.repository.ModeratorRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ModeratorService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ModeratorReadDTO getModerator(UUID id) {
        Moderator moderator = repositoryHelper.getEntityRequired(Moderator.class, id);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public List<ModeratorReadDTO> getInvalidModerators() {
        List<Moderator> invalidModerators = moderatorRepository.findByStatus(AccountStatus.INVALID);
        return invalidModerators.stream().map(s -> translationService.translate(
                s, ModeratorReadDTO.class)).collect(Collectors.toList());
    }

    @Transactional
    public ModeratorReadDTO createModerator(ModeratorCreateDTO create) {
        Moderator moderator = translationService.translate(create, Moderator.class);
        moderator.setEncodedPassword(passwordEncoder.encode(create.getPassword()));
        moderator.setStatus(AccountStatus.INVALID);

        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO patchModerator(UUID id, ModeratorPatchDTO patch) {
        Moderator moderator = repositoryHelper.getEntityRequired(Moderator.class, id);

        translationService.map(patch, moderator);

        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO changeModeratorStatus(UUID id, ModeratorStatusDTO statusDTO) {
        Moderator moderator = repositoryHelper.getEntityRequired(Moderator.class, id);
        moderator.setStatus(statusDTO.getStatus());

        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public void deleteModerator(UUID id) {
        moderatorRepository.delete(repositoryHelper.getEntityRequired(Moderator.class, id));
    }
}
