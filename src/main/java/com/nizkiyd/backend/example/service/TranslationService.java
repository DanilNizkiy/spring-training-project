package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.actor.ActorCreateDTO;
import com.nizkiyd.backend.example.dto.actor.ActorPatchDTO;
import com.nizkiyd.backend.example.dto.actor.ActorReadDTO;
import com.nizkiyd.backend.example.dto.admin.AdminCreateDTO;
import com.nizkiyd.backend.example.dto.admin.AdminPatchDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPatchDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPutDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintReadDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberCreateDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberPatchDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberReadDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorCreateDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorPatchDTO;
import com.nizkiyd.backend.example.dto.person.PersonPatchDTO;
import com.nizkiyd.backend.example.dto.registereduser.RegisteredUserCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.dto.movie.MoviePatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkCreateDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.dto.news.NewsPatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeCreateDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.dto.registereduser.RegisteredUserPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewCreateDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkCreateDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewCreateDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPatchDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.dto.role.RoleCreateDTO;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    private ObjectTranslator objectTranslator;

    public TranslationService() {
        objectTranslator = new ObjectTranslator(createConfiguration());
    }

    public <T> List<T> translateList(List<?> objects, Class<T> targetClass) {
        return objects.stream().map(o -> translate(o, targetClass)).collect(Collectors.toList());
    }

    public <E, T> PageResult<T> toPageResult(Page<E> page, Class<T> dtoType) {
        PageResult<T> res = new PageResult<>();
        res.setPage(page.getNumber());
        res.setPageSize(page.getSize());
        res.setTotalPages(page.getTotalPages());
        res.setTotalElements(page.getTotalElements());
        res.setData(page.getContent().stream().map(e -> translate(e, dtoType)).collect(Collectors.toList()));
        return res;
    }

    private Configuration createConfiguration() {
        Configuration c = new Configuration();
        configureForSignalToContentManager(c);
        configureForAbstractEntity(c);
        configureForNews(c);
        configureForNewsLike(c);
        configureForMovieReviewLike(c);
        configureForRoleReviewLike(c);
        configureForMovieMark(c);
        configureForRoleMark(c);
        configureForComplaint(c);
        configureForMovie(c);
        configureForMovieReview(c);
        configureForRoleReview(c);
        configureForRegisteredUser(c);
        configureForRole(c);
        configureForCrewMember(c);
        configureForPerson(c);
        configureForActor(c);
        configureForAdmin(c);
        configureForContentManager(c);
        configureForModerator(c);
        return c;
    }

    private void configureForAdmin(Configuration c) {
        Configuration.Translation fromCreateToEntity = c.beanOfClass(AdminCreateDTO.class)
                .translationTo(Admin.class);
        fromCreateToEntity.srcProperty("password").translatesTo("encodedPassword");

        c.beanOfClass(AdminPatchDTO.class).translationTo(Admin.class).mapOnlyNotNullProperties();
    }

    private void configureForContentManager(Configuration c) {
        Configuration.Translation fromCreateToEntity = c.beanOfClass(ContentManagerCreateDTO.class)
                .translationTo(ContentManager.class);
        fromCreateToEntity.srcProperty("password").translatesTo("encodedPassword");

        c.beanOfClass(ContentManagerPatchDTO.class).translationTo(ContentManager.class).mapOnlyNotNullProperties();
    }

    private void configureForModerator(Configuration c) {
        Configuration.Translation fromCreateToEntity = c.beanOfClass(ModeratorCreateDTO.class)
                .translationTo(Moderator.class);
        fromCreateToEntity.srcProperty("password").translatesTo("encodedPassword");

        c.beanOfClass(ModeratorPatchDTO.class).translationTo(Moderator.class).mapOnlyNotNullProperties();
    }

    private void configureForCrewMember(Configuration c) {
        Configuration.Translation t = c.beanOfClass(CrewMember.class)
                .translationTo(CrewMemberReadDTO.class);
        t.srcProperty("person.id").translatesTo("personId");

        Configuration.Translation fromPatchToEntity = c.beanOfClass(CrewMemberPatchDTO.class)
                .translationTo(CrewMember.class);
        fromPatchToEntity.srcProperty("personId").translatesTo("person.id");
        c.beanOfClass(CrewMemberPatchDTO.class).translationTo(CrewMember.class)
                .mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(CrewMemberCreateDTO.class)
                .translationTo(CrewMember.class);
        fromCreateToEntity.srcProperty("personId").translatesTo("person.id");
    }

    private void configureForMovieReviewLike(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MovieReviewLike.class)
                .translationTo(MovieReviewLikeReadDTO.class);
        t.srcProperty("movieReview.id").translatesTo("movieReviewId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(MovieReviewLikePatchDTO.class).translationTo(MovieReviewLike.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(MovieReviewLikeCreateDTO.class)
                .translationTo(MovieReviewLike.class);
        fromCreateToEntity.srcProperty("movieReviewId").translatesTo("movieReview.id");
    }

    private void configureForSignalToContentManager(Configuration c) {
        Configuration.Translation t = c.beanOfClass(SignalToContentManager.class)
                .translationTo(SignalToContentManagerReadDTO.class);
        t.srcProperty("contentManager.id").translatesTo("contentManagerId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        Configuration.Translation fromPatchToEntity = c.beanOfClass(SignalToContentManagerPatchDTO.class)
                .translationTo(SignalToContentManager.class);
        fromPatchToEntity.srcProperty("contentManagerId").translatesTo("contentManager.id");
        c.beanOfClass(SignalToContentManagerPatchDTO.class).translationTo(SignalToContentManager.class)
                .mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(SignalToContentManagerCreateDTO.class)
                .translationTo(SignalToContentManager.class);
        fromCreateToEntity.srcProperty("contentManagerId").translatesTo("contentManager.id");

        Configuration.Translation fromPutToEntity = c.beanOfClass(SignalToContentManagerPutDTO.class)
                .translationTo(SignalToContentManager.class);
        fromPutToEntity.srcProperty("contentManagerId").translatesTo("contentManager.id");
    }

    private void configureForComplaint(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Complaint.class)
                .translationTo(ComplaintReadDTO.class);
        t.srcProperty("moderator.id").translatesTo("moderatorId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        Configuration.Translation fromPatchToEntity = c.beanOfClass(ComplaintPatchDTO.class)
                .translationTo(Complaint.class);
        fromPatchToEntity.srcProperty("moderatorId").translatesTo("moderator.id");
        c.beanOfClass(ComplaintPatchDTO.class).translationTo(Complaint.class).mapOnlyNotNullProperties();

        Configuration.Translation fromPutToEntity = c.beanOfClass(ComplaintPutDTO.class)
                .translationTo(Complaint.class);
        fromPutToEntity.srcProperty("moderatorId").translatesTo("moderator.id");
    }


    private void configureForRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Role.class)
                .translationTo(RoleReadDTO.class);
        t.srcProperty("actor.id").translatesTo("actorId");
        t.srcProperty("movie.id").translatesTo("movieId");

        Configuration.Translation fromPatchToEntity = c.beanOfClass(RolePatchDTO.class)
                .translationTo(Role.class);
        fromPatchToEntity.srcProperty("actorId").translatesTo("actor.id");
        c.beanOfClass(RolePatchDTO.class).translationTo(Role.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(RoleCreateDTO.class)
                .translationTo(Role.class);
        fromCreateToEntity.srcProperty("actorId").translatesTo("actor.id");

        Configuration.Translation fromPutToEntity = c.beanOfClass(RolePutDTO.class)
                .translationTo(Role.class);
        fromPutToEntity.srcProperty("actorId").translatesTo("actor.id");
    }

    private void configureForActor(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Actor.class)
                .translationTo(ActorReadDTO.class);
        t.srcProperty("person.id").translatesTo("personId");

        Configuration.Translation fromCreateToEntity = c.beanOfClass(ActorCreateDTO.class)
                .translationTo(Actor.class);
        fromCreateToEntity.srcProperty("personId").translatesTo("person.id");

        Configuration.Translation fromPatchToEntity = c.beanOfClass(ActorPatchDTO.class)
                .translationTo(Actor.class);
        fromPatchToEntity.srcProperty("personId").translatesTo("person.id");
        c.beanOfClass(ActorPatchDTO.class).translationTo(Actor.class).mapOnlyNotNullProperties();
    }

    private void configureForMovie(Configuration c) {
        c.beanOfClass(MoviePatchDTO.class).translationTo(Movie.class).mapOnlyNotNullProperties();
    }


    private void configureForRegisteredUser(Configuration c) {
        c.beanOfClass(RegisteredUserPatchDTO.class).translationTo(RegisteredUser.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(RegisteredUserCreateDTO.class)
                .translationTo(RegisteredUser.class);
        fromCreateToEntity.srcProperty("password").translatesTo("encodedPassword");
    }

    private void configureForPerson(Configuration c) {
        c.beanOfClass(PersonPatchDTO.class).translationTo(Person.class).mapOnlyNotNullProperties();
    }

    private void configureForMovieReview(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MovieReview.class)
                .translationTo(MovieReviewReadDTO.class);
        t.srcProperty("moderator.id").translatesTo("moderatorId");
        t.srcProperty("movie.id").translatesTo("movieId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(MovieReviewPatchDTO.class).translationTo(MovieReview.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(MovieReviewCreateDTO.class)
                .translationTo(MovieReview.class);
        fromCreateToEntity.srcProperty("moderatorId").translatesTo("moderator.id");
        fromCreateToEntity.srcProperty("movieId").translatesTo("movie.id");
    }

    private void configureForRoleReview(Configuration c) {
        Configuration.Translation t = c.beanOfClass(RoleReview.class)
                .translationTo(RoleReviewReadDTO.class);
        t.srcProperty("moderator.id").translatesTo("moderatorId");
        t.srcProperty("role.id").translatesTo("roleId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(RoleReviewPatchDTO.class).translationTo(RoleReview.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(RoleReviewCreateDTO.class)
                .translationTo(RoleReview.class);
        fromCreateToEntity.srcProperty("moderatorId").translatesTo("moderator.id");
        fromCreateToEntity.srcProperty("roleId").translatesTo("role.id");
    }

    private void configureForMovieMark(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MovieMark.class)
                .translationTo(MovieMarkReadDTO.class);
        t.srcProperty("movie.id").translatesTo("movieId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(MovieMarkPatchDTO.class).translationTo(MovieMark.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(MovieMarkCreateDTO.class)
                .translationTo(MovieMark.class);
        fromCreateToEntity.srcProperty("movieId").translatesTo("movie.id");
    }

    private void configureForRoleMark(Configuration c) {
        Configuration.Translation t = c.beanOfClass(RoleMark.class)
                .translationTo(RoleMarkReadDTO.class);
        t.srcProperty("role.id").translatesTo("roleId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(RoleMarkPatchDTO.class).translationTo(RoleMark.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(RoleMarkCreateDTO.class)
                .translationTo(RoleMark.class);
        fromCreateToEntity.srcProperty("roleId").translatesTo("role.id");
    }

    private void configureForNewsLike(Configuration c) {
        Configuration.Translation t = c.beanOfClass(NewsLike.class)
                .translationTo(NewsLikeReadDTO.class);
        t.srcProperty("news.id").translatesTo("newsId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(NewsLikePatchDTO.class).translationTo(NewsLike.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(NewsLikeCreateDTO.class)
                .translationTo(NewsLike.class);
        fromCreateToEntity.srcProperty("newsId").translatesTo("news.id");
    }

    private void configureForRoleReviewLike(Configuration c) {
        Configuration.Translation t = c.beanOfClass(RoleReviewLike.class)
                .translationTo(RoleReviewLikeReadDTO.class);
        t.srcProperty("roleReview.id").translatesTo("roleReviewId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");

        c.beanOfClass(RoleReviewLikePatchDTO.class).translationTo(RoleReviewLike.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(RoleReviewLikeCreateDTO.class)
                .translationTo(RoleReviewLike.class);
        fromCreateToEntity.srcProperty("roleReviewId").translatesTo("roleReview.id");
    }

    private void configureForNews(Configuration c) {
        c.beanOfClass(NewsPatchDTO.class).translationTo(News.class).mapOnlyNotNullProperties();
    }

    private void configureForAbstractEntity(Configuration c) {
        c.beanOfClass(AbstractEntity.class).setIdentifierProperty("id");
        c.beanOfClass(AbstractEntity.class).setBeanFinder(
                (beanClass, id) -> repositoryHelper.getReferenceIfExists(beanClass, (UUID) id));
    }

    public <T> void map(Object srcObject, Object destObject) {
        try {
            objectTranslator.mapBean(srcObject, destObject);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> T translate(Object srcObject, Class<T> targetClass) {
        try {
            return objectTranslator.translate(srcObject, targetClass);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }
}