package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.MovieMark;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkFilter;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.repository.MovieMarkRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MovieMarkService {

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MovieMarkReadDTO getMovieMark(UUID id) {
        MovieMark movieMark = repositoryHelper.getEntityRequired(MovieMark.class, id);
        return translationService.translate(movieMark, MovieMarkReadDTO.class);
    }

    public PageResult<MovieMarkReadDTO> getMovieMark(MovieMarkFilter filter, Pageable pageable) {
        Page<MovieMark> movieMarks = movieMarkRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(movieMarks, MovieMarkReadDTO.class);
    }

    public MovieMarkReadDTO patchMovieMark(UUID id, MovieMarkPatchDTO patch) {
        MovieMark movieMark = repositoryHelper.getEntityRequired(MovieMark.class, id);

        translationService.map(patch, movieMark);

        movieMark = movieMarkRepository.save(movieMark);
        return translationService.translate(movieMark, MovieMarkReadDTO.class);
    }

    public void deleteMovieMark(UUID id) {
        movieMarkRepository.delete(repositoryHelper.getEntityRequired(MovieMark.class, id));
    }
}