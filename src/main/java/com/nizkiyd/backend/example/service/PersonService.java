package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Person;
import com.nizkiyd.backend.example.dto.person.PersonCreateDTO;
import com.nizkiyd.backend.example.dto.person.PersonPatchDTO;
import com.nizkiyd.backend.example.dto.person.PersonPutDTO;
import com.nizkiyd.backend.example.dto.person.PersonReadDTO;
import com.nizkiyd.backend.example.repository.PersonRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public PersonReadDTO getPerson(UUID id) {
        Person person = repositoryHelper.getEntityRequired(Person.class, id);
        return translationService.translate(person, PersonReadDTO.class);
    }

    public PersonReadDTO createPerson(PersonCreateDTO create) {
        Person person = translationService.translate(create, Person.class);

        person = personRepository.save(person);
        return translationService.translate(person, PersonReadDTO.class);
    }

    public PersonReadDTO patchPerson(UUID id, PersonPatchDTO patch) {
        Person person = repositoryHelper.getEntityRequired(Person.class, id);

        translationService.map(patch, person);

        person = personRepository.save(person);
        return translationService.translate(person, PersonReadDTO.class);
    }

    public void deletePerson(UUID id) {
        personRepository.delete(repositoryHelper.getEntityRequired(Person.class, id));
    }

    public void updatePerson(UUID id, PersonPutDTO put) {
        Person person = repositoryHelper.getEntityRequired(Person.class, id);

        translationService.map(put, person);

        personRepository.save(person);
    }
}