package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.repository.SignalToContentManagerRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserSignalToContentManagerService {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public SignalToContentManagerReadDTO getSignalToContentManager(
            UUID registeredUserId, UUID signalToContentManagerId) {
        SignalToContentManager signalToContentManager = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, SignalToContentManager.class, signalToContentManagerId);
        return translationService.translate(signalToContentManager, SignalToContentManagerReadDTO.class);
    }

    @Transactional
    public SignalToContentManagerReadDTO createSignalToContentManager(
            UUID registeredUserId, SignalToContentManagerCreateDTO create) {
        SignalToContentManager signalToContentManager = translationService.translate(
                create, SignalToContentManager.class);
        signalToContentManager.setRegisteredUser(repositoryHelper.getReferenceIfExists(
                RegisteredUser.class, registeredUserId));
        signalToContentManager.setSignalToContentManagerStatus(SignalToContentManagerStatus.NEED_TO_FIX);
        signalToContentManager = signalToContentManagerRepository.save(signalToContentManager);

        return translationService.translate(signalToContentManager, SignalToContentManagerReadDTO.class);
    }

    public SignalToContentManagerReadDTO patchSignalToContentManager(UUID registeredUserId,
                                                                     UUID signalToContentManagerId,
                                                                     SignalToContentManagerPatchDTO patch) {
        SignalToContentManager signalToContentManager = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, SignalToContentManager.class, signalToContentManagerId);

        translationService.map(patch, signalToContentManager);

        signalToContentManager = signalToContentManagerRepository.save(signalToContentManager);
        return translationService.translate(signalToContentManager, SignalToContentManagerReadDTO.class);
    }

    public void updateSignalToContentManager(
            UUID registeredUserId, UUID signalToContentManagerId, SignalToContentManagerPutDTO put) {
        SignalToContentManager signalToContentManager = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, SignalToContentManager.class, signalToContentManagerId);

        translationService.map(put, signalToContentManager);

        signalToContentManagerRepository.save(signalToContentManager);
    }

    public void deleteSignalToContentManager(UUID registeredUserId, UUID signalToContentManagerId) {
        signalToContentManagerRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, SignalToContentManager.class, signalToContentManagerId));
    }

    public List<SignalToContentManagerReadDTO> getRegisteredUserSignalToContentManagers(UUID registeredUserId) {
        List<SignalToContentManager> signalToContentManagers = signalToContentManagerRepository
                .findByRegisteredUserId(registeredUserId);
        return signalToContentManagers.stream().map(s -> translationService.translate(
                s, SignalToContentManagerReadDTO.class)).collect(Collectors.toList());
    }
}