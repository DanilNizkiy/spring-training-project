package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.news.*;
import com.nizkiyd.backend.example.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public NewsReadDTO getNews(UUID id) {
        News news = repositoryHelper.getEntityRequired(News.class, id);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public PageResult<NewsReadDTO> getNews(NewsFilter filter, Pageable pageable) {
        Page<News> news = newsRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(news, NewsReadDTO.class);
    }

    @Transactional
    public NewsReadDTO createNews(NewsCreateDTO create) {
        News news = translationService.translate(create, News.class);

        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public NewsReadDTO patchNews(UUID id, NewsPatchDTO patch) {
        News news = repositoryHelper.getEntityRequired(News.class, id);

        translationService.map(patch, news);

        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public void deleteNews(UUID id) {
        newsRepository.delete(repositoryHelper.getEntityRequired(News.class, id));
    }

    public void updateNews(UUID id, NewsPutDTO put) {
        News news = repositoryHelper.getEntityRequired(News.class, id);

        translationService.map(put, news);

        newsRepository.save(news);
    }
}
