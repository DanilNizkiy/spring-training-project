package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.complaint.*;
import com.nizkiyd.backend.example.repository.ComplaintRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Service
public class ComplaintService {

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ComplaintReadDTO getComplaint(UUID id) {
        Complaint complaint = repositoryHelper.getEntityRequired(Complaint.class, id);
        return translationService.translate(complaint, ComplaintReadDTO.class);
    }

    public PageResult<ComplaintReadDTO> getComplaint(ComplaintFilter filter, Pageable pageable) {
        Page<Complaint> complaints = complaintRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(complaints, ComplaintReadDTO.class);
    }

    public ComplaintReadDTO patchComplaint(UUID id, ComplaintPatchDTO patch) {
        Complaint complaint = repositoryHelper.getEntityRequired(Complaint.class, id);

        translationService.map(patch, complaint);

        complaint = complaintRepository.save(complaint);
        return translationService.translate(complaint, ComplaintReadDTO.class);
    }

    public void deleteComplaint(UUID id) {
        complaintRepository.delete(repositoryHelper.getEntityRequired(Complaint.class, id));
    }

    public void updateComplaint(UUID id, ComplaintPutDTO put) {
        Complaint complaint = repositoryHelper.getEntityRequired(Complaint.class, id);

        translationService.map(put, complaint);

        complaintRepository.save(complaint);
    }

    @Transactional
    public ComplaintReadDTO processingComplaint(UUID id, ComplaintProcessingDTO processingDTO) {
        Complaint complaint = repositoryHelper.getEntityRequired(Complaint.class, id);

        if (processingDTO.getNewStatus() == ComplaintStatus.APPROVED) {
            switch (complaint.getComplaintObjectType()) {
              case MOVIE_REVIEW:
                  MovieReview movieReview = repositoryHelper.getEntityRequired(
                          MovieReview.class, complaint.getComplaintObjectId());
                  sanction(movieReview::getRegisteredUser, movieReview::setReviewStatus, movieReview);
                  break;

              case ROLE_REVIEW:
                  RoleReview roleReview = repositoryHelper.getEntityRequired(
                          RoleReview.class, complaint.getComplaintObjectId());
                  sanction(roleReview::getRegisteredUser, roleReview::setReviewStatus, roleReview);
                  break;
              default:
            }
        }
        complaint.setComplaintStatus(processingDTO.getNewStatus());
        complaint.setProcessedAt(Instant.now());
        complaint.setModerator(repositoryHelper.getReferenceIfExists(Moderator.class, processingDTO.getModeratorId()));
        complaint = complaintRepository.save(complaint);

        List<Complaint> sameComplaints = complaintRepository.findByComplaintObjectIdAndType(
                complaint.getComplaintObjectId(), complaint.getType());

        Complaint finalComplaint = complaint;
        sameComplaints.forEach(c -> {
            c.setComplaintStatus(finalComplaint.getComplaintStatus());
            c.setProcessedAt(finalComplaint.getProcessedAt());
            c.setModerator(repositoryHelper.getReferenceIfExists(
                    Moderator.class, finalComplaint.getModerator().getId()));
            complaintRepository.save(c);
        });

        return translationService.translate(complaint, ComplaintReadDTO.class);
    }

    private void sanction(Supplier<RegisteredUser> getter, Consumer<ReviewStatus> setter, Object obj) {
        setter.accept(ReviewStatus.CANCELED);
        repositoryHelper.save(obj);

        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(
                RegisteredUser.class, getter.get().getId());
        registeredUser.setTrust(false);
        registeredUser.setStatus(AccountStatus.BLOCKED);
        repositoryHelper.save(registeredUser);
    }
}
