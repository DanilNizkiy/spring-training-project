package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Genre;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.dto.genre.GenreReadDTO;
import com.nizkiyd.backend.example.dto.genre.GenreTypeDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.GenreRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GenreService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private GenreRepository genreRepository;

    @Transactional
    public List<GenreReadDTO> addGenreToMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieId);
        Genre genre = repositoryHelper.getEntityRequired(Genre.class, id);

        if (movie.getGenres().stream().anyMatch(cm -> cm.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Movie %s already has genre %s", movieId, id));
        }

        movie.getGenres().add(genre);
        movie = movieRepository.save(movie);

        return movie.getGenres().stream()
                .map(g -> translationService.translate(g, GenreReadDTO.class))
                .collect(Collectors.toList());
    }

    public GenreReadDTO getStaticGenreIdByType(GenreTypeDTO genreTypeDTO) {
        Genre genre = genreRepository.findByType(genreTypeDTO.getType());

        return translationService.translate(genre, GenreReadDTO.class);
    }

    @Transactional
    public List<GenreReadDTO> removeGenreFromMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieId);

        boolean removed = movie.getGenres().removeIf(cm -> cm.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Movie " + movieId + " has no genrt " + id);
        }

        movie = movieRepository.save(movie);

        return movie.getGenres().stream()
                .map(g -> translationService.translate(g, GenreReadDTO.class))
                .collect(Collectors.toList());
    }

    public List<GenreReadDTO> getMovieGenres(UUID movieId) {
        List<Genre> movieGenres = genreRepository.findByMovieId(movieId);
        return movieGenres.stream()
                .map(g -> translationService.translate(g, GenreReadDTO.class))
                .collect(Collectors.toList());
    }
}
