package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.domain.ReviewStatus;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewCreateDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPutDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserMovieReviewService {

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MovieReviewReadDTO getMovieReview(UUID registeredUserId, UUID movieReviewId) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReview.class, movieReviewId);
        return translationService.translate(movieReview, MovieReviewReadDTO.class);
    }

    @Transactional
    public MovieReviewReadDTO createMovieReview(UUID registeredUserId, MovieReviewCreateDTO create) {
        MovieReview movieReview = translationService.translate(create, MovieReview.class);
        movieReview.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, registeredUserId));


        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        if (registeredUser.getTrust()) {
            movieReview.setReviewStatus(ReviewStatus.APPROVED);
        } else {
            movieReview.setReviewStatus(ReviewStatus.MODERATION);
        }
        movieReview = movieReviewRepository.save(movieReview);

        return translationService.translate(movieReview, MovieReviewReadDTO.class);
    }

    public MovieReviewReadDTO patchMovieReview(UUID registeredUserId, UUID movieReviewId, MovieReviewPatchDTO patch) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReview.class, movieReviewId);

        translationService.map(patch, movieReview);

        movieReview = movieReviewRepository.save(movieReview);
        return translationService.translate(movieReview, MovieReviewReadDTO.class);
    }

    public void updateMovieReview(UUID registeredUserId, UUID movieReviewId, MovieReviewPutDTO put) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReview.class, movieReviewId);

        translationService.map(put, movieReview);

        movieReviewRepository.save(movieReview);
    }

    public void deleteMovieReview(UUID registeredUserId, UUID movieReviewId) {
        movieReviewRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReview.class, movieReviewId));
    }

    public List<MovieReviewReadDTO> getRegisteredUserMovieReviews(UUID registeredUserId) {
        List<MovieReview> movieReviews = movieReviewRepository.findByRegisteredUserId(registeredUserId);
        return movieReviews.stream().map(mr -> translationService.translate(mr, MovieReviewReadDTO.class))
                .collect(Collectors.toList());
    }
}
