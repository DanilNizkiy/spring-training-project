package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.ContentManager;

import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerStatusDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerReadDTO;
import com.nizkiyd.backend.example.repository.ContentManagerRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ContentManagerService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ContentManagerReadDTO getContentManager(UUID id) {
        ContentManager contentManager = repositoryHelper.getEntityRequired(ContentManager.class, id);
        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public List<ContentManagerReadDTO> getInvalidContentManagers() {
        List<ContentManager> invalidContentManagers = contentManagerRepository.findByStatus(AccountStatus.INVALID);
        return invalidContentManagers.stream().map(s -> translationService.translate(
                s, ContentManagerReadDTO.class)).collect(Collectors.toList());
    }

    @Transactional
    public ContentManagerReadDTO createContentManager(ContentManagerCreateDTO create) {
        ContentManager contentManager = translationService.translate(create, ContentManager.class);
        contentManager.setEncodedPassword(passwordEncoder.encode(create.getPassword()));
        contentManager.setStatus(AccountStatus.INVALID);

        contentManager = contentManagerRepository.save(contentManager);
        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public ContentManagerReadDTO patchContentManager(UUID id, ContentManagerPatchDTO patch) {
        ContentManager contentManager = repositoryHelper.getEntityRequired(ContentManager.class, id);

        translationService.map(patch, contentManager);

        contentManager = contentManagerRepository.save(contentManager);
        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public ContentManagerReadDTO changeContentManagerStatus(UUID id, ContentManagerStatusDTO statusDTO) {
        ContentManager contentManager = repositoryHelper.getEntityRequired(ContentManager.class, id);
        contentManager.setStatus(statusDTO.getStatus());

        contentManager = contentManagerRepository.save(contentManager);
        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public void deleteContentManager(UUID id) {
        contentManagerRepository.delete(repositoryHelper.getEntityRequired(ContentManager.class, id));
    }
}