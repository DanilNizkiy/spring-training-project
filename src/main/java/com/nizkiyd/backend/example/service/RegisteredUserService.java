package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.registereduser.*;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RegisteredUserService {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public RegisteredUserReadDTO getRegisteredUser(UUID id) {
        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO createRegisteredUser(RegisteredUserCreateDTO create) {
        RegisteredUser registeredUser = translationService.translate(create, RegisteredUser.class);
        registeredUser.setEncodedPassword(passwordEncoder.encode(create.getPassword()));
        registeredUser.setStatus(AccountStatus.VALID);
        registeredUser.setTrust(false);

        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO patchRegisteredUser(UUID id, RegisteredUserPatchDTO patch) {
        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);

        translationService.map(patch, registeredUser);

        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO changeTrustRegisteredUser(UUID id, RegisteredUserTrustPatchDTO patch) {
        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);

        translationService.map(patch, registeredUser);

        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO changeAccountStatusRegisteredUser(
            UUID id, RegisteredUserAccountStatusPatchDTO statusDTO) {
        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);
        registeredUser.setStatus(statusDTO.getStatus());

        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public void deleteRegisteredUser(UUID id) {
        registeredUserRepository.delete(repositoryHelper.getEntityRequired(RegisteredUser.class, id));
    }

    public void updateRegisteredUser(UUID id, RegisteredUserPutDTO put) {
        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);

        translationService.map(put, registeredUser);

        registeredUserRepository.save(registeredUser);
    }
}
