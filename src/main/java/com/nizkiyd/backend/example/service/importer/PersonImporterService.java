package com.nizkiyd.backend.example.service.importer;

import com.nizkiyd.backend.example.client.themoviedb.TheMovieDbClient;
import com.nizkiyd.backend.example.client.themoviedb.dto.PersonReadDTO;
import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class PersonImporterService {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private PersonRepository personRepository;

    @Transactional
    public UUID importPerson(String personExternalId) throws ImportAlreadyPerformedException {
        log.info("Importing movie with external id={}", personExternalId);

        externalSystemImportService.validateNotImported(Person.class, personExternalId);
        PersonReadDTO read = movieDbClient.getPerson(personExternalId);
        Person existingPerson = personRepository.findByName(read.getName());
        if (existingPerson != null) {
            log.info("Person with name={} already exist", existingPerson.getName());
            return existingPerson.getId();
        }

        Person person = new Person();
        person.setName(read.getName());
        person.setGender(Gender.fromInteger(read.getGender()));
        person.setBiography(read.getBiography());
        person.setAdult(read.getAdult());
        person.setBirthday(read.getBirthday());
        person.setPlaceOfBirth(read.getPlaceOfBirth());
        person = personRepository.save(person);


        externalSystemImportService.createExternalSystemImport(person, personExternalId);

        log.info("Person with external id={} imported", personExternalId);
        return person.getId();
    }
}
