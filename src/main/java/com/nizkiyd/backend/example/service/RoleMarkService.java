package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.RoleMark;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkFilter;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleMarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RoleMarkService {

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleMarkReadDTO getRoleMark(UUID id) {
        RoleMark roleMark = repositoryHelper.getEntityRequired(RoleMark.class, id);
        return translationService.translate(roleMark, RoleMarkReadDTO.class);
    }

    public PageResult<RoleMarkReadDTO> getRoleMark(RoleMarkFilter filter, Pageable pageable) {
        Page<RoleMark> roleMarks = roleMarkRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(roleMarks, RoleMarkReadDTO.class);
    }

    public RoleMarkReadDTO patchRoleMark(UUID id, RoleMarkPatchDTO patch) {
        RoleMark roleMark = repositoryHelper.getEntityRequired(RoleMark.class, id);

        translationService.map(patch, roleMark);

        roleMark = roleMarkRepository.save(roleMark);
        return translationService.translate(roleMark, RoleMarkReadDTO.class);
    }

    public void deleteRoleMark(UUID id) {
        roleMarkRepository.delete(repositoryHelper.getEntityRequired(RoleMark.class, id));
    }
}

