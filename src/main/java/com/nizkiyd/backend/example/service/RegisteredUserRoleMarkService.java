package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.domain.RoleMark;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkCreateDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleMarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserRoleMarkService {

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleMarkReadDTO getRoleMark(UUID registeredUserId, UUID roleMarkId) {
        RoleMark roleMark = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleMark.class, roleMarkId);
        return translationService.translate(roleMark, RoleMarkReadDTO.class);
    }

    @Transactional
    public RoleMarkReadDTO createRoleMark(UUID registeredUserId, RoleMarkCreateDTO create) {
        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        if (registeredUser.getStatus() == AccountStatus.BLOCKED) {
            throw new AccessDeniedException("Access is denied");
        }
        RoleMark roleMark = translationService.translate(create, RoleMark.class);
        roleMark.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, registeredUserId));
        roleMark = roleMarkRepository.save(roleMark);
        return translationService.translate(roleMark, RoleMarkReadDTO.class);
    }

    public RoleMarkReadDTO patchRoleMark(UUID registeredUserId, UUID roleMarkId, RoleMarkPatchDTO patch) {
        RoleMark roleMark = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleMark.class, roleMarkId);

        translationService.map(patch, roleMark);

        roleMark = roleMarkRepository.save(roleMark);
        return translationService.translate(roleMark, RoleMarkReadDTO.class);
    }


    public void deleteRoleMark(UUID registeredUserId, UUID roleMarkId) {
        roleMarkRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleMark.class, roleMarkId));
    }

    public List<RoleMarkReadDTO> getRegisteredUserRoleMarks(UUID registeredUserId) {
        List<RoleMark> roleMarks = roleMarkRepository.findByRegisteredUserId(registeredUserId);
        return roleMarks.stream().map(rm -> translationService.translate(rm, RoleMarkReadDTO.class))
                .collect(Collectors.toList());
    }
}
