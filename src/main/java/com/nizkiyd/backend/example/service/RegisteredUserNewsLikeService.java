package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeCreateDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePutDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.repository.NewsLikeRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserNewsLikeService {

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private NewsRepository newsRepository;

    public NewsLikeReadDTO getNewsLike(UUID registeredUserId, UUID newsLikeId) {
        NewsLike newsLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, NewsLike.class, newsLikeId);
        return translationService.translate(newsLike, NewsLikeReadDTO.class);
    }

    @Transactional
    public NewsLikeReadDTO createNewsLike(UUID registeredUserId, NewsLikeCreateDTO create) {
        NewsLike newsLike = translationService.translate(create, NewsLike.class);
        updateLikeAndDislike(null, create.getNewsId(), create.getLike());

        newsLike.setRegisteredUser(repositoryHelper.getReferenceIfExists(
                RegisteredUser.class, registeredUserId));
        newsLike = newsLikeRepository.save(newsLike);
        return translationService.translate(newsLike, NewsLikeReadDTO.class);
    }

    public NewsLikeReadDTO patchNewsLike(UUID registeredUserId,
                                         UUID newsLikeId,
                                         NewsLikePatchDTO patch) {
        NewsLike newsLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, NewsLike.class, newsLikeId);
        updateLikeAndDislike(newsLike.getLike(), newsLike.getNews().getId(), patch.getLike());

        translationService.map(patch, newsLike);

        newsLike = newsLikeRepository.save(newsLike);
        return translationService.translate(newsLike, NewsLikeReadDTO.class);
    }

    public void deleteNewsLike(UUID registeredUserId, UUID newsLikeId) {
        newsLikeRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, NewsLike.class, newsLikeId));
    }

    public List<NewsLikeReadDTO> getRegisteredUserNewsLikes(UUID registeredUserId) {
        List<NewsLike> newsLikes = newsLikeRepository.findByRegisteredUserId(registeredUserId);
        return newsLikes.stream().map(nl -> translationService.translate(nl, NewsLikeReadDTO.class))
                .collect(Collectors.toList());
    }

    public void updateNewsLike(UUID registeredUserId, UUID newsLikeId, NewsLikePutDTO put) {
        NewsLike newsLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, NewsLike.class, newsLikeId);
        updateLikeAndDislike(newsLike.getLike(), newsLike.getNews().getId(), put.getLike());

        translationService.map(put, newsLike);
        newsLikeRepository.save(newsLike);
    }

    public void updateLikeAndDislike(Boolean oldLike, UUID newsId, Boolean newLike) {
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        if (newLike == null && oldLike) {
            cancelCounterLike(news);
        }
        if (newLike == null && !oldLike) {
            cancelCounterDislike(news);
        }

        if (newLike != null) {
            if (newLike && oldLike == null) {
                addCounterLike(news);
            }
            if (!newLike && oldLike == null) {
                addCounterDislike(news);
            }
            if (newLike && oldLike != null) {
                cancelCounterDislike(news);
                addCounterLike(news);
            }
            if (!newLike && oldLike != null) {
                cancelCounterLike(news);
                addCounterDislike(news);
            }
        }


    }

    private void addCounterLike(News news) {
        int like = news.getLike();
        like++;
        news.setLike(like);
        news = newsRepository.save(news);
    }

    private void addCounterDislike(News news) {
        int dislike = news.getDislike();
        dislike++;
        news.setDislike(dislike);
        news = newsRepository.save(news);
    }

    private void cancelCounterLike(News news) {
        int l = news.getLike();
        l--;
        news.setLike(l);
        news = newsRepository.save(news);
    }

    private void cancelCounterDislike(News news) {
        int dl = news.getDislike();
        dl--;
        news.setDislike(dl);
        news = newsRepository.save(news);
    }
}