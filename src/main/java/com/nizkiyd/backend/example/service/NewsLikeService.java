package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeFilter;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.repository.NewsLikeRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class NewsLikeService {

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public NewsLikeReadDTO getNewsLike(UUID id) {
        NewsLike newsLike = repositoryHelper.getEntityRequired(NewsLike.class, id);
        return translationService.translate(newsLike, NewsLikeReadDTO.class);
    }

    public PageResult<NewsLikeReadDTO> getNewsLike(
            NewsLikeFilter filter, Pageable pageable) {
        Page<NewsLike> likes = newsLikeRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(likes, NewsLikeReadDTO.class);
    }

    public NewsLikeReadDTO patchNewsLike(UUID id, NewsLikePatchDTO patch) {
        NewsLike newsLike = repositoryHelper.getEntityRequired(NewsLike.class, id);

        translationService.map(patch, newsLike);

        newsLike = newsLikeRepository.save(newsLike);
        return translationService.translate(newsLike, NewsLikeReadDTO.class);
    }

    public void deleteNewsLike(UUID id) {
        newsLikeRepository.delete(repositoryHelper.getEntityRequired(NewsLike.class, id));
    }
}
