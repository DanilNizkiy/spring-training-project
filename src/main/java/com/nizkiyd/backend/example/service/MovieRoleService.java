package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.role.RoleCreateDTO;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MovieRoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReadDTO getRole(UUID movieId, UUID roleId) {
        Role role = repositoryHelper.getEntityRequired(
                Movie.class, movieId, Role.class, roleId);
        return translationService.translate(role, RoleReadDTO.class);
    }

    @Transactional
    public RoleReadDTO createRole(UUID movieId, RoleCreateDTO create) {
        Role role = translationService.translate(create, Role.class);
        role.setMovie(repositoryHelper.getReferenceIfExists(Movie.class, movieId));
        role = roleRepository.save(role);

        return translationService.translate(role, RoleReadDTO.class);
    }

    public RoleReadDTO patchRole(UUID movieId, UUID roleId, RolePatchDTO patch) {
        Role role = repositoryHelper.getEntityRequired(
                Movie.class, movieId, Role.class, roleId);

        translationService.map(patch, role);

        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }


    public void deleteRole(UUID movieId, UUID roleId) {
        roleRepository.delete(repositoryHelper.getEntityRequired(
                Movie.class, movieId, Role.class, roleId));
    }

    public List<RoleReadDTO> getMovieRoles(UUID movieId) {
        List<Role> roles = roleRepository.findByMovieId(movieId);
        return roles.stream().map(r -> translationService.translate(r, RoleReadDTO.class))
                .collect(Collectors.toList());
    }

    public void updateRole(UUID movieId, UUID roleId, RolePutDTO put) {
        Role role = repositoryHelper.getEntityRequired(
                Movie.class, movieId, Role.class, roleId);

        translationService.map(put, role);

        roleRepository.save(role);
    }
}