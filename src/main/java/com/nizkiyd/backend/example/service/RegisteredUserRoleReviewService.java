package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewCreateDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPatchDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPutDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserRoleReviewService {

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReviewReadDTO getRoleReview(UUID registeredUserId, UUID roleReviewId) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReview.class, roleReviewId);
        return translationService.translate(roleReview, RoleReviewReadDTO.class);
    }

    @Transactional
    public RoleReviewReadDTO createRoleReview(UUID registeredUserId, RoleReviewCreateDTO create) {
        RoleReview roleReview = translationService.translate(create, RoleReview.class);
        roleReview.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, registeredUserId));
        roleReview = roleReviewRepository.save(roleReview);

        return translationService.translate(roleReview, RoleReviewReadDTO.class);
    }

    public RoleReviewReadDTO patchRoleReview(UUID registeredUserId, UUID roleReviewId, RoleReviewPatchDTO patch) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReview.class, roleReviewId);

        translationService.map(patch, roleReview);

        roleReview = roleReviewRepository.save(roleReview);
        return translationService.translate(roleReview, RoleReviewReadDTO.class);
    }

    public void updateRoleReview(UUID registeredUserId, UUID roleReviewId, RoleReviewPutDTO put) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReview.class, roleReviewId);

        translationService.map(put, roleReview);

        roleReviewRepository.save(roleReview);
    }

    public void deleteRoleReview(UUID registeredUserId, UUID roleReviewId) {
        roleReviewRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReview.class, roleReviewId));
    }

    public List<RoleReviewReadDTO> getRegisteredUserRoleReviews(UUID registeredUserId) {
        List<RoleReview> roleReviews = roleReviewRepository.findByRegisteredUserId(registeredUserId);
        return roleReviews.stream().map(rr -> translationService.translate(rr, RoleReviewReadDTO.class))
                .collect(Collectors.toList());
    }
}
