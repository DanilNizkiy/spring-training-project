package com.nizkiyd.backend.example.service.importer;

import com.nizkiyd.backend.example.client.themoviedb.TheMovieDbClient;
import com.nizkiyd.backend.example.client.themoviedb.dto.MovieCastDTO;
import com.nizkiyd.backend.example.client.themoviedb.dto.MovieCreditsReadDTO;
import com.nizkiyd.backend.example.client.themoviedb.dto.MovieCrewDTO;
import com.nizkiyd.backend.example.client.themoviedb.dto.MovieReadDTO;
import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.exception.ImportedEntityAlreadyExistException;
import com.nizkiyd.backend.example.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class MovieImporterService {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private PersonImporterService personImporterService;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;


    @Transactional
    public UUID importMovie(String movieExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        log.info("Importing movie with external id={}", movieExternalId);

        externalSystemImportService.validateNotImported(Movie.class, movieExternalId);
        MovieReadDTO read = movieDbClient.getMovie(movieExternalId);
        Movie existingMovie = movieRepository.findByTitle(read.getTitle());
        if (existingMovie != null) {
            throw new ImportedEntityAlreadyExistException(Movie.class, existingMovie.getId(),
                    "Movie with title=" + read.getTitle() + " already exist");
        }

        Movie movie = new Movie();
        movie.setTitle(read.getTitle());
        movie.setAdult(read.getAdult());
        movie.setBudget(read.getBudget());
        read.getGenres().forEach(g -> {
            Genre staticGenre = genreRepository.findByType(GenreType.fromString(g.getName()));
            movie.getGenres().add(staticGenre);
        });
        movie.setOriginalLanguage(read.getOriginalLanguage());
        movie.setDescription(read.getOverview());
        movie.setReleaseDate(read.getReleaseDate());
        movie.setRevenue(read.getRevenue());
        movie.setMovieStatus(MovieStatus.fromString(read.getStatus()));
        movieRepository.save(movie);

        externalSystemImportService.createExternalSystemImport(movie, movieExternalId);

        addActorAndCrewMember(movieExternalId, movie);

        log.info("Movie with external id={} imported", movieExternalId);
        return movie.getId();
    }

    private void addActorAndCrewMember(String movieExternalId, Movie movie) {
        MovieCreditsReadDTO movieCreditsReadDTO = movieDbClient.getMovieCredits(movieExternalId);

        if (CollectionUtils.isNotEmpty(movieCreditsReadDTO.getCast())) {
            movieCreditsReadDTO.getCast().forEach(c -> {
                addRoleToMovie(c, movie);
            });
        }

        if (CollectionUtils.isNotEmpty(movieCreditsReadDTO.getCrew())) {
            movieCreditsReadDTO.getCrew().forEach(c -> {
                addCrewMemberToMovie(c, movie);
            });
        }
    }

    private void addCrewMemberToMovie(MovieCrewDTO movieCrewDTO, Movie movie) {
        ExternalSystemImport esi = externalSystemImportRepository.findByIdInExternalSystemAndEntityType(
                movieCrewDTO.getId(), ImportedEntityType.PERSON);

        if (esi == null) {
            try {
                UUID importedPersonId = personImporterService.importPerson(movieCrewDTO.getId());
                CrewMember newCrewMember = createCrewMember(importedPersonId, movieCrewDTO.getJob());
                movie.getCrewMembers().add(newCrewMember);
            } catch (ImportAlreadyPerformedException e) {
                e.printStackTrace();
            }
        } else {
            CrewMemberType crewMemberType = CrewMemberType.fromString(movieCrewDTO.getJob());
            CrewMember crewMember = crewMemberRepository.findByPersonIdAndType(esi.getEntityId(), crewMemberType);
            if (crewMember == null) {
                CrewMember newCrewMember = createCrewMember(esi.getEntityId(), movieCrewDTO.getJob());
                movie.getCrewMembers().add(newCrewMember);
            } else {
                movie.getCrewMembers().add(crewMember);
            }
        }
        movieRepository.save(movie);
    }

    private CrewMember createCrewMember(UUID importedPersonId, String job) {
        CrewMember newCrewMember = new CrewMember();
        newCrewMember.setPerson(repositoryHelper.getReferenceIfExists(Person.class, importedPersonId));
        newCrewMember.setCrewMemberType(CrewMemberType.fromString(job));
        return newCrewMember = crewMemberRepository.save(newCrewMember);
    }


    private void addRoleToMovie(MovieCastDTO movieCastDTO, Movie movie) {
        Role role = new Role();
        role.setRoleName(movieCastDTO.getCharacter());
        role.setMovie(movie);
        role.setGender(Gender.fromInteger(movieCastDTO.getGender()));

        ExternalSystemImport esi = externalSystemImportRepository.findByIdInExternalSystemAndEntityType(
                movieCastDTO.getId(), ImportedEntityType.PERSON);
        if (esi == null) {
            try {
                UUID importedPersonId = personImporterService.importPerson(movieCastDTO.getId());

                Actor actor = actorRepository.findByPersonId(importedPersonId);
                if (actor == null) {
                    actor = createActor(importedPersonId);
                }
                role.setActor(actor);
            } catch (ImportAlreadyPerformedException e) {
                e.printStackTrace();
            }
        } else {
            Actor actor = actorRepository.findByPersonId(esi.getEntityId());
            if (actor == null) {
                Actor newActor = createActor(esi.getEntityId());
                role.setActor(newActor);
            } else {
                role.setActor(actor);
            }
        }
        role = roleRepository.save(role);
    }

    private Actor createActor(UUID importedPersonId) {
        Actor actor = new Actor();
        actor.setPerson(repositoryHelper.getReferenceIfExists(Person.class, importedPersonId));
        return actor = actorRepository.save(actor);
    }
}