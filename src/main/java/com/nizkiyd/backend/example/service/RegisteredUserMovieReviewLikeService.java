package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.MovieReviewLike;
import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.repository.MovieReviewLikeRepository;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserMovieReviewLikeService {

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    public MovieReviewLikeReadDTO getMovieReviewLike(UUID registeredUserId, UUID movieReviewLikeId) {
        MovieReviewLike movieReviewLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReviewLike.class, movieReviewLikeId);
        return translationService.translate(movieReviewLike, MovieReviewLikeReadDTO.class);
    }

    @Transactional
    public MovieReviewLikeReadDTO createMovieReviewLike(UUID registeredUserId, MovieReviewLikeCreateDTO create) {
        MovieReviewLike movieReviewLike = translationService.translate(create, MovieReviewLike.class);
        updateLikeAndDislike(null, create.getMovieReviewId(), create.getLike());

        movieReviewLike.setRegisteredUser(repositoryHelper.getReferenceIfExists(
                RegisteredUser.class, registeredUserId));
        movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);
        return translationService.translate(movieReviewLike, MovieReviewLikeReadDTO.class);
    }

    public MovieReviewLikeReadDTO patchMovieReviewLike(UUID registeredUserId,
                                                       UUID movieReviewLikeId,
                                                       MovieReviewLikePatchDTO patch) {
        MovieReviewLike movieReviewLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReviewLike.class, movieReviewLikeId);
        updateLikeAndDislike(movieReviewLike.getLike(), movieReviewLike.getMovieReview().getId(), patch.getLike());

        translationService.map(patch, movieReviewLike);

        movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);
        return translationService.translate(movieReviewLike, MovieReviewLikeReadDTO.class);
    }

    public void deleteMovieReviewLike(UUID registeredUserId, UUID movieReviewLikeId) {
        movieReviewLikeRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReviewLike.class, movieReviewLikeId));
    }

    public List<MovieReviewLikeReadDTO> getRegisteredUserMovieReviewLikes(UUID registeredUserId) {
        List<MovieReviewLike> movieReviewLikes = movieReviewLikeRepository.findByRegisteredUserId(registeredUserId);
        return movieReviewLikes.stream().map(nl -> translationService.translate(nl, MovieReviewLikeReadDTO.class))
                .collect(Collectors.toList());
    }

    public void updateMovieReviewLike(UUID registeredUserId, UUID movieReviewLikeId, MovieReviewLikePutDTO put) {
        MovieReviewLike movieReviewLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieReviewLike.class, movieReviewLikeId);
        updateLikeAndDislike(movieReviewLike.getLike(), movieReviewLike.getMovieReview().getId(), put.getLike());

        translationService.map(put, movieReviewLike);
        movieReviewLikeRepository.save(movieReviewLike);
    }

    public void updateLikeAndDislike(Boolean oldLike, UUID movieReviewId, Boolean newLike) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(MovieReview.class, movieReviewId);

        if (newLike == null && oldLike) {
            cancelCounterLike(movieReview);
        }
        if (newLike == null && !oldLike) {
            cancelCounterDislike(movieReview);
        }

        if (newLike != null) {
            if (newLike && oldLike == null) {
                addCounterLike(movieReview);
            }
            if (!newLike && oldLike == null) {
                addCounterDislike(movieReview);
            }
            if (newLike && oldLike != null) {
                cancelCounterDislike(movieReview);
                addCounterLike(movieReview);
            }
            if (!newLike && oldLike != null) {
                cancelCounterLike(movieReview);
                addCounterDislike(movieReview);
            }
        }


    }

    private void addCounterLike(MovieReview movieReview) {
        int like = movieReview.getLike();
        like++;
        movieReview.setLike(like);
        movieReview = movieReviewRepository.save(movieReview);
    }

    private void addCounterDislike(MovieReview movieReview) {
        int dislike = movieReview.getDislike();
        dislike++;
        movieReview.setDislike(dislike);
        movieReview = movieReviewRepository.save(movieReview);
    }

    private void cancelCounterLike(MovieReview movieReview) {
        int l = movieReview.getLike();
        l--;
        movieReview.setLike(l);
        movieReview = movieReviewRepository.save(movieReview);
    }

    private void cancelCounterDislike(MovieReview movieReview) {
        int dl = movieReview.getDislike();
        dl--;
        movieReview.setDislike(dl);
        movieReview = movieReviewRepository.save(movieReview);
    }
}
