package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.role.*;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleMarkRepository;
import com.nizkiyd.backend.example.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    public RoleReadDTO getRole(UUID id) {
        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public PageResult<RoleReadDTO> getRole(RoleFilter filter, Pageable pageable) {
        Page<Role> roles = roleRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(roles, RoleReadDTO.class);
    }

    public RoleReadDTO patchRole(UUID id, RolePatchDTO patch) {
        Role role = repositoryHelper.getEntityRequired(Role.class, id);

        translationService.map(patch, role);

        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public void deleteRole(UUID id) {
        roleRepository.delete(repositoryHelper.getEntityRequired(Role.class, id));
    }

    public void updateRole(UUID roleId, RolePutDTO put) {
        Role role = repositoryHelper.getEntityRequired(Role.class, roleId);

        translationService.map(put, role);

        roleRepository.save(role);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageMarkOfRole(UUID roleId) {
        Double averageMark = roleMarkRepository.calcAverageRoleMarkOfRole(roleId);
        Role role = repositoryHelper.getEntityRequired(Role.class, roleId);

        log.info("Setting new average mark of role: {}. Old value: {}, new value: {}", roleId,
                role.getAverageMark(), averageMark);
        role.setAverageMark(averageMark);
        roleRepository.save(role);
    }
}