package com.nizkiyd.backend.example.service.importer;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.repository.ExternalSystemImportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ExternalSystemImportService {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    public void validateNotImported(Class<? extends AbstractEntity> entityToImport,
                                    String idInExternalSystem) throws ImportAlreadyPerformedException {
        ImportedEntityType importedEntityType = getImportedEntityType(entityToImport);
        ExternalSystemImport esi = externalSystemImportRepository.findByIdInExternalSystemAndEntityType(
                idInExternalSystem, importedEntityType);

        if (esi != null) {
            throw new ImportAlreadyPerformedException(esi);
        }
    }

    public <T extends AbstractEntity> UUID createExternalSystemImport(T entity, String idInExternalSystem) {
        ImportedEntityType importedEntityType = getImportedEntityType(entity.getClass());
        ExternalSystemImport esi = new ExternalSystemImport();
        esi.setEntityType(importedEntityType);
        esi.setEntityId(entity.getId());
        esi.setIdInExternalSystem(idInExternalSystem);
        esi = externalSystemImportRepository.save(esi);
        return esi.getId();
    }

    private ImportedEntityType getImportedEntityType(Class<? extends AbstractEntity> entityToImport) {
        if (Movie.class.equals(entityToImport)) {
            return ImportedEntityType.MOVIE;
        } else if (Person.class.equals(entityToImport)) {
            return ImportedEntityType.PERSON;
        }

        throw new IllegalArgumentException("Importing of entities " + entityToImport + " is not supported");
    }
}
