package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import com.nizkiyd.backend.example.repository.SignalToContentManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RegisteredUserNotifacationService {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    public void notifyOnSignalToContentMangerStatusChangeToFixed(UUID id) {
        SignalToContentManager signal = signalToContentManagerRepository.findById(id).get();
        signal.setSignalToContentManagerStatus(SignalToContentManagerStatus.FIXED);
        signalToContentManagerRepository.save(signal);
    }
}
