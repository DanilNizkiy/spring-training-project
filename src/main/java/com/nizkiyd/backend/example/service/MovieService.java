package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.movie.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.exception.ImportedEntityAlreadyExistException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.MovieMarkRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.service.importer.MovieImporterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private NewsRepository newsRepository;

    public MovieReadDTO getMovie(UUID id) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, id);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public PageResult<MovieReadDTO> getMovie(MovieFilter filter, Pageable pageable) {
        Page<Movie> movies = movieRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(movies, MovieReadDTO.class);
    }

    public List<MovieReadDTO> getMoviesNews(UUID newsId) {
        List<Movie> movies = movieRepository.findByNewsId(newsId);
        return movies.stream().map(cm -> translationService.translate(cm, MovieReadDTO.class))
                .collect(Collectors.toList());
    }

    public List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard() {
        return movieRepository.getMoviesLeaderBoard();
    }

    @Transactional
    public MovieReadDTO createMovie(MovieCreateDTO create) {
        Movie movie = translationService.translate(create, Movie.class);

        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO patchMovie(UUID id, MoviePatchDTO patch) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, id);

        translationService.map(patch, movie);

        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public void deleteMovie(UUID id) {
        movieRepository.delete(repositoryHelper.getEntityRequired(Movie.class, id));
    }

    public void updateMovie(UUID id, MoviePutDTO put) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, id);

        translationService.map(put, movie);

        movieRepository.save(movie);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageMarkOfMovie(UUID movieId) {
        Double averageMark = movieMarkRepository.calcAverageMovieMarkOfMovie(movieId);
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieId);

        log.info("Setting new average mark of movie: {}. Old value: {}, new value: {}", movieId,
                movie.getAverageMark(), averageMark);
        movie.setAverageMark(averageMark);
        movieRepository.save(movie);
    }

    public MovieReadDTO importMovie(
            String movieExternalId) throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        UUID movieInternalId = movieImporterService.importMovie(movieExternalId);
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieInternalId);

        return translationService.translate(movie, MovieReadDTO.class);
    }

    @Transactional
    public List<MovieReadDTO> addMovieToNews(UUID newsId, UUID movieId) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieId);
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        if (news.getMovies().stream().anyMatch(m -> m.getId().equals(movieId))) {
            throw new LinkDuplicatedException(String.format("News %s already has movie %s", newsId, movieId));
        }

        news.getMovies().add(movie);
        news = newsRepository.save(news);

        return news.getMovies().stream()
                .map(m -> translationService.translate(m, MovieReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<MovieReadDTO> removeMovieFromNews(UUID newsId, UUID movieId) {
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        boolean removed = news.getMovies().removeIf(m -> m.getId().equals(movieId));
        if (!removed) {
            throw new EntityNotFoundException("News " + newsId + " has no movie " + movieId);
        }

        news = newsRepository.save(news);

        return news.getMovies().stream()
                .map(m -> translationService.translate(m, MovieReadDTO.class))
                .collect(Collectors.toList());
    }
}
