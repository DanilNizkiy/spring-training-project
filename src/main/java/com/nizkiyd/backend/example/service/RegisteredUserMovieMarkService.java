package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.MovieMark;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkCreateDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.repository.MovieMarkRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserMovieMarkService {

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MovieMarkReadDTO getMovieMark(UUID registeredUserId, UUID movieMarkId) {
        MovieMark movieMark = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieMark.class, movieMarkId);
        return translationService.translate(movieMark, MovieMarkReadDTO.class);
    }

    @Transactional
    public MovieMarkReadDTO createMovieMark(UUID registeredUserId, MovieMarkCreateDTO create) {
        MovieMark movieMark = translationService.translate(create, MovieMark.class);
        movieMark.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, registeredUserId));
        movieMark = movieMarkRepository.save(movieMark);

        return translationService.translate(movieMark, MovieMarkReadDTO.class);
    }

    public MovieMarkReadDTO patchMovieMark(UUID registeredUserId, UUID movieMarkId, MovieMarkPatchDTO patch) {
        MovieMark movieMark = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieMark.class, movieMarkId);

        translationService.map(patch, movieMark);

        movieMark = movieMarkRepository.save(movieMark);
        return translationService.translate(movieMark, MovieMarkReadDTO.class);
    }


    public void deleteMovieMark(UUID registeredUserId, UUID movieMarkId) {
        movieMarkRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, MovieMark.class, movieMarkId));
    }

    public List<MovieMarkReadDTO> getRegisteredUserMovieMarks(UUID registeredUserId) {
        List<MovieMark> movieMarks = movieMarkRepository.findByRegisteredUserId(registeredUserId);
        return movieMarks.stream().map(mm -> translationService.translate(mm, MovieMarkReadDTO.class))
                .collect(Collectors.toList());
    }
}