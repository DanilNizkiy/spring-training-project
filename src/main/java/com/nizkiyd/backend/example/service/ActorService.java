package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.actor.ActorCreateDTO;
import com.nizkiyd.backend.example.dto.actor.ActorPatchDTO;
import com.nizkiyd.backend.example.dto.actor.ActorReadDTO;
import com.nizkiyd.backend.example.dto.actor.ActorFilter;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private NewsRepository newsRepository;

    public ActorReadDTO getActor(UUID id) {
        Actor actor = repositoryHelper.getEntityRequired(Actor.class, id);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public PageResult<ActorReadDTO> getActor(ActorFilter filter, Pageable pageable) {
        Page<Actor> actors = actorRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(actors, ActorReadDTO.class);
    }

    @Transactional
    public ActorReadDTO createActor(ActorCreateDTO create) {
        Actor actor = translationService.translate(create, Actor.class);

        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public ActorReadDTO patchActor(UUID id, ActorPatchDTO patch) {
        Actor actor = repositoryHelper.getEntityRequired(Actor.class, id);

        translationService.map(patch, actor);

        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public void deleteActor(UUID id) {
        actorRepository.delete(repositoryHelper.getEntityRequired(Actor.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRatingsOfActor(UUID actorId) {
        Double averageRoleRating = roleRepository.calcAverageRoleRatingOfActor(actorId);
        Double averageMovieRating = movieRepository.calcAverageMovieRatingOfActor(actorId);
        Actor actor = repositoryHelper.getEntityRequired(Actor.class, actorId);

        log.info("Setting new average role rating of actor: {}. Old value: {}, new value: {}", actorId,
                actor.getAverageRoleRating(), averageRoleRating);
        actor.setAverageRoleRating(averageRoleRating);

        log.info("Setting new average movie rating of actor: {}. Old value: {}, new value: {}", actorId,
                actor.getAverageMovieRating(), averageMovieRating);
        actor.setAverageMovieRating(averageMovieRating);
        actorRepository.save(actor);
    }

    @Transactional
    public List<ActorReadDTO> addActorToNews(UUID newsId, UUID actorId) {
        Actor actor = repositoryHelper.getEntityRequired(Actor.class, actorId);
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        if (news.getActors().stream().anyMatch(m -> m.getId().equals(actorId))) {
            throw new LinkDuplicatedException(String.format("News %s already has actor %s", newsId, actorId));
        }

        news.getActors().add(actor);
        news = newsRepository.save(news);

        return news.getActors().stream()
                .map(m -> translationService.translate(m, ActorReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<ActorReadDTO> removeActorFromNews(UUID newsId, UUID actorId) {
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        boolean removed = news.getActors().removeIf(m -> m.getId().equals(actorId));
        if (!removed) {
            throw new EntityNotFoundException("News " + newsId + " has no actor " + actorId);
        }

        news = newsRepository.save(news);

        return news.getActors().stream()
                .map(m -> translationService.translate(m, ActorReadDTO.class))
                .collect(Collectors.toList());
    }

    public List<ActorReadDTO> getActorsNews(UUID newsId) {
        List<Actor> actors = actorRepository.findByNewsId(newsId);
        return actors.stream().map(a -> translationService.translate(a, ActorReadDTO.class))
                .collect(Collectors.toList());
    }
}