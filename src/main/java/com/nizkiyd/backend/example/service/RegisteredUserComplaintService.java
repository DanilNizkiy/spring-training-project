package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Complaint;
import com.nizkiyd.backend.example.domain.ComplaintStatus;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.complaint.ComplaintCreateDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPatchDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPutDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintReadDTO;
import com.nizkiyd.backend.example.repository.ComplaintRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserComplaintService {

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;


    public ComplaintReadDTO getComplaint(UUID registeredUserId, UUID complaintId) {
        Complaint complaint = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, Complaint.class, complaintId);
        return translationService.translate(complaint, ComplaintReadDTO.class);
    }

    @Transactional
    public ComplaintReadDTO createComplaint(UUID registeredUserId, ComplaintCreateDTO create) {
        Complaint complaint = translationService.translate(create, Complaint.class);
        complaint.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, registeredUserId));
        complaint.setComplaintStatus(ComplaintStatus.MODERATION);
        complaint = complaintRepository.save(complaint);

        return translationService.translate(complaint, ComplaintReadDTO.class);
    }

    public ComplaintReadDTO patchComplaint(UUID registeredUserId, UUID complaintId, ComplaintPatchDTO patch) {
        Complaint complaint = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, Complaint.class, complaintId);

        translationService.map(patch, complaint);

        complaint = complaintRepository.save(complaint);
        return translationService.translate(complaint, ComplaintReadDTO.class);
    }

    public void updateComplaint(UUID registeredUserId, UUID complaintId, ComplaintPutDTO put) {
        Complaint complaint = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, Complaint.class, complaintId);

        translationService.map(put, complaint);

        complaintRepository.save(complaint);
    }

    public void deleteComplaint(UUID registeredUserId, UUID complaintId) {
        complaintRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, Complaint.class, complaintId));
    }

    public List<ComplaintReadDTO> getRegisteredUserComplaints(UUID registeredUserId) {
        List<Complaint> complaints = complaintRepository.findByRegisteredUserId(registeredUserId);
        return complaints.stream().map(c -> translationService.translate(c, ComplaintReadDTO.class))
                .collect(Collectors.toList());
    }
}
