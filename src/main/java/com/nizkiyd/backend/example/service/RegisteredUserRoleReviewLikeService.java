package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.domain.RoleReviewLike;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.repository.RoleReviewLikeRepository;
import com.nizkiyd.backend.example.repository.RoleReviewRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegisteredUserRoleReviewLikeService {

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    public RoleReviewLikeReadDTO getRoleReviewLike(UUID registeredUserId, UUID roleReviewLikeId) {
        RoleReviewLike roleReviewLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReviewLike.class, roleReviewLikeId);
        return translationService.translate(roleReviewLike, RoleReviewLikeReadDTO.class);
    }

    @Transactional
    public RoleReviewLikeReadDTO createRoleReviewLike(UUID registeredUserId, RoleReviewLikeCreateDTO create) {
        RoleReviewLike roleReviewLike = translationService.translate(create, RoleReviewLike.class);
        updateLikeAndDislike(null, create.getRoleReviewId(), create.getLike());

        roleReviewLike.setRegisteredUser(repositoryHelper.getReferenceIfExists(
                RegisteredUser.class, registeredUserId));
        roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);
        return translationService.translate(roleReviewLike, RoleReviewLikeReadDTO.class);
    }

    public RoleReviewLikeReadDTO patchRoleReviewLike(UUID registeredUserId,
                                                     UUID roleReviewLikeId,
                                                     RoleReviewLikePatchDTO patch) {
        RoleReviewLike roleReviewLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReviewLike.class, roleReviewLikeId);
        updateLikeAndDislike(roleReviewLike.getLike(), roleReviewLike.getRoleReview().getId(), patch.getLike());

        translationService.map(patch, roleReviewLike);

        roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);
        return translationService.translate(roleReviewLike, RoleReviewLikeReadDTO.class);
    }

    public void deleteRoleReviewLike(UUID registeredUserId, UUID roleReviewLikeId) {
        roleReviewLikeRepository.delete(repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReviewLike.class, roleReviewLikeId));
    }

    public List<RoleReviewLikeReadDTO> getRegisteredUserRoleReviewLikes(UUID registeredUserId) {
        List<RoleReviewLike> roleReviewLikes = roleReviewLikeRepository.findByRegisteredUserId(registeredUserId);
        return roleReviewLikes.stream().map(nl -> translationService.translate(nl, RoleReviewLikeReadDTO.class))
                .collect(Collectors.toList());
    }

    public void updateRoleReviewLike(UUID registeredUserId, UUID roleReviewLikeId, RoleReviewLikePutDTO put) {
        RoleReviewLike roleReviewLike = repositoryHelper.getEntityRequired(
                RegisteredUser.class, registeredUserId, RoleReviewLike.class, roleReviewLikeId);
        updateLikeAndDislike(roleReviewLike.getLike(), roleReviewLike.getRoleReview().getId(), put.getLike());

        translationService.map(put, roleReviewLike);
        roleReviewLikeRepository.save(roleReviewLike);
    }

    public void updateLikeAndDislike(Boolean oldLike, UUID roleReviewId, Boolean newLike) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(RoleReview.class, roleReviewId);

        if (newLike == null && oldLike) {
            cancelCounterLike(roleReview);
        }
        if (newLike == null && !oldLike) {
            cancelCounterDislike(roleReview);
        }

        if (newLike != null) {
            if (newLike && oldLike == null) {
                addCounterLike(roleReview);
            }
            if (!newLike && oldLike == null) {
                addCounterDislike(roleReview);
            }
            if (newLike && oldLike != null) {
                cancelCounterDislike(roleReview);
                addCounterLike(roleReview);
            }
            if (!newLike && oldLike != null) {
                cancelCounterLike(roleReview);
                addCounterDislike(roleReview);
            }
        }
    }

    private void addCounterLike(RoleReview roleReview) {
        int like = roleReview.getLike();
        like++;
        roleReview.setLike(like);
        roleReview = roleReviewRepository.save(roleReview);
    }

    private void addCounterDislike(RoleReview roleReview) {
        int dislike = roleReview.getDislike();
        dislike++;
        roleReview.setDislike(dislike);
        roleReview = roleReviewRepository.save(roleReview);
    }

    private void cancelCounterLike(RoleReview roleReview) {
        int l = roleReview.getLike();
        l--;
        roleReview.setLike(l);
        roleReview = roleReviewRepository.save(roleReview);
    }

    private void cancelCounterDislike(RoleReview roleReview) {
        int dl = roleReview.getDislike();
        dl--;
        roleReview.setDislike(dl);
        roleReview = roleReviewRepository.save(roleReview);
    }
}
