package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.RoleReviewLike;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeFilter;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleReviewLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RoleReviewLikeService {

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReviewLikeReadDTO getRoleReviewLike(UUID id) {
        RoleReviewLike roleReviewLike = repositoryHelper.getEntityRequired(RoleReviewLike.class, id);
        return translationService.translate(roleReviewLike, RoleReviewLikeReadDTO.class);
    }

    public PageResult<RoleReviewLikeReadDTO> getRoleReviewLike(
            RoleReviewLikeFilter filter, Pageable pageable) {
        Page<RoleReviewLike> likes = roleReviewLikeRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(likes, RoleReviewLikeReadDTO.class);
    }

    public RoleReviewLikeReadDTO patchRoleReviewLike(UUID id, RoleReviewLikePatchDTO patch) {
        RoleReviewLike roleReviewLike = repositoryHelper.getEntityRequired(RoleReviewLike.class, id);

        translationService.map(patch, roleReviewLike);

        roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);
        return translationService.translate(roleReviewLike, RoleReviewLikeReadDTO.class);
    }

    public void deleteRoleReviewLike(UUID id) {
        roleReviewLikeRepository.delete(repositoryHelper.getEntityRequired(RoleReviewLike.class, id));
    }
}
