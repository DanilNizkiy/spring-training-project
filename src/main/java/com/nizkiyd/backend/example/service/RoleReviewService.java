package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewFilter;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPatchDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPutDTO;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import com.nizkiyd.backend.example.repository.RoleReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RoleReviewService {

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReviewReadDTO getRoleReview(UUID id) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(RoleReview.class, id);
        return translationService.translate(roleReview, RoleReviewReadDTO.class);
    }

    public PageResult<RoleReviewReadDTO> getRoleReview(RoleReviewFilter filter, Pageable pageable) {
        Page<RoleReview> roleReviews = roleReviewRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(roleReviews, RoleReviewReadDTO.class);
    }

    public RoleReviewReadDTO patchRoleReview(UUID id, RoleReviewPatchDTO patch) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(RoleReview.class, id);

        translationService.map(patch, roleReview);

        roleReview = roleReviewRepository.save(roleReview);
        return translationService.translate(roleReview, RoleReviewReadDTO.class);
    }

    public void updateRoleReview(UUID id, RoleReviewPutDTO put) {
        RoleReview roleReview = repositoryHelper.getEntityRequired(RoleReview.class, id);

        translationService.map(put, roleReview);

        roleReviewRepository.save(roleReview);
    }

    public void deleteRoleReview(UUID id) {
        roleReviewRepository.delete(repositoryHelper.getEntityRequired(RoleReview.class, id));
    }
}
