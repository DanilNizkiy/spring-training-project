package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.Admin;
import com.nizkiyd.backend.example.dto.admin.AdminCreateDTO;
import com.nizkiyd.backend.example.dto.admin.AdminPatchDTO;
import com.nizkiyd.backend.example.dto.admin.AdminReadDTO;
import com.nizkiyd.backend.example.dto.admin.AdminStatusDTO;
import com.nizkiyd.backend.example.repository.AdminRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class AdminService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public AdminReadDTO getAdmin(UUID id) {
        Admin admin = repositoryHelper.getEntityRequired(Admin.class, id);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    @Transactional
    public AdminReadDTO createAdmin(AdminCreateDTO create) {
        Admin admin = translationService.translate(create, Admin.class);
        admin.setEncodedPassword(passwordEncoder.encode(create.getPassword()));
        admin.setStatus(AccountStatus.VALID);

        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO patchAdmin(UUID id, AdminPatchDTO patch) {
        Admin admin = repositoryHelper.getEntityRequired(Admin.class, id);

        translationService.map(patch, admin);

        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO changeAdminStatus(UUID id, AdminStatusDTO statusDTO) {
        Admin admin = repositoryHelper.getEntityRequired(Admin.class, id);
        admin.setStatus(statusDTO.getStatus());

        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public void deleteAdmin(UUID id) {
        adminRepository.delete(repositoryHelper.getEntityRequired(Admin.class, id));
    }
}