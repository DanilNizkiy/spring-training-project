package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.MovieReviewLike;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeFilter;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.repository.MovieReviewLikeRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MovieReviewLikeService {

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MovieReviewLikeReadDTO getMovieReviewLike(UUID id) {
        MovieReviewLike movieReviewLike = repositoryHelper.getEntityRequired(MovieReviewLike.class, id);
        return translationService.translate(movieReviewLike, MovieReviewLikeReadDTO.class);
    }

    public PageResult<MovieReviewLikeReadDTO> getMovieReviewLike(
            MovieReviewLikeFilter filter, Pageable pageable) {
        Page<MovieReviewLike> likes = movieReviewLikeRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(likes, MovieReviewLikeReadDTO.class);
    }

    public MovieReviewLikeReadDTO patchMovieReviewLike(UUID id, MovieReviewLikePatchDTO patch) {
        MovieReviewLike movieReviewLike = repositoryHelper.getEntityRequired(MovieReviewLike.class, id);

        translationService.map(patch, movieReviewLike);

        movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);
        return translationService.translate(movieReviewLike, MovieReviewLikeReadDTO.class);
    }

    public void deleteMovieReviewLike(UUID id) {
        movieReviewLikeRepository.delete(repositoryHelper.getEntityRequired(MovieReviewLike.class, id));
    }
}
