package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.*;
import com.nizkiyd.backend.example.exception.SignalToContentManagerWrongStatusException;
import com.nizkiyd.backend.example.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Service
public class SignalToContentManagerService {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public SignalToContentManagerReadDTO getSignalToContentManager(UUID id) {
        SignalToContentManager signalToContentManager = repositoryHelper.getEntityRequired(
                SignalToContentManager.class, id);
        return translationService.translate(signalToContentManager, SignalToContentManagerReadDTO.class);
    }

    public PageResult<SignalToContentManagerReadDTO> getSignalToContentManager(
            SignalToContentManagerFilter filter, Pageable pageable) {
        Page<SignalToContentManager> signals = signalToContentManagerRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(signals, SignalToContentManagerReadDTO.class);
    }

    public SignalToContentManagerReadDTO patchSignalToContentManager(UUID id, SignalToContentManagerPatchDTO patch) {
        SignalToContentManager signalToContentManager = repositoryHelper.getEntityRequired(
                SignalToContentManager.class, id);

        translationService.map(patch, signalToContentManager);

        signalToContentManager = signalToContentManagerRepository.save(signalToContentManager);
        return translationService.translate(signalToContentManager, SignalToContentManagerReadDTO.class);

    }

    @Transactional
    public SignalToContentManagerReadDTO fixContent(UUID signalId,
                                                    SignalToContentManagerFixContent signalFixContent) {
        SignalToContentManager signal = repositoryHelper.getEntityRequired(SignalToContentManager.class, signalId);
        if (signal.getSignalToContentManagerStatus() != SignalToContentManagerStatus.NEED_TO_FIX) {
            throw new SignalToContentManagerWrongStatusException(
                    signal.getId(), signal.getSignalToContentManagerStatus());
        }

        Boolean status = null;
        switch (signal.getSignalToContentManagerObjectType()) {
          case NEWS:
              News news = repositoryHelper.getEntityRequired(News.class, signal.getSignalToContentManagerObjectId());
              status = changeText(news::getText, news::setText, signal, signalFixContent.getReplacement(), news);
              break;

          case MOVIE:
              Movie m = repositoryHelper.getEntityRequired(Movie.class, signal.getSignalToContentManagerObjectId());
              status = changeText(m::getDescription, m::setDescription, signal, signalFixContent.getReplacement(), m);
              break;

          case PERSON:
              Person p = repositoryHelper.getEntityRequired(Person.class, signal.getSignalToContentManagerObjectId());
              status = changeText(
                      p::getBiography, p::setBiography, signal, signalFixContent.getReplacement(), p);
              break;
          default:
        }

        if (!status) {
            signal.setSignalToContentManagerStatus(SignalToContentManagerStatus.CANCELED);
            repositoryHelper.save(signal);
            return translationService.translate(signal, SignalToContentManagerReadDTO.class);
        }

        signal.setSignalToContentManagerStatus(SignalToContentManagerStatus.FIXED);
        signal.setFixedAt(Instant.now());
        signal.setContentManager(repositoryHelper.getReferenceIfExists(
                ContentManager.class, signalFixContent.getContentManagerId()));
        signal.setReplacement(signalFixContent.getReplacement());
        signal = signalToContentManagerRepository.save(signal);

        List<SignalToContentManager> sameSignalsToContentManager = signalToContentManagerRepository.findByParameters(
                signal.getSignalToContentManagerObjectId(),
                signal.getWrongText(),
                SignalToContentManagerStatus.NEED_TO_FIX);

        SignalToContentManager finalSignal = signal;
        sameSignalsToContentManager.forEach(s -> {
            s.setSignalToContentManagerStatus(finalSignal.getSignalToContentManagerStatus());
            s.setFixedAt(finalSignal.getFixedAt());
            s.setContentManager(finalSignal.getContentManager());
            s.setReplacement(finalSignal.getReplacement());
            signalToContentManagerRepository.save(s);
        });
        return translationService.translate(signal, SignalToContentManagerReadDTO.class);
    }

    public void updateSignalToContentManager(UUID signalToContentManagerId, SignalToContentManagerPutDTO put) {
        SignalToContentManager signalToContentManager = repositoryHelper.getEntityRequired(
                SignalToContentManager.class, signalToContentManagerId);

        translationService.map(put, signalToContentManager);

        signalToContentManagerRepository.save(signalToContentManager);
    }

    public void deleteSignalToContentManager(UUID id) {
        signalToContentManagerRepository.delete(repositoryHelper.getEntityRequired(SignalToContentManager.class, id));
    }

    private Boolean changeText(Supplier<String> getter, Consumer<String> setter,
                            SignalToContentManager signal, String replacement, Object obj) {
        StringBuilder entityText = new StringBuilder(getter.get());
        String wrongText = entityText.substring(signal.getFromIndex(), signal.getToIndex());

        if (wrongText.equals(signal.getWrongText())) {
            entityText.replace(signal.getFromIndex(), signal.getToIndex(), replacement);
            setter.accept(entityText.toString());
            repositoryHelper.save(obj);
            return true;
        } else {
            return false;
        }
    }
}