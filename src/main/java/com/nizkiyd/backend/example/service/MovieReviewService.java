package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.domain.ReviewStatus;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviereview.*;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MovieReviewService {

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MovieReviewReadDTO getMovieReview(UUID id) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(MovieReview.class, id);
        return translationService.translate(movieReview, MovieReviewReadDTO.class);
    }

    public PageResult<MovieReviewReadDTO> getMovieReview(MovieReviewFilter filter, Pageable pageable) {
        Page<MovieReview> movieReviews = movieReviewRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(movieReviews, MovieReviewReadDTO.class);
    }

    public MovieReviewReadDTO patchMovieReview(UUID id, MovieReviewPatchDTO patch) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(MovieReview.class, id);

        translationService.map(patch, movieReview);

        movieReview = movieReviewRepository.save(movieReview);
        return translationService.translate(movieReview, MovieReviewReadDTO.class);
    }

    public void updateMovieReview(UUID movieReviewId, MovieReviewPutDTO put) {
        MovieReview movieReview = repositoryHelper.getEntityRequired(MovieReview.class, movieReviewId);

        translationService.map(put, movieReview);

        movieReviewRepository.save(movieReview);
    }

    public void deleteMovieReview(UUID id) {
        movieReviewRepository.delete(repositoryHelper.getEntityRequired(MovieReview.class, id));
    }

    public List<MovieReviewReadDTO> getReviewsMovie(UUID movieId) {
        List<MovieReview> movieReviews = movieReviewRepository.findByMovieIdAndReviewStatus(
                movieId, ReviewStatus.APPROVED, ReviewStatus.APPROVED_WITH_FIX);

        return movieReviews.stream().map(mr -> translationService.translate(mr, MovieReviewReadDTO.class))
                .collect(Collectors.toList());
    }
}
