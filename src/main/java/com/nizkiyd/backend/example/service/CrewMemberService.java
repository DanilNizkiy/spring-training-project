package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.CrewMember;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberCreateDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberFilter;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberPatchDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.CrewMemberRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CrewMemberService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private NewsRepository newsRepository;

    public List<CrewMemberReadDTO> getCrewMembersNews(UUID newsId) {
        List<CrewMember> crewMembers = crewMemberRepository.findByNewsId(newsId);
        return crewMembers.stream().map(cm -> translationService.translate(cm, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<CrewMemberReadDTO> addCrewMemberToMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieId);
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);

        if (movie.getCrewMembers().stream().anyMatch(cm -> cm.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Movie %s already has crew member %s", movieId, id));
        }

        movie.getCrewMembers().add(crewMember);
        movie = movieRepository.save(movie);

        return movie.getCrewMembers().stream().map(cm -> translationService.translate(cm, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<CrewMemberReadDTO> removeCrewMemberFromMovie(UUID movieId, UUID id) {
        Movie movie = repositoryHelper.getEntityRequired(Movie.class, movieId);

        boolean removed = movie.getCrewMembers().removeIf(cm -> cm.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Movie " + movieId + " has no crew member " + id);
        }

        movie = movieRepository.save(movie);

        return movie.getCrewMembers().stream().map(cm -> translationService.translate(cm, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }

    public List<CrewMemberReadDTO> getCrewMemberMovies(UUID movieId) {
        List<CrewMember> crewMembers = crewMemberRepository.findByMovieId(movieId);
        return crewMembers.stream().map(cm -> translationService.translate(cm, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }

    public List<CrewMemberReadDTO> getAllCrewMembers() {
        List<CrewMember> crewMembers = new ArrayList<>();
        crewMemberRepository.findAll().forEach(crewMembers::add);
        return crewMembers.stream().map(cm -> translationService.translate(cm, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }

    public CrewMemberReadDTO getCrewMember(UUID id) {
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);
        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    public PageResult<CrewMemberReadDTO> getCrewMember(CrewMemberFilter filter, Pageable pageable) {
        Page<CrewMember> crewMembers = crewMemberRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(crewMembers, CrewMemberReadDTO.class);
    }


    @Transactional
    public CrewMemberReadDTO createCrewMember(CrewMemberCreateDTO createDTO) {
        CrewMember crewMember = translationService.translate(createDTO, CrewMember.class);
        crewMember = crewMemberRepository.save(crewMember);

        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    public CrewMemberReadDTO patchCrewMember(UUID id, CrewMemberPatchDTO patch) {
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);

        translationService.map(patch, crewMember);

        crewMember = crewMemberRepository.save(crewMember);
        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    @Transactional
    public List<CrewMemberReadDTO> addCrewMemberToNews(UUID newsId, UUID crewMemberId) {
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, crewMemberId);
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        if (news.getCrewMembers().stream().anyMatch(m -> m.getId().equals(crewMemberId))) {
            throw new LinkDuplicatedException(String.format("News %s already has crewMember %s", newsId, crewMemberId));
        }

        news.getCrewMembers().add(crewMember);
        news = newsRepository.save(news);

        return news.getCrewMembers().stream()
                .map(m -> translationService.translate(m, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<CrewMemberReadDTO> removeCrewMemberFromNews(UUID newsId, UUID crewMemberId) {
        News news = repositoryHelper.getEntityRequired(News.class, newsId);

        boolean removed = news.getCrewMembers().removeIf(m -> m.getId().equals(crewMemberId));
        if (!removed) {
            throw new EntityNotFoundException("News " + newsId + " has no crewMember " + crewMemberId);
        }

        news = newsRepository.save(news);

        return news.getCrewMembers().stream()
                .map(m -> translationService.translate(m, CrewMemberReadDTO.class))
                .collect(Collectors.toList());
    }
}
