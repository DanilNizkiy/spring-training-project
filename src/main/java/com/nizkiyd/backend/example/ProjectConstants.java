package com.nizkiyd.backend.example;

public class ProjectConstants {

    public static final String BIOGRAPHY_BALABANOV = "Born February 25, 1959 in Sverdlovsk. In 1966-1976 he studied "
            + "there in high school. In 1981-1983 He served as an officer in the Soviet Army (military transport"
            + " aircraft in Vitebsk) [4], flew to Africa and Asia. This experience is reflected in the painting"
            + " 'Cargo 200'. Member of the war in Afghanistan. Navy. It was a marine vest."
            + "For four years he worked as an assistant director at the Sverdlovsk film studio. In 1990, the final"
            + " direction of the Higher Coursework Scriptwriters and Directors (experimental workshop 'Cinema of"
            + " Authors' L. Nikolaev, B. Galanter)."
            + "The first feature film 26-year-old Balabanov shot in the Urals in 1985. The script of the film was"
            + " written in one night. This low-budget picture was shot in a restaurant. The film was played by the "
            + "group Nautilus Pompilius, whose leader Vyacheslav Butusov, the director was familiar with. In mass"
            + " visits. After each debut in each subsequent film, Balabanov removed non-professional artists, "
            + "achieving the most natural and convincing incarnations of his characters through artistic"
            + " formations [4].Since 1990, Balabanov lived and worked in St. Petersburg. In 1992, together"
            + " with producer Sergey, he organized the STV film company.The first two feature films directed "
            + "by Happy Days and Castle"
            + "All-Russian fame brought Balabanov film 'Brother' (1997), telling the story of a young man "
            + "who became a killer. The film became one of the cult Russian films of the 1990s, the leading role "
            + "of Sergei Bodrov Jr. was called the hero of the generation. Due to the hostility experienced by "
            + "the protagonist of 'Brother', Balabanov was accused of nationalism and xenophobia."
            + "In 1998, Balabanov directed the film 'About Freaks and People' according to the script, which he "
            + "had written five years before. Despite the controversy of the subject (the film tells"
            + " about pornography producers in the Russian Empire), the picture was awarded the Nika "
            + "Prize for the best film." + "In 2000, Balabanov shot 'Brother 2' - the sequel to 'Brother', "
            + "which takes place in Moscow"
            + " and the United States. Accusations of politically incorrect events and the film 'War' (2002),"
            + " where the North Caucasus during the Second Chechen War became actions. Then Balabanov, who "
            + "changed the prevailing handwriting, made two 'light' films: the crime comedy 'Zhmurki'"
            + " and the melodrama 'It Doesn't Hurt Me'."
            + "Alexey Balabanov on the set of the film 'Morphine'"
            + "Alexei Balabanov was not only a colleague, but also a close friend of Sergei Bodrov, and he "
            + "was very worried about the tragedy that happened in 2002, because he was under the avalanche,"
            + " except for Bodrov, a whole team of relatives from his film crew died. After what happened, he"
            + " said that life was over. On that tragic day, when a group of Bodrov buried an avalanche of snow,"
            + " Balabanov was supposed to be on the set. Balabanov believed that he died along with a group of "
            + "Bodrov. The last five years of his life were especially difficult for him."
            + "In the film 'Cargo 200' (2007), the author was in a natural setting, due to the fact that the "
            + "Afghan wars were terrible and harsh. In many cities of Russia, the tape was withdrawn from hire."
            + "The film 'The Stoker' (2010) - about Yakut, a veteran of Afghanistan. The film tells about "
            + "the revenge of small people."
            + "In the last film 'I Want To' (2012), the director comprehends the problem of a person's "
            + "passing away from life. Balabanov appear in the frames of the film in a cameo role. This "
            + "was his last film. In total, Balabanov made 14 feature films, not including amateur and documentary.";

    public static final String BIOGRAPHY_SUKHORUKOV = "Viktor I. Sukhorukov (born 10 November 1951) is a Russian "
            + "actor. He has appeared in over 50 films and television shows since 1974. He starred in "
            + "Happy Days, which was screened in the Un Certain Regard section at the 1992 Cannes Film Festival.";

    public static final String BIOGRAPHY_PISMICHENKO = "Born on November 28, 1964 in the Kustanai region of"
            + " the Kazakh SSR. In 1986"
            + " she graduated from the Leningrad State Institute of Theater, Music and Cinematography (course of "
            + "I.P. Vladimirov). In the same year she joined the troupe of the Lensovet Theater. In 1993 she won"
            + " the television competition St. Petersburg Engagement. She works at the "
            + "Lensovet Theater, takes part in the performances of the Shelter of the Comedian and the Comedy "
            + "Academic Theater. She is married to two, she has two children. For the role in the film 'Brother'"
            + " Svetlana specially trained in driving a tram.";

    public static final String BIOGRAPHY_STOTSKIY = "Nikolai Stotsky was born on January 10, 1964 in Novosibirsk."
            + " In 1985, he graduated from the Theater School. B.V. Schukin (course T.K. Kopteva). "
            + "In 1985-1992 he worked as an actor in the Moscow Drama Theater. K. S. Stanislavsky [2] [3]. In "
            + "2002-2006, he was the host of the television game for high school students 'Pereputovye ostrov'"
            + " on the channel 'Culture'";

    public static final String BIOGRAPHY_SHEGALOV = "In 1991 he graduated from the operator department of VGIK, "
            + "since 1990 he worked at the Lenfilm studio. The first major work is The Castle (dir. Alexei Balabanov).";

    public static final String BIOGRAPHY_KURYOKHIN = "Soviet and Russian avant-garde musician, composer, screenwriter"
            + " and actor. In his youth, Kurkhin was a keyboard player in several Leningrad rock groups and played "
            + "in the jazz ensemble of Anatoly Vapirov, who published her first record abroad in 1981. In the 1980s,"
            + " he took part in the recording of several Aquarium albums. In 1984, the Pop Mechanics group made a "
            + "speech in which musicians, singers and artists of various schools and styles took part, as well as a "
            + "regular participant and ideological leader who was Kuryokhin himself. The theatrical concert"
            + " performances of Pop Mechanics included elements of performance, numbers, performances of circus"
            + " artists, fashion shows should be inserted in them. Kuryokhin is also the author of music for two"
            + " dozen films. Kurokhin was known as an artificial author of practical jokes and hoaxes. The most "
            + "famous was the performance in the TV program 'The Fifth Wheel', which gave rise to the media virus"
            + " 'Lenin - the mushroom.' In 1995, Kuryokhin joined the National Bolshevik Party.";

    public static final String DESCRIPTION_THE_CASTLE = "Closely based on Franz Kafka's book, the movie"
            + " shares the same action on a land surveyor who is called to a village to do a job that no one seems "
            + "to have ordered. Once there, he takes up the struggle against bureaucracy emanating from the castle.";

    public static final String NEWS_TEXT = "Today marks the twenty-fifth anniversary of the release of the film"
            + " 'Castle', directed by Alexei Balabanov. The film is freely available on our website.";
}
