package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.news.NewsFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NewsRepositoryCustom {
    Page<News> findByFilter(NewsFilter filter, Pageable pageable);
}
