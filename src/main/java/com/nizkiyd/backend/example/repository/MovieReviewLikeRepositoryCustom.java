package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieReviewLike;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MovieReviewLikeRepositoryCustom {

    Page<MovieReviewLike> findByFilter(MovieReviewLikeFilter filter, Pageable pageable);
}
