package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class NewsLikeRepositoryCustomImpl implements NewsLikeRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<NewsLike> findByFilter(NewsLikeFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select nl from NewsLike nl where 1=1");
        qb.append("and nl.news.id = :v", filter.getNewsId());
        qb.append("and nl.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and nl.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}

