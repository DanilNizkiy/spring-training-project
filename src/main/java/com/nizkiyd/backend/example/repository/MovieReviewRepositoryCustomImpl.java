package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MovieReviewRepositoryCustomImpl implements MovieReviewRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<MovieReview> findByFilter(MovieReviewFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select mr from MovieReview mr where 1=1");
        qb.append("and mr.movie.id = :v", filter.getMovieId());
        qb.append("and mr.moderator.id = :v", filter.getModeratorId());
        qb.append("and mr.reviewStatus = :v", filter.getReviewStatus());
        qb.append("and mr.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and mr.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
