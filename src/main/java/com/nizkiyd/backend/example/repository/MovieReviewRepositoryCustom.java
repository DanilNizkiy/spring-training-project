package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MovieReviewRepositoryCustom {

    Page<MovieReview> findByFilter(MovieReviewFilter filter, Pageable pageable);
}
