package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Complaint;
import com.nizkiyd.backend.example.domain.ComplaintType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ComplaintRepository extends CrudRepository<Complaint, UUID>, ComplaintRepositoryCustom {

    List<Complaint> findByRegisteredUserId(UUID registeredUserId);

    List<Complaint> findByComplaintObjectIdAndType(UUID complaintObjectId, ComplaintType type);
}
