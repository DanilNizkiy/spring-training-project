package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.CrewMember;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CrewMemberRepositoryCustom {
    Page<CrewMember> findByFilter(CrewMemberFilter filter, Pageable pageable);
}
