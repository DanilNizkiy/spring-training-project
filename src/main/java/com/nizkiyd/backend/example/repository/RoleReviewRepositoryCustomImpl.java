package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RoleReviewRepositoryCustomImpl implements RoleReviewRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<RoleReview> findByFilter(RoleReviewFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select rr from RoleReview rr where 1=1");
        qb.append("and rr.role.id = :v", filter.getRoleId());
        qb.append("and rr.moderator.id = :v", filter.getModeratorId());
        qb.append("and rr.reviewStatus = :v", filter.getReviewStatus());
        qb.append("and rr.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and rr.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}

