package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.dto.actor.ActorFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ActorRepositoryCustomImpl implements ActorRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Actor> findByFilter(ActorFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select a from Actor a");
        qb.appendJoin("join a.news n");
        qb.append("where 1=1");
        qb.append("and a.person.id = :v", filter.getPersonId());
        qb.append("and a.person.name = :v", filter.getName());
        qb.append("and a.averageMovieRating >= :v", filter.getAverageMovieRatingFrom());
        qb.append("and a.averageMovieRating < :v", filter.getAverageMovieRatingTo());
        qb.append("and a.averageRoleRating >= :v", filter.getAverageRoleRatingFrom());
        qb.append("and a.averageRoleRating < :v", filter.getAverageRoleRatingTo());
        qb.append("and n.id = :v", filter.getNewsId());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}