package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.CrewMember;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CrewMemberRepositoryCustomImpl implements CrewMemberRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<CrewMember> findByFilter(CrewMemberFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select cm from CrewMember cm");
        qb.appendJoin("join cm.news n");
        qb.append("where 1=1");
        qb.append("and cm.person.id = :v", filter.getPersonId());
        qb.append("and cm.person.name = :v", filter.getName());
        qb.append("and cm.crewMemberType = :v", filter.getCrewMemberType());
        qb.append("and n.id = :v", filter.getNewsId());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}