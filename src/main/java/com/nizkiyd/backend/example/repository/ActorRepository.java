package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface ActorRepository extends CrudRepository<Actor, UUID>, ActorRepositoryCustom {

    Actor findByPersonId(UUID personId);

    @Query("select a from Actor a join a.news n where n.id = :newsId")
    List<Actor> findByNewsId(@Param("newsId") UUID newsId);

    @Query("select a.id from Actor a")
    Stream<UUID> getIdsOfActors();
}
