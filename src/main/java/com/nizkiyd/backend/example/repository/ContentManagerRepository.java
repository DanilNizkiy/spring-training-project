package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.ContentManager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ContentManagerRepository extends CrudRepository<ContentManager, UUID> {

    ContentManager findByEmail(String email);

    List<ContentManager> findByStatus(AccountStatus status);

    boolean existsByIdAndEmail(UUID id, String email);
}
