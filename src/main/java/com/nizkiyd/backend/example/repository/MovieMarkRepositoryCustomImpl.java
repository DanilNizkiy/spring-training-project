package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieMark;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MovieMarkRepositoryCustomImpl implements MovieMarkRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<MovieMark> findByFilter(MovieMarkFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select mm from MovieMark mm where 1=1");
        qb.append("and mm.movie.id = :v", filter.getMovieId());
        qb.append("and mm.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and mm.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
