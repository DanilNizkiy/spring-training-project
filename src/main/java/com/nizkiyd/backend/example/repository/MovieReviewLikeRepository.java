package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieReviewLike;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MovieReviewLikeRepository extends CrudRepository<MovieReviewLike, UUID>,
        MovieReviewLikeRepositoryCustom {

    @Query("select mrl from MovieReviewLike mrl where mrl.registeredUser.id = :registeredUserId"
            + " and mrl.id = :movieReviewLikeId")
    Optional<MovieReviewLike> findRelatedEntities(@Param("registeredUserId") UUID registeredUserId,
                                                  @Param("movieReviewLikeId") UUID movieReviewLikeId);

    List<MovieReviewLike> findByRegisteredUserId(UUID registeredUserId);
}
