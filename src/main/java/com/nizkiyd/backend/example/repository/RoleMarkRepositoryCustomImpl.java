package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleMark;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RoleMarkRepositoryCustomImpl implements RoleMarkRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<RoleMark> findByFilter(RoleMarkFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select rm from RoleMark rm where 1=1");
        qb.append("and rm.role.id = :v", filter.getRoleId());
        qb.append("and rm.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and rm.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}