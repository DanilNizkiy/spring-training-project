package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface RoleRepository extends CrudRepository<Role, UUID>, RoleRepositoryCustom {

    List<Role> findByMovieId(UUID movieId);

    @Query("select r.id from Role r")
    Stream<UUID> getIdsOfRoles();

    @Query("select avg(r.averageMark) from Role r where r.actor.id = :actorId")
    Double calcAverageRoleRatingOfActor(@Param("actorId") UUID actorId);
}