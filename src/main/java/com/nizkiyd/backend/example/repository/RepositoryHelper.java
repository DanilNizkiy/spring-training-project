package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.UUID;

@Component
public class RepositoryHelper {

    @PersistenceContext
    private EntityManager entityManager;

    public <E> E getReferenceIfExists(Class<E> entityClass, UUID id) {
        validateExists(entityClass, id);
        return entityManager.getReference(entityClass, id);
    }

    public <E, N> N getEntityRequired(Class<E> entityInternalClass, UUID internalId,
                                      Class<N> entityClass, UUID entityId) {
        validateExists(entityInternalClass, internalId, entityClass, entityId);
        return entityManager.find(entityClass, entityId);
    }

    public <E> E getEntityRequired(Class<E> entityClass, UUID id) {
        E entity = entityManager.find(entityClass, id);
        if (entity == null) {
            throw new EntityNotFoundException(entityClass, id);
        }
        return entity;
    }

    private <E> void validateExists(Class<E> entityClass, UUID id) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, id);
        }
    }

    private <E, N> void validateExists(Class<E> entityInternalClass, UUID internalId,
                                       Class<N> entityClass, UUID entityId) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName()
                        + " e where e." + entityInternalClass.getSimpleName().substring(0, 1).toLowerCase()
                        + entityInternalClass.getSimpleName().substring(1)
                        + ".id = :internalId and e.id = :entityId");

        query.setParameter("internalId", internalId);
        query.setParameter("entityId", entityId);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityInternalClass, internalId, entityClass, entityId);
        }
    }

    public void save(Object entityClass) {
        entityManager.persist(entityClass);
    }


    public EntityManager getEntityManager() {
        return entityManager;
    }
}
