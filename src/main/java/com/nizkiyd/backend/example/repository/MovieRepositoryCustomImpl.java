package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.dto.movie.MovieFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MovieRepositoryCustomImpl implements MovieRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Movie> findByFilter(MovieFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select m from Movie m");
        qb.appendJoin("join m.news n");
        qb.append("where 1=1");
        qb.append("and m.averageMark >= :v", filter.getAverageMarkFrom());
        qb.append("and m.averageMark < :v", filter.getAverageMarkTo());
        qb.append("and m.movieStatus = :v", filter.getMovieStatus());
        qb.append("and m.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and m.createdAt < :v", filter.getCreatedAtTo());
        qb.append("and m.title = :v", filter.getTitle());
        qb.append("and n.id = :v", filter.getNewsId());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}