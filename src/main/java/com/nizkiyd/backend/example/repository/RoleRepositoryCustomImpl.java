package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.role.RoleFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RoleRepositoryCustomImpl implements RoleRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Role> findByFilter(RoleFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select r from Role r where 1=1");
        qb.append("and r.actor.id = :v", filter.getActorId());
        qb.append("and r.movie.id = :v", filter.getMovieId());
        qb.append("and r.averageMark >= :v", filter.getAverageMarkFrom());
        qb.append("and r.averageMark < :v", filter.getAverageMarkTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}