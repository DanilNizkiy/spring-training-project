package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleReviewRepositoryCustom {

    Page<RoleReview> findByFilter(RoleReviewFilter filter, Pageable pageable);
}
