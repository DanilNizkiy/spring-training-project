package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.ExternalSystemImport;
import com.nizkiyd.backend.example.domain.ImportedEntityType;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ExternalSystemImportRepository extends CrudRepository<ExternalSystemImport, UUID> {
    ExternalSystemImport findByIdInExternalSystemAndEntityType(String idInExternalSystem,
                                                               ImportedEntityType entityType);

}
