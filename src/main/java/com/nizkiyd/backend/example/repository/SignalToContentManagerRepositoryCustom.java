package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SignalToContentManagerRepositoryCustom {

    Page<SignalToContentManager> findByFilter(SignalToContentManagerFilter filter, Pageable pageable);
}
