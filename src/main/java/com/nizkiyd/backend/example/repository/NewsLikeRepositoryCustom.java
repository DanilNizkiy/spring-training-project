package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NewsLikeRepositoryCustom {
    Page<NewsLike> findByFilter(NewsLikeFilter filter, Pageable pageable);
}
