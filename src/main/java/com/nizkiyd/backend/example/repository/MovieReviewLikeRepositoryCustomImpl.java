package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieReviewLike;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MovieReviewLikeRepositoryCustomImpl implements MovieReviewLikeRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<MovieReviewLike> findByFilter(MovieReviewLikeFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select mrl from MovieReviewLike mrl where 1=1");
        qb.append("and mrl.movieReview.id = :v", filter.getMovieReviewId());
        qb.append("and mrl.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and mrl.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
