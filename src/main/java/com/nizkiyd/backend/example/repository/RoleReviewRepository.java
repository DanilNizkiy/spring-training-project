package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleReview;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleReviewRepository extends CrudRepository<RoleReview, UUID>, RoleReviewRepositoryCustom {

    @Query("select mr from RoleReview mr where mr.registeredUser.id = :registeredUserId and mr.id = :roleReviewId")
    Optional<RoleReview> findRelatedEntities(@Param("registeredUserId") UUID registeredUserId,
                                             @Param("roleReviewId") UUID roleReviewId);

    List<RoleReview> findByRegisteredUserId(UUID registeredUserId);
}
