package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.domain.ReviewStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MovieReviewRepository extends CrudRepository<MovieReview, UUID>, MovieReviewRepositoryCustom {

    @Query("select mr from MovieReview mr where mr.registeredUser.id = :registeredUserId and mr.id = :movieReviewId")
    Optional<MovieReview> findRelatedEntities(@Param("registeredUserId") UUID registeredUserId,
                                              @Param("movieReviewId") UUID movieReviewId);

    List<MovieReview> findByRegisteredUserId(UUID registeredUserId);

    @Query("select mr from MovieReview mr where mr.movie.id = :movieId and mr.reviewStatus = :reviewStatus1 or "
            + "mr.reviewStatus = :reviewStatus2")
    List<MovieReview> findByMovieIdAndReviewStatus(@Param("movieId") UUID movieId,
                                                   @Param("reviewStatus1") ReviewStatus reviewStatus1,
                                                   @Param("reviewStatus2") ReviewStatus reviewStatus2);
}