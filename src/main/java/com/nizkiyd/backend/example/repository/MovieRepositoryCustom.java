package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.dto.movie.MovieFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MovieRepositoryCustom {
    Page<Movie> findByFilter(MovieFilter filter, Pageable pageable);
}
