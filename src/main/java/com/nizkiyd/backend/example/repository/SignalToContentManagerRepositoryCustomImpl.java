package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class SignalToContentManagerRepositoryCustomImpl implements SignalToContentManagerRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<SignalToContentManager> findByFilter(SignalToContentManagerFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select stcm from SignalToContentManager stcm where 1=1");
        qb.append("and stcm.contentManager.id = :v", filter.getContentManagerId());
        qb.append("and stcm.signalToContentManagerStatus = :v", filter.getStatus());
        qb.append("and stcm.signalToContentManagerObjectId = :v", filter.getSignalToContentManagerObjectId());
        qb.append("and stcm.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and stcm.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
