package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.NewsLike;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface NewsLikeRepository extends CrudRepository<NewsLike, UUID>, NewsLikeRepositoryCustom {

    List<NewsLike> findByRegisteredUserId(UUID registeredUserId);
}
