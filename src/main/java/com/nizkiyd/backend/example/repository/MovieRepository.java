package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.MovieStatus;
import com.nizkiyd.backend.example.dto.movie.MovieInLeaderBoardReadDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface MovieRepository extends CrudRepository<Movie, UUID>, MovieRepositoryCustom {

    List<Movie> findByMovieStatus(MovieStatus movieStatus);

    @Query("select m.id from Movie m")
    Stream<UUID> getIdsOfMovies();

    @Query("select new com.nizkiyd.backend.example.dto.movie.MovieInLeaderBoardReadDTO(m.id, m.title, m.averageMark,"
            + "(select count(mark) from Mark mark where mark.movie.id = m.id and mark.mark is not null))"
            + " from Movie m order by m.averageMark desc")
    List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard();

    Movie findByTitle(String title);

    @Query("select avg(m.averageMark) from Movie m join m.roles r where r.actor.id = :actorId")
    Double calcAverageMovieRatingOfActor(@Param("actorId") UUID actorId);

    @Query("select m from Movie m join m.news n where n.id = :newsId")
    List<Movie> findByNewsId(@Param("newsId") UUID newsId);
}
