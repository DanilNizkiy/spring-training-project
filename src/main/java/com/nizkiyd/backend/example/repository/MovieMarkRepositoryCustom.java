package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieMark;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MovieMarkRepositoryCustom {
    Page<MovieMark> findByFilter(MovieMarkFilter filter, Pageable pageable);
}
