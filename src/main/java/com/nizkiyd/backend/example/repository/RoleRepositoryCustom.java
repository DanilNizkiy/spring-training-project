package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.role.RoleFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleRepositoryCustom {
    Page<Role> findByFilter(RoleFilter filter, Pageable pageable);
}
