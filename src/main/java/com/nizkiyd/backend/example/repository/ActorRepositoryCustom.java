package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.dto.actor.ActorFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ActorRepositoryCustom {
    Page<Actor> findByFilter(ActorFilter filter, Pageable pageable);
}
