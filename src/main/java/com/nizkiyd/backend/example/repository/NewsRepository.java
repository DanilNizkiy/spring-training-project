package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.News;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsRepository extends CrudRepository<News, UUID>, NewsRepositoryCustom {
}

