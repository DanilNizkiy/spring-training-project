package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleReviewLike;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RoleReviewLikeRepositoryCustomImpl implements RoleReviewLikeRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<RoleReviewLike> findByFilter(RoleReviewLikeFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select rrl from RoleReviewLike rrl where 1=1");
        qb.append("and rrl.roleReview.id = :v", filter.getRoleReviewId());
        qb.append("and rrl.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and rrl.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
