package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.CrewMember;
import com.nizkiyd.backend.example.domain.CrewMemberType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CrewMemberRepository extends CrudRepository<CrewMember, UUID>, CrewMemberRepositoryCustom {

    @Query("select cm from CrewMember cm join cm.movies m where m.id = :movieId")
    List<CrewMember> findByMovieId(@Param("movieId") UUID movieId);

    @Query("select cm from CrewMember cm join cm.news n where n.id = :newsId")
    List<CrewMember> findByNewsId(@Param("newsId") UUID newsId);

    @Query("select cm from CrewMember cm where cm.person.id = :personId and cm.crewMemberType = :crewMemberType")
    CrewMember findByPersonIdAndType(@Param("personId") UUID personId,
                                     @Param("crewMemberType") CrewMemberType crewMemberType);

    CrewMember findByPersonId(UUID personId);
}
