package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AdminRepository extends CrudRepository<Admin, UUID> {

    Admin findByEmail(String email);

    boolean existsByIdAndEmail(UUID id, String email);
}
