package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.news.NewsFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class NewsRepositoryCustomImpl implements NewsRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<News> findByFilter(NewsFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select n from News n");
        qb.appendJoin("join n.actors a");
        qb.appendJoin("join n.movies m");
        qb.appendJoin("join n.crewMembers cm");
        qb.append("where 1=1");
        qb.append("and a.id = :v", filter.getActorId());
        qb.append("and m.id = :v", filter.getMovieId());
        qb.append("and cm.id = :v", filter.getCrewMemberId());
        qb.append("and n.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and n.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
