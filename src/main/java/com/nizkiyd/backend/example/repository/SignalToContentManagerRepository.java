package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface SignalToContentManagerRepository extends CrudRepository<SignalToContentManager, UUID>,
        SignalToContentManagerRepositoryCustom {

    List<SignalToContentManager> findByRegisteredUserId(UUID registeredUserId);

    @Query("select stcm from SignalToContentManager stcm where stcm.signalToContentManagerStatus = :status"
            + " and stcm.signalToContentManagerObjectId = :newsId"
            + " and stcm.wrongText like concat('%',:wrongText,'%')")
    List<SignalToContentManager> findByParameters(@Param("newsId") UUID newsId,
                                                  @Param("wrongText") String wrongText,
                                                  @Param("status") SignalToContentManagerStatus status);
}
