package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.MovieMark;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MovieMarkRepository extends CrudRepository<MovieMark, UUID>, MovieMarkRepositoryCustom {

    @Query("select mm from MovieMark mm where mm.registeredUser.id = :registeredUserId and mm.id = :movieMarkId")
    Optional<MovieMark> findRelatedEntities(@Param("registeredUserId") UUID registeredUserId,
                                            @Param("movieMarkId") UUID movieMarkId);

    List<MovieMark> findByRegisteredUserId(UUID registeredUserId);

    @Query("select avg(m.mark) from Mark m where m.movie.id = :movieId")
    Double calcAverageMovieMarkOfMovie(@Param("movieId") UUID movieId);
}
