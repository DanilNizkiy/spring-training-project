package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleReviewLike;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleReviewLikeRepositoryCustom {

    Page<RoleReviewLike> findByFilter(RoleReviewLikeFilter filter, Pageable pageable);
}
