package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleMark;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleMarkRepository extends CrudRepository<RoleMark, UUID>, RoleMarkRepositoryCustom {

    @Query("select rm from RoleMark rm where rm.registeredUser.id = :registeredUserId and rm.id = :roleMarkId")
    Optional<RoleMark> findRelatedEntities(@Param("registeredUserId") UUID registeredUserId,
                                           @Param("roleMarkId") UUID roleMarkId);

    List<RoleMark> findByRegisteredUserId(UUID registeredUserId);

    @Query("select avg(m.mark) from Mark m where m.role.id = :roleId")
    Double calcAverageRoleMarkOfRole(@Param("roleId") UUID roleId);
}