package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleMark;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleMarkRepositoryCustom {
    Page<RoleMark> findByFilter(RoleMarkFilter filter, Pageable pageable);
}
