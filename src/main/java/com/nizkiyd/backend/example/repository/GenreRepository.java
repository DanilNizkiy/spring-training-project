package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Genre;
import com.nizkiyd.backend.example.domain.GenreType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GenreRepository extends CrudRepository<Genre, UUID> {

    @Query("select g from Genre g where g.type = :type")
    Genre findByType(@Param("type") GenreType type);

    @Query("select g from Genre g join g.movies m where m.id = :movieId")
    List<Genre> findByMovieId(@Param("movieId") UUID movieId);
}
