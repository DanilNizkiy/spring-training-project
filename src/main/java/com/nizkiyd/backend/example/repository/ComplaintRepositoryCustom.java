package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Complaint;
import com.nizkiyd.backend.example.dto.complaint.ComplaintFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ComplaintRepositoryCustom {
    Page<Complaint> findByFilter(ComplaintFilter filter, Pageable pageable);
}
