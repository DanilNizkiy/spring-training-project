package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Complaint;
import com.nizkiyd.backend.example.dto.complaint.ComplaintFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ComplaintRepositoryCustomImpl implements ComplaintRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Complaint> findByFilter(ComplaintFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select c from Complaint c where 1=1");
        qb.append("and c.moderator.id = :v", filter.getModeratorId());
        qb.append("and c.type = :v", filter.getType());
        qb.append("and c.complaintStatus = :v", filter.getComplaintStatus());
        qb.append("and c.complaintObjectId = :v", filter.getComplaintObjectId());
        qb.append("and c.createdAt >= :v", filter.getCreatedAtFrom());
        qb.append("and c.createdAt < :v", filter.getCreatedAtTo());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}

