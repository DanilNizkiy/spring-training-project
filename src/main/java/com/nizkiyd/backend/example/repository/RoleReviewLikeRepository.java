package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.RoleReviewLike;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleReviewLikeRepository extends CrudRepository<RoleReviewLike, UUID>,
        RoleReviewLikeRepositoryCustom {

    @Query("select rrl from RoleReviewLike rrl where rrl.registeredUser.id = :registeredUserId and "
            + "rrl.id = :roleReviewLikeId")
    Optional<RoleReviewLike> findRelatedEntities(@Param("registeredUserId") UUID registeredUserId,
                                                 @Param("roleReviewLikeId") UUID roleReviewLikeId);

    List<RoleReviewLike> findByRegisteredUserId(UUID registeredUserId);
}
