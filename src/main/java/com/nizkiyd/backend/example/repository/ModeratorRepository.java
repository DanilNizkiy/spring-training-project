package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.Moderator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ModeratorRepository extends CrudRepository<Moderator, UUID> {

    Moderator findByEmail(String email);

    List<Moderator> findByStatus(AccountStatus status);

    boolean existsByIdAndEmail(UUID id, String email);
}
