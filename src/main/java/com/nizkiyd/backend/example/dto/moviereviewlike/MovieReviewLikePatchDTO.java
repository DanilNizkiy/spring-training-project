package com.nizkiyd.backend.example.dto.moviereviewlike;

import lombok.Data;

@Data
public class MovieReviewLikePatchDTO {

    private Boolean like;
}
