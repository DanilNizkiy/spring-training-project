package com.nizkiyd.backend.example.dto.complaint;

import com.nizkiyd.backend.example.domain.*;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ComplaintReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String text;

    private ComplaintType type;

    private ComplaintObjectType complaintObjectType;

    private UUID complaintObjectId;

    private UUID registeredUserId;

    private UUID moderatorId;

    private ComplaintStatus complaintStatus;

    private Instant processedAt;
}
