package com.nizkiyd.backend.example.dto.moderator;

import lombok.Data;

@Data
public class ModeratorPatchDTO {

    private String name;
}
