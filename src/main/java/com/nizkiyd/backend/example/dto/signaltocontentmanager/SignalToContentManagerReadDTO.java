package com.nizkiyd.backend.example.dto.signaltocontentmanager;

import com.nizkiyd.backend.example.domain.SignalToContentManagerObjectType;
import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class SignalToContentManagerReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Instant fixedAt;

    private String replacement;

    private Integer fromIndex;

    private Integer toIndex;

    private SignalToContentManagerObjectType signalToContentManagerObjectType;

    private SignalToContentManagerStatus signalToContentManagerStatus;

    private UUID signalToContentManagerObjectId;

    private UUID contentManagerId;

    private UUID registeredUserId;

    private String wrongText;
}
