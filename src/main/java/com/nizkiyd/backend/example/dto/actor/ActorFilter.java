package com.nizkiyd.backend.example.dto.actor;

import lombok.Data;

import java.util.UUID;

@Data
public class ActorFilter {

    private String name;

    private UUID personId;

    private Double averageMovieRatingFrom;

    private Double averageMovieRatingTo;

    private Double averageRoleRatingFrom;

    private Double averageRoleRatingTo;

    private UUID newsId;
}
