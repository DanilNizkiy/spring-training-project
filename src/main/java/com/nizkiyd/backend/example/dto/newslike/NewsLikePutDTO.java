package com.nizkiyd.backend.example.dto.newslike;

import lombok.Data;

@Data
public class NewsLikePutDTO {

    private Boolean like;
}
