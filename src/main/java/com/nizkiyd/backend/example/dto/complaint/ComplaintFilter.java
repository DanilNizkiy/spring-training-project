package com.nizkiyd.backend.example.dto.complaint;

import com.nizkiyd.backend.example.domain.ComplaintStatus;
import com.nizkiyd.backend.example.domain.ComplaintType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ComplaintFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private ComplaintType type;

    private UUID complaintObjectId;

    private UUID moderatorId;

    private ComplaintStatus complaintStatus;
}
