package com.nizkiyd.backend.example.dto.moviereview;

import com.nizkiyd.backend.example.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieReviewReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String text;

    private UUID registeredUserId;

    private UUID movieId;

    private UUID moderatorId;

    private ReviewStatus reviewStatus;

    private int like;

    private int dislike;
}
