package com.nizkiyd.backend.example.dto.role;

import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String roleName;

    private UUID movieId;

    private UUID actorId;

    private Double averageMark;

    private Gender gender;
}
