package com.nizkiyd.backend.example.dto.admin;

import lombok.Data;

@Data
public class AdminPatchDTO {

    private String name;
}
