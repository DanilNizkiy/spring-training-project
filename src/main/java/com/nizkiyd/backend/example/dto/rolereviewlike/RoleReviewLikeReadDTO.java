package com.nizkiyd.backend.example.dto.rolereviewlike;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReviewLikeReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Boolean like;

    private UUID registeredUserId;

    private UUID roleReviewId;
}
