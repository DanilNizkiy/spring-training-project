package com.nizkiyd.backend.example.dto.crewmember;

import com.nizkiyd.backend.example.domain.CrewMemberType;
import lombok.Data;

import java.util.UUID;

@Data
public class CrewMemberReadDTO {

    private UUID id;

    private CrewMemberType crewMemberType;

    private UUID personId;
}
