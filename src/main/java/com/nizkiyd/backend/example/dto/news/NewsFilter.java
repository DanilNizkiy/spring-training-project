package com.nizkiyd.backend.example.dto.news;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class NewsFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID actorId;

    private UUID movieId;

    private UUID crewMemberId;
}
