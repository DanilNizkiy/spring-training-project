package com.nizkiyd.backend.example.dto.newslike;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class NewsLikeReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Boolean like;

    private UUID registeredUserId;

    private UUID newsId;
}
