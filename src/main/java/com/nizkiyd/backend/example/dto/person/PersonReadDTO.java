package com.nizkiyd.backend.example.dto.person;

import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class PersonReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String name;

    private LocalDate birthday;

    private String placeOfBirth;

    private String awards;

    private String biography;

    private Boolean adult;

    private Gender gender;
}
