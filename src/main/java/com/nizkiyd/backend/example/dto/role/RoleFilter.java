package com.nizkiyd.backend.example.dto.role;

import lombok.Data;

import java.util.UUID;

@Data
public class RoleFilter {

    private UUID actorId;

    private UUID movieId;

    private Double averageMarkFrom;

    private Double averageMarkTo;
}
