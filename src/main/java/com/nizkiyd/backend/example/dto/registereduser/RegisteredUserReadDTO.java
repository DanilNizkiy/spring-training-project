package com.nizkiyd.backend.example.dto.registereduser;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RegisteredUserReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String email;

    private String name;

    private Integer age;

    private Gender gender;

    private Boolean trust;

    private Double ratingReviews;

    private Double ratingActivity;

    private String favouriteGenre;

    private AccountStatus status;
}
