package com.nizkiyd.backend.example.dto.contentmanager;

import lombok.Data;

@Data
public class ContentManagerPatchDTO {

    private String name;
}
