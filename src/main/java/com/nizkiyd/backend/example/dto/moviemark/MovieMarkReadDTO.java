package com.nizkiyd.backend.example.dto.moviemark;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieMarkReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Integer mark;

    private UUID registeredUserId;

    private UUID movieId;
}
