package com.nizkiyd.backend.example.dto.crewmember;

import com.nizkiyd.backend.example.domain.CrewMemberType;
import lombok.Data;

import java.util.UUID;

@Data
public class CrewMemberFilter {

    private String name;

    private CrewMemberType crewMemberType;

    private UUID personId;

    private UUID newsId;
}
