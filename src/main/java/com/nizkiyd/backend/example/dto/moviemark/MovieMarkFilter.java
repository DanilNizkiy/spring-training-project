package com.nizkiyd.backend.example.dto.moviemark;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieMarkFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID movieId;
}
