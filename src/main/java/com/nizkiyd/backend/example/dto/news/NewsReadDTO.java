package com.nizkiyd.backend.example.dto.news;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class NewsReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String text;

    private int like;

    private int dislike;
}
