package com.nizkiyd.backend.example.dto.rolereviewlike;

import lombok.Data;

@Data
public class RoleReviewLikePatchDTO {

    private Boolean like;
}
