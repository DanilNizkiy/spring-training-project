package com.nizkiyd.backend.example.dto.moviereview;

import com.nizkiyd.backend.example.domain.ReviewStatus;
import lombok.Data;

@Data
public class MovieReviewPatchDTO {

    private String text;

    private ReviewStatus reviewStatus;
}
