package com.nizkiyd.backend.example.dto.moviereviewlike;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class MovieReviewLikeCreateDTO {

    @NotNull
    private Boolean like;

    @NotNull
    private UUID movieReviewId;
}
