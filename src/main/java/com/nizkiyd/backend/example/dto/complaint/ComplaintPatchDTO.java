package com.nizkiyd.backend.example.dto.complaint;

import com.nizkiyd.backend.example.domain.ComplaintObjectType;
import com.nizkiyd.backend.example.domain.ComplaintType;
import lombok.Data;

import java.util.UUID;

@Data
public class ComplaintPatchDTO {

    private String text;

    private ComplaintType type;

    private ComplaintObjectType complaintObjectType;

    private UUID complaintObjectId;

    private UUID moderatorId;
}
