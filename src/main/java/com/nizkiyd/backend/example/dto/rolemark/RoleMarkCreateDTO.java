package com.nizkiyd.backend.example.dto.rolemark;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RoleMarkCreateDTO {

    @NotNull
    private Integer mark;

    @NotNull
    private UUID roleId;
}
