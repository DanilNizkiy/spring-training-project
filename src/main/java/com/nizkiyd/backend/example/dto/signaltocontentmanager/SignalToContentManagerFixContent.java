package com.nizkiyd.backend.example.dto.signaltocontentmanager;

import lombok.Data;

import java.util.UUID;

@Data
public class SignalToContentManagerFixContent {

    private UUID contentManagerId;

    private String replacement;
}
