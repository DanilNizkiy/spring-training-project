package com.nizkiyd.backend.example.dto.rolereviewlike;

import lombok.Data;

@Data
public class RoleReviewLikePutDTO {

    private Boolean like;
}
