package com.nizkiyd.backend.example.dto.moderator;

import com.nizkiyd.backend.example.domain.AccountStatus;
import lombok.Data;

@Data
public class ModeratorStatusDTO {

    AccountStatus status;
}
