package com.nizkiyd.backend.example.dto.admin;

import com.nizkiyd.backend.example.domain.AccountStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class AdminReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String name;

    private String email;

    private AccountStatus status;
}
