package com.nizkiyd.backend.example.dto.contentmanager;

import com.nizkiyd.backend.example.domain.AccountStatus;
import lombok.Data;

@Data
public class ContentManagerStatusDTO {

    AccountStatus status;
}
