package com.nizkiyd.backend.example.dto.moderator;

import com.nizkiyd.backend.example.domain.AccountStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ModeratorReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String name;

    private String email;

    private AccountStatus status;
}
