package com.nizkiyd.backend.example.dto.rolemark;

import lombok.Data;

@Data
public class RoleMarkPatchDTO {

    private Integer mark;
}
