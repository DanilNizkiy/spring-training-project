package com.nizkiyd.backend.example.dto.complaint;

import com.nizkiyd.backend.example.domain.ComplaintObjectType;
import com.nizkiyd.backend.example.domain.ComplaintType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ComplaintCreateDTO {

    private String text;

    @NotNull
    private ComplaintType type;

    @NotNull
    private ComplaintObjectType complaintObjectType;

    @NotNull
    private UUID complaintObjectId;
}
