package com.nizkiyd.backend.example.dto.role;

import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RoleCreateDTO {

    @NotNull
    private String roleName;

    private UUID actorId;

    private Gender gender;
}
