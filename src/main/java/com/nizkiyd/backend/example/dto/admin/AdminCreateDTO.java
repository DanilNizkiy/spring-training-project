package com.nizkiyd.backend.example.dto.admin;

import lombok.Data;

@Data
public class AdminCreateDTO {

    private String name;

    private String email;

    private String password;
}
