package com.nizkiyd.backend.example.dto.registereduser;

import com.nizkiyd.backend.example.domain.AccountStatus;
import lombok.Data;

@Data
public class RegisteredUserAccountStatusPatchDTO {

    AccountStatus status;
}
