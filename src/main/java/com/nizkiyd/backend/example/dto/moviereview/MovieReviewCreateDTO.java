package com.nizkiyd.backend.example.dto.moviereview;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class MovieReviewCreateDTO {

    @NotNull
    private String text;

    @NotNull
    private UUID movieId;

    private UUID moderatorId;
}
