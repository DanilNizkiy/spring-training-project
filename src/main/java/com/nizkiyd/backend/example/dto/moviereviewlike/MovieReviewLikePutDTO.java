package com.nizkiyd.backend.example.dto.moviereviewlike;

import lombok.Data;

@Data
public class MovieReviewLikePutDTO {

    private Boolean like;
}
