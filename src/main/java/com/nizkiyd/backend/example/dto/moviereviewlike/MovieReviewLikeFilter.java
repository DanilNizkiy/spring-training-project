package com.nizkiyd.backend.example.dto.moviereviewlike;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieReviewLikeFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID movieReviewId;
}
