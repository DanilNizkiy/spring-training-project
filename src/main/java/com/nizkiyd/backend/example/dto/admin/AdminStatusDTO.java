package com.nizkiyd.backend.example.dto.admin;

import com.nizkiyd.backend.example.domain.AccountStatus;
import lombok.Data;

@Data
public class AdminStatusDTO {

    AccountStatus status;
}
