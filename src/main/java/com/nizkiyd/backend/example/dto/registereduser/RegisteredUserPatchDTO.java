package com.nizkiyd.backend.example.dto.registereduser;

import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

@Data
public class RegisteredUserPatchDTO {

    private String name;

    private Integer age;

    private Gender gender;
}
