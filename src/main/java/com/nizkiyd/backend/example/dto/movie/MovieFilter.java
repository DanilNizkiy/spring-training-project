package com.nizkiyd.backend.example.dto.movie;

import com.nizkiyd.backend.example.domain.MovieStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private MovieStatus movieStatus;

    private Double averageMarkFrom;

    private Double averageMarkTo;

    private String title;

    private UUID newsId;
}
