package com.nizkiyd.backend.example.dto.genre;

import com.nizkiyd.backend.example.domain.GenreType;
import lombok.Data;

@Data
public class GenreTypeDTO {

    private GenreType type;
}
