package com.nizkiyd.backend.example.dto.moviemark;

import lombok.Data;

@Data
public class MovieMarkPatchDTO {

    private Integer mark;
}
