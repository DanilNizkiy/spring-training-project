package com.nizkiyd.backend.example.dto.movie;

import com.nizkiyd.backend.example.domain.MovieStatus;
import lombok.Data;

import java.time.LocalDate;

@Data
public class MoviePatchDTO {

    private String title;

    private LocalDate releaseDate;

    private String country;

    private String duration;

    private MovieStatus movieStatus;

    private Double averageMark;

    private Integer budget;

}
