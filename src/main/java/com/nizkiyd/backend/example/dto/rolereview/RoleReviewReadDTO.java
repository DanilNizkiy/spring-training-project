package com.nizkiyd.backend.example.dto.rolereview;

import com.nizkiyd.backend.example.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReviewReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String text;

    private UUID registeredUserId;

    private UUID roleId;

    private UUID moderatorId;

    private ReviewStatus reviewStatus;

    private int like;

    private int dislike;
}
