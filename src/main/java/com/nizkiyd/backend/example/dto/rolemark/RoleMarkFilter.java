package com.nizkiyd.backend.example.dto.rolemark;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleMarkFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID roleId;
}
