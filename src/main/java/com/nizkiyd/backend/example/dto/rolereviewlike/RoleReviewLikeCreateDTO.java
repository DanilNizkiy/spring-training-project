package com.nizkiyd.backend.example.dto.rolereviewlike;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RoleReviewLikeCreateDTO {

    @NotNull
    private Boolean like;

    @NotNull
    private UUID roleReviewId;
}
