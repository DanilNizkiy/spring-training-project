package com.nizkiyd.backend.example.dto.moviereview;

import com.nizkiyd.backend.example.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MovieReviewFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID movieId;

    private UUID moderatorId;

    private ReviewStatus reviewStatus;
}
