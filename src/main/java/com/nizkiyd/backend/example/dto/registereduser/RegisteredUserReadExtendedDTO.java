package com.nizkiyd.backend.example.dto.registereduser;

import com.nizkiyd.backend.example.domain.Gender;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class RegisteredUserReadExtendedDTO {

    private UUID id;

    private String name;

    private Double ratingReviews;

    private Double ratingActivity;

    private String favouriteGenre;

    private Integer age;

    private Boolean trust;

    private List<NewsLikeReadDTO> listNewsLikeDislikeRead;

    private List<MovieReviewLikeReadDTO> listReviewMovieLikeDislikeRead;

    private List<RoleReviewLikeReadDTO> listReviewRoleLikeDislikeRead;

    private Gender gender;
}
