package com.nizkiyd.backend.example.dto.rolereview;

import com.nizkiyd.backend.example.domain.ReviewStatus;
import lombok.Data;

@Data
public class RoleReviewPutDTO {

    private String text;

    private ReviewStatus reviewStatus;
}
