package com.nizkiyd.backend.example.dto.rolemark;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleMarkReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private Integer mark;

    private UUID registeredUserId;

    private UUID roleId;
}
