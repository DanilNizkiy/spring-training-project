package com.nizkiyd.backend.example.dto.rolereview;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RoleReviewCreateDTO {

    @NotNull
    private String text;

    @NotNull
    private UUID roleId;

    private UUID moderatorId;
}
