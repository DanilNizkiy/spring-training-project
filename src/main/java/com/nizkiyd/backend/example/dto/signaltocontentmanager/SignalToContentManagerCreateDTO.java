package com.nizkiyd.backend.example.dto.signaltocontentmanager;

import com.nizkiyd.backend.example.domain.SignalToContentManagerObjectType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class SignalToContentManagerCreateDTO {

    private String replacement;

    @NotNull
    private Integer fromIndex;

    @NotNull
    private Integer toIndex;

    @NotNull
    private SignalToContentManagerObjectType signalToContentManagerObjectType;

    @NotNull
    private UUID signalToContentManagerObjectId;

    @NotNull
    private String wrongText;

    private UUID contentManagerId;
}
