package com.nizkiyd.backend.example.dto.newslike;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class NewsLikeCreateDTO {

    @NotNull
    private Boolean like;

    @NotNull
    private UUID newsId;
}
