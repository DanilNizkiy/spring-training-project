package com.nizkiyd.backend.example.dto.role;

import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

import java.util.UUID;

@Data
public class RolePatchDTO {

    private String roleName;

    private UUID actorId;

    private Gender gender;
}
