package com.nizkiyd.backend.example.dto.moviemark;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class MovieMarkCreateDTO {

    @NotNull
    private Integer mark;

    @NotNull
    private UUID movieId;
}
