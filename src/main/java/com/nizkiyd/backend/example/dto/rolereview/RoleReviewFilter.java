package com.nizkiyd.backend.example.dto.rolereview;

import com.nizkiyd.backend.example.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReviewFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID roleId;

    private UUID moderatorId;

    private ReviewStatus reviewStatus;
}
