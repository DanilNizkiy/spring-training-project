package com.nizkiyd.backend.example.dto.contentmanager;

import lombok.Data;

@Data
public class ContentManagerCreateDTO {

    private String name;

    private String email;

    private String password;
}
