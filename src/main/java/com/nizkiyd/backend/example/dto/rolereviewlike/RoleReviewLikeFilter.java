package com.nizkiyd.backend.example.dto.rolereviewlike;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReviewLikeFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID roleReviewId;
}
