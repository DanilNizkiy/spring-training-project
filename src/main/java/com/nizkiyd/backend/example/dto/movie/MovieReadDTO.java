package com.nizkiyd.backend.example.dto.movie;

import com.nizkiyd.backend.example.domain.MovieStatus;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class MovieReadDTO {

    private UUID id;

    private Instant createdAt;

    private Instant updatedAt;

    private String title;

    private LocalDate releaseDate;

    private String country;

    private String duration;

    private MovieStatus movieStatus;

    private Double averageMark;

    private Integer budget;

    private String description;

    private Boolean adult;

    private String originalLanguage;

    private Integer revenue;
}
