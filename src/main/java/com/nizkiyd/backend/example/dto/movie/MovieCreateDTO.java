package com.nizkiyd.backend.example.dto.movie;

import com.nizkiyd.backend.example.domain.MovieStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class MovieCreateDTO {

    @NotNull
    private String title;

    private LocalDate releaseDate;

    private String country;

    private String duration;

    private MovieStatus movieStatus;

    private Integer budget;

    private String description;

    private Boolean adult;

    private String originalLanguage;

    private Integer revenue;
}
