package com.nizkiyd.backend.example.dto.signaltocontentmanager;

import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class SignalToContentManagerFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID signalToContentManagerObjectId;

    private UUID contentManagerId;

    private SignalToContentManagerStatus status;
}
