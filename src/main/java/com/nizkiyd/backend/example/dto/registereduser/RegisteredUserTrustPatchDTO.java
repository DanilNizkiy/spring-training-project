package com.nizkiyd.backend.example.dto.registereduser;

import lombok.Data;

@Data
public class RegisteredUserTrustPatchDTO {

    private Boolean trust;
}
