package com.nizkiyd.backend.example.dto.moderator;

import lombok.Data;

@Data
public class ModeratorCreateDTO {

    private String name;

    private String email;

    private String password;
}
