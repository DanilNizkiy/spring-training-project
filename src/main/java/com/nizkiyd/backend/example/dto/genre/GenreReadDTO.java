package com.nizkiyd.backend.example.dto.genre;

import com.nizkiyd.backend.example.domain.GenreType;
import lombok.Data;

import java.util.UUID;

@Data
public class GenreReadDTO {

    private UUID id;

    private GenreType type;
}
