package com.nizkiyd.backend.example.dto.actor;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ActorReadDTO {

    private UUID id;

    private UUID personId;

    private Instant createdAt;

    private Instant updatedAt;

    private Double averageMovieRating;

    private Double averageRoleRating;
}
