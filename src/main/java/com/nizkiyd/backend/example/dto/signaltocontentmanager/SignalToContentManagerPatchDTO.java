package com.nizkiyd.backend.example.dto.signaltocontentmanager;

import com.nizkiyd.backend.example.domain.SignalToContentManagerObjectType;
import lombok.Data;

import java.util.UUID;

@Data
public class SignalToContentManagerPatchDTO {

    private String replacement;

    private Integer fromIndex;

    private Integer toIndex;

    private SignalToContentManagerObjectType signalToContentManagerObjectType;

    private UUID signalToContentManagerObjectId;

    private UUID contentManagerId;
}
