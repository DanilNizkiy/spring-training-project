package com.nizkiyd.backend.example.dto.news;

import lombok.Data;

@Data
public class NewsPutDTO {

    private String text;
}
