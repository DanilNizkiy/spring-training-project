package com.nizkiyd.backend.example.dto.person;

import com.nizkiyd.backend.example.domain.Gender;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class PersonPutDTO {

    @NotNull
    private String name;

    private LocalDate birthday;

    private String placeOfBirth;

    private String awards;

    private String biography;

    private Boolean adult;

    @NotNull
    private Gender gender;
}
