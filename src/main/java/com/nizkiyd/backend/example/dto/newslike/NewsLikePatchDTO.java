package com.nizkiyd.backend.example.dto.newslike;

import lombok.Data;

@Data
public class NewsLikePatchDTO {

    private Boolean like;
}
