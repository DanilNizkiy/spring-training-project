package com.nizkiyd.backend.example.dto.actor;

import lombok.Data;

import java.util.UUID;

@Data
public class ActorPatchDTO {

    private UUID personId;
}
