package com.nizkiyd.backend.example.dto.movie;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class MovieInLeaderBoardReadDTO {

    private UUID id;

    private String title;

    private Double averageMark;

    private long movieMarkCount;
}
