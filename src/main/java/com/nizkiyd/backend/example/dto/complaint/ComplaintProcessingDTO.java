package com.nizkiyd.backend.example.dto.complaint;

import com.nizkiyd.backend.example.domain.ComplaintStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class ComplaintProcessingDTO {

    private UUID moderatorId;

    private ComplaintStatus newStatus;
}
