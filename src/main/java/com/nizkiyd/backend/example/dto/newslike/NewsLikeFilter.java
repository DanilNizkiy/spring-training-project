package com.nizkiyd.backend.example.dto.newslike;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class NewsLikeFilter {

    private Instant createdAtFrom;

    private Instant createdAtTo;

    private UUID newsId;
}
