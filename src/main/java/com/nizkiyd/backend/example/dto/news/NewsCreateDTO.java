package com.nizkiyd.backend.example.dto.news;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class NewsCreateDTO {

    @NotNull
    private String text;
}
