package com.nizkiyd.backend.example.security;

import com.nizkiyd.backend.example.domain.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;
import java.util.UUID;

@Getter
@Setter
public class UserDetailsImpl extends User {

    private UUID id;

    public <T extends Account> UserDetailsImpl(T entity) {
        super(entity.getEmail(), entity.getEncodedPassword(), Collections.singleton(
                new SimpleGrantedAuthority(entity.getClass().getSimpleName())));
        id = entity.getId();
    }
}