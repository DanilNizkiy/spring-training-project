package com.nizkiyd.backend.example.security;

import com.nizkiyd.backend.example.domain.Admin;
import com.nizkiyd.backend.example.domain.ContentManager;
import com.nizkiyd.backend.example.domain.Moderator;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        RegisteredUser registeredUser = registeredUserRepository.findByEmail(username);
        if (registeredUser != null) {
            return new UserDetailsImpl(registeredUser);
        }
        Admin admin = adminRepository.findByEmail(username);
        if (admin != null) {
            return new UserDetailsImpl(admin);
        }
        ContentManager contentManager = contentManagerRepository.findByEmail(username);
        if (contentManager != null) {
            return new UserDetailsImpl(contentManager);
        }
        Moderator moderator = moderatorRepository.findByEmail(username);
        if (moderator != null) {
            return new UserDetailsImpl(moderator);
        }

        throw new UsernameNotFoundException("Account " + username + " is not found!");
    }
}
