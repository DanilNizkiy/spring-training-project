package com.nizkiyd.backend.example.domain;

public enum MovieStatus {
    RELEASED("Released"),
    POST_PRODUCTION("Post Production"),
    CANCELED("Canceled"),
    IN_PRODUCTION("In Production"),
    RUMORED("Rumored"),
    PLANNED("Planned");

    private String text;

    MovieStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public static MovieStatus fromString(String text) {
        for (MovieStatus b : MovieStatus.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
