package com.nizkiyd.backend.example.domain;

public enum ComplaintObjectType {
    MOVIE_REVIEW,
    ROLE_REVIEW
}
