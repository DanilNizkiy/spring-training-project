package com.nizkiyd.backend.example.domain;

public enum CrewMemberType {
    AUTHOR("Author"),
    DIRECTOR("Director"),
    PRODUCER("Producer"),
    EXECUTIVE_PRODUCER("Executive Producer"),
    STORY("Story"),
    WRITER("Writer"),
    CAMERA("Camera"),
    MUSIC("Music");

    private String text;

    CrewMemberType(String text) {
        this.text = text;
    }

    public static CrewMemberType fromString(String text) {
        for (CrewMemberType b : CrewMemberType.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
