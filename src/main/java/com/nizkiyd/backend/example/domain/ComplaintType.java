package com.nizkiyd.backend.example.domain;

public enum ComplaintType {
    SPOILER,
    SPAM,
    SWEARING,
    OTHER
}
