package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.Instant;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Role extends AbstractEntity {

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @NotNull
    private String roleName;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Movie movie;

    @ManyToOne
    private Actor actor;

    private Double averageMark;

    @Enumerated(EnumType.STRING)
    private Gender gender;
}
