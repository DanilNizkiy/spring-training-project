package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
public class CrewMember extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private CrewMemberType crewMemberType;

    @ManyToOne
    private Person person;

    @ManyToMany(mappedBy = "crewMembers")
    private List<Movie> movies = new ArrayList<>();

    @ManyToMany(mappedBy = "crewMembers")
    private List<News> news = new ArrayList<>();
}
