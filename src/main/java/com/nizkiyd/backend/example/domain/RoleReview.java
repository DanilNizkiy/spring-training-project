package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@DiscriminatorValue("RR")
public class RoleReview extends Review {

    @NotNull
    @ManyToOne
    private Role role;
}
