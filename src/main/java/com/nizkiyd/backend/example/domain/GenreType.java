package com.nizkiyd.backend.example.domain;

public enum GenreType {
    WESTERN("Western"),
    DRAMA("Drama"),
    THRILLER("Thriller"),
    SCIENCE_FICTION("Science Fiction"),
    ACTION("Action"),
    ADVENTURE("Adventure"),
    ANIMATION("Animation"),
    COMEDY("Comedy"),
    CRIME("Crime"),
    DOCUMENTARY("Documentary"),
    FAMILY("Family"),
    FANTASY("Fantasy"),
    HORROR("Horror"),
    HISTORY("Mystery"),
    MUSIC("Music"),
    MYSTERY("Mystery"),
    ROMANCE("Romance"),
    TV_MOVIE("TV Movie"),
    WAR("War");

    private String text;

    GenreType(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public static GenreType fromString(String text) {
        for (GenreType b : GenreType.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
