package com.nizkiyd.backend.example.domain;

public enum SignalToContentManagerObjectType {
    NEWS,
    PERSON,
    MOVIE
}
