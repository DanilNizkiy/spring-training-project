package com.nizkiyd.backend.example.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class Account extends AbstractEntity {

    private String email;

    private String encodedPassword;

    @Enumerated(EnumType.STRING)
    private AccountStatus status;

    private String name;
}
