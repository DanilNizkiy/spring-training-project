package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Complaint extends AbstractEntity {

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    private String text;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ComplaintType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ComplaintObjectType complaintObjectType;

    @NotNull
    private UUID complaintObjectId;

    @ManyToOne
    private Moderator moderator;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;

    @Enumerated(EnumType.STRING)
    private ComplaintStatus complaintStatus;

    private Instant processedAt;
}
