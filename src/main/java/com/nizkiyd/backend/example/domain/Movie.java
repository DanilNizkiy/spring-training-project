package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Movie extends AbstractEntity {

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @NotNull
    private String title;

    private LocalDate releaseDate;

    private String country;

    @ManyToMany
    @JoinTable(name = "movie_genre",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> genres = new ArrayList<>();

    @ManyToMany(mappedBy = "movies")
    private List<News> news = new ArrayList<>();

    private String duration;

    @Enumerated(EnumType.STRING)
    private MovieStatus movieStatus;

    private Double averageMark;

    @OneToMany(mappedBy = "movie")
    private List<Role> roles = new ArrayList<>();

    private Integer budget;

    private String description;

    @ManyToMany
    @JoinTable(name = "movie_crew_member",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "crew_member_id"))
    private List<CrewMember> crewMembers = new ArrayList<>();

    private Boolean adult;

    private String originalLanguage;

    private Integer revenue;
}
