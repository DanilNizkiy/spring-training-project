package com.nizkiyd.backend.example.domain;

public enum ImportedEntityType {
    MOVIE,
    PERSON
}
