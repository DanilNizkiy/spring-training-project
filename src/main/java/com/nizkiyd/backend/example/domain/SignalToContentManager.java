package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
public class SignalToContentManager extends AbstractEntity {

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    private Instant fixedAt;

    private String replacement;

    @NotNull
    private String wrongText;

    @NotNull
    private Integer fromIndex;

    @NotNull
    private Integer toIndex;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SignalToContentManagerObjectType signalToContentManagerObjectType;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SignalToContentManagerStatus signalToContentManagerStatus;

    @NotNull
    private UUID signalToContentManagerObjectId;

    @ManyToOne
    private ContentManager contentManager;
}
