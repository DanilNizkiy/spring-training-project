package com.nizkiyd.backend.example.domain;

public enum ComplaintStatus {
    MODERATION,
    APPROVED,
    CANCELED
}
