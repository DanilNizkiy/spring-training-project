package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@DiscriminatorValue("MM")
public class MovieMark extends Mark {

    @NotNull
    @ManyToOne
    private Movie movie;
}
