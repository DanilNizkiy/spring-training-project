package com.nizkiyd.backend.example.domain;

public enum AccountStatus {
    VALID,
    INVALID,
    BLOCKED
}
