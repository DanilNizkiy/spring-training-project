package com.nizkiyd.backend.example.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
public class RegisteredUser extends Account {

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @Min(value = 0, message = "Age should not be less than 18")
    @Max(value = 100, message = "Age should not be greater than 150")
    @NotNull
    private Integer age;

    private Boolean trust;

    private Double ratingReviews;

    private Double ratingActivity;

    private String favouriteGenre;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToMany(mappedBy = "registeredUser")
    private List<NewsLike> newsLikes;

    @OneToMany(mappedBy = "registeredUser")
    private List<MovieReviewLike> movieReviewLikes;

    @OneToMany(mappedBy = "registeredUser")
    private List<RoleReviewLike> roleReviewLikes;

    @OneToMany(mappedBy = "registeredUser")
    private List<MovieMark> movieMarks;

    @OneToMany(mappedBy = "registeredUser")
    private List<RoleMark> roleMarks;

    @OneToMany(mappedBy = "registeredUser")
    private List<RoleReview> roleReviews;

    @OneToMany(mappedBy = "registeredUser")
    private List<MovieReview> movieReviews;

    @OneToMany(mappedBy = "registeredUser")
    private List<Complaint> complaints;

    @OneToMany(mappedBy = "registeredUser")
    private List<SignalToContentManager> signalToContentManagers;
}
