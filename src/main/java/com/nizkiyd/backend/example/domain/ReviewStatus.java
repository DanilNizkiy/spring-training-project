package com.nizkiyd.backend.example.domain;

public enum ReviewStatus {
    MODERATION,
    APPROVED,
    APPROVED_WITH_FIX,
    CANCELED
}
