package com.nizkiyd.backend.example.domain;

public enum SignalToContentManagerStatus {
    NEED_TO_FIX,
    CANCELED,
    FIXED
}
