package com.nizkiyd.backend.example.domain;

public enum Gender {
    MALE(2),
    FEMALE(1);

    private Integer number;

    Gender(Integer number) {
        this.number = number;
    }

    public static Gender fromInteger(Integer number) {
        if (number == 0) {
            return null;
        }

        for (Gender g : Gender.values()) {
            if (g.number.equals(number)) {
                return g;
            }
        }
        return null;
    }
}
