package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.ContentManager;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.List;

public class ContentManagerRepositoryTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Test
    public void testFindByStatus() {
        ContentManager cm1 = testObjectsFactory.createContentManager(AccountStatus.INVALID);
        testObjectsFactory.createContentManager(AccountStatus.VALID);
        testObjectsFactory.createContentManager(AccountStatus.BLOCKED);

        List<ContentManager> invalidContentManagers = contentManagerRepository.findByStatus(AccountStatus.INVALID);

        Assert.assertEquals(invalidContentManagers.size(), 1);
        Assert.assertEquals(invalidContentManagers.get(0).getId(), cm1.getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        ContentManager contentManager = testObjectsFactory.createContentManager();

        Instant createdAtBeforeReload = contentManager.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        contentManager = contentManagerRepository.findById(contentManager.getId()).get();

        Instant createdAtAfterReload = contentManager.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        ContentManager contentManager = testObjectsFactory.createContentManager();

        Instant updatedAtBeforeUpdate = contentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        contentManager.setName("qwe123");
        contentManager = contentManagerRepository.save(contentManager);
        contentManager = contentManagerRepository.findById(contentManager.getId()).get();

        Instant updatedAtAfterUpdate = contentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }
}
