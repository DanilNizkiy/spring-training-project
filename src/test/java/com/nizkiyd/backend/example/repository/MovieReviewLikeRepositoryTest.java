package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class MovieReviewLikeRepositoryTest extends BaseTest {

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        Instant createdAtBeforeReload = movieReviewLike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        movieReviewLike = movieReviewLikeRepository.findById(movieReviewLike.getId()).get();

        Instant createdAtAfterReload = movieReviewLike.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        Instant updatedAtBeforeUpdate = movieReviewLike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        movieReviewLike.setLike(true);
        movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);
        movieReviewLike = movieReviewLikeRepository.findById(movieReviewLike.getId()).get();

        Instant updatedAtAfterUpdate = movieReviewLike.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveMovieReviewLikeValidation() {
        MovieReviewLike movieReviewLike = new MovieReviewLike();
        movieReviewLikeRepository.save(movieReviewLike);
    }
}
