package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;

public class ComplaintRepositoryTest extends BaseTest {

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        Instant createdAtBeforeReload = complaint.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        complaint = complaintRepository.findById(complaint.getId()).get();

        Instant createdAtAfterReload = complaint.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        Instant updatedAtBeforeUpdate = complaint.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        complaint.setText("qwe12113");
        complaint = complaintRepository.save(complaint);
        complaint = complaintRepository.findById(complaint.getId()).get();

        Instant updatedAtAfterUpdate = complaint.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveComplaintValidation() {
        Complaint complaint = new Complaint();
        complaintRepository.save(complaint);
    }
}

