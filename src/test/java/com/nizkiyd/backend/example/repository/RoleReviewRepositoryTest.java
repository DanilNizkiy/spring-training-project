package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class RoleReviewRepositoryTest extends BaseTest {

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        Instant createdAtBeforeReload = roleReview.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        roleReview = roleReviewRepository.findById(roleReview.getId()).get();

        Instant createdAtAfterReload = roleReview.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        Instant updatedAtBeforeUpdate = roleReview.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        roleReview.setText("dsdqqs");
        roleReview = roleReviewRepository.save(roleReview);
        roleReview = roleReviewRepository.findById(roleReview.getId()).get();

        Instant updatedAtAfterUpdate = roleReview.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRoleReviewValidation() {
        RoleReview roleReview = new RoleReview();
        roleReviewRepository.save(roleReview);
    }
}
