package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Gender;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class RegisteredUserRepositoryTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test(expected = TransactionSystemException.class)
    public void testSaveRegisteredUserValidation() {
        RegisteredUser registeredUser = new RegisteredUser();
        registeredUserRepository.save(registeredUser);
    }

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        Instant createdAtBeforeReload = registeredUser.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        registeredUser = registeredUserRepository.findById(registeredUser.getId()).get();

        Instant createdAtAfterReload = registeredUser.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        Instant updatedAtBeforeUpdate = registeredUser.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        registeredUser.setName("qwe123");
        registeredUser = registeredUserRepository.save(registeredUser);
        registeredUser = registeredUserRepository.findById(registeredUser.getId()).get();

        Instant updatedAtAfterUpdate = registeredUser.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test
    public void testIntegerToEnumGender() {
        Gender genderFemale = Gender.fromInteger(1);
        Gender genderMale = Gender.fromInteger(2);
        Gender genderNull = Gender.fromInteger(0);

        Assert.assertEquals(genderFemale, Gender.FEMALE);
        Assert.assertEquals(genderMale, Gender.MALE);
        Assert.assertNull(genderNull);
    }
}
