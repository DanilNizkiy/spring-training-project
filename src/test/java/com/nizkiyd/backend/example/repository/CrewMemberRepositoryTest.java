package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.CrewMember;
import com.nizkiyd.backend.example.domain.CrewMemberType;
import com.nizkiyd.backend.example.domain.Person;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CrewMemberRepositoryTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private CrewMemberRepository crewMemberRepository;


    @Test
    public void testFindByPersonId() {
        Person person = testObjectsFactory.cretePerson();

        CrewMember crewMember = new CrewMember();
        crewMember.setPerson(person);
        crewMember = crewMemberRepository.save(crewMember);

        CrewMember foundCrewMember = crewMemberRepository.findByPersonId(person.getId());

        Assert.assertEquals(foundCrewMember.getPerson().getId(), person.getId());
    }

    @Test
    public void testStringToEnumCrewMemberType() {
        CrewMemberType crewMemberTypeDirector = CrewMemberType.fromString("Director");
        CrewMemberType crewMemberTypeProducer = CrewMemberType.fromString("Producer");
        CrewMemberType wrongCrewMemberType = CrewMemberType.fromString("qwe");

        Assert.assertEquals(crewMemberTypeDirector, CrewMemberType.DIRECTOR);
        Assert.assertEquals(crewMemberTypeProducer, CrewMemberType.PRODUCER);
        Assert.assertNull(wrongCrewMemberType);
    }
}
