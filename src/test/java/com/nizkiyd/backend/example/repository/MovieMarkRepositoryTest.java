package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class MovieMarkRepositoryTest extends BaseTest {

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test(expected = TransactionSystemException.class)
    public void testSaveMovieMarkValidation() {
        MovieMark movieMark = new MovieMark();
        movieMarkRepository.save(movieMark);
    }

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        Instant createdAtBeforeReload = movieMark.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        movieMark = movieMarkRepository.findById(movieMark.getId()).get();

        Instant createdAtAfterReload = movieMark.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        Instant updatedAtBeforeUpdate = movieMark.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        movieMark.setMark(7);
        movieMark = movieMarkRepository.save(movieMark);
        movieMark = movieMarkRepository.findById(movieMark.getId()).get();

        Instant updatedAtAfterUpdate = movieMark.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test
    public void testCalcAverageMovieMark() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        testObjectsFactory.createMovieMark(ru, m1, 5);
        testObjectsFactory.createMovieMark(ru, m1, 10);
        testObjectsFactory.createMovieMark(ru, m2, 7);

        Assert.assertEquals(7.5, movieMarkRepository.calcAverageMovieMarkOfMovie(m1.getId()), Double.MIN_NORMAL);
    }
}
