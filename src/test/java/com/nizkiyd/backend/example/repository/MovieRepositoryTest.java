package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.movie.MovieInLeaderBoardReadDTO;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MovieRepositoryTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSave() {
        Movie m = testObjectsFactory.createMovie();
        assertNotNull(m.getId());
        assertTrue(movieRepository.findById(m.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() {
        Movie movie = testObjectsFactory.createMovie();

        Instant createdAtBeforeReload = movie.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        movie = movieRepository.findById(movie.getId()).get();

        Instant createdAtAfterReload = movie.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Movie movie = testObjectsFactory.createMovie();

        Instant updatedAtBeforeUpdate = movie.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        movie.setTitle("qwe123");
        movie = movieRepository.save(movie);
        movie = movieRepository.findById(movie.getId()).get();

        Instant updatedAtAfterUpdate = movie.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test
    public void testGetIdsOfMovies() {
        Set<UUID> expectedIdsOfMovies = new HashSet<>();
        expectedIdsOfMovies.add(testObjectsFactory.createMovie().getId());
        expectedIdsOfMovies.add(testObjectsFactory.createMovie().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfMovies, movieRepository.getIdsOfMovies().collect(Collectors.toSet()));
        });
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveMovieValidation() {
        Movie movie = new Movie();
        movieRepository.save(movie);
    }

    @Test
    public void testGetMoviesLeaderBoard() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();

        int moviesCount = 100;
        Set<UUID> movieIds = new HashSet<>();
        for (int i = 0; i < moviesCount; i++) {
            Movie m = testObjectsFactory.createMovie();
            movieIds.add(m.getId());

            testObjectsFactory.createMovieMark(ru, m, true);
            testObjectsFactory.createMovieMark(ru, m, true);
            testObjectsFactory.createMovieMark(ru, m, false);
        }

        List<MovieInLeaderBoardReadDTO> moviesLeaderBoard = movieRepository.getMoviesLeaderBoard();
        Assertions.assertThat(moviesLeaderBoard).isSortedAccordingTo(
                Comparator.comparing(MovieInLeaderBoardReadDTO::getAverageMark).reversed());

        Assert.assertEquals(movieIds, moviesLeaderBoard.stream().map(MovieInLeaderBoardReadDTO::getId)
                .collect(Collectors.toSet()));

        for (MovieInLeaderBoardReadDTO m : moviesLeaderBoard) {
            Assert.assertNotNull(m.getTitle());
            Assert.assertNotNull(m.getAverageMark());
            Assert.assertEquals(2, m.getMovieMarkCount());
        }
    }

    @Test
    public void testCalcAverageMovieRatingOfActor() {
        Movie m1 = testObjectsFactory.createMovie(10.0);
        Movie m2 = testObjectsFactory.createMovie(5.0);

        Actor a1 = testObjectsFactory.createActor();

        testObjectsFactory.createRole(m1, a1);
        testObjectsFactory.createRole(m2, a1);

        Double averageMark = movieRepository.calcAverageMovieRatingOfActor(a1.getId());
        Assert.assertEquals(Double.valueOf(7.5), averageMark);
    }
}