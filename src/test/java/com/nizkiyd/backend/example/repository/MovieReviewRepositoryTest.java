package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class MovieReviewRepositoryTest extends BaseTest {

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        Instant createdAtBeforeReload = movieReview.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        movieReview = movieReviewRepository.findById(movieReview.getId()).get();

        Instant createdAtAfterReload = movieReview.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        Instant updatedAtBeforeUpdate = movieReview.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        movieReview.setText("dsdqqs");
        movieReview = movieReviewRepository.save(movieReview);
        movieReview = movieReviewRepository.findById(movieReview.getId()).get();

        Instant updatedAtAfterUpdate = movieReview.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveMovieReviewValidation() {
        MovieReview movieReview = new MovieReview();
        movieReviewRepository.save(movieReview);
    }
}
