package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class RoleRepositoryTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetIdsOfRoles() {
        Set<UUID> expectedIdsOfRoles = new HashSet<>();
        expectedIdsOfRoles.add(testObjectsFactory.createRole().getId());
        expectedIdsOfRoles.add(testObjectsFactory.createRole().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfRoles, roleRepository.getIdsOfRoles().collect(Collectors.toSet()));
        });
    }

    @Test
    public void testCreatedAtIsSet() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);

        Instant createdAtBeforeReload = role.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        role = roleRepository.findById(role.getId()).get();

        Instant createdAtAfterReload = role.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);

        Instant updatedAtBeforeUpdate = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        role.setRoleName("qwe123");
        role = roleRepository.save(role);
        role = roleRepository.findById(role.getId()).get();

        Instant updatedAtAfterUpdate = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRoleValidation() {
        Role role = new Role();
        roleRepository.save(role);
    }

    @Test
    public void testCalcAverageRoleRatingOfActor() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        testObjectsFactory.createRole(movie, a1, 10.0);
        testObjectsFactory.createRole(movie, a2, 2.0);
        testObjectsFactory.createRole(movie, a1, 5.0);

        Double averageMark = roleRepository.calcAverageRoleRatingOfActor(a1.getId());
        Assert.assertEquals(Double.valueOf(7.5), averageMark);
    }
}