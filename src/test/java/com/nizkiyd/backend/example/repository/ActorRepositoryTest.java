package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.domain.Person;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class ActorRepositoryTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testFindByPersonId() {
        Person person = testObjectsFactory.cretePerson();

        Actor actor = new Actor();
        actor.setPerson(person);
        actor = actorRepository.save(actor);

        Actor foundActor = actorRepository.findByPersonId(person.getId());

        Assert.assertEquals(foundActor.getPerson().getId(), person.getId());
    }

    @Test
    public void testGetIdsOfActors() {
        Set<UUID> expectedIdsOfActors = new HashSet<>();
        expectedIdsOfActors.add(testObjectsFactory.createActor().getId());
        expectedIdsOfActors.add(testObjectsFactory.createActor().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfActors, actorRepository.getIdsOfActors().collect(Collectors.toSet()));
        });
    }
}
