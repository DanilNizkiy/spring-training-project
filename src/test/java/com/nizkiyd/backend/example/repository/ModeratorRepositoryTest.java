package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.Moderator;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.List;

public class ModeratorRepositoryTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Test
    public void testFindByStatus() {
        Moderator m1 = testObjectsFactory.createModerator(AccountStatus.INVALID);
        testObjectsFactory.createModerator(AccountStatus.VALID);
        testObjectsFactory.createModerator(AccountStatus.BLOCKED);

        List<Moderator> invalidModerators = moderatorRepository.findByStatus(AccountStatus.INVALID);

        Assert.assertEquals(invalidModerators.size(), 1);
        Assert.assertEquals(invalidModerators.get(0).getId(), m1.getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        Moderator moderator = testObjectsFactory.createModerator();

        Instant createdAtBeforeReload = moderator.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        moderator = moderatorRepository.findById(moderator.getId()).get();

        Instant createdAtAfterReload = moderator.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Moderator moderator = testObjectsFactory.createModerator();

        Instant updatedAtBeforeUpdate = moderator.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        moderator.setName("qwe123");
        moderator = moderatorRepository.save(moderator);
        moderator = moderatorRepository.findById(moderator.getId()).get();

        Instant updatedAtAfterUpdate = moderator.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }
}
