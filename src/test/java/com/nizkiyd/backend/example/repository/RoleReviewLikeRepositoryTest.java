package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class RoleReviewLikeRepositoryTest extends BaseTest {

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        Instant createdAtBeforeReload = roleReviewLike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        roleReviewLike = roleReviewLikeRepository.findById(roleReviewLike.getId()).get();

        Instant createdAtAfterReload = roleReviewLike.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        Instant updatedAtBeforeUpdate = roleReviewLike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        roleReviewLike.setLike(false);
        roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);
        roleReviewLike = roleReviewLikeRepository.findById(roleReviewLike.getId()).get();

        Instant updatedAtAfterUpdate = roleReviewLike.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRoleReviewLikeValidation() {
        RoleReviewLike roleReviewLike = new RoleReviewLike();
        roleReviewLikeRepository.save(roleReviewLike);
    }
}
