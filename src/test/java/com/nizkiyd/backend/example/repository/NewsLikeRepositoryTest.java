package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class NewsLikeRepositoryTest extends BaseTest {

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        Instant createdAtBeforeReload = newsLike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        newsLike = newsLikeRepository.findById(newsLike.getId()).get();

        Instant createdAtAfterReload = newsLike.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        Instant updatedAtBeforeUpdate = newsLike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        newsLike.setLike(true);
        newsLike = newsLikeRepository.save(newsLike);
        newsLike = newsLikeRepository.findById(newsLike.getId()).get();

        Instant updatedAtAfterUpdate = newsLike.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveNewsLikeValidation() {
        NewsLike newsLike = new NewsLike();
        newsLikeRepository.save(newsLike);
    }
}
