package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class RoleMarkRepositoryTest extends BaseTest {

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        Instant createdAtBeforeReload = roleMark.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        roleMark = roleMarkRepository.findById(roleMark.getId()).get();

        Instant createdAtAfterReload = roleMark.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        Instant updatedAtBeforeUpdate = roleMark.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        roleMark.setMark(8);
        roleMark = roleMarkRepository.save(roleMark);
        roleMark = roleMarkRepository.findById(roleMark.getId()).get();

        Instant updatedAtAfterUpdate = roleMark.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test
    public void testCalcAverageRoleMark() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        testObjectsFactory.createRoleMark(ru, r1, 5);
        testObjectsFactory.createRoleMark(ru, r1, 10);
        testObjectsFactory.createRoleMark(ru, r2, 7);

        Assert.assertEquals(7.5, roleMarkRepository.calcAverageRoleMarkOfRole(r1.getId()), Double.MIN_NORMAL);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRoleMarkValidation() {
        RoleMark roleMark = new RoleMark();
        roleMarkRepository.save(roleMark);
    }
}
