package com.nizkiyd.backend.example.repository;


import com.nizkiyd.backend.example.service.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
public class LiquibaseLoadDataTest extends BaseTest {

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void testDataLoaded() {
        Assert.assertTrue(movieRepository.count() > 0);
        Assert.assertTrue(registeredUserRepository.count() > 0);
        Assert.assertTrue(roleRepository.count() > 0);
        Assert.assertTrue(newsRepository.count() > 0);
        Assert.assertTrue(newsRepository.count() > 0);
        Assert.assertTrue(complaintRepository.count() > 0);
    }
}
