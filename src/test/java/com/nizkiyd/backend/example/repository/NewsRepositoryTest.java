package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;

public class NewsRepositoryTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {
        Actor actor = testObjectsFactory.createActor();
        Movie movie = testObjectsFactory.createMovie();
        News news = testObjectsFactory.createNews(actor, movie);

        Instant createdAtBeforeReload = news.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        news = newsRepository.findById(news.getId()).get();

        Instant createdAtAfterReload = news.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Actor actor = testObjectsFactory.createActor();
        Movie movie = testObjectsFactory.createMovie();
        News news = testObjectsFactory.createNews(actor, movie);

        Instant updatedAtBeforeUpdate = news.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        news.setText("qwe12113");
        news = newsRepository.save(news);
        news = newsRepository.findById(news.getId()).get();

        Instant updatedAtAfterUpdate = news.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveNewsValidation() {
        News news = new News();
        newsRepository.save(news);
    }
}
