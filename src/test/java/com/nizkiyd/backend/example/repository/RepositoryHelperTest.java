package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.hibernate.LazyInitializationException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class RepositoryHelperTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Test
    public void testGetReferenceIfExistExactlyReference() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RegisteredUser registeredUserReference = repositoryHelper.getReferenceIfExists(
                RegisteredUser.class, registeredUser.getId());
        Assert.assertEquals(registeredUser.getId(), registeredUserReference.getId());
        Assert.assertNotNull(registeredUser.getName());
        assertThatThrownBy(registeredUserReference::getName).isInstanceOf(LazyInitializationException.class);
    }
}
