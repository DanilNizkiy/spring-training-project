package com.nizkiyd.backend.example.repository;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

public class SignalToContentManagerRepositoryTest extends BaseTest {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, movie.getId(), contentManager);

        Instant createdAtBeforeReload = signalToContentManager.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        signalToContentManager = signalToContentManagerRepository.findById(signalToContentManager.getId()).get();

        Instant createdAtAfterReload = signalToContentManager.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, movie.getId(), contentManager);

        Instant updatedAtBeforeUpdate = signalToContentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        signalToContentManager.setReplacement("dsdqqs");
        signalToContentManager = signalToContentManagerRepository.save(signalToContentManager);
        signalToContentManager = signalToContentManagerRepository.findById(signalToContentManager.getId()).get();

        Instant updatedAtAfterUpdate = signalToContentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveSignalToContentManagerValidation() {
        SignalToContentManager signal = new SignalToContentManager();
        signalToContentManagerRepository.save(signal);
    }
}