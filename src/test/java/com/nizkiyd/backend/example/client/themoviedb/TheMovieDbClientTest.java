package com.nizkiyd.backend.example.client.themoviedb;

import com.nizkiyd.backend.example.client.themoviedb.dto.*;
import com.nizkiyd.backend.example.repository.ActorRepository;
import com.nizkiyd.backend.example.repository.CrewMemberRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RoleRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.service.importer.MovieImporterService;
import com.nizkiyd.backend.example.service.importer.PersonImporterService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;

public class TheMovieDbClientTest extends BaseTest {

    @Autowired
    private TheMovieDbClient theMovieDbClient;

    @Autowired
    private PersonImporterService personImporterService;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetMovieGenre() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, null);

        Assertions.assertThat(movie.getGenres())
                .extracting(GenreReadDTO::getName).isEqualTo(Arrays.asList("Action", "Thriller", "Science Fiction"));
    }

    @Test
    public void testGetMovieRu() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, "ru");
        System.out.println("qweqwe" + movie.getReleaseDate());

        Assert.assertEquals(movieId, movie.getId());
        Assert.assertEquals("Terminator 2: Judgment Day", movie.getOriginalTitle());
        Assert.assertEquals("Терминатор 2: Судный день", movie.getTitle());
    }

    @Test
    public void testGetMovieDefaultLanguage() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, null);
        Assert.assertEquals(movieId, movie.getId());
        Assert.assertEquals("Terminator 2: Judgment Day", movie.getOriginalTitle());
        Assert.assertEquals(movie.getOriginalTitle(), movie.getTitle());
    }

    @Test
    public void testGetTopRatedMovies() {
        MoviesPageDTO moviesPage = theMovieDbClient.getTopRatedMovies();
        Assert.assertTrue(moviesPage.getTotalPages() > 0);
        Assert.assertTrue(moviesPage.getTotalResults() > 0);
        Assert.assertTrue(moviesPage.getResults().size() > 0);
        for (MovieReadShortDTO read : moviesPage.getResults()) {
            Assert.assertNotNull(read.getId());
            Assert.assertNotNull(read.getTitle());
        }
    }
}