package com.nizkiyd.backend.example.job.oneoff;

import com.nizkiyd.backend.example.client.themoviedb.TheMovieDbClient;
import com.nizkiyd.backend.example.client.themoviedb.dto.MovieReadShortDTO;
import com.nizkiyd.backend.example.client.themoviedb.dto.MoviesPageDTO;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.exception.ImportedEntityAlreadyExistException;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.service.importer.MovieImporterService;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.Assert.*;

public class TheMovieDbImportOneOffJobTest extends BaseTest {

    @Autowired
    private TheMovieDbImportOneOffJob job;

    @MockBean
    private TheMovieDbClient client;

    @MockBean
    private MovieImporterService movieImporterService;

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    @Test
    public void testDoImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWith2Result();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    @Test
    public void testDoImportNoExceptionIfGetPageFailed() {
        Mockito.when(client.getTopRatedMovies()).thenThrow(RuntimeException.class);

        job.doImport();

        Mockito.verifyNoInteractions(movieImporterService);
    }

    @Test
    public void testDoImportFirstFailedAndSecondSuccess()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        MoviesPageDTO page = generatePageWith2Result();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);
        Mockito.when(movieImporterService.importMovie(page.getResults().get(0).getId())).thenThrow(
                RuntimeException.class);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    private MoviesPageDTO generatePageWith2Result() {
        MoviesPageDTO page = generator.generateRandomObject(MoviesPageDTO.class);
        page.getResults().add(generator.generateRandomObject(MovieReadShortDTO.class));
        Assert.assertEquals(2, page.getResults().size());

        return page;
    }
}