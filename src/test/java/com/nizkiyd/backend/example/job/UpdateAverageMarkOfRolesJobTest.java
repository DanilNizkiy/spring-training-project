package com.nizkiyd.backend.example.job;

import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.repository.RoleRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.service.RoleService;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.UUID;

public class UpdateAverageMarkOfRolesJobTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @SpyBean
    private RoleService roleService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageMarkOfRolesJob updateAverageMarkOfRolesJob;

    @Test
    public void testUpdateAverageMarkOfRoles() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();

        testObjectsFactory.createRoleMark(ru, r1, 5);
        testObjectsFactory.createRoleMark(ru, r1, 10);
        updateAverageMarkOfRolesJob.updateAverageMarkOfRoles();

        r1 = roleRepository.findById(r1.getId()).get();
        Assert.assertEquals(7.5, r1.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    public void testRolesUpdatedIndependently() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();
        testObjectsFactory.createRoleMark(ru, r1, 4);
        testObjectsFactory.createRoleMark(ru, r2, 5);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(roleService).updateAverageMarkOfRole(Mockito.any());

        updateAverageMarkOfRolesJob.updateAverageMarkOfRoles();

        for (Role r : roleRepository.findAll()) {
            if (r.getId().equals(failedId[0])) {
                Assert.assertNull(r.getAverageMark());
            } else {
                Assert.assertNotNull(r.getAverageMark());
            }
        }
    }

}