package com.nizkiyd.backend.example.job;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.repository.ActorRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.service.ActorService;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.UUID;

public class UpdateAverageRatingsOfActorsJobTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @SpyBean
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageRatingsOfActorsJob updateAverageRatingsOfActorsJob;

    @Test
    public void testUpdateAverageRatingsOfActors() {
        Movie m1 = testObjectsFactory.createMovie(8.0);
        Movie m2 = testObjectsFactory.createMovie(4.0);
        Actor a1 = testObjectsFactory.createActor();

        testObjectsFactory.createRole(m1, a1, 10.0);
        testObjectsFactory.createRole(m2, a1, 5.0);
        updateAverageRatingsOfActorsJob.updateAverageRoleRatingOfActors();

        a1 = actorRepository.findById(a1.getId()).get();
        Assert.assertEquals(7.5, a1.getAverageRoleRating(), Double.MIN_NORMAL);
        Assert.assertEquals(6.0, a1.getAverageMovieRating(), Double.MIN_NORMAL);
    }

    @Test
    public void testActorsUpdatedIndependently() {
        Movie m1 = testObjectsFactory.createMovie(8.0);
        Movie m2 = testObjectsFactory.createMovie(4.0);
        Actor a1 = testObjectsFactory.createActor();

        testObjectsFactory.createRole(m1, a1, 10.0);
        testObjectsFactory.createRole(m2, a1, 5.0);


        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(actorService).updateAverageRatingsOfActor(Mockito.any());

        updateAverageRatingsOfActorsJob.updateAverageRoleRatingOfActors();

        for (Actor a : actorRepository.findAll()) {
            if (a.getId().equals(failedId[0])) {
                Assert.assertNull(a.getAverageRoleRating());
            } else {
                Assert.assertNotNull(a.getAverageRoleRating());
            }
        }
    }

}