package com.nizkiyd.backend.example.job;

import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.service.MovieService;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.UUID;

public class UpdateAverageMarkOfMoviesJobTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @SpyBean
    private MovieService movieService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageMarkOfMoviesJob updateAverageMarkOfMoviesJob;

    @Test
    public void testUpdateAverageMarkOfMovies() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();

        testObjectsFactory.createMovieMark(ru, m1, 5);
        testObjectsFactory.createMovieMark(ru, m1, 10);
        updateAverageMarkOfMoviesJob.updateAverageMarkOfMovies();

        m1 = movieRepository.findById(m1.getId()).get();
        Assert.assertEquals(7.5, m1.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    public void testMoviesUpdatedIndependently() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovieWithEmptyAverageMark();
        Movie m2 = testObjectsFactory.createMovieWithEmptyAverageMark();
        testObjectsFactory.createMovieMark(ru, m1, 4);
        testObjectsFactory.createMovieMark(ru, m2, 5);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(movieService).updateAverageMarkOfMovie(Mockito.any());

        updateAverageMarkOfMoviesJob.updateAverageMarkOfMovies();

        for (Movie m : movieRepository.findAll()) {
            if (m.getId().equals(failedId[0])) {
                Assert.assertNull(m.getAverageMark());
            } else {
                Assert.assertNotNull(m.getAverageMark());
            }
        }
    }

}