package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.registereduser.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class RegisteredUserServiceTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RegisteredUserService registeredUserService;

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRegisteredUser() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUserReadDTO readDTO = registeredUserService.getRegisteredUser(registeredUser.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(registeredUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserWrongId() {
        registeredUserService.getRegisteredUser(UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUser() {
        String passwordBeforeSaving = "password123";

        RegisteredUserCreateDTO create = new RegisteredUserCreateDTO();
        create.setName("qwer");
        create.setAge(22);
        create.setGender(Gender.MALE);
        create.setPassword(passwordBeforeSaving);
        create.setEmail("email@gmail.com");

        RegisteredUserReadDTO read = registeredUserService.createRegisteredUser(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());

        RegisteredUser registeredUser = registeredUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(registeredUser);

        String passwordAfterSaving = registeredUser.getEncodedPassword();
        Assert.assertNotEquals(passwordBeforeSaving, passwordAfterSaving);
        Assert.assertEquals(registeredUser.getStatus(), AccountStatus.VALID);
    }

    @Test
    public void testPatchRegisteredUser() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        testObjectsFactory.createNewsLike(registeredUser);
        testObjectsFactory.createMovieReviewLike(registeredUser);
        testObjectsFactory.createRoleReviewLike(registeredUser);
        testObjectsFactory.createMovieMark(registeredUser);
        testObjectsFactory.createRoleMark(registeredUser);
        testObjectsFactory.createRoleReview(registeredUser);
        testObjectsFactory.createMovieReview(registeredUser);
        testObjectsFactory.createComplaint(registeredUser);
        testObjectsFactory.createSignalToContentManager(registeredUser);

        RegisteredUserPatchDTO patch = new RegisteredUserPatchDTO();
        patch.setName("qwear");
        patch.setAge(26);
        patch.setGender(Gender.MALE);

        RegisteredUserReadDTO read = registeredUserService.patchRegisteredUser(registeredUser.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        registeredUser = registeredUserRepository.findById(read.getId()).get();
        Assertions.assertThat(registeredUser).isEqualToIgnoringGivenFields(read,
                "newsLikes", "movieReviewLikes", "roleReviewLikes", "movieMarks",
                "roleMarks", "roleReviews", "movieReviews", "complaints", "signalToContentManagers",
                "encodedPassword", "status");
    }

    @Test
    public void testPatchRegisteredUserEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUserPatchDTO patch = new RegisteredUserPatchDTO();
        RegisteredUserReadDTO read = registeredUserService.patchRegisteredUser(registeredUser.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RegisteredUser registeredUserAfterUpdate = registeredUserRepository.findById(read.getId()).get();
        Assertions.assertThat(registeredUserAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(registeredUser).isEqualToIgnoringGivenFields(registeredUserAfterUpdate,
                "newsLikes", "movieReviewLikes", "roleReviewLikes", "movieMarks",
                "roleMarks", "roleReviews", "movieReviews", "complaints", "signalToContentManagers");
    }

    @Test
    public void testDeleteRegisteredUser() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser);
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser);
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser);
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Complaint complaint = testObjectsFactory.createComplaint(registeredUser);
        SignalToContentManager signal = testObjectsFactory.createSignalToContentManager(registeredUser);


        registeredUserService.deleteRegisteredUser(registeredUser.getId());

        Assert.assertFalse(registeredUserRepository.existsById(registeredUser.getId()));
        Assert.assertFalse(newsLikeRepository.existsById(newsLike.getId()));
        Assert.assertFalse(movieReviewLikeRepository.existsById(movieReviewLike.getId()));
        Assert.assertFalse(roleReviewLikeRepository.existsById(roleReviewLike.getId()));
        Assert.assertFalse(movieMarkRepository.existsById(movieMark.getId()));
        Assert.assertFalse(roleMarkRepository.existsById(roleMark.getId()));
        Assert.assertFalse(roleReviewRepository.existsById(roleReview.getId()));
        Assert.assertFalse(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertFalse(complaintRepository.existsById(complaint.getId()));
        Assert.assertFalse(signalToContentManagerRepository.existsById(signal.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRegisteredUserNotFound() {
        registeredUserService.deleteRegisteredUser(UUID.randomUUID());
    }

    @Test
    public void testPutRegisteredUser() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUserPutDTO put = new RegisteredUserPutDTO();
        put.setName("ddd");
        put.setAge(64);
        put.setGender(Gender.MALE);
        registeredUserService.updateRegisteredUser(registeredUser.getId(), put);

        RegisteredUserReadDTO read = registeredUserService.getRegisteredUser(registeredUser.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        registeredUser = registeredUserRepository.findById(registeredUser.getId()).get();
        Assertions.assertThat(registeredUser).isEqualToIgnoringGivenFields(read,
                "newsLikes", "movieReviewLikes", "roleReviewLikes", "movieMarks",
                "roleMarks", "roleReviews", "movieReviews", "complaints", "signalToContentManagers",
                "encodedPassword", "status");
    }

    @Test
    public void testChangeAccountStatusRegisteredUser() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser(AccountStatus.BLOCKED);

        RegisteredUserAccountStatusPatchDTO statusDTO = new RegisteredUserAccountStatusPatchDTO();
        statusDTO.setStatus(AccountStatus.VALID);

        RegisteredUserReadDTO registeredUserReadDTO = registeredUserService.changeAccountStatusRegisteredUser(
                registeredUser.getId(), statusDTO);
        Assert.assertEquals(registeredUserReadDTO.getStatus(), statusDTO.getStatus());

        RegisteredUser registeredUserAfterChange = registeredUserRepository.findById(registeredUser.getId()).get();
        Assert.assertEquals(registeredUserAfterChange.getStatus(), statusDTO.getStatus());
    }

    @Test
    public void testChangeTrustRegisteredUser() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser(false);

        RegisteredUserTrustPatchDTO patch = new RegisteredUserTrustPatchDTO();
        patch.setTrust(true);

        RegisteredUserReadDTO registeredUserReadDTO = registeredUserService.changeTrustRegisteredUser(
                registeredUser.getId(), patch);
        Assert.assertEquals(registeredUserReadDTO.getTrust(), patch.getTrust());

        RegisteredUser registeredUserAfterChange = registeredUserRepository.findById(registeredUser.getId()).get();
        Assert.assertEquals(registeredUserAfterChange.getTrust(), patch.getTrust());
    }
}