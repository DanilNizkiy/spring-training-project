package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import com.nizkiyd.backend.example.repository.SignalToContentManagerRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RegisteredUserNotifacationServiceTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private RegisteredUserNotifacationService registeredUserNotifacationService;

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Test
    public void testNotifyOnSignalToContentMangerStatusChangeToFixed() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();

        SignalToContentManagerStatus statusBeforeChange = SignalToContentManagerStatus.NEED_TO_FIX;
        SignalToContentManager signal = testObjectsFactory.createSignalToContentManager(ru, statusBeforeChange);

        registeredUserNotifacationService.notifyOnSignalToContentMangerStatusChangeToFixed(signal.getId());

        signal = signalToContentManagerRepository.findById(signal.getId()).get();

        Assert.assertEquals(signal.getSignalToContentManagerStatus(), SignalToContentManagerStatus.FIXED);
        Assert.assertNotEquals(signal.getSignalToContentManagerStatus(), statusBeforeChange);
    }
}