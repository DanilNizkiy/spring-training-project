package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.ContentManager;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerStatusDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.ContentManagerRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class ContentManagerServiceTest extends BaseTest {

    @Autowired
    private ContentManagerService contentManagerService;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetContentManager() {
        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerReadDTO readDTO = contentManagerService.getContentManager(contentManager.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(contentManager);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetContentManagerWrongId() {
        contentManagerService.getContentManager(UUID.randomUUID());
    }

    @Test
    public void testCreateContentManager() {
        String passwordBeforeSaving = "password123";

        ContentManagerCreateDTO create = new ContentManagerCreateDTO();
        create.setName("qwer");
        create.setPassword(passwordBeforeSaving);
        create.setEmail("email@gmail.com");

        ContentManagerReadDTO read = contentManagerService.createContentManager(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());

        ContentManager contentManager = contentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(contentManager);

        String passwordAfterSaving = contentManager.getEncodedPassword();
        Assert.assertNotEquals(passwordBeforeSaving, passwordAfterSaving);
        Assert.assertEquals(contentManager.getStatus(), AccountStatus.INVALID);
    }

    @Test
    public void testPatchContentManager() {
        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerPatchDTO patch = new ContentManagerPatchDTO();
        patch.setName("qwear");

        ContentManagerReadDTO read = contentManagerService.patchContentManager(contentManager.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        contentManager = contentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(contentManager).isEqualToIgnoringGivenFields(read, "encodedPassword", "status");
    }

    @Test
    public void testPatchContentManagerEmptyPatch() {
        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerPatchDTO patch = new ContentManagerPatchDTO();
        ContentManagerReadDTO read = contentManagerService.patchContentManager(contentManager.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        ContentManager contentManagerAfterUpdate = contentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(contentManagerAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(contentManager).isEqualToComparingFieldByField(contentManagerAfterUpdate);
    }

    @Test
    public void testDeleteContentManager() {
        ContentManager contentManager = testObjectsFactory.createContentManager();

        contentManagerService.deleteContentManager(contentManager.getId());
        Assert.assertFalse(contentManagerRepository.existsById(contentManager.getId()));
    }

    @Test
    public void testGetInvalidContentManagers() {
        testObjectsFactory.createContentManager(AccountStatus.BLOCKED);
        testObjectsFactory.createContentManager(AccountStatus.VALID);

        ContentManagerCreateDTO create = new ContentManagerCreateDTO();
        create.setName("qwer");
        create.setPassword("password123");
        create.setEmail("email@gmail.com");

        ContentManagerReadDTO newContentManager = contentManagerService.createContentManager(create);

        List<ContentManagerReadDTO> invalidContentManagers = contentManagerService.getInvalidContentManagers();
        Assert.assertEquals(invalidContentManagers.size(), 1);
        Assert.assertEquals(invalidContentManagers.get(0).getId(), newContentManager.getId());
    }

    @Test
    public void testChangeContentManagerAccountStatus() {
        ContentManager contentManager = testObjectsFactory.createContentManager(AccountStatus.BLOCKED);

        ContentManagerStatusDTO statusDTO = new ContentManagerStatusDTO();
        statusDTO.setStatus(AccountStatus.VALID);

        ContentManagerReadDTO contentManagerReadDTO = contentManagerService.changeContentManagerStatus(
                contentManager.getId(), statusDTO);
        Assert.assertEquals(contentManagerReadDTO.getStatus(), statusDTO.getStatus());

        ContentManager contentManagerAfterChange = contentManagerRepository.findById(contentManager.getId()).get();
        Assert.assertEquals(contentManagerAfterChange.getStatus(), statusDTO.getStatus());
    }
}