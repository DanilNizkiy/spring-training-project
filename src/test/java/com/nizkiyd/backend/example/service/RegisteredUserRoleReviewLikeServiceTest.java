package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.repository.RoleReviewLikeRepository;
import com.nizkiyd.backend.example.repository.RoleReviewRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserRoleReviewLikeServiceTest extends BaseTest {

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private RegisteredUserRoleReviewLikeService registeredUserRoleReviewLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        RoleReviewLikeReadDTO readDTO = registeredUserRoleReviewLikeService.getRoleReviewLike(
                registeredUser.getId(), roleReviewLike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(roleReviewLike,
                "registeredUserId", "roleReviewId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), roleReviewLike.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getRoleReviewId(), roleReviewLike.getRoleReview().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleReviewLikeWrongId() {
        registeredUserRoleReviewLikeService.getRoleReviewLike(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleReviewLikeWithWrongRole() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RoleReviewLikeCreateDTO create = new RoleReviewLikeCreateDTO();
        create.setLike(true);
        create.setRoleReviewId(UUID.randomUUID());

        registeredUserRoleReviewLikeService.createRoleReviewLike(registeredUser.getId(), create);
    }

    @Test
    public void testCreateRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLikeCreateDTO create = new RoleReviewLikeCreateDTO();
        create.setLike(true);
        create.setRoleReviewId(roleReview.getId());

        RoleReviewLikeReadDTO read = registeredUserRoleReviewLikeService
                .createRoleReviewLike(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RoleReviewLike roleReviewLike = roleReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(roleReviewLike,
                "roleReviewId", "registeredUserId");
        Assert.assertEquals(read.getRegisteredUserId(), roleReviewLike.getRegisteredUser().getId());
        Assert.assertEquals(read.getRoleReviewId(), roleReviewLike.getRoleReview().getId());
    }

    @Test
    public void testPatchRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        RoleReviewLikePatchDTO patch = new RoleReviewLikePatchDTO();
        patch.setLike(true);
        RoleReviewLikeReadDTO read = registeredUserRoleReviewLikeService.patchRoleReviewLike(
                registeredUser.getId(), roleReviewLike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        roleReviewLike = roleReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewLike).isEqualToIgnoringGivenFields(read,
                "registeredUser", "roleReview");
        Assert.assertEquals(roleReviewLike.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleReviewLike.getRoleReview().getId(), read.getRoleReviewId());
    }

    @Test
    public void testPatchRoleReviewLikeEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);
        RoleReviewLikePatchDTO patch = new RoleReviewLikePatchDTO();

        RoleReviewLikeReadDTO read = registeredUserRoleReviewLikeService.patchRoleReviewLike(
                registeredUser.getId(), roleReviewLike.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RoleReviewLike roleReviewLikeAfterUpdate = roleReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewLikeAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(roleReviewLike).isEqualToIgnoringGivenFields(roleReviewLikeAfterUpdate,
                "registeredUser", "roleReview");
        Assert.assertEquals(
                roleReviewLike.getRegisteredUser().getId(), roleReviewLikeAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(
                roleReviewLike.getRoleReview().getId(), roleReviewLikeAfterUpdate.getRoleReview().getId());
    }

    @Test
    public void testDeleteRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        registeredUserRoleReviewLikeService.deleteRoleReviewLike(registeredUser.getId(), roleReviewLike.getId());
        Assert.assertFalse(roleReviewLikeRepository.existsById(roleReviewLike.getId()));
        Assert.assertTrue(roleReviewRepository.existsById(roleReview.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRoleReviewLikeNotFound() {
        registeredUserRoleReviewLikeService.deleteRoleReviewLike(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetRegisteredUserRoleReviewLikes() {
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();
        RoleReview mr = testObjectsFactory.createRoleReview(r1);

        RoleReviewLike rrl1 = testObjectsFactory.createRoleReviewLike(r1, mr);
        testObjectsFactory.createRoleReviewLike(r2, mr);
        testObjectsFactory.createRoleReviewLike(r3, mr);
        RoleReviewLike rrl2 = testObjectsFactory.createRoleReviewLike(r1, mr);
        RoleReviewLike rrl3 = testObjectsFactory.createRoleReviewLike(r1, mr);

        List<RoleReviewLikeReadDTO> roleReviewLikesReadDTO = registeredUserRoleReviewLikeService
                .getRegisteredUserRoleReviewLikes(r1.getId());
        Assertions.assertThat(roleReviewLikesReadDTO)
                .extracting(RoleReviewLikeReadDTO::getId).isEqualTo(Arrays.asList(
                rrl1.getId(), rrl2.getId(), rrl3.getId()));
    }

    @Test
    public void testPutRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);

        RoleReviewLike roleReviewLike = new RoleReviewLike();
        roleReviewLike.setRoleReview(roleReview);
        roleReviewLike.setRegisteredUser(registeredUser);
        roleReviewLike.setLike(true);
        roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);

        RoleReviewLikePutDTO put = new RoleReviewLikePutDTO();
        put.setLike(false);
        registeredUserRoleReviewLikeService.updateRoleReviewLike(registeredUser.getId(), roleReviewLike.getId(), put);

        RoleReviewLikeReadDTO read = registeredUserRoleReviewLikeService.getRoleReviewLike(registeredUser.getId(),
                roleReviewLike.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        roleReviewLike = roleReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewLike).isEqualToIgnoringGivenFields(read, "registeredUser", "roleReview");
    }
}