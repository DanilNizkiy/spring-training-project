package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.domain.Gender;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.role.RoleCreateDTO;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.ActorRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RoleRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MovieRoleServiceTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MovieRoleService movieRoleService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);

        RoleReadDTO readDTO = movieRoleService.getRole(movie.getId(),
                role.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(role, "movieId", "actorId");
        Assert.assertEquals(readDTO.getMovieId(), role.getMovie().getId());
        Assert.assertEquals(readDTO.getActorId(), role.getActor().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleWrongId() {
        movieRoleService.getRole(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleWithWrongActor() {
        Movie movie = testObjectsFactory.createMovie();

        RoleCreateDTO create = new RoleCreateDTO();
        create.setRoleName("qwer");
        create.setActorId(UUID.randomUUID());

        movieRoleService.createRole(movie.getId(), create);
    }

    @Test
    public void testCreateRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        RoleCreateDTO create = new RoleCreateDTO();
        create.setActorId(actor.getId());
        create.setRoleName("asdd");

        RoleReadDTO read = movieRoleService.createRole(movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Role role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(role,
                "actorId", "movieId");
        Assert.assertEquals(read.getMovieId(), role.getMovie().getId());
        Assert.assertEquals(read.getActorId(), role.getActor().getId());
    }

    @Test
    public void testPatchRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, a1);

        RolePatchDTO patch = new RolePatchDTO();
        patch.setActorId(a2.getId());
        patch.setRoleName("asdd");
        patch.setGender(Gender.FEMALE);
        RoleReadDTO read = movieRoleService.patchRole(
                movie.getId(), role.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read,
                "movie", "actor");
        Assert.assertEquals(role.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(role.getActor().getId(), read.getActorId());
    }

    @Test
    public void testPatchRoleEmptyPatch() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);
        RolePatchDTO patch = new RolePatchDTO();

        RoleReadDTO read = movieRoleService.patchRole(
                movie.getId(), role.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("averageMark");

        Role roleAfterUpdate = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(roleAfterUpdate).hasNoNullFieldsOrPropertiesExcept("averageMark");

        Assertions.assertThat(role).isEqualToIgnoringGivenFields(roleAfterUpdate,
                "movie", "actor");
        Assert.assertEquals(role.getMovie().getId(), roleAfterUpdate.getMovie().getId());
        Assert.assertEquals(role.getActor().getId(), roleAfterUpdate.getActor().getId());
    }

    @Test
    public void testDeleteRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);

        movieRoleService.deleteRole(movie.getId(), role.getId());
        Assert.assertFalse(roleRepository.existsById(role.getId()));
        Assert.assertTrue(actorRepository.existsById(actor.getId()));
        Assert.assertTrue(movieRepository.existsById(movie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRoleNotFound() {
        movieRoleService.deleteRole(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testPutRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, a1);

        RolePutDTO put = new RolePutDTO();
        put.setActorId(a2.getId());
        put.setRoleName("asdd");
        movieRoleService.updateRole(movie.getId(), role.getId(), put);

        RoleReadDTO read = movieRoleService.getRole(movie.getId(), role.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read,
                "actor", "movie");
        Assert.assertEquals(role.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(role.getActor().getId(), read.getActorId());
    }

    @Test
    public void testGetMovieRoles() {
        Actor actor = testObjectsFactory.createActor();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Movie m3 = testObjectsFactory.createMovie();

        Role r1 = testObjectsFactory.createRole(m1, actor);
        testObjectsFactory.createRole(m2, actor);
        testObjectsFactory.createRole(m3, actor);
        Role r2 = testObjectsFactory.createRole(m1, actor);
        Role r3 = testObjectsFactory.createRole(m1, actor);

        List<RoleReadDTO> rolesReadDTO = movieRoleService.getMovieRoles(m1.getId());
        Assertions.assertThat(rolesReadDTO)
                .extracting(RoleReadDTO::getId).isEqualTo(Arrays.asList(r1.getId(), r2.getId(), r3.getId()));
    }
}