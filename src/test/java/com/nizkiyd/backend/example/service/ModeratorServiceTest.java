package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moderator.ModeratorStatusDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorCreateDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorPatchDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.ModeratorRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class ModeratorServiceTest extends BaseTest {

    @Autowired
    private ModeratorService moderatorService;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetModerator() {
        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorReadDTO readDTO = moderatorService.getModerator(moderator.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(moderator);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetModeratorWrongId() {
        moderatorService.getModerator(UUID.randomUUID());
    }

    @Test
    public void testCreateModerator() {
        String passwordBeforeSaving = "password123";

        ModeratorCreateDTO create = new ModeratorCreateDTO();
        create.setName("qwer");
        create.setPassword(passwordBeforeSaving);
        create.setEmail("email@gmail.com");

        ModeratorReadDTO read = moderatorService.createModerator(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());

        Moderator moderator = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(moderator);

        String passwordAfterSaving = moderator.getEncodedPassword();
        Assert.assertNotEquals(passwordBeforeSaving, passwordAfterSaving);
        Assert.assertEquals(moderator.getStatus(), AccountStatus.INVALID);
    }

    @Test
    public void testPatchModerator() {
        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorPatchDTO patch = new ModeratorPatchDTO();
        patch.setName("qwear");

        ModeratorReadDTO read = moderatorService.patchModerator(moderator.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        moderator = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(moderator).isEqualToIgnoringGivenFields(read, "encodedPassword", "status");
    }

    @Test
    public void testPatchModeratorEmptyPatch() {
        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorPatchDTO patch = new ModeratorPatchDTO();
        ModeratorReadDTO read = moderatorService.patchModerator(moderator.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Moderator moderatorAfterUpdate = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(moderatorAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(moderator).isEqualToComparingFieldByField(moderatorAfterUpdate);
    }

    @Test
    public void testDeleteModerator() {
        Moderator moderator = testObjectsFactory.createModerator();

        moderatorService.deleteModerator(moderator.getId());
        Assert.assertFalse(moderatorRepository.existsById(moderator.getId()));
    }

    @Test
    public void testGetInvalidModerators() {
        testObjectsFactory.createModerator(AccountStatus.BLOCKED);
        testObjectsFactory.createModerator(AccountStatus.VALID);

        ModeratorCreateDTO create = new ModeratorCreateDTO();
        create.setName("qwer");
        create.setPassword("password123");
        create.setEmail("email@gmail.com");

        ModeratorReadDTO newModerator = moderatorService.createModerator(create);

        List<ModeratorReadDTO> invalidModerators = moderatorService.getInvalidModerators();
        Assert.assertEquals(invalidModerators.size(), 1);
        Assert.assertEquals(invalidModerators.get(0).getId(), newModerator.getId());
    }

    @Test
    public void testChangeModeratorAccountStatus() {
        Moderator moderator = testObjectsFactory.createModerator(AccountStatus.BLOCKED);

        ModeratorStatusDTO statusDTO = new ModeratorStatusDTO();
        statusDTO.setStatus(AccountStatus.VALID);

        ModeratorReadDTO moderatorReadDTO = moderatorService.changeModeratorStatus(moderator.getId(), statusDTO);
        Assert.assertEquals(moderatorReadDTO.getStatus(), statusDTO.getStatus());

        Moderator moderatorAfterChange = moderatorRepository.findById(moderator.getId()).get();
        Assert.assertEquals(moderatorAfterChange.getStatus(), statusDTO.getStatus());
    }
}