package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeFilter;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class RoleReviewLikeServiceTest extends BaseTest {

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private RoleReviewLikeService roleReviewLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeLikeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        RoleReviewLikeReadDTO readDTO = roleReviewLikeService.getRoleReviewLike(roleReviewLike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(roleReviewLike,
                "roleReviewId", "registeredUserId");
        Assert.assertEquals(readDTO.getRoleReviewId(), roleReviewLike.getRoleReview().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), roleReviewLike.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleReviewLikeWrongId() {
        roleReviewLikeService.getRoleReviewLike(UUID.randomUUID());
    }

    @Test
    public void testPatchRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        RoleReviewLikePatchDTO patch = new RoleReviewLikePatchDTO();
        patch.setLike(true);
        RoleReviewLikeReadDTO read = roleReviewLikeService.patchRoleReviewLike(roleReviewLike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        roleReviewLike = roleReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewLike).isEqualToIgnoringGivenFields(read,
                "roleReview", "registeredUser");
        Assert.assertEquals(roleReviewLike.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleReviewLike.getRoleReview().getId(), read.getRoleReviewId());
    }

    @Test
    public void testPatchRoleReviewLikeEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);
        RoleReviewLikePatchDTO patch = new RoleReviewLikePatchDTO();

        RoleReviewLikeReadDTO read = roleReviewLikeService.patchRoleReviewLike(roleReviewLike.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RoleReviewLike roleReviewLikeAfterUpdate = roleReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewLikeAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(roleReviewLike).isEqualToIgnoringGivenFields(roleReviewLikeAfterUpdate,
                "roleReview", "registeredUser");
        Assert.assertEquals(roleReviewLike.getRoleReview().getId(), roleReviewLikeAfterUpdate.getRoleReview().getId());
        Assert.assertEquals(
                roleReviewLike.getRegisteredUser().getId(), roleReviewLikeAfterUpdate.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteRoleReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser);
        RoleReviewLike roleReviewLike = testObjectsFactory.createRoleReviewLike(registeredUser, roleReview);

        roleReviewLikeService.deleteRoleReviewLike(roleReviewLike.getId());
        Assert.assertFalse(roleReviewLikeRepository.existsById(roleReviewLike.getId()));
        Assert.assertTrue(roleReviewRepository.existsById(roleReview.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewRoleNotFound() {
        roleReviewLikeService.deleteRoleReviewLike(UUID.randomUUID());
    }

    @Test
    public void testGetRoleReviewLikesWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview rr1 = testObjectsFactory.createRoleReview(registeredUser);
        RoleReview rr2 = testObjectsFactory.createRoleReview(registeredUser);
        RoleReview rr3 = testObjectsFactory.createRoleReview(registeredUser);

        RoleReviewLike rrl1 = testObjectsFactory.createRoleReviewLike(registeredUser, rr1);
        RoleReviewLike rrl2 = testObjectsFactory.createRoleReviewLike(registeredUser, rr2);
        RoleReviewLike rrl3 = testObjectsFactory.createRoleReviewLike(registeredUser, rr3);

        RoleReviewLikeFilter filter = new RoleReviewLikeFilter();
        Assertions.assertThat(roleReviewLikeService.getRoleReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rrl1.getId(), rrl2.getId(), rrl3.getId());
    }

    @Test
    public void testGetRoleReviewLikeByRoleReview() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1);
        RoleReview rr2 = testObjectsFactory.createRoleReview(ru1);

        RoleReviewLike rrl1 = testObjectsFactory.createRoleReviewLike(ru1, rr1);
        testObjectsFactory.createRoleReviewLike(ru1, rr2);
        RoleReviewLike rrl2 = testObjectsFactory.createRoleReviewLike(ru2, rr1);

        RoleReviewLikeFilter filter = new RoleReviewLikeFilter();
        filter.setRoleReviewId(rr1.getId());
        Assertions.assertThat(roleReviewLikeService.getRoleReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rrl1.getId(), rrl2.getId());
    }

    @Test
    public void testGetRoleReviewLikeByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1);
        RoleReview rr2 = testObjectsFactory.createRoleReview(ru1);

        RoleReviewLike rrl1 = testObjectsFactory.createRoleReviewLike(ru1, rr1);
        rrl1.setCreatedAt(testObjectsFactory.createInstant(2));
        rrl1 = roleReviewLikeRepository.save(rrl1);

        RoleReviewLike rrl2 = testObjectsFactory.createRoleReviewLike(ru1, rr2);
        rr2.setCreatedAt(testObjectsFactory.createInstant(7));
        roleReviewLikeRepository.save(rrl2);

        RoleReviewLike rrl3 = testObjectsFactory.createRoleReviewLike(ru2, rr1);
        rrl3.setCreatedAt(testObjectsFactory.createInstant(4));
        rrl3 = roleReviewLikeRepository.save(rrl3);

        RoleReviewLikeFilter filter = new RoleReviewLikeFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(roleReviewLikeService.getRoleReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rrl1.getId(), rrl3.getId());
    }

    @Test
    public void testGetRoleReviewLikeByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1);
        RoleReview rr2 = testObjectsFactory.createRoleReview(ru1);

        RoleReviewLike rrl1 = testObjectsFactory.createRoleReviewLike(ru1, rr1);
        rrl1.setCreatedAt(testObjectsFactory.createInstant(2));
        rrl1 = roleReviewLikeRepository.save(rrl1);

        RoleReviewLike rrl2 = testObjectsFactory.createRoleReviewLike(ru1, rr2);
        rrl2.setCreatedAt(testObjectsFactory.createInstant(7));
        roleReviewLikeRepository.save(rrl2);

        RoleReviewLike rrl3 = testObjectsFactory.createRoleReviewLike(ru2, rr1);
        rrl3.setCreatedAt(testObjectsFactory.createInstant(4));
        rrl3 = roleReviewLikeRepository.save(rrl3);

        RoleReviewLikeFilter filter = new RoleReviewLikeFilter();
        filter.setRoleReviewId(rr1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(roleReviewLikeService.getRoleReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rrl1.getId(), rrl3.getId());
    }

    @Test
    public void testGetRoleReviewLikeWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RoleReview rr1 = testObjectsFactory.createRoleReview(registeredUser);
        RoleReview rr2 = testObjectsFactory.createRoleReview(registeredUser);
        RoleReview rr3 = testObjectsFactory.createRoleReview(registeredUser);

        RoleReviewLike rrl1 = testObjectsFactory.createRoleReviewLike(registeredUser, rr1);
        RoleReviewLike rrl2 = testObjectsFactory.createRoleReviewLike(registeredUser, rr2);
        testObjectsFactory.createRoleReviewLike(registeredUser, rr3);

        RoleReviewLikeFilter filter = new RoleReviewLikeFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                roleReviewLikeService.getRoleReviewLike(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rrl1.getId(), rrl2.getId()));
    }
}