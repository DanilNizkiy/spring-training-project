package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.person.PersonCreateDTO;
import com.nizkiyd.backend.example.dto.person.PersonPatchDTO;
import com.nizkiyd.backend.example.dto.person.PersonPutDTO;
import com.nizkiyd.backend.example.dto.person.PersonReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.UUID;

public class PersonServiceTest extends BaseTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetPerson() {
        Person person = testObjectsFactory.cretePerson();

        PersonReadDTO readDTO = personService.getPerson(person.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(person);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetPersonWrongId() {
        personService.getPerson(UUID.randomUUID());
    }

    @Test
    public void testCreatePerson() {
        PersonCreateDTO create = new PersonCreateDTO();
        create.setName("qwer");
        create.setBirthday(LocalDate.now());
        create.setPlaceOfBirth("asdas");
        create.setGender(Gender.MALE);
        create.setAwards("asda");
        create.setBiography("sadsds");
        create.setAdult(false);

        PersonReadDTO read = personService.createPerson(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Person person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPerson() {
        Person person = testObjectsFactory.cretePerson();

        PersonPatchDTO patch = new PersonPatchDTO();
        patch.setName("aaa");
        patch.setBirthday(LocalDate.now());
        patch.setPlaceOfBirth("sdsh");
        patch.setGender(Gender.FEMALE);
        patch.setAwards("xcvx");
        patch.setBiography("hjgd");
        patch.setAdult(true);

        PersonReadDTO read = personService.patchPerson(person.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(person).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchPersonEmptyPatch() {
        Person person = testObjectsFactory.cretePerson();

        PersonPatchDTO patch = new PersonPatchDTO();
        PersonReadDTO read = personService.patchPerson(person.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Person personAfterUpdate = personRepository.findById(read.getId()).get();
        Assertions.assertThat(personAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(person).isEqualToComparingFieldByField(personAfterUpdate);
    }

    @Test
    public void testDeletePerson() {
        Person person = testObjectsFactory.cretePerson();

        personService.deletePerson(person.getId());
        Assert.assertFalse(personRepository.existsById(person.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeletePersonNotFound() {
        personService.deletePerson(UUID.randomUUID());
    }

    @Test
    public void testPutPerson() {
        Person person = testObjectsFactory.cretePerson();

        PersonPutDTO put = new PersonPutDTO();
        put.setName("aaa");
        put.setBirthday(LocalDate.now());
        put.setPlaceOfBirth("sdsh");
        put.setGender(Gender.FEMALE);
        put.setAwards("xcvx");
        put.setBiography("hjgd");
        put.setAdult(true);
        personService.updatePerson(person.getId(), put);

        PersonReadDTO read = personService.getPerson(person.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        person = personRepository.findById(person.getId()).get();
        Assertions.assertThat(person).isEqualToComparingFieldByField(read);
    }
}