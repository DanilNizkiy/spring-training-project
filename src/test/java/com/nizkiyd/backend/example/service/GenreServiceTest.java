package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.domain.Genre;
import com.nizkiyd.backend.example.dto.genre.GenreReadDTO;
import com.nizkiyd.backend.example.dto.genre.GenreTypeDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.GenreRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class GenreServiceTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private GenreService genreService;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testAddGenreToMovie() {
        Movie movie = testObjectsFactory.createMovie();
        Genre genre = genreRepository.findByType(GenreType.DRAMA);

        List<GenreReadDTO> res = genreService.addGenreToMovie(movie.getId(), genre.getId());

        GenreReadDTO expectedRead = new GenreReadDTO();
        expectedRead.setId(genre.getId());
        expectedRead.setType(genre.getType());
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterSave = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfterSave.getGenres())
                    .extracting(Genre::getId).containsExactlyInAnyOrder(genre.getId());
        });
    }

    @Test
    public void testDuplicatedGenre() {
        Movie movie = testObjectsFactory.createMovie();
        Genre genre = genreRepository.findByType(GenreType.DRAMA);

        genreService.addGenreToMovie(movie.getId(), genre.getId());
        Assertions.assertThatThrownBy(() -> {
            genreService.addGenreToMovie(movie.getId(), genre.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        UUID wrongMovieId = UUID.randomUUID();
        Genre genre = genreRepository.findByType(GenreType.DRAMA);
        genreService.addGenreToMovie(wrongMovieId, genre.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongGenreId() {
        Movie movie = testObjectsFactory.createMovie();
        UUID wrongGenreId = UUID.randomUUID();
        genreService.addGenreToMovie(movie.getId(), wrongGenreId);
    }

    @Test
    public void testGetStaticGenreIdByType() {
        GenreTypeDTO genreTypeDTO = new GenreTypeDTO();
        genreTypeDTO.setType(GenreType.WESTERN);

        GenreReadDTO genreReadDTO = genreService.getStaticGenreIdByType(genreTypeDTO);
        Assert.assertEquals(genreReadDTO.getType(), genreTypeDTO.getType());
    }

    @Test
    public void testGetMovieGenres() {
        Movie movie = testObjectsFactory.createMovie();

        Genre genre1 = genreRepository.findByType(GenreType.DRAMA);
        genreService.addGenreToMovie(movie.getId(), genre1.getId());

        Genre genre2 = genreRepository.findByType(GenreType.COMEDY);
        genreService.addGenreToMovie(movie.getId(), genre2.getId());

        List<GenreReadDTO> movieGenres = genreService.getMovieGenres(movie.getId());
        Assertions.assertThat(movieGenres).extracting(GenreReadDTO::getId)
                .containsExactlyInAnyOrder(genre1.getId(), genre2.getId());
        Assert.assertEquals(2, movieGenres.size());
    }

    @Test
    public void testRemoveGenreFromMovie() {
        Movie movie = testObjectsFactory.createMovie();
        Genre genre = genreRepository.findByType(GenreType.DRAMA);
        genreService.addGenreToMovie(movie.getId(), genre.getId());

        List<GenreReadDTO> remainingGenres = genreService.removeGenreFromMovie(
                movie.getId(), genre.getId());
        Assert.assertTrue(remainingGenres.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfterRemove = movieRepository.findById(movie.getId()).get();
            Assert.assertTrue(movieAfterRemove.getGenres().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedGenre() {
        Movie movie = testObjectsFactory.createMovie();
        Genre genre = genreRepository.findByType(GenreType.DRAMA);

        genreService.removeGenreFromMovie(movie.getId(), genre.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedGenre() {
        Movie movie = testObjectsFactory.createMovie();
        genreService.addGenreToMovie(movie.getId(), UUID.randomUUID());
    }
}