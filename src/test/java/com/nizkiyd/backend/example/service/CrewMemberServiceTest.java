package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberCreateDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberFilter;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberPatchDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.CrewMemberRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class CrewMemberServiceTest extends BaseTest {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CrewMemberService crewMemberService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testGetCrewMember() {
        Person person = testObjectsFactory.cretePerson();
        CrewMember crewMember = testObjectsFactory.createCrewMember(person);

        CrewMemberReadDTO readDTO = crewMemberService.getCrewMember(crewMember.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(crewMember, "personId");
        Assert.assertEquals(readDTO.getPersonId(), crewMember.getPerson().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCrewMemberWrongId() {
        crewMemberService.getCrewMember(UUID.randomUUID());
    }

    @Test
    public void testGetAllCrewMembers() {
        CrewMember cm1 = testObjectsFactory.createCrewMember();
        CrewMember cm2 = testObjectsFactory.createCrewMember();
        CrewMember cm3 = testObjectsFactory.createCrewMember();

        List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService.getAllCrewMembers();
        Assertions.assertThat(crewMembersReadDTO).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(cm1.getId(), cm2.getId(), cm3.getId());
        Assert.assertEquals(crewMembersReadDTO.size(), 3);
    }

    @Test
    public void testGetCrewMemberMovies() {
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        CrewMember cm1 = testObjectsFactory.createCrewMember();
        CrewMember cm2 = testObjectsFactory.createCrewMember();
        CrewMember cm3 = testObjectsFactory.createCrewMember();

        crewMemberService.addCrewMemberToMovie(m1.getId(), cm1.getId());
        crewMemberService.addCrewMemberToMovie(m1.getId(), cm2.getId());
        crewMemberService.addCrewMemberToMovie(m2.getId(), cm3.getId());

        List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService.getCrewMemberMovies(m1.getId());
        Assertions.assertThat(crewMembersReadDTO)
                .extracting(CrewMemberReadDTO::getId).containsExactlyInAnyOrder(cm1.getId(), cm2.getId());
    }


    @Test
    public void testGetCrewMembersNews() {
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        CrewMember cm1 = testObjectsFactory.createCrewMember();
        CrewMember cm2 = testObjectsFactory.createCrewMember();
        CrewMember cm3 = testObjectsFactory.createCrewMember();

        crewMemberService.addCrewMemberToNews(n1.getId(), cm1.getId());
        crewMemberService.addCrewMemberToNews(n1.getId(), cm2.getId());
        crewMemberService.addCrewMemberToNews(n2.getId(), cm3.getId());

        List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService.getCrewMembersNews(n1.getId());
        Assertions.assertThat(crewMembersReadDTO)
                .extracting(CrewMemberReadDTO::getId).containsExactlyInAnyOrder(cm1.getId(), cm2.getId());
        Assert.assertEquals(2, crewMembersReadDTO.size());
    }

    @Test
    public void testCreateCrewMember() {
        Person person = testObjectsFactory.cretePerson();

        CrewMemberCreateDTO create = new CrewMemberCreateDTO();
        create.setCrewMemberType(CrewMemberType.PRODUCER);
        create.setPersonId(person.getId());

        CrewMemberReadDTO read = crewMemberService.createCrewMember(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CrewMember crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(crewMember, "personId");
        Assert.assertEquals(read.getPersonId(), person.getId());
    }

    @Test
    public void testPatchCrewMember() {
        Person person = testObjectsFactory.cretePerson();
        CrewMember crewMember = testObjectsFactory.createCrewMember(person);

        CrewMemberPatchDTO patch = new CrewMemberPatchDTO();
        patch.setCrewMemberType(CrewMemberType.PRODUCER);
        patch.setPersonId(person.getId());
        CrewMemberReadDTO read = crewMemberService.patchCrewMember(crewMember.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read,
                "movies", "person", "news");
        Assert.assertEquals(crewMember.getPerson().getId(), person.getId());
    }

    @Test
    public void testPatchCrewMemberEmptyPatch() {
        Person person = testObjectsFactory.cretePerson();
        CrewMember crewMember = testObjectsFactory.createCrewMember(person);

        CrewMemberPatchDTO patch = new CrewMemberPatchDTO();

        CrewMemberReadDTO read = crewMemberService.patchCrewMember(crewMember.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        CrewMember crewMemberAfterUpdate = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(crewMemberAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(crewMemberAfterUpdate,
                "movies", "person", "news");
        Assert.assertEquals(crewMember.getPerson().getId(), crewMemberAfterUpdate.getPerson().getId());
    }

    @Test
    public void testGetCrewMemberWithEmptyFilter() {
        Person p1 = testObjectsFactory.cretePerson();
        Person p2 = testObjectsFactory.cretePerson();

        CrewMember cm1 = testObjectsFactory.createCrewMember(p1, CrewMemberType.PRODUCER);
        CrewMember cm2 = testObjectsFactory.createCrewMember(p2, CrewMemberType.MUSIC);
        CrewMember cm3 = testObjectsFactory.createCrewMember(p1, CrewMemberType.CAMERA);

        CrewMemberFilter filter = new CrewMemberFilter();
        Assertions.assertThat(
                crewMemberService.getCrewMember(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(cm1.getId(), cm2.getId(), cm3.getId());
    }


    @Test
    public void testGetCrewMemberByName() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.MALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.MALE);


        CrewMember cm1 = testObjectsFactory.createCrewMember(p1);
        testObjectsFactory.createCrewMember(p2);

        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setName("a");
        Assertions.assertThat(
                crewMemberService.getCrewMember(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(cm1.getId());
    }

    @Test
    public void testGetCrewMemberByNewsId() {
        CrewMember cm1 = testObjectsFactory.createCrewMember();
        CrewMember cm2 = testObjectsFactory.createCrewMember();
        CrewMember cm3 = testObjectsFactory.createCrewMember();

        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        crewMemberService.addCrewMemberToNews(n1.getId(), cm1.getId());
        crewMemberService.addCrewMemberToNews(n1.getId(), cm2.getId());
        crewMemberService.addCrewMemberToNews(n2.getId(), cm3.getId());

        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setNewsId(n1.getId());
        Assertions.assertThat(
                crewMemberService.getCrewMember(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(cm1.getId(), cm2.getId());
    }

    @Test
    public void testGetCrewMemberByType() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.MALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.FEMALE);
        Person p3 = testObjectsFactory.cretePerson("c", Gender.MALE);

        CrewMember cm1 = testObjectsFactory.createCrewMember(p1, CrewMemberType.PRODUCER);
        testObjectsFactory.createCrewMember(p2, CrewMemberType.MUSIC);
        CrewMember cm2 = testObjectsFactory.createCrewMember(p3, CrewMemberType.PRODUCER);

        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setCrewMemberType(CrewMemberType.PRODUCER);
        Assertions.assertThat(
                crewMemberService.getCrewMember(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(cm1.getId(), cm2.getId());
    }

    @Test
    public void testGetCrewMemberByPersonId() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.MALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.FEMALE);

        CrewMember cm1 = testObjectsFactory.createCrewMember(p1, CrewMemberType.PRODUCER);
        testObjectsFactory.createCrewMember(p2, CrewMemberType.MUSIC);
        CrewMember cm2 = testObjectsFactory.createCrewMember(p1, CrewMemberType.PRODUCER);

        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setPersonId(p1.getId());
        Assertions.assertThat(
                crewMemberService.getCrewMember(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(cm1.getId(), cm2.getId());
    }

    @Test
    public void testGetCrewMemberByAllFilters() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.FEMALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.MALE);

        CrewMember cm1 = testObjectsFactory.createCrewMember(p1, CrewMemberType.PRODUCER);
        testObjectsFactory.createCrewMember(p2, CrewMemberType.MUSIC);
        CrewMember cm2 = testObjectsFactory.createCrewMember(p1, CrewMemberType.PRODUCER);

        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setName("a");
        filter.setPersonId(p1.getId());
        filter.setCrewMemberType(CrewMemberType.PRODUCER);
        Assertions.assertThat(crewMemberService.getCrewMember(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(cm1.getId(), cm2.getId());
    }


    @Test
    public void testAddCrewMemberToNews() {
        CrewMember crewMember = testObjectsFactory.createCrewMember();
        News news = testObjectsFactory.createNews();

        List<CrewMemberReadDTO> res = crewMemberService.addCrewMemberToNews(news.getId(), crewMember.getId());
        CrewMemberReadDTO crewMemberReadDTO = crewMemberService.getCrewMember(crewMember.getId());

        Assertions.assertThat(res).containsExactlyInAnyOrder(crewMemberReadDTO);

        transactionTemplate.executeWithoutResult(status -> {
            News newsAfterSave = newsRepository.findById(news.getId()).get();
            Assertions.assertThat(newsAfterSave.getCrewMembers())
                    .extracting(CrewMember::getId).containsExactlyInAnyOrder(crewMember.getId());
        });
    }

    @Test
    public void testDuplicatedCrewMember() {
        CrewMember crewMember = testObjectsFactory.createCrewMember();
        News news = testObjectsFactory.createNews();

        crewMemberService.addCrewMemberToNews(news.getId(), crewMember.getId());
        Assertions.assertThatThrownBy(() -> {
            crewMemberService.addCrewMemberToNews(news.getId(), crewMember.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongCrewMemberId() {
        UUID wrongCrewMemberId = UUID.randomUUID();
        News news = testObjectsFactory.createNews();
        crewMemberService.addCrewMemberToNews(news.getId(), wrongCrewMemberId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongNewsId() {
        CrewMember crewMember = testObjectsFactory.createCrewMember();
        UUID wrongNewsId = UUID.randomUUID();
        crewMemberService.addCrewMemberToNews(wrongNewsId, crewMember.getId());
    }

    @Test
    public void testRemoveCrewMemberFromNews() {
        CrewMember crewMember = testObjectsFactory.createCrewMember();
        News news = testObjectsFactory.createNews();
        crewMemberService.addCrewMemberToNews(news.getId(), crewMember.getId());

        List<CrewMemberReadDTO> remainingCrewMember = crewMemberService.removeCrewMemberFromNews(
                news.getId(), crewMember.getId());
        Assert.assertTrue(remainingCrewMember.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            News newsAfterRemove = newsRepository.findById(news.getId()).get();
            Assert.assertTrue(newsAfterRemove.getCrewMembers().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCrewMember() {
        CrewMember crewMember = testObjectsFactory.createCrewMember();
        News news = testObjectsFactory.createNews();

        crewMemberService.removeCrewMemberFromNews(news.getId(), crewMember.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedCrewMember() {
        News news = testObjectsFactory.createNews();
        crewMemberService.addCrewMemberToNews(news.getId(), UUID.randomUUID());
    }
}