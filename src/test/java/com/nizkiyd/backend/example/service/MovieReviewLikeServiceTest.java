package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeFilter;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.repository.MovieReviewLikeRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class MovieReviewLikeServiceTest extends BaseTest {

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private MovieReviewLikeService movieReviewLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeLikeRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        MovieReviewLikeReadDTO readDTO = movieReviewLikeService.getMovieReviewLike(movieReviewLike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movieReviewLike,
                "movieReviewId", "registeredUserId");
        Assert.assertEquals(readDTO.getMovieReviewId(), movieReviewLike.getMovieReview().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), movieReviewLike.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieReviewLikeWrongId() {
        movieReviewLikeService.getMovieReviewLike(UUID.randomUUID());
    }

    @Test
    public void testPatchMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        MovieReviewLikePatchDTO patch = new MovieReviewLikePatchDTO();
        patch.setLike(true);
        MovieReviewLikeReadDTO read = movieReviewLikeService.patchMovieReviewLike(movieReviewLike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movieReviewLike = movieReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewLike).isEqualToIgnoringGivenFields(read,
                "movieReview", "registeredUser");
        Assert.assertEquals(movieReviewLike.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieReviewLike.getMovieReview().getId(), read.getMovieReviewId());
    }

    @Test
    public void testPatchMovieReviewLikeEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);
        MovieReviewLikePatchDTO patch = new MovieReviewLikePatchDTO();

        MovieReviewLikeReadDTO read = movieReviewLikeService.patchMovieReviewLike(movieReviewLike.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        MovieReviewLike movieReviewLikeAfterUpdate = movieReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewLikeAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movieReviewLike).isEqualToIgnoringGivenFields(movieReviewLikeAfterUpdate,
                "movieReview", "registeredUser");
        Assert.assertEquals(
                movieReviewLike.getMovieReview().getId(), movieReviewLikeAfterUpdate.getMovieReview().getId());
        Assert.assertEquals(
                movieReviewLike.getRegisteredUser().getId(), movieReviewLikeAfterUpdate.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        movieReviewLikeService.deleteMovieReviewLike(movieReviewLike.getId());
        Assert.assertFalse(movieReviewLikeRepository.existsById(movieReviewLike.getId()));
        Assert.assertTrue(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewMovieNotFound() {
        movieReviewLikeService.deleteMovieReviewLike(UUID.randomUUID());
    }

    @Test
    public void testGetMovieReviewLikesWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr3 = testObjectsFactory.createMovieReview(registeredUser);

        MovieReviewLike mrl1 = testObjectsFactory.createMovieReviewLike(registeredUser, mr1);
        MovieReviewLike mrl2 = testObjectsFactory.createMovieReviewLike(registeredUser, mr2);
        MovieReviewLike mrl3 = testObjectsFactory.createMovieReviewLike(registeredUser, mr3);

        MovieReviewLikeFilter filter = new MovieReviewLikeFilter();
        Assertions.assertThat(movieReviewLikeService.getMovieReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mrl1.getId(), mrl2.getId(), mrl3.getId());
    }

    @Test
    public void testGetMovieReviewLikeByMovieReview() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1);

        MovieReviewLike mrl1 = testObjectsFactory.createMovieReviewLike(ru1, mr1);
        testObjectsFactory.createMovieReviewLike(ru1, mr2);
        MovieReviewLike mrl2 = testObjectsFactory.createMovieReviewLike(ru2, mr1);

        MovieReviewLikeFilter filter = new MovieReviewLikeFilter();
        filter.setMovieReviewId(mr1.getId());
        Assertions.assertThat(movieReviewLikeService.getMovieReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mrl1.getId(), mrl2.getId());
    }

    @Test
    public void testGetMovieReviewLikeByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1);

        MovieReviewLike mrl1 = testObjectsFactory.createMovieReviewLike(ru1, mr1);
        mrl1.setCreatedAt(testObjectsFactory.createInstant(2));
        mrl1 = movieReviewLikeRepository.save(mrl1);

        MovieReviewLike mrl2 = testObjectsFactory.createMovieReviewLike(ru1, mr2);
        mrl2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieReviewLikeRepository.save(mrl2);

        MovieReviewLike mrl3 = testObjectsFactory.createMovieReviewLike(ru2, mr1);
        mrl3.setCreatedAt(testObjectsFactory.createInstant(4));
        mrl3 = movieReviewLikeRepository.save(mrl3);

        MovieReviewLikeFilter filter = new MovieReviewLikeFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(movieReviewLikeService.getMovieReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mrl1.getId(), mrl3.getId());
    }

    @Test
    public void testGetMovieReviewLikeByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1);

        MovieReviewLike mrl1 = testObjectsFactory.createMovieReviewLike(ru1, mr1);
        mrl1.setCreatedAt(testObjectsFactory.createInstant(2));
        mrl1 = movieReviewLikeRepository.save(mrl1);

        MovieReviewLike mrl2 = testObjectsFactory.createMovieReviewLike(ru1, mr2);
        mrl2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieReviewLikeRepository.save(mrl2);

        MovieReviewLike mrl3 = testObjectsFactory.createMovieReviewLike(ru2, mr1);
        mrl3.setCreatedAt(testObjectsFactory.createInstant(4));
        mrl3 = movieReviewLikeRepository.save(mrl3);

        MovieReviewLikeFilter filter = new MovieReviewLikeFilter();
        filter.setMovieReviewId(mr1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(movieReviewLikeService.getMovieReviewLike(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mrl1.getId(), mrl3.getId());
    }

    @Test
    public void testGetMovieReviewLikeWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr3 = testObjectsFactory.createMovieReview(registeredUser);

        MovieReviewLike mrl1 = testObjectsFactory.createMovieReviewLike(registeredUser, mr1);
        MovieReviewLike mrl2 = testObjectsFactory.createMovieReviewLike(registeredUser, mr2);
        testObjectsFactory.createMovieReviewLike(registeredUser, mr3);

        MovieReviewLikeFilter filter = new MovieReviewLikeFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                movieReviewLikeService.getMovieReviewLike(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(mrl1.getId(), mrl2.getId()));
    }
}