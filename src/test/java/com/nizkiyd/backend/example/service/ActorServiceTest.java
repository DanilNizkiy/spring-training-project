package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.actor.ActorCreateDTO;
import com.nizkiyd.backend.example.dto.actor.ActorPatchDTO;
import com.nizkiyd.backend.example.dto.actor.ActorReadDTO;
import com.nizkiyd.backend.example.dto.actor.ActorFilter;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.repository.ActorRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ActorServiceTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testGetActor() {
        Person person = testObjectsFactory.cretePerson();
        Actor actor = testObjectsFactory.createActor(person);

        ActorReadDTO readDTO = actorService.getActor(actor.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actor, "personId");
        Assert.assertEquals(readDTO.getPersonId(), actor.getPerson().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetActorWrongId() {
        actorService.getActor(UUID.randomUUID());
    }

    @Test
    public void testCreateActor() {
        Person person = testObjectsFactory.cretePerson();

        ActorCreateDTO create = new ActorCreateDTO();
        create.setPersonId(person.getId());

        ActorReadDTO read = actorService.createActor(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Actor actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actor, "personId");
        Assert.assertEquals(read.getPersonId(), actor.getPerson().getId());
    }

    @Test
    public void testPatchActor() {
        Actor actor = testObjectsFactory.createActor();
        Person person = testObjectsFactory.cretePerson();

        ActorPatchDTO patch = new ActorPatchDTO();
        patch.setPersonId(person.getId());

        ActorReadDTO read = actorService.patchActor(actor.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(read, "person", "news");
        Assert.assertEquals(actor.getPerson().getId(), read.getPersonId());
    }

    @Test
    public void testPatchActorEmptyPatch() {
        Person person = testObjectsFactory.cretePerson();
        Actor actor = testObjectsFactory.createActor(person);

        ActorPatchDTO patch = new ActorPatchDTO();
        ActorReadDTO read = actorService.patchActor(actor.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Actor actorAfterUpdate = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(actorAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(actorAfterUpdate, "news");
    }

    @Test
    public void testDeleteActor() {
        Actor actor = testObjectsFactory.createActor();

        actorService.deleteActor(actor.getId());
        Assert.assertFalse(actorRepository.existsById(actor.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteActorNotFound() {
        actorService.deleteActor(UUID.randomUUID());
    }

    @Test
    public void testGetActorWithEmptyFilter() {
        Person p1 = testObjectsFactory.cretePerson();
        Person p2 = testObjectsFactory.cretePerson();

        Actor a1 = testObjectsFactory.createActor(p1, 4.0);
        Actor a2 = testObjectsFactory.createActor(p2, 7.0);
        Actor a3 = testObjectsFactory.createActor(p1, 2.0);

        ActorFilter filter = new ActorFilter();
        Assertions.assertThat(actorService.getActor(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(a1.getId(), a2.getId(), a3.getId());
    }


    @Test
    public void testGetActorByName() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.MALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.MALE);

        Actor a1 = testObjectsFactory.createActor(p1);
        testObjectsFactory.createActor(p2);

        ActorFilter filter = new ActorFilter();
        filter.setName("a");
        Assertions.assertThat(actorService.getActor(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(a1.getId());
    }

    @Test
    public void testGetActorByNewsId() {
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Actor a3 = testObjectsFactory.createActor();

        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        actorService.addActorToNews(n1.getId(), a1.getId());
        actorService.addActorToNews(n1.getId(), a2.getId());
        actorService.addActorToNews(n2.getId(), a3.getId());

        ActorFilter filter = new ActorFilter();
        filter.setNewsId(n1.getId());
        Assertions.assertThat(actorService.getActor(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(a1.getId(), a2.getId());
    }

    @Test
    public void testGetActorByAverageMovieRatingInterval() {
        Person p1 = testObjectsFactory.cretePerson();

        Actor a1 = testObjectsFactory.createActor(p1, 5.0);
        testObjectsFactory.createActor(p1, 4.9);
        Actor a2 = testObjectsFactory.createActor(p1, 7.5);

        ActorFilter filter = new ActorFilter();
        filter.setAverageMovieRatingFrom(5.0);
        filter.setAverageMovieRatingTo(8.0);
        Assertions.assertThat(actorService.getActor(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(a1.getId(), a2.getId());
    }

    @Test
    public void testGetActorByAverageRoleRatingInterval() {
        Actor a1 = testObjectsFactory.createActor(5.0);
        testObjectsFactory.createActor(4.9);
        Actor a2 = testObjectsFactory.createActor(7.5);

        ActorFilter filter = new ActorFilter();
        filter.setAverageRoleRatingFrom(5.0);
        filter.setAverageRoleRatingTo(8.0);
        Assertions.assertThat(actorService.getActor(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(a1.getId(), a2.getId());
    }

    @Test
    public void testGetActorByAllFilters() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.FEMALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.MALE);

        Actor a1 = testObjectsFactory.createActor(5.0, 7.6, p1);
        testObjectsFactory.createActor(4.3, 9.5, p2);
        Actor a2 = testObjectsFactory.createActor(5.0, 7.6, p1);

        ActorFilter filter = new ActorFilter();
        filter.setName("a");
        filter.setPersonId(p1.getId());
        filter.setAverageMovieRatingFrom(5.0);
        filter.setAverageMovieRatingTo(9.0);
        filter.setAverageRoleRatingFrom(5.0);
        filter.setAverageRoleRatingTo(9.0);
        Assertions.assertThat(actorService.getActor(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(a1.getId(), a2.getId());
    }


    @Test
    public void testGetActorWithEmptyFilterWithPagingAndSorting() {
        Person p1 = testObjectsFactory.cretePerson("a", Gender.FEMALE);
        Person p2 = testObjectsFactory.cretePerson("b", Gender.MALE);

        testObjectsFactory.createActor(5.7, 7.6, p1);
        Actor a1 = testObjectsFactory.createActor(4.3, 9.5, p2);
        Actor a2 = testObjectsFactory.createActor(5.0, 7.6, p1);

        ActorFilter filter = new ActorFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "averageRoleRating"));
        Assertions.assertThat(
                actorService.getActor(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(a1.getId(), a2.getId()));
    }

    @Test
    public void testUpdateAverageRatingsOfActor() {
        Movie m1 = testObjectsFactory.createMovie(4.0);
        Movie m2 = testObjectsFactory.createMovie(8.0);
        Actor a1 = testObjectsFactory.createActor();

        testObjectsFactory.createRole(m1, a1, 10.0);
        testObjectsFactory.createRole(m2, a1, 5.0);

        actorService.updateAverageRatingsOfActor(a1.getId());
        Actor a1AfterUpdateRating = actorRepository.findById(a1.getId()).get();
        Assert.assertEquals(7.5, a1AfterUpdateRating.getAverageRoleRating(), Double.MIN_NORMAL);
        Assert.assertEquals(6.0, a1AfterUpdateRating.getAverageMovieRating(), Double.MIN_NORMAL);
    }

    @Test
    public void testAddActorToNews() {
        Actor actor = testObjectsFactory.createActor();
        News news = testObjectsFactory.createNews();

        List<ActorReadDTO> res = actorService.addActorToNews(news.getId(), actor.getId());
        ActorReadDTO actorReadDTO = actorService.getActor(actor.getId());

        Assertions.assertThat(res).containsExactlyInAnyOrder(actorReadDTO);

        transactionTemplate.executeWithoutResult(status -> {
            News newsAfterSave = newsRepository.findById(news.getId()).get();
            Assertions.assertThat(newsAfterSave.getActors())
                    .extracting(Actor::getId).containsExactlyInAnyOrder(actor.getId());
        });
    }

    @Test
    public void testDuplicatedActor() {
        Actor actor = testObjectsFactory.createActor();
        News news = testObjectsFactory.createNews();

        actorService.addActorToNews(news.getId(), actor.getId());
        Assertions.assertThatThrownBy(() -> {
            actorService.addActorToNews(news.getId(), actor.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongActorId() {
        UUID wrongActorId = UUID.randomUUID();
        News news = testObjectsFactory.createNews();
        actorService.addActorToNews(news.getId(), wrongActorId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongNewsId() {
        Actor actor = testObjectsFactory.createActor();
        UUID wrongNewsId = UUID.randomUUID();
        actorService.addActorToNews(wrongNewsId, actor.getId());
    }

    @Test
    public void testRemoveActorFromNews() {
        Actor actor = testObjectsFactory.createActor();
        News news = testObjectsFactory.createNews();
        actorService.addActorToNews(news.getId(), actor.getId());

        List<ActorReadDTO> remainingActor = actorService.removeActorFromNews(
                news.getId(), actor.getId());
        Assert.assertTrue(remainingActor.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            News newsAfterRemove = newsRepository.findById(news.getId()).get();
            Assert.assertTrue(newsAfterRemove.getActors().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedActor() {
        Actor actor = testObjectsFactory.createActor();
        News news = testObjectsFactory.createNews();

        actorService.removeActorFromNews(news.getId(), actor.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedActor() {
        News news = testObjectsFactory.createNews();
        actorService.addActorToNews(news.getId(), UUID.randomUUID());
    }

    @Test
    public void testGetActorsNews() {
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Actor a3 = testObjectsFactory.createActor();

        actorService.addActorToNews(n1.getId(), a1.getId());
        actorService.addActorToNews(n1.getId(), a2.getId());
        actorService.addActorToNews(n2.getId(), a3.getId());

        List<ActorReadDTO> actorsReadDTO = actorService.getActorsNews(n1.getId());
        Assertions.assertThat(actorsReadDTO)
                .extracting(ActorReadDTO::getId).containsExactlyInAnyOrder(a1.getId(), a2.getId());
        Assert.assertEquals(2, actorsReadDTO.size());
    }

}