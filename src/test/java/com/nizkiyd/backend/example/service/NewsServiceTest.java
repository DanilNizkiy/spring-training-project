package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.news.NewsReadDTO;
import com.nizkiyd.backend.example.dto.news.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;
import java.util.UUID;

public class NewsServiceTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private CrewMemberService crewMemberService;

    @Test
    public void testGetNews() {
        News news = testObjectsFactory.createNews();

        NewsReadDTO readDTO = newsService.getNews(news.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(news);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsWrongId() {
        newsService.getNews(UUID.randomUUID());
    }

    @Test
    public void testCreateNews() {
        NewsCreateDTO create = new NewsCreateDTO();
        create.setText("dsd");

        NewsReadDTO read = newsService.createNews(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        News news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(news);
    }

    @Test
    public void testPatchNews() {
        News news = testObjectsFactory.createNews();

        NewsPatchDTO patch = new NewsPatchDTO();
        patch.setText("dsd");

        NewsReadDTO read = newsService.patchNews(news.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(news).isEqualToIgnoringGivenFields(read,
                "crewMembers", "actors", "movies");
    }

    @Test
    public void testPatchNewsEmptyPatch() {
        News news = testObjectsFactory.createNews();
        NewsPatchDTO patch = new NewsPatchDTO();

        NewsReadDTO read = newsService.patchNews(news.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        News newsAfterUpdate = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(newsAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(news).isEqualToIgnoringGivenFields(newsAfterUpdate,
                "crewMembers", "actors", "movies");
    }

    @Test
    public void testDeleteNews() {
        News news = testObjectsFactory.createNews();

        newsService.deleteNews(news.getId());
        Assert.assertFalse(newsRepository.existsById(news.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsNotFound() {
        newsService.deleteNews(UUID.randomUUID());
    }

    @Test
    public void testPutNews() {
        News news = testObjectsFactory.createNews();

        NewsPutDTO put = new NewsPutDTO();
        put.setText("dsd");
        newsService.updateNews(news.getId(), put);

        NewsReadDTO read = newsService.getNews(news.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(news).isEqualToIgnoringGivenFields(read,
                "crewMembers", "actors", "movies");
    }

    @Test
    public void testGetNewsWithEmptyFilter() {
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Actor a3 = testObjectsFactory.createActor();

        News n1 = testObjectsFactory.createNews();
        News n3 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        actorService.addActorToNews(n1.getId(), a1.getId());
        actorService.addActorToNews(n2.getId(), a2.getId());
        actorService.addActorToNews(n3.getId(), a3.getId());

        NewsFilter filter = new NewsFilter();
        Assertions.assertThat(newsService.getNews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(n1.getId(), n2.getId(), n3.getId());
    }

    @Test
    public void testGetNewsByActor() {
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        News n1 = testObjectsFactory.createNews();
        News n3 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        actorService.addActorToNews(n1.getId(), a1.getId());
        actorService.addActorToNews(n2.getId(), a1.getId());
        actorService.addActorToNews(n3.getId(), a2.getId());

        NewsFilter filter = new NewsFilter();
        filter.setActorId(a1.getId());
        Assertions.assertThat(newsService.getNews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(n1.getId(), n2.getId());
    }

    @Test
    public void testGetNewsByMovie() {
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        News n1 = testObjectsFactory.createNews();
        News n3 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        movieService.addMovieToNews(n1.getId(), m1.getId());
        movieService.addMovieToNews(n2.getId(), m1.getId());
        movieService.addMovieToNews(n3.getId(), m2.getId());

        NewsFilter filter = new NewsFilter();
        filter.setMovieId(m1.getId());
        Assertions.assertThat(newsService.getNews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(n1.getId(), n2.getId());
    }

    @Test
    public void testGetNewsByCrewMember() {
        CrewMember cm1 = testObjectsFactory.createCrewMember();
        CrewMember cm2 = testObjectsFactory.createCrewMember();

        News n1 = testObjectsFactory.createNews();
        News n3 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        crewMemberService.addCrewMemberToNews(n1.getId(), cm1.getId());
        crewMemberService.addCrewMemberToNews(n2.getId(), cm1.getId());
        crewMemberService.addCrewMemberToNews(n3.getId(), cm2.getId());

        NewsFilter filter = new NewsFilter();
        filter.setCrewMemberId(cm1.getId());
        Assertions.assertThat(newsService.getNews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(n1.getId(), n2.getId());
    }

    @Test
    public void testGetNewsByCreatedAtInterval() {
        News n1 = testObjectsFactory.createNews();
        n1.setCreatedAt(testObjectsFactory.createInstant(2));
        n1 = newsRepository.save(n1);

        News n2 = testObjectsFactory.createNews();
        n2.setCreatedAt(testObjectsFactory.createInstant(7));
        newsRepository.save(n2);

        News n3 = testObjectsFactory.createNews();
        n3.setCreatedAt(testObjectsFactory.createInstant(4));
        n3 = newsRepository.save(n3);

        NewsFilter filter = new NewsFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(newsService.getNews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(n1.getId(), n3.getId());
    }

    @Test
    public void testGetNewsByAllFilters() {
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        CrewMember cm1 = testObjectsFactory.createCrewMember();
        CrewMember cm2 = testObjectsFactory.createCrewMember();

        News n1 = testObjectsFactory.createNews();
        n1.setCreatedAt(testObjectsFactory.createInstant(2));
        n1 = newsRepository.save(n1);

        News n2 = testObjectsFactory.createNews();
        n2.setCreatedAt(testObjectsFactory.createInstant(7));
        newsRepository.save(n2);

        News n3 = testObjectsFactory.createNews();
        n3.setCreatedAt(testObjectsFactory.createInstant(4));
        n3 = newsRepository.save(n3);

        actorService.addActorToNews(n1.getId(), a1.getId());
        actorService.addActorToNews(n2.getId(), a2.getId());
        actorService.addActorToNews(n3.getId(), a1.getId());
        movieService.addMovieToNews(n1.getId(), m1.getId());
        movieService.addMovieToNews(n2.getId(), m2.getId());
        movieService.addMovieToNews(n3.getId(), m1.getId());
        crewMemberService.addCrewMemberToNews(n1.getId(), cm1.getId());
        crewMemberService.addCrewMemberToNews(n2.getId(), cm2.getId());
        crewMemberService.addCrewMemberToNews(n3.getId(), cm1.getId());


        NewsFilter filter = new NewsFilter();
        filter.setMovieId(m1.getId());
        filter.setActorId(a1.getId());
        filter.setCrewMemberId(cm1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(newsService.getNews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(n1.getId(), n3.getId());
    }

    @Test
    public void testGetNewsWithEmptyFilterWithPagingAndSorting() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        News n1 = testObjectsFactory.createNews(a1, movie);
        News n3 = testObjectsFactory.createNews(a2, movie);
        testObjectsFactory.createNews(a1, movie);

        NewsFilter filter = new NewsFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                newsService.getNews(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(n1.getId(), n3.getId()));
    }
}