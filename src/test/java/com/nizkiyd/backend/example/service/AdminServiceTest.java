package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.AccountStatus;
import com.nizkiyd.backend.example.domain.Admin;
import com.nizkiyd.backend.example.dto.admin.AdminCreateDTO;
import com.nizkiyd.backend.example.dto.admin.AdminPatchDTO;
import com.nizkiyd.backend.example.dto.admin.AdminReadDTO;
import com.nizkiyd.backend.example.dto.admin.AdminStatusDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.AdminRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class AdminServiceTest extends BaseTest {

    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetAdmin() {
        Admin admin = testObjectsFactory.creteAdmin();

        AdminReadDTO readDTO = adminService.getAdmin(admin.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(admin);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetAdminWrongId() {
        adminService.getAdmin(UUID.randomUUID());
    }

    @Test
    public void testCreateAdmin() {
        String passwordBeforeSaving = "password123";

        AdminCreateDTO create = new AdminCreateDTO();
        create.setName("qwer");
        create.setPassword(passwordBeforeSaving);
        create.setEmail("email@gmail.com");

        AdminReadDTO read = adminService.createAdmin(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());

        Admin admin = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(admin);

        String passwordAfterSaving = admin.getEncodedPassword();
        Assert.assertNotEquals(passwordBeforeSaving, passwordAfterSaving);
        Assert.assertEquals(admin.getStatus(), AccountStatus.VALID);
    }

    @Test
    public void testPatchAdmin() {
        Admin admin = testObjectsFactory.creteAdmin();

        AdminPatchDTO patch = new AdminPatchDTO();
        patch.setName("qwear");

        AdminReadDTO read = adminService.patchAdmin(admin.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        admin = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(admin).isEqualToIgnoringGivenFields(read, "encodedPassword", "status");
    }

    @Test
    public void testPatchAdminEmptyPatch() {
        Admin admin = testObjectsFactory.creteAdmin();

        AdminPatchDTO patch = new AdminPatchDTO();
        AdminReadDTO read = adminService.patchAdmin(admin.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Admin adminAfterUpdate = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(adminAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(admin).isEqualToComparingFieldByField(adminAfterUpdate);
    }

    @Test
    public void testDeleteAdmin() {
        Admin admin = testObjectsFactory.creteAdmin();

        adminService.deleteAdmin(admin.getId());
        Assert.assertFalse(adminRepository.existsById(admin.getId()));
    }

    @Test
    public void testChangeAdminAccountStatus() {
        Admin admin = testObjectsFactory.creteAdmin(AccountStatus.BLOCKED);

        AdminStatusDTO statusDTO = new AdminStatusDTO();
        statusDTO.setStatus(AccountStatus.VALID);

        AdminReadDTO adminReadDTO = adminService.changeAdminStatus(admin.getId(), statusDTO);
        Assert.assertEquals(adminReadDTO.getStatus(), statusDTO.getStatus());

        Admin adminAfterChange = adminRepository.findById(admin.getId()).get();
        Assert.assertEquals(adminAfterChange.getStatus(), statusDTO.getStatus());
    }
}