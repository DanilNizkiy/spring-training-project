package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkFilter;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieMarkRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class MovieMarkServiceTest extends BaseTest {

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private MovieMarkService movieMarkService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        MovieMarkReadDTO readDTO = movieMarkService.getMovieMark(movieMark.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movieMark,
                "movieId", "registeredUserId");
        Assert.assertEquals(readDTO.getMovieId(), movieMark.getMovie().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), movieMark.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieMarkWrongId() {
        movieMarkService.getMovieMark(UUID.randomUUID());
    }

    @Test
    public void testPatchMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        MovieMarkPatchDTO patch = new MovieMarkPatchDTO();
        patch.setMark(5);
        MovieMarkReadDTO read = movieMarkService.patchMovieMark(movieMark.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movieMark = movieMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(movieMark).isEqualToIgnoringGivenFields(read,
                "movie", "registeredUser");
        Assert.assertEquals(movieMark.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieMark.getMovie().getId(), read.getMovieId());
    }

    @Test
    public void testPatchMovieMarkEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);
        MovieMarkPatchDTO patch = new MovieMarkPatchDTO();

        MovieMarkReadDTO read = movieMarkService.patchMovieMark(movieMark.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        MovieMark movieMarkAfterUpdate = movieMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(movieMarkAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movieMark).isEqualToIgnoringGivenFields(movieMarkAfterUpdate,
                "movie", "registeredUser");
        Assert.assertEquals(movieMark.getMovie().getId(), movieMarkAfterUpdate.getMovie().getId());
        Assert.assertEquals(movieMark.getRegisteredUser().getId(), movieMarkAfterUpdate.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        movieMarkService.deleteMovieMark(movieMark.getId());
        Assert.assertFalse(movieMarkRepository.existsById(movieMark.getId()));
        Assert.assertTrue(movieRepository.existsById(movie.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMarkMovieNotFound() {
        movieMarkService.deleteMovieMark(UUID.randomUUID());
    }

    @Test
    public void testGetMovieMarksWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Movie m3 = testObjectsFactory.createMovie();

        MovieMark mm1 = testObjectsFactory.createMovieMark(registeredUser, m1);
        MovieMark mm2 = testObjectsFactory.createMovieMark(registeredUser, m2);
        MovieMark mm3 = testObjectsFactory.createMovieMark(registeredUser, m3);

        MovieMarkFilter filter = new MovieMarkFilter();
        Assertions.assertThat(movieMarkService.getMovieMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mm1.getId(), mm2.getId(), mm3.getId());
    }

    @Test
    public void testGetMovieMarkByMovie() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieMark mm1 = testObjectsFactory.createMovieMark(ru1, m1);
        testObjectsFactory.createMovieMark(ru1, m2);
        MovieMark mm2 = testObjectsFactory.createMovieMark(ru2, m1);

        MovieMarkFilter filter = new MovieMarkFilter();
        filter.setMovieId(m1.getId());
        Assertions.assertThat(movieMarkService.getMovieMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mm1.getId(), mm2.getId());
    }

    @Test
    public void testGetMovieMarkByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieMark mm1 = testObjectsFactory.createMovieMark(ru1, m1);
        mm1.setCreatedAt(testObjectsFactory.createInstant(2));
        mm1 = movieMarkRepository.save(mm1);

        MovieMark mm2 = testObjectsFactory.createMovieMark(ru1, m2);
        mm2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieMarkRepository.save(mm2);

        MovieMark mm3 = testObjectsFactory.createMovieMark(ru2, m1);
        mm3.setCreatedAt(testObjectsFactory.createInstant(4));
        mm3 = movieMarkRepository.save(mm3);

        MovieMarkFilter filter = new MovieMarkFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(movieMarkService.getMovieMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mm1.getId(), mm3.getId());
    }

    @Test
    public void testGetMovieMarkByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieMark mm1 = testObjectsFactory.createMovieMark(ru1, m1);
        mm1.setCreatedAt(testObjectsFactory.createInstant(2));
        mm1 = movieMarkRepository.save(mm1);

        MovieMark mm2 = testObjectsFactory.createMovieMark(ru1, m2);
        mm2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieMarkRepository.save(mm2);

        MovieMark mm3 = testObjectsFactory.createMovieMark(ru2, m1);
        mm3.setCreatedAt(testObjectsFactory.createInstant(4));
        mm3 = movieMarkRepository.save(mm3);

        MovieMarkFilter filter = new MovieMarkFilter();
        filter.setMovieId(m1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(movieMarkService.getMovieMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mm1.getId(), mm3.getId());
    }

    @Test
    public void testGetMovieMarkWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieMark movieMark1 = testObjectsFactory.createMovieMark(ru, m1, 3);
        MovieMark movieMark2 = testObjectsFactory.createMovieMark(ru, m1, 5);
        testObjectsFactory.createMovieMark(ru, m2, 7);

        MovieMarkFilter filter = new MovieMarkFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "mark"));
        Assertions.assertThat(
                movieMarkService.getMovieMark(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(movieMark1.getId(), movieMark2.getId()));
    }
}