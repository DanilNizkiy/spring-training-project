package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkCreateDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserRoleMarkServiceTest extends BaseTest {

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private RegisteredUserRoleMarkService registeredUserRoleMarkService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        RoleMarkReadDTO readDTO = registeredUserRoleMarkService.getRoleMark(registeredUser.getId(),
                roleMark.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(roleMark, "registeredUserId", "roleId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), roleMark.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getRoleId(), roleMark.getRole().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleMarkWrongId() {
        registeredUserRoleMarkService.getRoleMark(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleMarkWithWrongRole() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RoleMarkCreateDTO create = new RoleMarkCreateDTO();
        create.setMark(6);
        create.setRoleId(UUID.randomUUID());

        registeredUserRoleMarkService.createRoleMark(registeredUser.getId(), create);
    }

    @Test
    public void testCreateRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMarkCreateDTO create = new RoleMarkCreateDTO();
        create.setMark(6);
        create.setRoleId(role.getId());

        RoleMarkReadDTO read = registeredUserRoleMarkService.createRoleMark(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RoleMark roleMark = roleMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(roleMark,
                "roleId", "registeredUserId");
        Assert.assertEquals(read.getRegisteredUserId(), roleMark.getRegisteredUser().getId());
        Assert.assertEquals(read.getRoleId(), roleMark.getRole().getId());
    }

    @Test
    public void testPatchRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        RoleMarkPatchDTO patch = new RoleMarkPatchDTO();
        patch.setMark(6);
        RoleMarkReadDTO read = registeredUserRoleMarkService.patchRoleMark(
                registeredUser.getId(), roleMark.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        roleMark = roleMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(roleMark).isEqualToIgnoringGivenFields(read,
                "registeredUser", "role");
        Assert.assertEquals(roleMark.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleMark.getRole().getId(), read.getRoleId());
    }

    @Test
    public void testPatchRoleMarkEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);
        RoleMarkPatchDTO patch = new RoleMarkPatchDTO();

        RoleMarkReadDTO read = registeredUserRoleMarkService.patchRoleMark(
                registeredUser.getId(), roleMark.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RoleMark roleMarkAfterUpdate = roleMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(roleMarkAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(roleMark).isEqualToIgnoringGivenFields(roleMarkAfterUpdate,
                "registeredUser", "role");
        Assert.assertEquals(roleMark.getRegisteredUser().getId(), roleMarkAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(roleMark.getRole().getId(), roleMarkAfterUpdate.getRole().getId());
    }

    @Test
    public void testDeleteRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        registeredUserRoleMarkService.deleteRoleMark(registeredUser.getId(), roleMark.getId());
        Assert.assertFalse(roleMarkRepository.existsById(roleMark.getId()));
        Assert.assertTrue(roleRepository.existsById(role.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRoleMarkNotFound() {
        registeredUserRoleMarkService.deleteRoleMark(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetRegisteredUserRoleMarks() {
        Role role = testObjectsFactory.createRole();
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();

        RoleMark mr1 = testObjectsFactory.createRoleMark(r1, role);
        testObjectsFactory.createRoleMark(r2, role);
        testObjectsFactory.createRoleMark(r3, role);
        RoleMark mr2 = testObjectsFactory.createRoleMark(r1, role);
        RoleMark mr3 = testObjectsFactory.createRoleMark(r1, role);

        List<RoleMarkReadDTO> roleMarksReadDTO = registeredUserRoleMarkService
                .getRegisteredUserRoleMarks(r1.getId());
        Assertions.assertThat(roleMarksReadDTO)
                .extracting(RoleMarkReadDTO::getId).isEqualTo(Arrays.asList(mr1.getId(), mr2.getId(), mr3.getId()));
    }
}