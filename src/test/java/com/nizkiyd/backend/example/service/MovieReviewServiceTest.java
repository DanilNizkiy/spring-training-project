package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moviereview.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MovieReviewServiceTest extends BaseTest {

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private MovieReviewService movieReviewService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        MovieReviewReadDTO readDTO = movieReviewService.getMovieReview(movieReview.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movieReview,
                "movieId", "registeredUserId", "moderatorId");
        Assert.assertEquals(readDTO.getMovieId(), movieReview.getMovie().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), movieReview.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getModeratorId(), movieReview.getModerator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieReviewWrongId() {
        movieReviewService.getMovieReview(UUID.randomUUID());
    }

    @Test
    public void testGetReviewsMovie() {
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        Movie movie1 = testObjectsFactory.createMovie();
        Moderator moderator1 = testObjectsFactory.createModerator();
        MovieReview movieReview1 = testObjectsFactory.createMovieReview(
                registeredUser1, movie1, moderator1, ReviewStatus.APPROVED_WITH_FIX);

        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        Moderator moderator2 = testObjectsFactory.createModerator();
        MovieReview movieReview2 = testObjectsFactory.createMovieReview(
                registeredUser2, movie1, moderator2, ReviewStatus.APPROVED);

        RegisteredUser registeredUser3 = testObjectsFactory.createRegisteredUser();
        Moderator moderator3 = testObjectsFactory.createModerator();
        testObjectsFactory.createMovieReview(registeredUser3, movie1, moderator3, ReviewStatus.MODERATION);

        List<MovieReviewReadDTO> movieReviews = movieReviewService.getReviewsMovie(movie1.getId());
        Assertions.assertThat(movieReviews).extracting(MovieReviewReadDTO::getId)
                .containsExactlyInAnyOrder(movieReview1.getId(), movieReview2.getId());
        Assert.assertEquals(movieReviews.size(), 2);
    }

    @Test
    public void testPatchMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        MovieReviewPatchDTO patch = new MovieReviewPatchDTO();
        patch.setReviewStatus(ReviewStatus.APPROVED_WITH_FIX);
        patch.setText("sds");
        MovieReviewReadDTO read = movieReviewService.patchMovieReview(movieReview.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movieReview = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReview).isEqualToIgnoringGivenFields(read,
                "movie", "registeredUser", "moderator");
        Assert.assertEquals(movieReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieReview.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(movieReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testPatchMovieReviewEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);
        MovieReviewPatchDTO patch = new MovieReviewPatchDTO();

        MovieReviewReadDTO read = movieReviewService.patchMovieReview(movieReview.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        MovieReview movieReviewAfterUpdate = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movieReview).isEqualToIgnoringGivenFields(movieReviewAfterUpdate,
                "movie", "registeredUser", "moderator");
        Assert.assertEquals(movieReview.getMovie().getId(), movieReviewAfterUpdate.getMovie().getId());
        Assert.assertEquals(
                movieReview.getRegisteredUser().getId(), movieReviewAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(movieReview.getModerator().getId(), movieReviewAfterUpdate.getModerator().getId());
    }

    @Test
    public void testDeleteMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        movieReviewService.deleteMovieReview(movieReview.getId());
        Assert.assertFalse(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertTrue(movieRepository.existsById(movie.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewMovieNotFound() {
        movieReviewService.deleteMovieReview(UUID.randomUUID());
    }

    @Test
    public void testPutMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        MovieReviewPutDTO put = new MovieReviewPutDTO();
        put.setText("rrr");
        put.setReviewStatus(ReviewStatus.MODERATION);
        movieReviewService.updateMovieReview(movieReview.getId(), put);

        MovieReviewReadDTO read = movieReviewService.getMovieReview(movieReview.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        movieReview = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReview).isEqualToIgnoringGivenFields(read,
                "registeredUser", "movie", "moderator");
        Assert.assertEquals(movieReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieReview.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(movieReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testGetMovieReviewsWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Movie m3 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser, m1, mod);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser, m2, mod);
        MovieReview mr3 = testObjectsFactory.createMovieReview(registeredUser, m3, mod);

        MovieReviewFilter filter = new MovieReviewFilter();
        Assertions.assertThat(movieReviewService.getMovieReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mr1.getId(), mr2.getId(), mr3.getId());
    }

    @Test
    public void testGetMovieReviewByMovie() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1, m1, mod);
        testObjectsFactory.createMovieReview(ru1, m2, mod);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru2, m1, mod);

        MovieReviewFilter filter = new MovieReviewFilter();
        filter.setMovieId(m1.getId());
        Assertions.assertThat(movieReviewService.getMovieReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mr1.getId(), mr2.getId());
    }

    @Test
    public void testGetMovieReviewByModerator() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1, m1, mod1);
        testObjectsFactory.createMovieReview(ru1, m2, mod2);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru2, m1, mod1);

        MovieReviewFilter filter = new MovieReviewFilter();
        filter.setModeratorId(mod1.getId());
        Assertions.assertThat(movieReviewService.getMovieReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mr1.getId(), mr2.getId());
    }

    @Test
    public void testGetMovieReviewByReviewStatus() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1, m1, mod1, ReviewStatus.APPROVED_WITH_FIX);
        testObjectsFactory.createMovieReview(ru1, m2, mod2, ReviewStatus.APPROVED);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru2, m1, mod1, ReviewStatus.APPROVED_WITH_FIX);

        MovieReviewFilter filter = new MovieReviewFilter();
        filter.setReviewStatus(ReviewStatus.APPROVED_WITH_FIX);
        Assertions.assertThat(movieReviewService.getMovieReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mr1.getId(), mr2.getId());
    }

    @Test
    public void testGetMovieReviewByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1, m1, mod);
        mr1.setCreatedAt(testObjectsFactory.createInstant(2));
        mr1 = movieReviewRepository.save(mr1);

        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1, m2, mod);
        mr2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieReviewRepository.save(mr2);

        MovieReview mr3 = testObjectsFactory.createMovieReview(ru2, m1, mod);
        mr3.setCreatedAt(testObjectsFactory.createInstant(4));
        mr3 = movieReviewRepository.save(mr3);

        MovieReviewFilter filter = new MovieReviewFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(movieReviewService.getMovieReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mr1.getId(), mr3.getId());
    }

    @Test
    public void testGetMovieReviewByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1, m1, mod1, ReviewStatus.APPROVED);
        mr1.setCreatedAt(testObjectsFactory.createInstant(2));
        mr1 = movieReviewRepository.save(mr1);

        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1, m2, mod2, ReviewStatus.APPROVED_WITH_FIX);
        mr2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieReviewRepository.save(mr2);

        MovieReview mr3 = testObjectsFactory.createMovieReview(ru2, m1, mod1, ReviewStatus.APPROVED);
        mr3.setCreatedAt(testObjectsFactory.createInstant(4));
        mr3 = movieReviewRepository.save(mr3);

        MovieReviewFilter filter = new MovieReviewFilter();
        filter.setMovieId(m1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        filter.setModeratorId(mod1.getId());
        filter.setReviewStatus(ReviewStatus.APPROVED);
        Assertions.assertThat(movieReviewService.getMovieReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(mr1.getId(), mr3.getId());
    }

    @Test
    public void testGetMovieReviewWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Movie m3 = testObjectsFactory.createMovie();

        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser, m1, mod);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser, m2, mod);
        testObjectsFactory.createMovieReview(registeredUser, m3, mod);
        testObjectsFactory.createMovieReview(registeredUser, m3, mod);


        MovieReviewFilter filter = new MovieReviewFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                movieReviewService.getMovieReview(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(mr1.getId(), mr2.getId()));
    }
}