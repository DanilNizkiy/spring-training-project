package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewFilter;
import com.nizkiyd.backend.example.dto.rolereview.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class RoleReviewServiceTest extends BaseTest {

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private RoleReviewService roleReviewService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewReadDTO readDTO = roleReviewService.getRoleReview(roleReview.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(roleReview,
                "roleId", "registeredUserId", "moderatorId");
        Assert.assertEquals(readDTO.getRoleId(), roleReview.getRole().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), roleReview.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getModeratorId(), roleReview.getModerator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleReviewWrongId() {
        roleReviewService.getRoleReview(UUID.randomUUID());
    }

    @Test
    public void testPatchRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewPatchDTO patch = new RoleReviewPatchDTO();
        patch.setReviewStatus(ReviewStatus.CANCELED);
        patch.setText("sds");
        RoleReviewReadDTO read = roleReviewService.patchRoleReview(roleReview.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        roleReview = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReview).isEqualToIgnoringGivenFields(read,
                "role", "registeredUser", "moderator");
        Assert.assertEquals(roleReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleReview.getRole().getId(), read.getRoleId());
        Assert.assertEquals(roleReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testPatchRoleReviewEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewPatchDTO patch = new RoleReviewPatchDTO();
        RoleReviewReadDTO read = roleReviewService.patchRoleReview(roleReview.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RoleReview roleReviewAfterUpdate = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(roleReview).isEqualToIgnoringGivenFields(roleReviewAfterUpdate,
                "role", "registeredUser", "moderator");
        Assert.assertEquals(roleReview.getRole().getId(), roleReviewAfterUpdate.getRole().getId());
        Assert.assertEquals(
                roleReview.getRegisteredUser().getId(), roleReviewAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(roleReview.getModerator().getId(), roleReviewAfterUpdate.getModerator().getId());
    }

    @Test
    public void testDeleteRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        roleReviewService.deleteRoleReview(roleReview.getId());
        Assert.assertFalse(roleReviewRepository.existsById(roleReview.getId()));
        Assert.assertTrue(roleRepository.existsById(role.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewRoleNotFound() {
        roleReviewService.deleteRoleReview(UUID.randomUUID());
    }

    @Test
    public void testPutRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewPutDTO put = new RoleReviewPutDTO();
        put.setText("rrr");
        put.setReviewStatus(ReviewStatus.CANCELED);
        roleReviewService.updateRoleReview(roleReview.getId(), put);

        RoleReviewReadDTO read = roleReviewService.getRoleReview(roleReview.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        roleReview = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReview).isEqualToIgnoringGivenFields(read,
                "registeredUser", "role", "moderator");
        Assert.assertEquals(roleReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleReview.getRole().getId(), read.getRoleId());
        Assert.assertEquals(roleReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testGetRoleReviewsWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();
        Role r3 = testObjectsFactory.createRole();

        RoleReview rr1 = testObjectsFactory.createRoleReview(registeredUser, r1, mod);
        RoleReview rr2 = testObjectsFactory.createRoleReview(registeredUser, r2, mod);
        RoleReview rr3 = testObjectsFactory.createRoleReview(registeredUser, r3, mod);

        RoleReviewFilter filter = new RoleReviewFilter();
        Assertions.assertThat(roleReviewService.getRoleReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr2.getId(), rr3.getId());
    }

    @Test
    public void testGetRoleReviewByRole() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1, r1, mod);
        testObjectsFactory.createRoleReview(ru1, r2, mod);
        RoleReview rr2 = testObjectsFactory.createRoleReview(ru2, r1, mod);

        RoleReviewFilter filter = new RoleReviewFilter();
        filter.setRoleId(r1.getId());
        Assertions.assertThat(roleReviewService.getRoleReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr2.getId());
    }

    @Test
    public void testGetRoleReviewByModerator() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1, r1, mod1);
        testObjectsFactory.createRoleReview(ru1, r2, mod2);
        RoleReview rr2 = testObjectsFactory.createRoleReview(ru2, r1, mod1);

        RoleReviewFilter filter = new RoleReviewFilter();
        filter.setModeratorId(mod1.getId());
        Assertions.assertThat(roleReviewService.getRoleReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr2.getId());
    }

    @Test
    public void testGetRoleReviewByReviewStatus() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1, r1, mod1, ReviewStatus.APPROVED_WITH_FIX);
        testObjectsFactory.createRoleReview(ru1, r2, mod2, ReviewStatus.APPROVED);
        RoleReview rr2 = testObjectsFactory.createRoleReview(ru2, r1, mod1, ReviewStatus.APPROVED_WITH_FIX);

        RoleReviewFilter filter = new RoleReviewFilter();
        filter.setReviewStatus(ReviewStatus.APPROVED_WITH_FIX);
        Assertions.assertThat(roleReviewService.getRoleReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr2.getId());
    }

    @Test
    public void testGetRoleReviewByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1, r1, mod);
        rr1.setCreatedAt(testObjectsFactory.createInstant(2));
        rr1 = roleReviewRepository.save(rr1);

        RoleReview rr2 = testObjectsFactory.createRoleReview(ru1, r2, mod);
        rr2.setCreatedAt(testObjectsFactory.createInstant(7));
        roleReviewRepository.save(rr2);

        RoleReview rr3 = testObjectsFactory.createRoleReview(ru2, r1, mod);
        rr3.setCreatedAt(testObjectsFactory.createInstant(4));
        rr3 = roleReviewRepository.save(rr3);

        RoleReviewFilter filter = new RoleReviewFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(roleReviewService.getRoleReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr3.getId());
    }

    @Test
    public void testGetRoleReviewByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleReview rr1 = testObjectsFactory.createRoleReview(ru1, r1, mod1, ReviewStatus.APPROVED);
        rr1.setCreatedAt(testObjectsFactory.createInstant(2));
        rr1 = roleReviewRepository.save(rr1);

        RoleReview rr2 = testObjectsFactory.createRoleReview(ru1, r2, mod2, ReviewStatus.APPROVED_WITH_FIX);
        rr2.setCreatedAt(testObjectsFactory.createInstant(7));
        roleReviewRepository.save(rr2);

        RoleReview rr3 = testObjectsFactory.createRoleReview(ru2, r1, mod1, ReviewStatus.APPROVED);
        rr3.setCreatedAt(testObjectsFactory.createInstant(4));
        rr3 = roleReviewRepository.save(rr3);

        RoleReviewFilter filter = new RoleReviewFilter();
        filter.setRoleId(r1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        filter.setModeratorId(mod1.getId());
        filter.setReviewStatus(ReviewStatus.APPROVED);
        Assertions.assertThat(roleReviewService.getRoleReview(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr3.getId());
    }

    @Test
    public void testGetRoleReviewWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Moderator mod = testObjectsFactory.createModerator();
        Role m1 = testObjectsFactory.createRole();
        Role m2 = testObjectsFactory.createRole();
        Role m3 = testObjectsFactory.createRole();

        RoleReview mr1 = testObjectsFactory.createRoleReview(registeredUser, m1, mod);
        RoleReview mr2 = testObjectsFactory.createRoleReview(registeredUser, m2, mod);
        testObjectsFactory.createRoleReview(registeredUser, m3, mod);
        testObjectsFactory.createRoleReview(registeredUser, m3, mod);


        RoleReviewFilter filter = new RoleReviewFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                roleReviewService.getRoleReview(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(mr1.getId(), mr2.getId()));
    }
}