package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkFilter;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.RoleRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.repository.RoleMarkRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class RoleMarkServiceTest extends BaseTest {

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private RoleMarkService roleMarkService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        RoleMarkReadDTO readDTO = roleMarkService.getRoleMark(roleMark.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(roleMark,
                "roleId", "registeredUserId");
        Assert.assertEquals(readDTO.getRoleId(), roleMark.getRole().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), roleMark.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleMarkWrongId() {
        roleMarkService.getRoleMark(UUID.randomUUID());
    }

    @Test
    public void testPatchRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        RoleMarkPatchDTO patch = new RoleMarkPatchDTO();
        patch.setMark(5);
        RoleMarkReadDTO read = roleMarkService.patchRoleMark(roleMark.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        roleMark = roleMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(roleMark).isEqualToIgnoringGivenFields(read,
                "role", "registeredUser");
        Assert.assertEquals(roleMark.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleMark.getRole().getId(), read.getRoleId());
    }

    @Test
    public void testPatchRoleMarkEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        RoleMarkPatchDTO patch = new RoleMarkPatchDTO();
        RoleMarkReadDTO read = roleMarkService.patchRoleMark(roleMark.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RoleMark roleMarkAfterUpdate = roleMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(roleMarkAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(roleMark).isEqualToIgnoringGivenFields(roleMarkAfterUpdate,
                "role", "registeredUser");
        Assert.assertEquals(roleMark.getRole().getId(), roleMarkAfterUpdate.getRole().getId());
        Assert.assertEquals(roleMark.getRegisteredUser().getId(), roleMarkAfterUpdate.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteRoleMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        RoleMark roleMark = testObjectsFactory.createRoleMark(registeredUser, role);

        roleMarkService.deleteRoleMark(roleMark.getId());
        Assert.assertFalse(roleMarkRepository.existsById(roleMark.getId()));
        Assert.assertTrue(roleRepository.existsById(role.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMarkRoleNotFound() {
        roleMarkService.deleteRoleMark(UUID.randomUUID());
    }

    @Test
    public void testGetRoleMarksWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();
        Role r3 = testObjectsFactory.createRole();

        RoleMark rm1 = testObjectsFactory.createRoleMark(registeredUser, r1);
        RoleMark rm2 = testObjectsFactory.createRoleMark(registeredUser, r2);
        RoleMark rm3 = testObjectsFactory.createRoleMark(registeredUser, r3);

        RoleMarkFilter filter = new RoleMarkFilter();
        Assertions.assertThat(roleMarkService.getRoleMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rm1.getId(), rm2.getId(), rm3.getId());
    }

    @Test
    public void testGetRoleMarkByRole() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleMark rm1 = testObjectsFactory.createRoleMark(ru1, r1);
        testObjectsFactory.createRoleMark(ru1, r2);
        RoleMark rm2 = testObjectsFactory.createRoleMark(ru2, r1);

        RoleMarkFilter filter = new RoleMarkFilter();
        filter.setRoleId(r1.getId());
        Assertions.assertThat(roleMarkService.getRoleMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rm1.getId(), rm2.getId());
    }

    @Test
    public void testGetRoleMarkByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleMark rm1 = testObjectsFactory.createRoleMark(ru1, r1);
        rm1.setCreatedAt(testObjectsFactory.createInstant(2));
        rm1 = roleMarkRepository.save(rm1);

        RoleMark rm2 = testObjectsFactory.createRoleMark(ru1, r2);
        rm2.setCreatedAt(testObjectsFactory.createInstant(7));
        roleMarkRepository.save(rm2);

        RoleMark rm3 = testObjectsFactory.createRoleMark(ru2, r1);
        rm3.setCreatedAt(testObjectsFactory.createInstant(4));
        rm3 = roleMarkRepository.save(rm3);

        RoleMarkFilter filter = new RoleMarkFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(roleMarkService.getRoleMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rm1.getId(), rm3.getId());
    }

    @Test
    public void testGetRoleMarkByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleMark rm1 = testObjectsFactory.createRoleMark(ru1, r1);
        rm1.setCreatedAt(testObjectsFactory.createInstant(2));
        rm1 = roleMarkRepository.save(rm1);

        RoleMark rm2 = testObjectsFactory.createRoleMark(ru1, r2);
        rm2.setCreatedAt(testObjectsFactory.createInstant(7));
        roleMarkRepository.save(rm2);

        RoleMark rm3 = testObjectsFactory.createRoleMark(ru2, r1);
        rm3.setCreatedAt(testObjectsFactory.createInstant(4));
        rm3 = roleMarkRepository.save(rm3);

        RoleMarkFilter filter = new RoleMarkFilter();
        filter.setRoleId(r1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(roleMarkService.getRoleMark(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(rm1.getId(), rm3.getId());
    }

    @Test
    public void testGetRoleMarkWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();
        Role r2 = testObjectsFactory.createRole();

        RoleMark roleMark1 = testObjectsFactory.createRoleMark(ru, r1, 3);
        RoleMark roleMark2 = testObjectsFactory.createRoleMark(ru, r1, 5);
        testObjectsFactory.createRoleMark(ru, r2, 7);

        RoleMarkFilter filter = new RoleMarkFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "mark"));
        Assertions.assertThat(
                roleMarkService.getRoleMark(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(roleMark1.getId(), roleMark2.getId()));
    }
}