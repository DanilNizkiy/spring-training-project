package com.nizkiyd.backend.example.service.importer;

import com.nizkiyd.backend.example.client.themoviedb.TheMovieDbClient;
import com.nizkiyd.backend.example.client.themoviedb.dto.*;
import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.exception.ImportedEntityAlreadyExistException;
import com.nizkiyd.backend.example.repository.ExternalSystemImportRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.PersonRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;
import java.util.UUID;

public class MovieImporterServiceTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private MovieRepository movieRepository;

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieImporterService movieImporterService;

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testMovieImport() {
        String movieExternalId = "id1";

        MovieReadDTO movieReadDTO = generator.generateRandomObject(MovieReadDTO.class);
        movieReadDTO.getGenres().set(0, new GenreReadDTO("Drama"));
        movieReadDTO.getGenres().add(new GenreReadDTO("Western"));
        movieReadDTO.getGenres().add(new GenreReadDTO("War"));
        movieReadDTO.setStatus("Released");

        MovieCreditsReadDTO movieCreditsReadDTO = generator.generateRandomObject(MovieCreditsReadDTO.class);
        movieCreditsReadDTO.getCrew().get(0).setJob("Author");
        movieCreditsReadDTO.getCast().add(movieCreditsReadDTO.getCast().get(0));
        movieCreditsReadDTO.getCrew().add(movieCreditsReadDTO.getCrew().get(0));

        PersonCreditsReadDTO personCreditsReadDTO = generator.generateRandomObject(PersonCreditsReadDTO.class);
        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        personReadDTO.setGender(1);

        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(movieReadDTO);
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(movieCreditsReadDTO);
        Mockito.when(movieDbClient.getPersonCredits(movieCreditsReadDTO.getCrew().get(0).getId()))
                .thenReturn(personCreditsReadDTO);
        Mockito.when(movieDbClient.getPerson(movieCreditsReadDTO.getCrew().get(0).getId()))
                .thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getPersonCredits(movieCreditsReadDTO.getCast().get(0).getId()))
                .thenReturn(personCreditsReadDTO);
        PersonReadDTO personReadDTO1 = generator.generateRandomObject(PersonReadDTO.class);
        personReadDTO1.setGender(2);
        Mockito.when(movieDbClient.getPerson(movieCreditsReadDTO.getCast().get(0).getId()))
                .thenReturn(personReadDTO1);

        UUID movieId = null;
        try {
            movieId = movieImporterService.importMovie(movieExternalId);
        } catch (ImportedEntityAlreadyExistException | ImportAlreadyPerformedException e) {
            e.printStackTrace();
        }

        UUID finalMovieId = movieId;
        transactionTemplate.executeWithoutResult(status -> {
            Movie movie = movieRepository.findById(finalMovieId).get();
            Assert.assertEquals(movieReadDTO.getTitle(), movie.getTitle());
            Assert.assertEquals(movieReadDTO.getAdult(), movie.getAdult());
            Assert.assertEquals(movieReadDTO.getBudget(), movie.getBudget());
            Assertions.assertThat(movie.getGenres())
                    .extracting(Genre::getType).isEqualTo(Arrays.asList(
                    GenreType.DRAMA, GenreType.WESTERN, GenreType.WAR));
            Assert.assertEquals(movieReadDTO.getOriginalLanguage(), movie.getOriginalLanguage());
            Assert.assertEquals(movieReadDTO.getOverview(), movie.getDescription());
            Assert.assertEquals(movieReadDTO.getReleaseDate(), movie.getReleaseDate());
            Assert.assertEquals(movieReadDTO.getRevenue(), movie.getRevenue());
            Assert.assertEquals(movie.getMovieStatus(), MovieStatus.RELEASED);
        });
    }

    @Test
    public void testMovieImporterAlreadyExist() {
        String movieExternalId = "id1";

        Movie existingMovie = testObjectsFactory.createMovie();

        MovieReadDTO read = generator.generateRandomObject(MovieReadDTO.class);
        read.setTitle(existingMovie.getTitle());
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions.catchThrowableOfType(
                () -> movieImporterService.importMovie(movieExternalId), ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Movie.class, ex.getEntityClass());
        Assert.assertEquals(existingMovie.getId(), ex.getEntityId());
    }

    @Test
    public void testNoCallToClientOnDuplicateImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieCreditsReadDTO movieCreditsReadDTO = generator.generateRandomObject(MovieCreditsReadDTO.class);
        MovieReadDTO movieReadDTO = generator.generateRandomObject(MovieReadDTO.class);
        PersonCreditsReadDTO personCreditsReadDTO = generator.generateRandomObject(PersonCreditsReadDTO.class);
        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);
        personReadDTO.setGender(1);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(movieReadDTO);
        Mockito.when(movieDbClient.getMovieCredits(movieExternalId)).thenReturn(movieCreditsReadDTO);
        Mockito.when(movieDbClient.getPersonCredits(movieCreditsReadDTO.getCrew().get(0).getId()))
                .thenReturn(personCreditsReadDTO);
        Mockito.when(movieDbClient.getPerson(movieCreditsReadDTO.getCrew().get(0).getId())).thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getPersonCredits(movieCreditsReadDTO.getCast().get(0).getId()))
                .thenReturn(personCreditsReadDTO);
        Mockito.when(movieDbClient.getPerson(movieCreditsReadDTO.getCast().get(0).getId())).thenReturn(personReadDTO);


        movieImporterService.importMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId);
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> movieImporterService.importMovie(movieExternalId))
                .isInstanceOf(ImportAlreadyPerformedException.class);

        Mockito.verifyNoInteractions(movieDbClient);
    }
}