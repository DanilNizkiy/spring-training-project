package com.nizkiyd.backend.example.service.importer;

import com.nizkiyd.backend.example.domain.ExternalSystemImport;
import com.nizkiyd.backend.example.domain.ImportedEntityType;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.repository.ExternalSystemImportRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.*;

public class ExternalSystemImportServiceTest extends BaseTest {

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Test
    public void testValidateNotImportedFromExternalSystem() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Movie.class, "some-id");
    }

    @Test
    public void testExceptionWhenAlreadyImported() {
        ExternalSystemImport esi = testObjectsFactory.createExternalSystemImport();
        esi.setEntityType(ImportedEntityType.MOVIE);
        esi = externalSystemImportRepository.save(esi);

        String idExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(Movie.class, idExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testNoExceptionWhenAlreadyImportedButDifferentEntityType() throws ImportAlreadyPerformedException {
        ExternalSystemImport esi = testObjectsFactory.createExternalSystemImport();
        esi.setEntityType(ImportedEntityType.PERSON);
        esi = externalSystemImportRepository.save(esi);

        externalSystemImportService.validateNotImported(Movie.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testCreateExternalSystemImport() {
        Movie movie = testObjectsFactory.createMovie();
        String idInExternalSystem = "id1";
        UUID importId = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi.getEntityType());
        Assert.assertEquals(movie.getId(), esi.getEntityId());

    }
}