package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.complaint.ComplaintCreateDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPatchDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPutDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserComplaintServiceTest extends BaseTest {

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private RegisteredUserComplaintService registeredUserComplaintService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Test
    public void testGetComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        ComplaintReadDTO readDTO = registeredUserComplaintService.getComplaint(
                registeredUser.getId(), complaint.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(complaint,
                "registeredUserId", "moderatorId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), complaint.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getModeratorId(), complaint.getModerator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetComplaintWrongId() {
        registeredUserComplaintService.getComplaint(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testCreateComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);

        ComplaintCreateDTO create = new ComplaintCreateDTO();
        create.setText("qwe");
        create.setComplaintObjectType(ComplaintObjectType.MOVIE_REVIEW);
        create.setComplaintObjectId(movieReview.getId());
        create.setType(ComplaintType.SPAM);

        ComplaintReadDTO read = registeredUserComplaintService.createComplaint(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Complaint complaint = complaintRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(complaint,
                "registeredUserId", "moderatorId");
        Assert.assertEquals(read.getRegisteredUserId(), complaint.getRegisteredUser().getId());
    }

    @Test
    public void testPatchComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser);
        Moderator m1 = testObjectsFactory.createModerator();
        Moderator m2 = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), m1);

        ComplaintPatchDTO patch = new ComplaintPatchDTO();
        patch.setText("qq");
        patch.setModeratorId(m2.getId());
        patch.setComplaintObjectType(ComplaintObjectType.MOVIE_REVIEW);
        patch.setComplaintObjectId(mr2.getId());
        patch.setType(ComplaintType.SWEARING);
        ComplaintReadDTO read = registeredUserComplaintService.patchComplaint(
                registeredUser.getId(), complaint.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        complaint = complaintRepository.findById(read.getId()).get();
        Assertions.assertThat(complaint).isEqualToIgnoringGivenFields(read,
                "registeredUser", "moderator");
        Assert.assertEquals(complaint.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(complaint.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testPatchComplaintEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);
        ComplaintPatchDTO patch = new ComplaintPatchDTO();

        ComplaintReadDTO read = registeredUserComplaintService.patchComplaint(
                registeredUser.getId(), complaint.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Complaint complaintAfterUpdate = complaintRepository.findById(read.getId()).get();
        Assertions.assertThat(complaintAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(complaint).isEqualToIgnoringGivenFields(complaintAfterUpdate,
                "registeredUser", "moderator");
        Assert.assertEquals(complaint.getRegisteredUser().getId(), complaintAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(complaint.getModerator().getId(), complaintAfterUpdate.getModerator().getId());
    }

    @Test
    public void testDeleteComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        registeredUserComplaintService.deleteComplaint(registeredUser.getId(), complaint.getId());

        Assert.assertFalse(complaintRepository.existsById(complaint.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
        Assert.assertTrue(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertTrue(moderatorRepository.existsById(moderator.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteComplaintNotFound() {
        registeredUserComplaintService.deleteComplaint(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testPutComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser);
        Moderator m1 = testObjectsFactory.createModerator();
        Moderator m2 = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), m1);

        ComplaintPutDTO put = new ComplaintPutDTO();
        put.setText("qq");
        put.setModeratorId(m2.getId());
        put.setComplaintObjectType(ComplaintObjectType.MOVIE_REVIEW);
        put.setComplaintObjectId(mr2.getId());
        put.setType(ComplaintType.SWEARING);
        registeredUserComplaintService.updateComplaint(registeredUser.getId(), complaint.getId(), put);

        ComplaintReadDTO read = registeredUserComplaintService.getComplaint(
                registeredUser.getId(), complaint.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        complaint = complaintRepository.findById(read.getId()).get();
        Assertions.assertThat(complaint).isEqualToIgnoringGivenFields(read,
                "registeredUser", "moderator");
        Assert.assertEquals(complaint.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(complaint.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testGetRegisteredUserComplaints() {
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(r2);
        Moderator moderator = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(
                r1, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);
        testObjectsFactory.createComplaint(r2, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);
        testObjectsFactory.createComplaint(r3, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);
        Complaint c2 = testObjectsFactory.createComplaint(
                r1, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);
        Complaint c3 = testObjectsFactory.createComplaint(
                r1, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        List<ComplaintReadDTO> complaintsReadDTO = registeredUserComplaintService
                .getRegisteredUserComplaints(r1.getId());
        Assertions.assertThat(complaintsReadDTO)
                .extracting(ComplaintReadDTO::getId).isEqualTo(Arrays.asList(c1.getId(), c2.getId(), c3.getId()));
    }
}