package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.ContentManagerRepository;
import com.nizkiyd.backend.example.repository.SignalToContentManagerRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserSignalToContentManagerServiceTest extends BaseTest {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private RegisteredUserSignalToContentManagerService registeredUserSignalToContentManagerService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Test
    public void testGetSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, movie.getId(), contentManager);

        SignalToContentManagerReadDTO readDTO = registeredUserSignalToContentManagerService.getSignalToContentManager(
                registeredUser.getId(), signalToContentManager.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(signalToContentManager,
                "registeredUserId", "contentManagerId");
        Assert.assertEquals(readDTO.getContentManagerId(), signalToContentManager.getContentManager().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), signalToContentManager.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetSignalToContentManagerWrongId() {
        registeredUserSignalToContentManagerService.getSignalToContentManager(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateSignalToContentManagerWithWrongMovie() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        SignalToContentManagerCreateDTO create = new SignalToContentManagerCreateDTO();
        create.setReplacement("sds");
        create.setFromIndex(4);
        create.setToIndex(8);
        create.setSignalToContentManagerObjectId(UUID.randomUUID());
        create.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        create.setContentManagerId(UUID.randomUUID());
        create.setWrongText("qweqwe");

        registeredUserSignalToContentManagerService.createSignalToContentManager(registeredUser.getId(), create);
    }

    @Test
    public void testCreateSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        SignalToContentManagerCreateDTO create = new SignalToContentManagerCreateDTO();
        create.setReplacement("sds");
        create.setFromIndex(4);
        create.setToIndex(8);
        create.setSignalToContentManagerObjectId(movie.getId());
        create.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        create.setContentManagerId(contentManager.getId());
        create.setWrongText("qweqwe");

        SignalToContentManagerReadDTO read = registeredUserSignalToContentManagerService.createSignalToContentManager(
                registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        SignalToContentManager signalToContentManager = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(signalToContentManager,
                "registeredUserId", "contentManagerId");
        Assert.assertEquals(read.getRegisteredUserId(), signalToContentManager.getRegisteredUser().getId());
        Assert.assertEquals(read.getContentManagerId(), signalToContentManager.getContentManager().getId());
    }

    @Test
    public void testPatchSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);

        SignalToContentManagerPatchDTO patch = new SignalToContentManagerPatchDTO();
        patch.setReplacement("sds");
        patch.setFromIndex(2);
        patch.setToIndex(6);
        patch.setSignalToContentManagerObjectId(m2.getId());
        patch.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        patch.setContentManagerId(cm2.getId());
        SignalToContentManagerReadDTO read = registeredUserSignalToContentManagerService.patchSignalToContentManager(
                registeredUser.getId(), signalToContentManager.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        signalToContentManager = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(signalToContentManager).isEqualToIgnoringGivenFields(read,
                "registeredUser", "contentManager");
        Assert.assertEquals(signalToContentManager.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(signalToContentManager.getContentManager().getId(), read.getContentManagerId());
    }

    @Test
    public void testPatchSignalToContentManagerEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        SignalToContentManagerPatchDTO patch = new SignalToContentManagerPatchDTO();

        SignalToContentManagerReadDTO read = registeredUserSignalToContentManagerService.patchSignalToContentManager(
                registeredUser.getId(), signalToContentManager.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        SignalToContentManager signalToContentManagerAfterUpdate = signalToContentManagerRepository.findById(
                read.getId()).get();
        Assertions.assertThat(signalToContentManagerAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(signalToContentManager).isEqualToIgnoringGivenFields(signalToContentManagerAfterUpdate,
                "registeredUser", "contentManager");
        Assert.assertEquals(
                signalToContentManager.getRegisteredUser().getId(),
                signalToContentManagerAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(
                signalToContentManager.getContentManager().getId(),
                signalToContentManagerAfterUpdate.getContentManager().getId());
    }

    @Test
    public void testDeleteSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);

        registeredUserSignalToContentManagerService.deleteSignalToContentManager(
                registeredUser.getId(), signalToContentManager.getId());
        Assert.assertFalse(signalToContentManagerRepository.existsById(signalToContentManager.getId()));
        Assert.assertTrue(contentManagerRepository.existsById(cm1.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteSignalToContentManagerNotFound() {
        registeredUserSignalToContentManagerService.deleteSignalToContentManager(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testPutSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);

        SignalToContentManagerPutDTO put = new SignalToContentManagerPutDTO();
        put.setReplacement("sds");
        put.setFromIndex(4);
        put.setToIndex(8);
        put.setSignalToContentManagerObjectId(m2.getId());
        put.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        put.setContentManagerId(cm2.getId());
        registeredUserSignalToContentManagerService.updateSignalToContentManager(
                registeredUser.getId(), signalToContentManager.getId(), put);

        SignalToContentManagerReadDTO read = registeredUserSignalToContentManagerService.getSignalToContentManager(
                registeredUser.getId(), signalToContentManager.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        signalToContentManager = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(signalToContentManager).isEqualToIgnoringGivenFields(read,
                "registeredUser", "contentManager");
        Assert.assertEquals(signalToContentManager.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(signalToContentManager.getContentManager().getId(), read.getContentManagerId());
    }

    @Test
    public void testGetRegisteredUserSignalToContentManagers() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru3 = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                ru1, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        testObjectsFactory.createSignalToContentManager(ru2, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        testObjectsFactory.createSignalToContentManager(ru3, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                ru1, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        SignalToContentManager signalToContentManager3 = testObjectsFactory.createSignalToContentManager(
                ru1, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);


        List<SignalToContentManagerReadDTO> signalReadDTO = registeredUserSignalToContentManagerService
                .getRegisteredUserSignalToContentManagers(ru1.getId());
        Assertions.assertThat(signalReadDTO).extracting(SignalToContentManagerReadDTO::getId).isEqualTo(Arrays.asList(
                signalToContentManager1.getId(),
                signalToContentManager2.getId(),
                signalToContentManager3.getId()));
    }
}