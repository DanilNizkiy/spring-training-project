package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeFilter;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.repository.NewsLikeRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class NewsLikeServiceTest extends BaseTest{

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private NewsLikeService newsLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private NewsLikeRepository newsLikeLikeRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        NewsLikeReadDTO readDTO = newsLikeService.getNewsLike(newsLike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(newsLike,
                "newsId", "registeredUserId");
        Assert.assertEquals(readDTO.getNewsId(), newsLike.getNews().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), newsLike.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsLikeWrongId() {
        newsLikeService.getNewsLike(UUID.randomUUID());
    }

    @Test
    public void testPatchNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        NewsLikePatchDTO patch = new NewsLikePatchDTO();
        patch.setLike(true);
        NewsLikeReadDTO read = newsLikeService.patchNewsLike(newsLike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        newsLike = newsLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLike).isEqualToIgnoringGivenFields(read,
                "news", "registeredUser");
        Assert.assertEquals(newsLike.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(newsLike.getNews().getId(), read.getNewsId());
    }

    @Test
    public void testPatchNewsLikeEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);
        NewsLikePatchDTO patch = new NewsLikePatchDTO();

        NewsLikeReadDTO read = newsLikeService.patchNewsLike(newsLike.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        NewsLike newsLikeAfterUpdate = newsLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLikeAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(newsLike).isEqualToIgnoringGivenFields(newsLikeAfterUpdate,
                "news", "registeredUser");
        Assert.assertEquals(newsLike.getNews().getId(), newsLikeAfterUpdate.getNews().getId());
        Assert.assertEquals(newsLike.getRegisteredUser().getId(), newsLikeAfterUpdate.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        newsLikeService.deleteNewsLike(newsLike.getId());
        Assert.assertFalse(newsLikeRepository.existsById(newsLike.getId()));
        Assert.assertTrue(newsRepository.existsById(news.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewMovieNotFound() {
        newsLikeService.deleteNewsLike(UUID.randomUUID());
    }

    @Test
    public void testGetNewsLikesWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        News n3 = testObjectsFactory.createNews();

        NewsLike nl1 = testObjectsFactory.createNewsLike(registeredUser, n1);
        NewsLike nl2 = testObjectsFactory.createNewsLike(registeredUser, n2);
        NewsLike nl3 = testObjectsFactory.createNewsLike(registeredUser, n3);

        NewsLikeFilter filter = new NewsLikeFilter();
        Assertions.assertThat(newsLikeService.getNewsLike(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(nl1.getId(), nl2.getId(), nl3.getId());
    }

    @Test
    public void testGetNewsLikeByNews() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        NewsLike nl1 = testObjectsFactory.createNewsLike(ru1, n1);
        testObjectsFactory.createNewsLike(ru1, n2);
        NewsLike nl2 = testObjectsFactory.createNewsLike(ru2, n1);

        NewsLikeFilter filter = new NewsLikeFilter();
        filter.setNewsId(n1.getId());
        Assertions.assertThat(newsLikeService.getNewsLike(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(nl1.getId(), nl2.getId());
    }

    @Test
    public void testGetNewsLikeByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        NewsLike nl1 = testObjectsFactory.createNewsLike(ru1, n1);
        nl1.setCreatedAt(testObjectsFactory.createInstant(2));
        nl1 = newsLikeRepository.save(nl1);

        NewsLike nl2 = testObjectsFactory.createNewsLike(ru1, n2);
        nl2.setCreatedAt(testObjectsFactory.createInstant(7));
        newsLikeRepository.save(nl2);

        NewsLike nl3 = testObjectsFactory.createNewsLike(ru2, n1);
        nl3.setCreatedAt(testObjectsFactory.createInstant(4));
        nl3 = newsLikeRepository.save(nl3);

        NewsLikeFilter filter = new NewsLikeFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(newsLikeService.getNewsLike(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(nl1.getId(), nl3.getId());
    }

    @Test
    public void testGetNewsLikeByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();

        NewsLike nl1 = testObjectsFactory.createNewsLike(ru1, n1);
        nl1.setCreatedAt(testObjectsFactory.createInstant(2));
        nl1 = newsLikeRepository.save(nl1);

        NewsLike nl2 = testObjectsFactory.createNewsLike(ru1, n2);
        nl2.setCreatedAt(testObjectsFactory.createInstant(7));
        newsLikeRepository.save(nl2);

        NewsLike nl3 = testObjectsFactory.createNewsLike(ru2, n1);
        nl3.setCreatedAt(testObjectsFactory.createInstant(4));
        nl3 = newsLikeRepository.save(nl3);

        NewsLikeFilter filter = new NewsLikeFilter();
        filter.setNewsId(n1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(newsLikeService.getNewsLike(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(nl1.getId(), nl3.getId());
    }

    @Test
    public void testGetNewsLikeWithEmptyFilterWithPagingAndSorting(){
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News mr1 = testObjectsFactory.createNews();
        News mr2 = testObjectsFactory.createNews();
        News mr3 = testObjectsFactory.createNews();

        NewsLike mrl1 = testObjectsFactory.createNewsLike(registeredUser, mr1);
        NewsLike mrl2 = testObjectsFactory.createNewsLike(registeredUser, mr2);
        testObjectsFactory.createNewsLike(registeredUser, mr3);

        NewsLikeFilter filter = new NewsLikeFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                newsLikeService.getNewsLike(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(mrl1.getId(), mrl2.getId()));
    }
}