package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkCreateDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieMarkRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserMovieMarkServiceTest extends BaseTest {

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private RegisteredUserMovieMarkService registeredUserMovieMarkService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        MovieMarkReadDTO readDTO = registeredUserMovieMarkService.getMovieMark(registeredUser.getId(),
                movieMark.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movieMark, "registeredUserId", "movieId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), movieMark.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getMovieId(), movieMark.getMovie().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieMarkWrongId() {
        registeredUserMovieMarkService.getMovieMark(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateMovieMarkWithWrongMovie() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MovieMarkCreateDTO create = new MovieMarkCreateDTO();
        create.setMark(6);
        create.setMovieId(UUID.randomUUID());

        registeredUserMovieMarkService.createMovieMark(registeredUser.getId(), create);
    }

    @Test
    public void testCreateMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMarkCreateDTO create = new MovieMarkCreateDTO();
        create.setMark(6);
        create.setMovieId(movie.getId());

        MovieMarkReadDTO read = registeredUserMovieMarkService.createMovieMark(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MovieMark movieMark = movieMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(movieMark,
                "movieId", "registeredUserId");
        Assert.assertEquals(read.getRegisteredUserId(), movieMark.getRegisteredUser().getId());
        Assert.assertEquals(read.getMovieId(), movieMark.getMovie().getId());
    }

    @Test
    public void testPatchMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        MovieMarkPatchDTO patch = new MovieMarkPatchDTO();
        patch.setMark(6);
        MovieMarkReadDTO read = registeredUserMovieMarkService.patchMovieMark(
                registeredUser.getId(), movieMark.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movieMark = movieMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(movieMark).isEqualToIgnoringGivenFields(read,
                "registeredUser", "movie");
        Assert.assertEquals(movieMark.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieMark.getMovie().getId(), read.getMovieId());
    }

    @Test
    public void testPatchMovieMarkEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);
        MovieMarkPatchDTO patch = new MovieMarkPatchDTO();

        MovieMarkReadDTO read = registeredUserMovieMarkService.patchMovieMark(
                registeredUser.getId(), movieMark.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        MovieMark movieMarkAfterUpdate = movieMarkRepository.findById(read.getId()).get();
        Assertions.assertThat(movieMarkAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movieMark).isEqualToIgnoringGivenFields(movieMarkAfterUpdate,
                "registeredUser", "movie");
        Assert.assertEquals(movieMark.getRegisteredUser().getId(), movieMarkAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(movieMark.getMovie().getId(), movieMarkAfterUpdate.getMovie().getId());
    }

    @Test
    public void testDeleteMovieMark() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        MovieMark movieMark = testObjectsFactory.createMovieMark(registeredUser, movie);

        registeredUserMovieMarkService.deleteMovieMark(registeredUser.getId(), movieMark.getId());
        Assert.assertFalse(movieMarkRepository.existsById(movieMark.getId()));
        Assert.assertTrue(movieRepository.existsById(movie.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieMarkNotFound() {
        registeredUserMovieMarkService.deleteMovieMark(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetRegisteredUserMovieMarks() {
        Movie movie = testObjectsFactory.createMovie();
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();

        MovieMark mr1 = testObjectsFactory.createMovieMark(r1, movie);
        testObjectsFactory.createMovieMark(r2, movie);
        testObjectsFactory.createMovieMark(r3, movie);
        MovieMark mr2 = testObjectsFactory.createMovieMark(r1, movie);
        MovieMark mr3 = testObjectsFactory.createMovieMark(r1, movie);

        List<MovieMarkReadDTO> movieMarksReadDTO = registeredUserMovieMarkService
                .getRegisteredUserMovieMarks(r1.getId());
        Assertions.assertThat(movieMarksReadDTO)
                .extracting(MovieMarkReadDTO::getId).isEqualTo(Arrays.asList(mr1.getId(), mr2.getId(), mr3.getId()));
    }
}