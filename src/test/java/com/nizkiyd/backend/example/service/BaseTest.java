package com.nizkiyd.backend.example.service;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from complaint",
        "delete from mark",
        "delete from signal_to_content_manager",
        "delete from like",
        "delete from review",
        "delete from news",
        "delete from role",
        "delete from actor",
        "delete from content_manager",
        "delete from moderator",
        "delete from movie",
        "delete from actor",
        "delete from review",
        "delete from registered_user",
        "delete from crew_member",
        "delete from external_system_import"
}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public abstract class BaseTest {
}
