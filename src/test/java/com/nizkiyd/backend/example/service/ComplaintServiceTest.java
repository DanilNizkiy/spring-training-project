package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.complaint.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class ComplaintServiceTest extends BaseTest {

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private ComplaintService complaintService;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Test
    public void testGetComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        ComplaintReadDTO readDTO = complaintService.getComplaint(complaint.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(complaint,
                "registeredUserId", "moderatorId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), complaint.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getModeratorId(), complaint.getModerator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetComplaintWrongId() {
        complaintService.getComplaint(UUID.randomUUID());
    }

    @Test
    public void testPatchComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser);
        Moderator m1 = testObjectsFactory.createModerator();
        Moderator m2 = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), m1);

        ComplaintPatchDTO patch = new ComplaintPatchDTO();
        patch.setText("sds");
        patch.setType(ComplaintType.SWEARING);
        patch.setComplaintObjectType(ComplaintObjectType.ROLE_REVIEW);
        patch.setComplaintObjectId(mr2.getId());
        patch.setModeratorId(m2.getId());

        ComplaintReadDTO read = complaintService.patchComplaint(complaint.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        complaint = complaintRepository.findById(read.getId()).get();
        Assertions.assertThat(complaint).isEqualToIgnoringGivenFields(read,
                "registeredUser", "moderator");
        Assert.assertEquals(complaint.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(complaint.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testPatchComplaintEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        ComplaintPatchDTO patch = new ComplaintPatchDTO();
        ComplaintReadDTO read = complaintService.patchComplaint(complaint.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Complaint complaintAfterUpdate = complaintRepository.findById(read.getId()).get();
        Assertions.assertThat(complaintAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(complaint).isEqualToIgnoringGivenFields(complaintAfterUpdate,
                "registeredUser", "moderator");
        Assert.assertEquals(complaint.getRegisteredUser().getId(), complaint.getRegisteredUser().getId());
        Assert.assertEquals(complaint.getModerator().getId(), complaint.getModerator().getId());
    }

    @Test
    public void testDeleteComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        Moderator moderator = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, movieReview.getId(), moderator);

        complaintService.deleteComplaint(complaint.getId());

        Assert.assertFalse(complaintRepository.existsById(complaint.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
        Assert.assertTrue(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertTrue(moderatorRepository.existsById(moderator.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteComplaintNotFound() {
        complaintService.deleteComplaint(UUID.randomUUID());
    }

    @Test
    public void testPutComplaint() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(registeredUser);
        MovieReview mr2 = testObjectsFactory.createMovieReview(registeredUser);
        Moderator m1 = testObjectsFactory.createModerator();
        Moderator m2 = testObjectsFactory.createModerator();
        Complaint complaint = testObjectsFactory.createComplaint(
                registeredUser, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), m1);

        ComplaintPutDTO put = new ComplaintPutDTO();
        put.setText("sds");
        put.setType(ComplaintType.SWEARING);
        put.setComplaintObjectType(ComplaintObjectType.ROLE_REVIEW);
        put.setComplaintObjectId(mr2.getId());
        put.setModeratorId(m2.getId());
        complaintService.updateComplaint(complaint.getId(), put);

        ComplaintReadDTO read = complaintService.getComplaint(complaint.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        complaint = complaintRepository.findById(complaint.getId()).get();
        Assertions.assertThat(complaint).isEqualToIgnoringGivenFields(read,
                "registeredUser", "moderator");
        Assert.assertEquals(complaint.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(complaint.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testGetComplaintWithEmptyFilter() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1);
        Complaint c2 = testObjectsFactory.createComplaint(ru2, ComplaintObjectType.ROLE_REVIEW, mr.getId(), mod2);
        Complaint c3 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1);

        ComplaintFilter filter = new ComplaintFilter();
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(c1.getId(), c2.getId(), c3.getId());
    }

    @Test
    public void testGetComplaintByType() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1, ComplaintType.SPAM);
        testObjectsFactory.createComplaint(
                ru2, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod2, ComplaintType.SPOILER);
        Complaint c3 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1, ComplaintType.SPAM);

        ComplaintFilter filter = new ComplaintFilter();
        filter.setType(ComplaintType.SPAM);
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(c1.getId(), c3.getId());
    }

    @Test
    public void testGetComplaintByStatus() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1, ComplaintStatus.MODERATION);
        testObjectsFactory.createComplaint(
                ru2, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod2, ComplaintStatus.APPROVED);
        Complaint c3 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1, ComplaintStatus.MODERATION);

        ComplaintFilter filter = new ComplaintFilter();
        filter.setComplaintStatus(ComplaintStatus.MODERATION);
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(c1.getId(), c3.getId());
    }

    @Test
    public void testGetComplaintByModerator() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1, ComplaintType.SPAM);
        testObjectsFactory.createComplaint(
                ru2, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod2, ComplaintType.SPOILER);
        Complaint c3 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1, ComplaintType.SPAM);

        ComplaintFilter filter = new ComplaintFilter();
        filter.setModeratorId(mod1.getId());
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(c1.getId(), c3.getId());
    }

    @Test
    public void testGetComplaintByComplaintObjectId() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), mod1);
        testObjectsFactory.createComplaint(ru2, ComplaintObjectType.ROLE_REVIEW, mr2.getId(), mod2);
        Complaint c3 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), mod1);

        ComplaintFilter filter = new ComplaintFilter();
        filter.setComplaintObjectId(mr1.getId());
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(c1.getId(), c3.getId());
    }

    @Test
    public void testGetComplaintByCreatedAtInterval() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1);
        c1.setCreatedAt(testObjectsFactory.createInstant(2));
        c1 = complaintRepository.save(c1);

        Complaint c2 = testObjectsFactory.createComplaint(ru2, ComplaintObjectType.ROLE_REVIEW, mr.getId(), mod2);
        c2.setCreatedAt(testObjectsFactory.createInstant(7));
        complaintRepository.save(c2);

        Complaint c3 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1);
        c3.setCreatedAt(testObjectsFactory.createInstant(4));
        c3 = complaintRepository.save(c3);

        ComplaintFilter filter = new ComplaintFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(c1.getId(), c3.getId());
    }

    @Test
    public void testGetMovieByAllFilters() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr1 = testObjectsFactory.createMovieReview(ru1);
        MovieReview mr2 = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), mod1, ComplaintType.SPAM);
        c1.setCreatedAt(testObjectsFactory.createInstant(2));
        c1 = complaintRepository.save(c1);

        Complaint c2 = testObjectsFactory.createComplaint(
                ru2, ComplaintObjectType.ROLE_REVIEW, mr2.getId(), mod2, ComplaintType.SPOILER);
        c2.setCreatedAt(testObjectsFactory.createInstant(7));
        complaintRepository.save(c2);

        Complaint c3 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr1.getId(), mod1, ComplaintType.SPAM);
        c3.setCreatedAt(testObjectsFactory.createInstant(4));
        c3 = complaintRepository.save(c3);

        ComplaintFilter filter = new ComplaintFilter();
        filter.setType(ComplaintType.SPAM);
        filter.setComplaintObjectId(mr1.getId());
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        filter.setModeratorId(mod1.getId());
        Assertions.assertThat(complaintService.getComplaint(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(c1.getId(), c3.getId());
    }

    @Test
    public void testGetComplaintWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        MovieReview mr = testObjectsFactory.createMovieReview(ru1);
        Moderator mod1 = testObjectsFactory.createModerator();
        Moderator mod2 = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1);
        Complaint c2 = testObjectsFactory.createComplaint(ru2, ComplaintObjectType.ROLE_REVIEW, mr.getId(), mod2);
        testObjectsFactory.createComplaint(ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), mod1);

        ComplaintFilter filter = new ComplaintFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                complaintService.getComplaint(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(c1.getId(), c2.getId()));
    }

    @Test
    public void testProcessingComplaint() {
        RegisteredUser ru1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser ru3 = testObjectsFactory.createRegisteredUser(AccountStatus.VALID, true);
        MovieReview mr = testObjectsFactory.createMovieReview(ru3);
        Moderator moderator = testObjectsFactory.createModerator();

        Complaint c1 = testObjectsFactory.createComplaint(
                ru1, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), ComplaintType.SWEARING);
        Complaint c2 = testObjectsFactory.createComplaint(
                ru2, ComplaintObjectType.MOVIE_REVIEW, mr.getId(), ComplaintType.SWEARING);

        ComplaintProcessingDTO processingDTO = new ComplaintProcessingDTO();
        processingDTO.setModeratorId(moderator.getId());
        processingDTO.setNewStatus(ComplaintStatus.APPROVED);

        ComplaintReadDTO complaintReadDTO = complaintService.processingComplaint(c1.getId(), processingDTO);
        Assert.assertEquals(moderator.getId(), complaintReadDTO.getModeratorId());
        Assert.assertEquals(ComplaintStatus.APPROVED, complaintReadDTO.getComplaintStatus());
        Assert.assertNotNull(complaintReadDTO.getProcessedAt());

        c2 = complaintRepository.findById(c2.getId()).get();
        Assert.assertEquals(moderator.getId(), c2.getModerator().getId());
        Assert.assertEquals(ComplaintStatus.APPROVED, c2.getComplaintStatus());
        Assert.assertNotNull(c2.getProcessedAt());

        RegisteredUser ru3AfterSanctions = registeredUserRepository.findById(ru3.getId()).get();
        Assert.assertEquals(AccountStatus.BLOCKED, ru3AfterSanctions.getStatus());
        Assert.assertEquals(false, ru3AfterSanctions.getTrust());
    }
}