package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.role.RoleFilter;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.ActorRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.RoleRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.UUID;

public class RoleServiceTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);

        RoleReadDTO readDTO = roleService.getRole(role.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(role,
                "actorId", "movieId");
        Assert.assertEquals(readDTO.getActorId(), role.getActor().getId());
        Assert.assertEquals(readDTO.getMovieId(), role.getMovie().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleWrongId() {
        roleService.getRole(UUID.randomUUID());
    }

    @Test
    public void testPatchRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, a1);

        RolePatchDTO patch = new RolePatchDTO();
        patch.setActorId(a2.getId());
        patch.setRoleName("asdad");
        patch.setGender(Gender.FEMALE);
        RoleReadDTO read = roleService.patchRole(role.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read,
                "actor", "movie");
        Assert.assertEquals(role.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(role.getActor().getId(), read.getActorId());
    }

    @Test
    public void testPatchRoleEmptyPatch() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);
        RolePatchDTO patch = new RolePatchDTO();

        RoleReadDTO read = roleService.patchRole(role.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("averageMark");

        Role roleAfterUpdate = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(roleAfterUpdate).hasNoNullFieldsOrPropertiesExcept("averageMark");

        Assertions.assertThat(role).isEqualToIgnoringGivenFields(roleAfterUpdate,
                "actor", "movie");
        Assert.assertEquals(role.getActor().getId(), roleAfterUpdate.getActor().getId());
        Assert.assertEquals(role.getMovie().getId(), roleAfterUpdate.getMovie().getId());
    }

    @Test
    public void testDeleteRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor actor = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, actor);

        roleService.deleteRole(role.getId());
        Assert.assertFalse(roleRepository.existsById(role.getId()));
        Assert.assertTrue(actorRepository.existsById(actor.getId()));
        Assert.assertTrue(movieRepository.existsById(movie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMarkActorNotFound() {
        roleService.deleteRole(UUID.randomUUID());
    }

    @Test
    public void testPutRole() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Role role = testObjectsFactory.createRole(movie, a1);

        RolePutDTO put = new RolePutDTO();
        put.setActorId(a2.getId());
        put.setRoleName("asdad");
        roleService.updateRole(role.getId(), put);

        RoleReadDTO read = roleService.getRole(role.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read,
                "actor", "movie");
        Assert.assertEquals(role.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(role.getActor().getId(), read.getActorId());
    }

    @Test
    public void testGetRolesWithEmptyFilter() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Actor a3 = testObjectsFactory.createActor();

        Role r1 = testObjectsFactory.createRole(movie, a1);
        Role r2 = testObjectsFactory.createRole(movie, a2);
        Role r3 = testObjectsFactory.createRole(movie, a3);

        RoleFilter filter = new RoleFilter();
        Assertions.assertThat(roleService.getRole(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r2.getId(), r3.getId());
    }

    @Test
    public void testGetRoleByActor() {
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        Role r1 = testObjectsFactory.createRole(m1, a1);
        testObjectsFactory.createRole(m1, a2);
        Role r2 = testObjectsFactory.createRole(m2, a1);

        RoleFilter filter = new RoleFilter();
        filter.setActorId(a1.getId());
        Assertions.assertThat(roleService.getRole(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r2.getId());
    }

    @Test
    public void testGetRoleByMovie() {
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        Role r1 = testObjectsFactory.createRole(m1, a2);
        testObjectsFactory.createRole(m2, a1);
        Role r2 = testObjectsFactory.createRole(m1, a1);

        RoleFilter filter = new RoleFilter();
        filter.setMovieId(m1.getId());
        Assertions.assertThat(roleService.getRole(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r2.getId());
    }

    @Test
    public void testGetRoleByAverageMarkInterval() {
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        Role r1 = testObjectsFactory.createRole(m1, a1, 7.3);
        testObjectsFactory.createRole(m1, a2, 4.9);
        Role r3 = testObjectsFactory.createRole(m2, a1, 9.1);


        RoleFilter filter = new RoleFilter();
        filter.setAverageMarkFrom(5.0);
        filter.setAverageMarkTo(10.0);

        Assertions.assertThat(roleService.getRole(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r3.getId());
    }

    @Test
    public void testGetRoleByAllFilters() {
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();

        Role r1 = testObjectsFactory.createRole(m1, a1, 7.3);
        testObjectsFactory.createRole(m1, a2, 4.9);
        Role r3 = testObjectsFactory.createRole(m2, a1, 9.1);

        RoleFilter filter = new RoleFilter();
        filter.setActorId(a1.getId());
        filter.setAverageMarkFrom(5.0);
        filter.setAverageMarkTo(10.0);
        Assertions.assertThat(roleService.getRole(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r3.getId());
    }

    @Test
    public void testUpdateAverageMarkOfRole() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Role r1 = testObjectsFactory.createRole();

        testObjectsFactory.createRoleMark(ru, r1, 5);
        testObjectsFactory.createRoleMark(ru, r1, 10);

        roleService.updateAverageMarkOfRole(r1.getId());
        r1 = roleRepository.findById(r1.getId()).get();
        Assert.assertEquals(7.5, r1.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    public void testGetRoleWithEmptyFilterWithPagingAndSorting() {
        Movie movie = testObjectsFactory.createMovie();
        Actor a1 = testObjectsFactory.createActor();
        Actor a2 = testObjectsFactory.createActor();
        Actor a3 = testObjectsFactory.createActor();

        Role r1 = testObjectsFactory.createRole(movie, a1, 4.0);
        Role r2 = testObjectsFactory.createRole(movie, a2, 7.0);
        testObjectsFactory.createRole(movie, a3, 9.0);

        RoleFilter filter = new RoleFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "averageMark"));
        Assertions.assertThat(
                roleService.getRole(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(r1.getId(), r2.getId()));
    }
}