package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewCreateDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPutDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserMovieReviewServiceTest extends BaseTest {

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private RegisteredUserMovieReviewService registeredUserMovieReviewService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        MovieReviewReadDTO readDTO = registeredUserMovieReviewService.getMovieReview(
                registeredUser.getId(), movieReview.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movieReview,
                "registeredUserId", "movieId", "moderatorId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), movieReview.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getMovieId(), movieReview.getMovie().getId());
        Assert.assertEquals(readDTO.getModeratorId(), movieReview.getModerator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieReviewWrongId() {
        registeredUserMovieReviewService.getMovieReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateMovieReviewWithWrongMovie() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MovieReviewCreateDTO create = new MovieReviewCreateDTO();
        create.setText("qwe");
        create.setModeratorId(UUID.randomUUID());
        create.setMovieId(UUID.randomUUID());

        registeredUserMovieReviewService.createMovieReview(registeredUser.getId(), create);
    }

    @Test
    public void testCreateMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReviewCreateDTO create = new MovieReviewCreateDTO();
        create.setText("qwe");
        create.setModeratorId(moderator.getId());
        create.setMovieId(movie.getId());

        MovieReviewReadDTO read = registeredUserMovieReviewService.createMovieReview(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MovieReview movieReview = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(movieReview,
                "movieId", "registeredUserId", "moderatorId");
        Assert.assertEquals(read.getRegisteredUserId(), movieReview.getRegisteredUser().getId());
        Assert.assertEquals(read.getMovieId(), movieReview.getMovie().getId());
        Assert.assertEquals(read.getModeratorId(), movieReview.getModerator().getId());
    }

    @Test
    public void testPatchMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        MovieReviewPatchDTO patch = new MovieReviewPatchDTO();
        patch.setText("asd");
        patch.setReviewStatus(ReviewStatus.CANCELED);
        MovieReviewReadDTO read = registeredUserMovieReviewService.patchMovieReview(
                registeredUser.getId(), movieReview.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movieReview = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReview).isEqualToIgnoringGivenFields(read,
                "registeredUser", "movie", "moderator");
        Assert.assertEquals(movieReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieReview.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(movieReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testPatchMovieReviewEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);
        MovieReviewPatchDTO patch = new MovieReviewPatchDTO();

        MovieReviewReadDTO read = registeredUserMovieReviewService.patchMovieReview(
                registeredUser.getId(), movieReview.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        MovieReview movieReviewAfterUpdate = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movieReview).isEqualToIgnoringGivenFields(movieReviewAfterUpdate,
                "registeredUser", "movie", "moderator");
        Assert.assertEquals(
                movieReview.getRegisteredUser().getId(), movieReviewAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(movieReview.getMovie().getId(), movieReviewAfterUpdate.getMovie().getId());
        Assert.assertEquals(movieReview.getModerator().getId(), movieReviewAfterUpdate.getModerator().getId());
    }

    @Test
    public void testDeleteMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        registeredUserMovieReviewService.deleteMovieReview(registeredUser.getId(), movieReview.getId());
        Assert.assertFalse(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertTrue(movieRepository.existsById(movie.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieReviewNotFound() {
        registeredUserMovieReviewService.deleteMovieReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testPutMovieReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        Moderator moderator = testObjectsFactory.createModerator();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser, movie, moderator);

        MovieReviewPutDTO put = new MovieReviewPutDTO();
        put.setText("rrr");
        put.setReviewStatus(ReviewStatus.CANCELED);
        registeredUserMovieReviewService.updateMovieReview(registeredUser.getId(), movieReview.getId(), put);

        MovieReviewReadDTO read = registeredUserMovieReviewService.getMovieReview(
                registeredUser.getId(), movieReview.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        movieReview = movieReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReview).isEqualToIgnoringGivenFields(read,
                "registeredUser", "movie", "moderator");
        Assert.assertEquals(movieReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieReview.getMovie().getId(), read.getMovieId());
        Assert.assertEquals(movieReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testGetRegisteredUserMovieReviews() {
        Movie m = testObjectsFactory.createMovie();
        Moderator mod = testObjectsFactory.createModerator();
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();

        MovieReview mr1 = testObjectsFactory.createMovieReview(r1, m, mod);
        testObjectsFactory.createMovieReview(r2, m, mod);
        testObjectsFactory.createMovieReview(r3, m, mod);
        MovieReview mr2 = testObjectsFactory.createMovieReview(r1, m, mod);
        MovieReview mr3 = testObjectsFactory.createMovieReview(r1, m, mod);

        List<MovieReviewReadDTO> movieReviewsReadDTO = registeredUserMovieReviewService
                .getRegisteredUserMovieReviews(r1.getId());
        Assertions.assertThat(movieReviewsReadDTO)
                .extracting(MovieReviewReadDTO::getId).isEqualTo(Arrays.asList(mr1.getId(), mr2.getId(), mr3.getId()));
    }
}