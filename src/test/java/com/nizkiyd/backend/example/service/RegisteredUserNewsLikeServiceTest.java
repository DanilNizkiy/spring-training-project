package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeCreateDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePutDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.NewsLikeRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserNewsLikeServiceTest extends BaseTest {

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private RegisteredUserNewsLikeService registeredUserNewsLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        NewsLikeReadDTO readDTO = registeredUserNewsLikeService.getNewsLike(
                registeredUser.getId(), newsLike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(newsLike,
                "registeredUserId", "newsId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), newsLike.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getNewsId(), newsLike.getNews().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsLikeWrongId() {
        registeredUserNewsLikeService.getNewsLike(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateNewsLikeWithWrongNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        NewsLikeCreateDTO create = new NewsLikeCreateDTO();
        create.setLike(true);
        create.setNewsId(UUID.randomUUID());

        registeredUserNewsLikeService.createNewsLike(registeredUser.getId(), create);
    }

    @Test
    public void testCreateNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLikeCreateDTO create = new NewsLikeCreateDTO();
        create.setLike(true);
        create.setNewsId(news.getId());

        NewsLikeReadDTO read = registeredUserNewsLikeService
                .createNewsLike(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        NewsLike newsLike = newsLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(newsLike,
                "newsId", "registeredUserId");
        Assert.assertEquals(read.getRegisteredUserId(), newsLike.getRegisteredUser().getId());
        Assert.assertEquals(read.getNewsId(), newsLike.getNews().getId());
    }

    @Test
    public void testPatchNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        NewsLikePatchDTO patch = new NewsLikePatchDTO();
        patch.setLike(true);
        NewsLikeReadDTO read = registeredUserNewsLikeService.patchNewsLike(
                registeredUser.getId(), newsLike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        newsLike = newsLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLike).isEqualToIgnoringGivenFields(read,
                "registeredUser", "news");
        Assert.assertEquals(newsLike.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(newsLike.getNews().getId(), read.getNewsId());
    }

    @Test
    public void testPatchNewsLikeEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);
        NewsLikePatchDTO patch = new NewsLikePatchDTO();

        NewsLikeReadDTO read = registeredUserNewsLikeService.patchNewsLike(
                registeredUser.getId(), newsLike.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        NewsLike newsLikeAfterUpdate = newsLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLikeAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(newsLike).isEqualToIgnoringGivenFields(newsLikeAfterUpdate,
                "registeredUser", "news");
        Assert.assertEquals(newsLike.getRegisteredUser().getId(), newsLikeAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(newsLike.getNews().getId(), newsLikeAfterUpdate.getNews().getId());
    }

    @Test
    public void testDeleteNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();
        NewsLike newsLike = testObjectsFactory.createNewsLike(registeredUser, news);

        registeredUserNewsLikeService.deleteNewsLike(registeredUser.getId(), newsLike.getId());
        Assert.assertFalse(newsLikeRepository.existsById(newsLike.getId()));
        Assert.assertTrue(newsRepository.existsById(news.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsLikeNotFound() {
        registeredUserNewsLikeService.deleteNewsLike(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testPutNewsLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        News news = testObjectsFactory.createNews();

        NewsLike newsLike = new NewsLike();
        newsLike.setNews(news);
        newsLike.setRegisteredUser(registeredUser);
        newsLike.setLike(true);
        newsLike = newsLikeRepository.save(newsLike);

        NewsLikePutDTO put = new NewsLikePutDTO();
        put.setLike(null);
        registeredUserNewsLikeService.updateNewsLike(registeredUser.getId(), newsLike.getId(), put);

        NewsLikeReadDTO read = registeredUserNewsLikeService.getNewsLike(registeredUser.getId(), newsLike.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        newsLike = newsLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLike).isEqualToIgnoringGivenFields(read, "registeredUser", "news");
    }

    @Test
    public void testGetRegisteredUserNewsLikes() {
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();
        News n = testObjectsFactory.createNews();

        NewsLike nl1 = testObjectsFactory.createNewsLike(r1, n);
        testObjectsFactory.createNewsLike(r2, n);
        testObjectsFactory.createNewsLike(r3, n);
        NewsLike nl2 = testObjectsFactory.createNewsLike(r1, n);
        NewsLike nl3 = testObjectsFactory.createNewsLike(r1, n);

        List<NewsLikeReadDTO> newsLikesReadDTO = registeredUserNewsLikeService
                .getRegisteredUserNewsLikes(r1.getId());
        Assertions.assertThat(newsLikesReadDTO)
                .extracting(NewsLikeReadDTO::getId).isEqualTo(Arrays.asList(
                nl1.getId(), nl2.getId(), nl3.getId()));
    }
}