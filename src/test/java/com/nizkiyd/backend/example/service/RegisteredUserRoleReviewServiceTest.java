package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewCreateDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPatchDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPutDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.RoleRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.repository.RoleReviewRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserRoleReviewServiceTest extends BaseTest {

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private RegisteredUserRoleReviewService registeredUserRoleReviewService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewReadDTO readDTO = registeredUserRoleReviewService.getRoleReview(
                registeredUser.getId(), roleReview.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(roleReview,
                "registeredUserId", "roleId", "moderatorId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), roleReview.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getRoleId(), roleReview.getRole().getId());
        Assert.assertEquals(readDTO.getModeratorId(), roleReview.getModerator().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleReviewWrongId() {
        registeredUserRoleReviewService.getRoleReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleReviewWithWrongRole() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RoleReviewCreateDTO create = new RoleReviewCreateDTO();
        create.setText("qwe");
        create.setModeratorId(UUID.randomUUID());
        create.setRoleId(UUID.randomUUID());

        registeredUserRoleReviewService.createRoleReview(registeredUser.getId(), create);
    }

    @Test
    public void testCreateRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReviewCreateDTO create = new RoleReviewCreateDTO();
        create.setText("qwe");
        create.setModeratorId(moderator.getId());
        create.setRoleId(role.getId());

        RoleReviewReadDTO read = registeredUserRoleReviewService.createRoleReview(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RoleReview roleReview = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(roleReview,
                "roleId", "registeredUserId", "moderatorId");
        Assert.assertEquals(read.getRegisteredUserId(), roleReview.getRegisteredUser().getId());
        Assert.assertEquals(read.getRoleId(), roleReview.getRole().getId());
        Assert.assertEquals(read.getModeratorId(), roleReview.getModerator().getId());
    }

    @Test
    public void testPatchRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewPatchDTO patch = new RoleReviewPatchDTO();
        patch.setText("asd");
        patch.setReviewStatus(ReviewStatus.CANCELED);
        RoleReviewReadDTO read = registeredUserRoleReviewService.patchRoleReview(
                registeredUser.getId(), roleReview.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        roleReview = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReview).isEqualToIgnoringGivenFields(read,
                "registeredUser", "role", "moderator");
        Assert.assertEquals(roleReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleReview.getRole().getId(), read.getRoleId());
        Assert.assertEquals(roleReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testPatchRoleReviewEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);
        RoleReviewPatchDTO patch = new RoleReviewPatchDTO();

        RoleReviewReadDTO read = registeredUserRoleReviewService.patchRoleReview(
                registeredUser.getId(), roleReview.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RoleReview roleReviewAfterUpdate = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReviewAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(roleReview).isEqualToIgnoringGivenFields(roleReviewAfterUpdate,
                "registeredUser", "role", "moderator");
        Assert.assertEquals(roleReview.getRegisteredUser().getId(), roleReviewAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(roleReview.getRole().getId(), roleReviewAfterUpdate.getRole().getId());
        Assert.assertEquals(roleReview.getModerator().getId(), roleReviewAfterUpdate.getModerator().getId());
    }

    @Test
    public void testDeleteRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        registeredUserRoleReviewService.deleteRoleReview(registeredUser.getId(), roleReview.getId());
        Assert.assertFalse(roleReviewRepository.existsById(roleReview.getId()));
        Assert.assertTrue(roleRepository.existsById(role.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRoleReviewNotFound() {
        registeredUserRoleReviewService.deleteRoleReview(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testPutRoleReview() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Role role = testObjectsFactory.createRole();
        Moderator moderator = testObjectsFactory.createModerator();
        RoleReview roleReview = testObjectsFactory.createRoleReview(registeredUser, role, moderator);

        RoleReviewPutDTO put = new RoleReviewPutDTO();
        put.setText("rrr");
        put.setReviewStatus(ReviewStatus.CANCELED);
        registeredUserRoleReviewService.updateRoleReview(registeredUser.getId(), roleReview.getId(), put);

        RoleReviewReadDTO read = registeredUserRoleReviewService.getRoleReview(
                registeredUser.getId(), roleReview.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        roleReview = roleReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(roleReview).isEqualToIgnoringGivenFields(read,
                "registeredUser", "role", "moderator");
        Assert.assertEquals(roleReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(roleReview.getRole().getId(), read.getRoleId());
        Assert.assertEquals(roleReview.getModerator().getId(), read.getModeratorId());
    }

    @Test
    public void testGetRegisteredUserRoleReviews() {
        Role m = testObjectsFactory.createRole();
        Moderator mod = testObjectsFactory.createModerator();
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();

        RoleReview mr1 = testObjectsFactory.createRoleReview(r1, m, mod);
        testObjectsFactory.createRoleReview(r2, m, mod);
        testObjectsFactory.createRoleReview(r3, m, mod);
        RoleReview mr2 = testObjectsFactory.createRoleReview(r1, m, mod);
        RoleReview mr3 = testObjectsFactory.createRoleReview(r1, m, mod);

        List<RoleReviewReadDTO> roleReviewsReadDTO = registeredUserRoleReviewService
                .getRegisteredUserRoleReviews(r1.getId());
        Assertions.assertThat(roleReviewsReadDTO)
                .extracting(RoleReviewReadDTO::getId).isEqualTo(Arrays.asList(mr1.getId(), mr2.getId(), mr3.getId()));
    }
}