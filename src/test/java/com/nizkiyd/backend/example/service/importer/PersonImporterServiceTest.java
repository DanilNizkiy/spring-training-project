package com.nizkiyd.backend.example.service.importer;

import com.nizkiyd.backend.example.client.themoviedb.TheMovieDbClient;
import com.nizkiyd.backend.example.client.themoviedb.dto.*;
import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.exception.ImportAlreadyPerformedException;
import com.nizkiyd.backend.example.repository.CrewMemberRepository;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.PersonRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.UUID;

public class PersonImporterServiceTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private MovieRepository movieRepository;

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private PersonImporterService personImporterService;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    @Test
    public void testPersonImport() throws ImportAlreadyPerformedException {
        String personExternalId = "id1";

        PersonReadDTO personReadDTO = generator.generateRandomObject(PersonReadDTO.class);

        Mockito.when(movieDbClient.getPerson(personExternalId)).thenReturn(personReadDTO);

        UUID personId = personImporterService.importPerson(personExternalId);
        Person person = personRepository.findById(personId).get();

        Assert.assertEquals(personReadDTO.getName(), person.getName());
        Assert.assertEquals(Gender.fromInteger(personReadDTO.getGender()), person.getGender());
        Assert.assertEquals(personReadDTO.getBiography(), person.getBiography());
        Assert.assertEquals(personReadDTO.getAdult(), person.getAdult());
        Assert.assertEquals(personReadDTO.getBirthday(), person.getBirthday());
        Assert.assertEquals(personReadDTO.getPlaceOfBirth(), person.getPlaceOfBirth());
    }
}