package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.repository.MovieReviewLikeRepository;
import com.nizkiyd.backend.example.repository.MovieReviewRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RegisteredUserMovieReviewLikeServiceTest extends BaseTest {

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private RegisteredUserMovieReviewLikeService registeredUserMovieReviewLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        MovieReviewLikeReadDTO readDTO = registeredUserMovieReviewLikeService.getMovieReviewLike(
                registeredUser.getId(), movieReviewLike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movieReviewLike,
                "registeredUserId", "movieReviewId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), movieReviewLike.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getMovieReviewId(), movieReviewLike.getMovieReview().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieReviewLikeWrongId() {
        registeredUserMovieReviewLikeService.getMovieReviewLike(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateMovieReviewLikeWithWrongMovie() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MovieReviewLikeCreateDTO create = new MovieReviewLikeCreateDTO();
        create.setLike(true);
        create.setMovieReviewId(UUID.randomUUID());

        registeredUserMovieReviewLikeService.createMovieReviewLike(registeredUser.getId(), create);
    }

    @Test
    public void testCreateMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLikeCreateDTO create = new MovieReviewLikeCreateDTO();
        create.setLike(true);
        create.setMovieReviewId(movieReview.getId());

        MovieReviewLikeReadDTO read = registeredUserMovieReviewLikeService
                .createMovieReviewLike(registeredUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MovieReviewLike movieReviewLike = movieReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(movieReviewLike,
                "movieReviewId", "registeredUserId");
        Assert.assertEquals(read.getRegisteredUserId(), movieReviewLike.getRegisteredUser().getId());
        Assert.assertEquals(read.getMovieReviewId(), movieReviewLike.getMovieReview().getId());
    }

    @Test
    public void testPatchMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        MovieReviewLikePatchDTO patch = new MovieReviewLikePatchDTO();
        patch.setLike(true);
        MovieReviewLikeReadDTO read = registeredUserMovieReviewLikeService.patchMovieReviewLike(
                registeredUser.getId(), movieReviewLike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movieReviewLike = movieReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewLike).isEqualToIgnoringGivenFields(read,
                "registeredUser", "movieReview");
        Assert.assertEquals(movieReviewLike.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(movieReviewLike.getMovieReview().getId(), read.getMovieReviewId());
    }

    @Test
    public void testPatchMovieReviewLikeEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);
        MovieReviewLikePatchDTO patch = new MovieReviewLikePatchDTO();

        MovieReviewLikeReadDTO read = registeredUserMovieReviewLikeService.patchMovieReviewLike(
                registeredUser.getId(), movieReviewLike.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        MovieReviewLike movieReviewLikeAfterUpdate = movieReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewLikeAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(movieReviewLike).isEqualToIgnoringGivenFields(movieReviewLikeAfterUpdate,
                "registeredUser", "movieReview");
        Assert.assertEquals(
                movieReviewLike.getRegisteredUser().getId(), movieReviewLikeAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(
                movieReviewLike.getMovieReview().getId(), movieReviewLikeAfterUpdate.getMovieReview().getId());
    }

    @Test
    public void testDeleteMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);
        MovieReviewLike movieReviewLike = testObjectsFactory.createMovieReviewLike(registeredUser, movieReview);

        registeredUserMovieReviewLikeService.deleteMovieReviewLike(registeredUser.getId(), movieReviewLike.getId());
        Assert.assertFalse(movieReviewLikeRepository.existsById(movieReviewLike.getId()));
        Assert.assertTrue(movieReviewRepository.existsById(movieReview.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieReviewLikeNotFound() {
        registeredUserMovieReviewLikeService.deleteMovieReviewLike(UUID.randomUUID(), UUID.randomUUID());
    }

    @Test
    public void testGetRegisteredUserMovieReviewLikes() {
        RegisteredUser r1 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r2 = testObjectsFactory.createRegisteredUser();
        RegisteredUser r3 = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(r1);

        MovieReviewLike mrl1 = testObjectsFactory.createMovieReviewLike(r1, movieReview);
        testObjectsFactory.createMovieReviewLike(r2, movieReview);
        testObjectsFactory.createMovieReviewLike(r3, movieReview);
        MovieReviewLike mrl2 = testObjectsFactory.createMovieReviewLike(r1, movieReview);
        MovieReviewLike mrl3 = testObjectsFactory.createMovieReviewLike(r1, movieReview);

        List<MovieReviewLikeReadDTO> movieReviewLikesReadDTO = registeredUserMovieReviewLikeService
                .getRegisteredUserMovieReviewLikes(r1.getId());
        Assertions.assertThat(movieReviewLikesReadDTO)
                .extracting(MovieReviewLikeReadDTO::getId).isEqualTo(Arrays.asList(
                mrl1.getId(), mrl2.getId(), mrl3.getId()));
    }

    @Test
    public void testPutMovieReviewLike() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        MovieReview movieReview = testObjectsFactory.createMovieReview(registeredUser);

        MovieReviewLike movieReviewLike = new MovieReviewLike();
        movieReviewLike.setMovieReview(movieReview);
        movieReviewLike.setRegisteredUser(registeredUser);
        movieReviewLike.setLike(true);
        movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);

        MovieReviewLikePutDTO put = new MovieReviewLikePutDTO();
        put.setLike(false);
        registeredUserMovieReviewLikeService.updateMovieReviewLike(
                registeredUser.getId(), movieReviewLike.getId(), put);

        MovieReviewLikeReadDTO read = registeredUserMovieReviewLikeService.getMovieReviewLike(registeredUser.getId(),
                movieReviewLike.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        movieReviewLike = movieReviewLikeRepository.findById(read.getId()).get();
        Assertions.assertThat(movieReviewLike).isEqualToIgnoringGivenFields(read, "registeredUser", "movieReview");
    }
}