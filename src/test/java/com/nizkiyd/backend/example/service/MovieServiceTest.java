package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.movie.*;
import com.nizkiyd.backend.example.dto.movie.MovieFilter;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.LinkDuplicatedException;
import com.nizkiyd.backend.example.job.UpdateAverageMarkOfMoviesJob;
import com.nizkiyd.backend.example.repository.MovieRepository;
import com.nizkiyd.backend.example.repository.NewsRepository;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MovieServiceTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageMarkOfMoviesJob updateAverageMarkOfMoviesJob;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testGetMovie() {
        Movie movie = testObjectsFactory.createMovie();

        MovieReadDTO readDTO = movieService.getMovie(movie.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(movie);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieWrongId() {
        movieService.getMovie(UUID.randomUUID());
    }

    @Test
    public void testCreateMovie() {
        MovieCreateDTO create = new MovieCreateDTO();
        create.setTitle("qwe");
        create.setReleaseDate(LocalDate.now());
        create.setCountry("qwe");
        create.setDuration("qwe");
        create.setMovieStatus(MovieStatus.RELEASED);

        MovieReadDTO read = movieService.createMovie(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Movie movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(movie);
    }

    @Test
    public void testPatchMovie() {
        Movie movie = testObjectsFactory.createMovie();

        MoviePatchDTO patch = new MoviePatchDTO();
        patch.setTitle("asd");
        patch.setReleaseDate(LocalDate.now());
        patch.setCountry("asd");
        patch.setDuration("asd");
        patch.setAverageMark(7.7);
        patch.setBudget(123456);
        patch.setMovieStatus(MovieStatus.RELEASED);
        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movie).isEqualToIgnoringGivenFields(read,
                "roles", "movies", "genres", "news", "crewMembers");
    }

    @Test
    public void testPatchMovieEmptyPatch() {
        Movie movie = testObjectsFactory.createMovie();
        MoviePatchDTO patch = new MoviePatchDTO();

        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("averageMark");

        Movie movieAfterUpdate = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movieAfterUpdate).hasNoNullFieldsOrPropertiesExcept("averageMark");

        Assertions.assertThat(movie).isEqualToIgnoringGivenFields(movieAfterUpdate,
                "roles", "movies", "genres", "news", "crewMembers");
    }

    @Test
    public void testDeleteMovie() {
        Movie movie = testObjectsFactory.createMovie();

        movieService.deleteMovie(movie.getId());
        Assert.assertFalse(movieRepository.existsById(movie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieNotFound() {
        movieService.deleteMovie(UUID.randomUUID());
    }

    @Test
    public void testPutMovie() {
        Movie movie = testObjectsFactory.createMovie();

        MoviePutDTO put = new MoviePutDTO();
        put.setTitle("rrr");
        put.setReleaseDate(LocalDate.now());
        put.setCountry("rrr");
        put.setDuration("rrr");
        put.setAverageMark(4.8);
        put.setMovieStatus(MovieStatus.RELEASED);
        put.setBudget(7733);
        movieService.updateMovie(movie.getId(), put);

        MovieReadDTO read = movieService.getMovie(movie.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movie).isEqualToIgnoringGivenFields(read,
                "roles", "movies", "genres", "news", "crewMembers");
    }

    @Test
    public void testGetStatusMovie() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED);
        testObjectsFactory.createMovie(MovieStatus.RELEASED);
        Movie m2 = testObjectsFactory.createMovie(MovieStatus.CANCELED);
        testObjectsFactory.createMovie(MovieStatus.RELEASED);

        List<Movie> movies = movieRepository.findByMovieStatus(MovieStatus.CANCELED);

        Assertions.assertThat(movies).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m2.getId());
    }

    @Test
    public void testGetMovieWithEmptyFilter() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 9.3);
        Movie m2 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 8.0);
        Movie m3 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 4.6);

        MovieFilter filter = new MovieFilter();
        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m2.getId(), m3.getId());
    }

    @Test
    public void testGetMovieByMovieStatus() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 9.3);
        testObjectsFactory.createMovie(MovieStatus.RELEASED, 8.0);
        Movie m3 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 4.6);

        MovieFilter filter = new MovieFilter();
        filter.setMovieStatus(MovieStatus.CANCELED);
        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m3.getId());
    }

    @Test
    public void testGetMovieByNewsId() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 9.3);
        testObjectsFactory.createMovie(MovieStatus.RELEASED, 8.0);
        Movie m2 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 4.6);

        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        movieService.addMovieToNews(n1.getId(), m1.getId());
        movieService.addMovieToNews(n1.getId(), m2.getId());
        movieService.addMovieToNews(n2.getId(), m1.getId());
        movieService.addMovieToNews(n2.getId(), m2.getId());

        MovieFilter filter = new MovieFilter();
        filter.setNewsId(n1.getId());
        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m2.getId());
    }

    @Test
    public void testGetMovieByTitle() {
        Movie m1 = testObjectsFactory.createMovie("title", MovieStatus.RELEASED);
        testObjectsFactory.createMovie("abc", MovieStatus.RELEASED);
        Movie m3 = testObjectsFactory.createMovie("title", MovieStatus.RELEASED);

        MovieFilter filter = new MovieFilter();
        filter.setTitle("title");
        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m3.getId());
    }

    @Test
    public void testGetMovieByAverageMarkInterval() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 5.0);
        testObjectsFactory.createMovie(MovieStatus.RELEASED, 4.9);
        Movie m3 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 6.5);

        MovieFilter filter = new MovieFilter();
        filter.setAverageMarkFrom(5.0);
        filter.setAverageMarkTo(8.0);
        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m3.getId());
    }

    @Test
    public void testGetMovieByCreatedAtInterval() {
        Movie m1 = testObjectsFactory.createMovie();
        m1.setCreatedAt(testObjectsFactory.createInstant(2));
        m1 = movieRepository.save(m1);

        Movie m2 = testObjectsFactory.createMovie();
        m2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieRepository.save(m2);

        Movie m3 = testObjectsFactory.createMovie();
        m3.setCreatedAt(testObjectsFactory.createInstant(4));
        m3 = movieRepository.save(m3);

        MovieFilter filter = new MovieFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m3.getId());
    }

    @Test
    public void testGetMovieByAllFilters() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 5.0);
        m1.setCreatedAt(testObjectsFactory.createInstant(2));
        m1 = movieRepository.save(m1);

        Movie m2 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 9.3);
        m2.setCreatedAt(testObjectsFactory.createInstant(7));
        movieRepository.save(m2);

        Movie m3 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 8.5);
        m3.setCreatedAt(testObjectsFactory.createInstant(4));
        m3 = movieRepository.save(m3);

        Movie m4 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 6.3);
        m4.setCreatedAt(testObjectsFactory.createInstant(9));
        movieRepository.save(m4);

        MovieFilter filter = new MovieFilter();
        filter.setMovieStatus(MovieStatus.CANCELED);
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        filter.setAverageMarkFrom(5.0);
        filter.setAverageMarkTo(9.0);
        Assertions.assertThat(movieService.getMovie(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m3.getId());
    }

    @Test
    public void testUpdateAverageMarkOfMovie() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();

        testObjectsFactory.createMovieMark(ru, m1, 5);
        testObjectsFactory.createMovieMark(ru, m1, 10);

        movieService.updateAverageMarkOfMovie(m1.getId());
        m1 = movieRepository.findById(m1.getId()).get();
        Assert.assertEquals(7.5, m1.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    public void testGetMovieWithEmptyFilterWithPagingAndSorting() {
        Movie m1 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 3.3);
        Movie m2 = testObjectsFactory.createMovie(MovieStatus.CANCELED, 4.0);
        testObjectsFactory.createMovie(MovieStatus.CANCELED, 6.0);

        MovieFilter filter = new MovieFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "averageMark"));
        Assertions.assertThat(
                movieService.getMovie(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(m1.getId(), m2.getId()));
    }

    @Test
    public void testGetMoviesNews() {
        News n1 = testObjectsFactory.createNews();
        News n2 = testObjectsFactory.createNews();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        Movie m3 = testObjectsFactory.createMovie();

        movieService.addMovieToNews(n1.getId(), m1.getId());
        movieService.addMovieToNews(n1.getId(), m2.getId());
        movieService.addMovieToNews(n2.getId(), m3.getId());

        List<MovieReadDTO> moviesReadDTO = movieService.getMoviesNews(n1.getId());
        Assertions.assertThat(moviesReadDTO)
                .extracting(MovieReadDTO::getId).containsExactlyInAnyOrder(m1.getId(), m2.getId());
        Assert.assertEquals(2, moviesReadDTO.size());
    }

    @Test
    public void testAddMovieToNews() {
        Movie movie = testObjectsFactory.createMovie();
        News news = testObjectsFactory.createNews();

        List<MovieReadDTO> res = movieService.addMovieToNews(news.getId(), movie.getId());
        MovieReadDTO movieReadDTO = movieService.getMovie(movie.getId());

        Assertions.assertThat(res).containsExactlyInAnyOrder(movieReadDTO);

        transactionTemplate.executeWithoutResult(status -> {
            News newsAfterSave = newsRepository.findById(news.getId()).get();
            Assertions.assertThat(newsAfterSave.getMovies())
                    .extracting(Movie::getId).containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test
    public void testDuplicatedMovie() {
        Movie movie = testObjectsFactory.createMovie();
        News news = testObjectsFactory.createNews();

        movieService.addMovieToNews(news.getId(), movie.getId());
        Assertions.assertThatThrownBy(() -> {
            movieService.addMovieToNews(news.getId(), movie.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        UUID wrongMovieId = UUID.randomUUID();
        News news = testObjectsFactory.createNews();
        movieService.addMovieToNews(news.getId(), wrongMovieId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongNewsId() {
        Movie movie = testObjectsFactory.createMovie();
        UUID wrongNewsId = UUID.randomUUID();
        movieService.addMovieToNews(wrongNewsId, movie.getId());
    }

    @Test
    public void testRemoveMovieFromNews() {
        Movie movie = testObjectsFactory.createMovie();
        News news = testObjectsFactory.createNews();
        movieService.addMovieToNews(news.getId(), movie.getId());

        List<MovieReadDTO> remainingMovie = movieService.removeMovieFromNews(
                news.getId(), movie.getId());
        Assert.assertTrue(remainingMovie.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            News newsAfterRemove = newsRepository.findById(news.getId()).get();
            Assert.assertTrue(newsAfterRemove.getMovies().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedMovie() {
        Movie movie = testObjectsFactory.createMovie();
        News news = testObjectsFactory.createNews();

        movieService.removeMovieFromNews(news.getId(), movie.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedMovie() {
        News news = testObjectsFactory.createNews();
        movieService.addMovieToNews(news.getId(), UUID.randomUUID());
    }
}