package com.nizkiyd.backend.example.service;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import com.nizkiyd.backend.example.repository.MovieRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@DBRider
public class MovieServiceDbRiderTest extends BaseTest {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieRepository movieRepository;

    @Test
    @DataSet(value = "/datasets/testUpdateAverageMarkOfMovie.xml")
    @ExpectedDataSet("datasets/testUpdateAverageMarkOfMovie_result.xml")
    public void testUpdateAverageMarkOfMovie() {
        UUID movieId = UUID.fromString("810bdfa4-78df-443e-b333-212030f0ae52");
        movieService.updateAverageMarkOfMovie(movieId);
    }
}
