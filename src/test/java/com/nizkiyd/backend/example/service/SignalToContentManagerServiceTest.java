package com.nizkiyd.backend.example.service;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.SignalToContentManagerWrongStatusException;
import com.nizkiyd.backend.example.repository.*;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

public class SignalToContentManagerServiceTest extends BaseTest {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private SignalToContentManagerService signalToContentManagerService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testFixContentFixSimilar() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        News n = testObjectsFactory.createNews("hello world hello world");
        ContentManager contentManager = testObjectsFactory.createContentManager();

        SignalToContentManager s1 = testObjectsFactory.createSignalToContentManager(
                ru, SignalToContentManagerObjectType.NEWS, n.getId(), "java", "world");

        SignalToContentManager s2 = testObjectsFactory.createSignalToContentManager(
                ru, SignalToContentManagerObjectType.NEWS, n.getId(), "php", "hello world");

        SignalToContentManager s3 = testObjectsFactory.createSignalToContentManager(
                ru, SignalToContentManagerObjectType.NEWS, n.getId(), "hi", "hello");

        SignalToContentManagerFixContent signalToContentManagerFixContent = new SignalToContentManagerFixContent();
        signalToContentManagerFixContent.setContentManagerId(contentManager.getId());
        signalToContentManagerFixContent.setReplacement("java");
        signalToContentManagerService.fixContent(s1.getId(), signalToContentManagerFixContent);

        List<SignalToContentManager> allSignals = new ArrayList<>();
        signalToContentManagerRepository.findAll().forEach(allSignals::add);
        Stream<SignalToContentManager> needToFixSignals = allSignals.stream().filter(
                s -> s.getSignalToContentManagerStatus() == SignalToContentManagerStatus.NEED_TO_FIX);

        Assertions.assertThat(needToFixSignals).extracting("id").containsExactlyInAnyOrder(s3.getId());

        SignalToContentManager s1AfterChange = signalToContentManagerRepository.findById(s1.getId()).get();
        SignalToContentManager s2AfterChange = signalToContentManagerRepository.findById(s2.getId()).get();
        Assert.assertEquals(s1AfterChange.getContentManager().getId(), s2AfterChange.getContentManager().getId());
        Assert.assertEquals(s1AfterChange.getFixedAt(), s2AfterChange.getFixedAt());
        Assert.assertEquals(
                s1AfterChange.getSignalToContentManagerStatus(), s2AfterChange.getSignalToContentManagerStatus());
        Assert.assertEquals(s1AfterChange.getReplacement(), s2AfterChange.getReplacement());
    }

    @Test
    public void testFixContentParametersVerification() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        Movie m = testObjectsFactory.createMovie("hello world");
        ContentManager contentManager = testObjectsFactory.createContentManager();

        SignalToContentManager signal = testObjectsFactory.createSignalToContentManager(
                ru, SignalToContentManagerObjectType.MOVIE, m.getId(), 6, 11,
                SignalToContentManagerStatus.NEED_TO_FIX);

        SignalToContentManagerFixContent signalToContentManagerFixContent = new SignalToContentManagerFixContent();
        signalToContentManagerFixContent.setContentManagerId(contentManager.getId());
        signalToContentManagerFixContent.setReplacement("java");
        signalToContentManagerService.fixContent(signal.getId(), signalToContentManagerFixContent);

        SignalToContentManager signalAfterChange = signalToContentManagerRepository.findById(signal.getId()).get();
        Assert.assertEquals(signalAfterChange.getSignalToContentManagerStatus(), SignalToContentManagerStatus.FIXED);
        Assert.assertEquals(signalAfterChange.getContentManager().getId(), contentManager.getId());
        Assert.assertEquals(signalAfterChange.getReplacement(), "java");
        Assert.assertNotNull(signalAfterChange.getFixedAt());
    }

    @Test
    public void testFixContentReplacementVerification() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Person person = testObjectsFactory.cretePerson("hello world hello world");
        ContentManager contentManager = testObjectsFactory.createContentManager();

        SignalToContentManager s1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.PERSON, person.getId(),
                6, 11, "java", "world");

        SignalToContentManagerFixContent signalToContentManagerFixContent = new SignalToContentManagerFixContent();
        signalToContentManagerFixContent.setContentManagerId(contentManager.getId());
        signalToContentManagerFixContent.setReplacement("java");
        signalToContentManagerService.fixContent(s1.getId(), signalToContentManagerFixContent);

        Person personAfterChange = personRepository.findById(person.getId()).get();
        Assert.assertEquals(personAfterChange.getBiography(), "hello java hello world");
    }

    @Test(expected = SignalToContentManagerWrongStatusException.class)
    public void testFixContentWithWrongStatus() {
        RegisteredUser ru = testObjectsFactory.createRegisteredUser();
        News n = testObjectsFactory.createNews("hello world hello world");
        ContentManager contentManager = testObjectsFactory.createContentManager();

        SignalToContentManager signal = testObjectsFactory.createSignalToContentManager(
                ru, SignalToContentManagerObjectType.NEWS, n.getId(), 6, 11, SignalToContentManagerStatus.FIXED);

        SignalToContentManagerFixContent signalToContentManagerFixContent = new SignalToContentManagerFixContent();
        signalToContentManagerFixContent.setContentManagerId(contentManager.getId());
        signalToContentManagerFixContent.setReplacement("java");
        signalToContentManagerService.fixContent(signal.getId(), signalToContentManagerFixContent);
    }

    @Test
    public void testGetSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie movie = testObjectsFactory.createMovie();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, movie.getId(), contentManager);

        SignalToContentManagerReadDTO readDTO = signalToContentManagerService.getSignalToContentManager(
                signalToContentManager.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(signalToContentManager,
                "registeredUserId", "contentManagerId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), signalToContentManager.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getContentManagerId(), signalToContentManager.getContentManager().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetSignalToContentManagerWrongId() {
        signalToContentManagerService.getSignalToContentManager(UUID.randomUUID());
    }

    @Test
    public void testPatchSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);

        SignalToContentManagerPatchDTO patch = new SignalToContentManagerPatchDTO();
        patch.setReplacement("sds");
        patch.setFromIndex(4);
        patch.setToIndex(8);
        patch.setSignalToContentManagerObjectId(m2.getId());
        patch.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        patch.setContentManagerId(cm2.getId());
        SignalToContentManagerReadDTO read = signalToContentManagerService.patchSignalToContentManager(
                signalToContentManager.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        signalToContentManager = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(signalToContentManager).isEqualToIgnoringGivenFields(read,
                "registeredUser", "contentManager");
        Assert.assertEquals(signalToContentManager.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(signalToContentManager.getContentManager().getId(), read.getContentManagerId());

    }

    @Test
    public void testPatchSignalToContentManagerEmptyPatch() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        SignalToContentManagerPatchDTO patch = new SignalToContentManagerPatchDTO();

        SignalToContentManagerReadDTO read = signalToContentManagerService.patchSignalToContentManager(
                signalToContentManager.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        SignalToContentManager signalToContentManagerAfterUpdate = signalToContentManagerRepository
                .findById(read.getId()).get();
        Assertions.assertThat(signalToContentManagerAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(signalToContentManager).isEqualToIgnoringGivenFields(signalToContentManagerAfterUpdate,
                "registeredUser", "contentManager");
        Assert.assertEquals(signalToContentManager.getRegisteredUser().getId(),
                signalToContentManagerAfterUpdate.getRegisteredUser().getId());
        Assert.assertEquals(signalToContentManager.getContentManager().getId(),
                signalToContentManagerAfterUpdate.getContentManager().getId());
    }

    @Test
    public void testDeleteSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);

        signalToContentManagerService.deleteSignalToContentManager(signalToContentManager.getId());
        Assert.assertFalse(signalToContentManagerRepository.existsById(signalToContentManager.getId()));
        Assert.assertTrue(registeredUserRepository.existsById(registeredUser.getId()));
        Assert.assertTrue(contentManagerRepository.existsById(cm1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewMovieNotFound() {
        signalToContentManagerService.deleteSignalToContentManager(UUID.randomUUID());
    }

    @Test
    public void testPutSignalToContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();
        SignalToContentManager signalToContentManager = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);

        SignalToContentManagerPutDTO put = new SignalToContentManagerPutDTO();
        put.setReplacement("sds");
        put.setFromIndex(4);
        put.setToIndex(8);
        put.setSignalToContentManagerObjectId(m2.getId());
        put.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        put.setContentManagerId(cm2.getId());
        signalToContentManagerService.updateSignalToContentManager(signalToContentManager.getId(), put);

        SignalToContentManagerReadDTO read = signalToContentManagerService.getSignalToContentManager(
                signalToContentManager.getId());
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        signalToContentManager = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(signalToContentManager).isEqualToIgnoringGivenFields(read,
                "registeredUser", "contentManager");
        Assert.assertEquals(signalToContentManager.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(signalToContentManager.getContentManager().getId(), read.getContentManagerId());
    }

    @Test
    public void testGetSignalToContentManagersWithEmptyFilter() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm2);
        SignalToContentManager signalToContentManager3 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m2.getId(), cm1);


        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        Assertions.assertThat(signalToContentManagerService.getSignalToContentManager(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(),
                        signalToContentManager2.getId(), signalToContentManager3.getId());
    }

    @Test
    public void testGetSignalToContentManagerByContentManager() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm2);
        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m2.getId(), cm1);


        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        filter.setContentManagerId(cm1.getId());
        Assertions.assertThat(signalToContentManagerService.getSignalToContentManager(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(), signalToContentManager2.getId());
    }

    @Test
    public void testGetSignalToContentManagerByStatus() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerStatus.NEED_TO_FIX);
        testObjectsFactory.createSignalToContentManager(registeredUser, SignalToContentManagerStatus.CANCELED);
        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerStatus.NEED_TO_FIX);


        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        filter.setStatus(SignalToContentManagerStatus.NEED_TO_FIX);
        Assertions.assertThat(signalToContentManagerService.getSignalToContentManager(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(), signalToContentManager2.getId());
    }

    @Test
    public void testGetSignalToContentManagerBySignalToContentManagerObjectId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m2.getId(), cm2);
        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm2);


        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        filter.setSignalToContentManagerObjectId(m1.getId());
        Assertions.assertThat(signalToContentManagerService.getSignalToContentManager(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(), signalToContentManager2.getId());
    }


    @Test
    public void testGetSignalToContentManagerByCreatedAtInterval() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m2 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        signalToContentManager1.setCreatedAt(testObjectsFactory.createInstant(2));
        signalToContentManager1 = signalToContentManagerRepository.save(signalToContentManager1);

        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m2.getId(), cm2);
        signalToContentManager2.setCreatedAt(testObjectsFactory.createInstant(7));
        signalToContentManagerRepository.save(signalToContentManager2);

        SignalToContentManager signalToContentManager3 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm2);
        signalToContentManager3.setCreatedAt(testObjectsFactory.createInstant(4));
        signalToContentManager3 = signalToContentManagerRepository.save(signalToContentManager3);

        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));

        Assertions.assertThat(signalToContentManagerService.getSignalToContentManager(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(), signalToContentManager3.getId());
    }

    @Test
    public void testGetSignalToContentManagerByAllFilters() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1,
                SignalToContentManagerStatus.NEED_TO_FIX);
        signalToContentManager1.setCreatedAt(testObjectsFactory.createInstant(2));
        signalToContentManager1 = signalToContentManagerRepository.save(signalToContentManager1);

        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm2,
                SignalToContentManagerStatus.CANCELED);
        signalToContentManager2.setCreatedAt(testObjectsFactory.createInstant(7));
        signalToContentManagerRepository.save(signalToContentManager2);

        SignalToContentManager signalToContentManager3 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1,
                SignalToContentManagerStatus.NEED_TO_FIX);
        signalToContentManager3.setCreatedAt(testObjectsFactory.createInstant(4));
        signalToContentManager3 = signalToContentManagerRepository.save(signalToContentManager3);

        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        filter.setCreatedAtFrom(testObjectsFactory.createInstant(1));
        filter.setCreatedAtTo(testObjectsFactory.createInstant(5));
        filter.setContentManagerId(cm1.getId());
        filter.setSignalToContentManagerObjectId(m1.getId());
        filter.setStatus(SignalToContentManagerStatus.NEED_TO_FIX);
        Assertions.assertThat(signalToContentManagerService.getSignalToContentManager(
                filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(), signalToContentManager3.getId());
    }

    @Test
    public void testGetSignalToContentManagerWithEmptyFilterWithPagingAndSorting() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Movie m1 = testObjectsFactory.createMovie();
        Movie m3 = testObjectsFactory.createMovie();
        ContentManager cm1 = testObjectsFactory.createContentManager();
        ContentManager cm2 = testObjectsFactory.createContentManager();

        SignalToContentManager signalToContentManager1 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        SignalToContentManager signalToContentManager2 = testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm2);
        testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m1.getId(), cm1);
        testObjectsFactory.createSignalToContentManager(
                registeredUser, SignalToContentManagerObjectType.MOVIE, m3.getId(), cm1);

        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        PageRequest pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));
        Assertions.assertThat(
                signalToContentManagerService.getSignalToContentManager(filter, pageable).getData()).extracting("id")
                .isEqualTo(Arrays.asList(signalToContentManager1.getId(), signalToContentManager2.getId()));
    }
}