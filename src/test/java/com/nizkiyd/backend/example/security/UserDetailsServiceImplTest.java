package com.nizkiyd.backend.example.security;

import com.nizkiyd.backend.example.domain.Admin;
import com.nizkiyd.backend.example.domain.ContentManager;
import com.nizkiyd.backend.example.domain.Moderator;
import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.support.TransactionTemplate;

public class UserDetailsServiceImplTest extends BaseTest {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Test
    public void testLoadRegisteredUserByUsername() {
        RegisteredUser registeredUser = transactionTemplate.execute(status -> {
            return testObjectsFactory.createRegisteredUser();
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(registeredUser.getEmail());
        Assert.assertEquals(registeredUser.getEmail(), userDetails.getUsername());
        Assert.assertEquals(registeredUser.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
        Assertions.assertThat(userDetails.getAuthorities()).extracting("authority")
                .containsExactlyInAnyOrder(registeredUser.getClass().getSimpleName());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testRegisteredUserNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }

    @Test
    public void testLoadAdminByUsername() {
        Admin admin = transactionTemplate.execute(status -> {
            return testObjectsFactory.creteAdmin();
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(admin.getEmail());
        Assert.assertEquals(admin.getEmail(), userDetails.getUsername());
        Assert.assertEquals(admin.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testAdminNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }

    @Test
    public void testLoadContentManagerByUsername() {
        ContentManager contentManager = transactionTemplate.execute(status -> {
            return testObjectsFactory.creteContentManager();
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(contentManager.getEmail());
        Assert.assertEquals(contentManager.getEmail(), userDetails.getUsername());
        Assert.assertEquals(contentManager.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testContentManagerNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }

    @Test
    public void testLoadModeratorByUsername() {
        Moderator moderator = transactionTemplate.execute(status -> {
            return testObjectsFactory.creteModerator();
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(moderator.getEmail());
        Assert.assertEquals(moderator.getEmail(), userDetails.getUsername());
        Assert.assertEquals(moderator.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testModeratorNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }

}