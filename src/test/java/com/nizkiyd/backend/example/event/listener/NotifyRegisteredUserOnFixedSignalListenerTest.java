package com.nizkiyd.backend.example.event.listener;

import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import com.nizkiyd.backend.example.event.SignalToContentManagerStatusChangedEvent;
import com.nizkiyd.backend.example.service.RegisteredUserNotifacationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotifyRegisteredUserOnFixedSignalListenerTest {

    @MockBean
    private RegisteredUserNotifacationService registeredUserNotifacationService;

    @SpyBean
    private NotifyRegisteredUserOnFixedSignalListener notifyRegisteredUserOnFixedSignalListener;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void testOnEvent() {
        SignalToContentManagerStatusChangedEvent event = new SignalToContentManagerStatusChangedEvent();
        event.setSignalToContentManagerId(UUID.randomUUID());
        event.setNewStatus(SignalToContentManagerStatus.FIXED);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyRegisteredUserOnFixedSignalListener, Mockito.timeout(500)).onEvent(event);
        Mockito.verify(registeredUserNotifacationService, Mockito.timeout(500))
                .notifyOnSignalToContentMangerStatusChangeToFixed(event.getSignalToContentManagerId());
    }

    @Test
    public void testOnEventNotFinished() {
        for (SignalToContentManagerStatus signalStatus : SignalToContentManagerStatus.values()) {
            if (signalStatus == SignalToContentManagerStatus.FIXED) {
                continue;
            }

            SignalToContentManagerStatusChangedEvent event = new SignalToContentManagerStatusChangedEvent();
            event.setSignalToContentManagerId(UUID.randomUUID());
            event.setNewStatus(signalStatus);
            applicationEventPublisher.publishEvent(event);

            Mockito.verify(notifyRegisteredUserOnFixedSignalListener, Mockito.never()).onEvent(any());
            Mockito.verify(registeredUserNotifacationService, Mockito.never())
                    .notifyOnSignalToContentMangerStatusChangeToFixed(any());
        }
    }

    @Test
    public void testOnEventAsync() throws InterruptedException {
        SignalToContentManagerStatusChangedEvent event = new SignalToContentManagerStatusChangedEvent();
        event.setSignalToContentManagerId(UUID.randomUUID());
        event.setNewStatus(SignalToContentManagerStatus.FIXED);

        List<Integer> checkCollection = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            checkCollection.add(2);
            latch.countDown();
            return null;
        }).when(registeredUserNotifacationService).notifyOnSignalToContentMangerStatusChangeToFixed(
                event.getSignalToContentManagerId());

        applicationEventPublisher.publishEvent(event);
        checkCollection.add(1);

        latch.await();
        Mockito.verify(notifyRegisteredUserOnFixedSignalListener).onEvent(event);
        Mockito.verify(registeredUserNotifacationService).notifyOnSignalToContentMangerStatusChangeToFixed(
                event.getSignalToContentManagerId());
        Assert.assertEquals(Arrays.asList(1, 2), checkCollection);
    }

}