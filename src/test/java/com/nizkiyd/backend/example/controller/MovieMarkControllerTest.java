package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.MovieMark;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkFilter;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.MovieMarkService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MovieMarkController.class)
public class MovieMarkControllerTest extends BaseControllerTest {

    @MockBean
    private MovieMarkService movieMarkService;

    @Test
    public void testGetMovieMark() throws Exception {
        MovieMarkReadDTO movieMark = generateObject(MovieMarkReadDTO.class);

        Mockito.when(movieMarkService.getMovieMark(movieMark.getId())).thenReturn(movieMark);

        String resultJson = mvc.perform(get("/api/v1/movie-marks/{id}", movieMark.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieMarkReadDTO actualMovieMark = objectMapper.readValue(resultJson, MovieMarkReadDTO.class);
        Assertions.assertThat(actualMovieMark).isEqualToComparingFieldByField(movieMark);

        Mockito.verify(movieMarkService).getMovieMark(movieMark.getId());
    }

    @Test
    public void testGetMovieMarkWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MovieMark.class, wrongId);
        Mockito.when(movieMarkService.getMovieMark(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movie-marks/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testPatchMovieMark() throws Exception {
        MovieMarkPatchDTO patch = generateObject(MovieMarkPatchDTO.class);
        MovieMarkReadDTO read = generateObject(MovieMarkReadDTO.class);

        Mockito.when(movieMarkService.patchMovieMark(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movie-marks/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieMarkReadDTO actualMovieMark = objectMapper.readValue(resultJson, MovieMarkReadDTO.class);
        Assert.assertEquals(read, actualMovieMark);
    }

    @WithMockUser
    @Test
    public void testDeleteMovieMark() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movie-marks/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(movieMarkService).deleteMovieMark(id);
    }

    @Test
    public void testGetMovieMarks() throws Exception {
        MovieMarkFilter filter = new MovieMarkFilter();
        filter.setMovieId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        MovieMarkReadDTO readDTO = generateObject(MovieMarkReadDTO.class);

        PageResult<MovieMarkReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(movieMarkService.getMovieMark(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movie-marks")
                .param("movieId", filter.getMovieId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieMarkReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testGetMovieMarksWithUserId() throws Exception {
        MovieMarkFilter filter = new MovieMarkFilter();
        filter.setMovieId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        MovieMarkReadDTO readDTO = generateObject(MovieMarkReadDTO.class);

        PageResult<MovieMarkReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(movieMarkService.getMovieMark(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/movie-marks", UUID.randomUUID())
                .param("movieId", filter.getMovieId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieMarkReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetMovieMarkWithPagingAndSorting() throws Exception {
        MovieMarkFilter movieMarkFilter = new MovieMarkFilter();
        MovieMarkReadDTO readDTO = generateObject(MovieMarkReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<MovieMarkReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "mark"));
        Mockito.when(movieMarkService.getMovieMark(movieMarkFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movie-marks")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "mark,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieMarkReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }


}