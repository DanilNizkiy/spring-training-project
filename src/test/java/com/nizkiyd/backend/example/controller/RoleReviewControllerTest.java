package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewFilter;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.dto.rolereview.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.RoleReviewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RoleReviewController.class)
public class RoleReviewControllerTest extends BaseControllerTest {

    @MockBean
    private RoleReviewService roleReviewService;

    @Test
    public void testGetRoleReview() throws Exception {
        RoleReviewReadDTO roleReview = generateObject(RoleReviewReadDTO.class);

        Mockito.when(roleReviewService.getRoleReview(roleReview.getId())).thenReturn(roleReview);

        String resultJson = mvc.perform(get("/api/v1/role-reviews/{id}", roleReview.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewReadDTO actualRoleReview = objectMapper.readValue(resultJson, RoleReviewReadDTO.class);
        Assertions.assertThat(actualRoleReview).isEqualToComparingFieldByField(roleReview);

        Mockito.verify(roleReviewService).getRoleReview(roleReview.getId());
    }

    @Test
    public void testGetRoleReviewWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RoleReview.class, wrongId);
        Mockito.when(roleReviewService.getRoleReview(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/role-reviews/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchRoleReview() throws Exception {
        RoleReviewPatchDTO patch = generateObject(RoleReviewPatchDTO.class);

        RoleReviewReadDTO read = generateObject(RoleReviewReadDTO.class);

        Mockito.when(roleReviewService.patchRoleReview(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/role-reviews/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewReadDTO actualRoleReview = objectMapper.readValue(resultJson, RoleReviewReadDTO.class);
        Assert.assertEquals(read, actualRoleReview);
    }

    @Test
    public void testPutRoleReview() throws Exception {
        UUID id = UUID.randomUUID();

        RoleReviewPutDTO put = generateObject(RoleReviewPutDTO.class);

        mvc.perform(put("/api/v1/role-reviews/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(roleReviewService).updateRoleReview(id, put);
    }

    @Test
    public void testDeleteRoleReview() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/role-reviews/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(roleReviewService).deleteRoleReview(id);
    }

    @Test
    public void testGetRoleReviews() throws Exception {
        RoleReviewFilter filter = new RoleReviewFilter();
        filter.setRoleId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        RoleReviewReadDTO readDTO = generateObject(RoleReviewReadDTO.class);

        PageResult<RoleReviewReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(roleReviewService.getRoleReview(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/role-reviews")
                .param("roleId", filter.getRoleId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleReviewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetRoleReviewWithPagingAndSorting() throws Exception {
        RoleReviewFilter roleReviewFilter = new RoleReviewFilter();
        RoleReviewReadDTO readDTO = generateObject(RoleReviewReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<RoleReviewReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(roleReviewService.getRoleReview(roleReviewFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/role-reviews")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleReviewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}