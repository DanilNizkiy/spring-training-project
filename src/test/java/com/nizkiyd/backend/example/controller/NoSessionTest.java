package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.service.MovieReviewService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = MovieReviewController.class)
public class NoSessionTest extends BaseControllerTest {

    @MockBean
    private MovieReviewService movieReviewService;

    @Test
    public void testNoSession() throws Exception {
        UUID wrongId = UUID.randomUUID();

        Mockito.when(movieReviewService.getMovieReview(wrongId)).thenReturn(new MovieReviewReadDTO());

        MvcResult mvcResult = mvc.perform(get("/api/v1/movie-reviews/{id}", wrongId))
                .andExpect(status().isOk())
                .andReturn();
        Assert.assertNull(mvcResult.getRequest().getSession(false));
    }
}
