package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Complaint;
import com.nizkiyd.backend.example.dto.complaint.ComplaintCreateDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPatchDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintPutDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserComplaintService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserComplaintController.class)
public class RegisteredUserComplaintControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserComplaintService service;

    @Test
    public void testCreateComplaintValidationFailed() throws Exception {
        ComplaintCreateDTO createDTO = new ComplaintCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/complaints", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createComplaint(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetComplaint() throws Exception {
        ComplaintReadDTO complaint = generateObject(ComplaintReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getComplaint(randomUUID, complaint.getId())).thenReturn(complaint);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                "complaints/{complaintId}", randomUUID, complaint.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ComplaintReadDTO actualComplaint = objectMapper.readValue(resultJson, ComplaintReadDTO.class);
        Assertions.assertThat(actualComplaint).isEqualToComparingFieldByField(complaint);

        Mockito.verify(service).getComplaint(randomUUID, complaint.getId());
    }

    @Test
    public void testGetComplaintWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Complaint.class, wrongId);
        Mockito.when(service.getComplaint(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                        "complaints/{complaintId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateComplaint() throws Exception {
        ComplaintCreateDTO create = generateObject(ComplaintCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        ComplaintReadDTO read = generateObject(ComplaintReadDTO.class);

        Mockito.when(service.createComplaint(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/complaints", randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ComplaintReadDTO actualComplaint = objectMapper.readValue(resultJson, ComplaintReadDTO.class);
        Assertions.assertThat(actualComplaint).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchComplaint() throws Exception {
        ComplaintPatchDTO patch = generateObject(ComplaintPatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        ComplaintReadDTO read = generateObject(ComplaintReadDTO.class);

        Mockito.when(service.patchComplaint(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}" +
                "/complaints/{complaintId}", randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ComplaintReadDTO actualComplaint = objectMapper.readValue(resultJson, ComplaintReadDTO.class);
        Assert.assertEquals(read, actualComplaint);
    }

    @Test
    public void testDeleteComplaint() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/complaints/{complaintId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteComplaint(randomUUID, id);
    }

    @Test
    public void testPutComplaint() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        ComplaintPutDTO put = generateObject(ComplaintPutDTO.class);
        ;

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/complaints/{complaintId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateComplaint(randomUUID, id, put);
    }

    @Test
    public void testGetRegisteredUserComplaints() throws Exception {
        ComplaintReadDTO complaint1 = generateObject(ComplaintReadDTO.class);
        ComplaintReadDTO complaint2 = generateObject(ComplaintReadDTO.class);

        List<ComplaintReadDTO> complaintsReadDTO = new ArrayList<>();
        complaintsReadDTO.add(complaint1);
        complaintsReadDTO.add(complaint2);

        Mockito.when(service.getRegisteredUserComplaints(complaint1.getRegisteredUserId()))
                .thenReturn(complaintsReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/complaints",
                complaint1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ComplaintReadDTO> actualComplaintsReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualComplaintsReadDTO.size());
        Assertions.assertThat(actualComplaintsReadDTO).extracting("id")
                .containsExactlyInAnyOrder(complaint1.getId(), complaint2.getId());

        Mockito.verify(service).getRegisteredUserComplaints(complaint1.getRegisteredUserId());
    }
}
