package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.MovieReviewLike;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeFilter;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.MovieReviewLikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = MovieReviewLikeController.class)
public class MovieReviewLikeControllerTest extends BaseControllerTest {

    @MockBean
    private MovieReviewLikeService movieReviewLikeService;

    @Test
    public void testGetMovieReviewLike() throws Exception {
        MovieReviewLikeReadDTO movieReviewLike = generateObject(MovieReviewLikeReadDTO.class);

        Mockito.when(movieReviewLikeService.getMovieReviewLike(movieReviewLike.getId())).thenReturn(movieReviewLike);

        String resultJson = mvc.perform(get("/api/v1/movie-review-likes/{id}", movieReviewLike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewLikeReadDTO actualMovieReviewLike = objectMapper.readValue(
                resultJson, MovieReviewLikeReadDTO.class);
        Assertions.assertThat(actualMovieReviewLike).isEqualToComparingFieldByField(movieReviewLike);

        Mockito.verify(movieReviewLikeService).getMovieReviewLike(movieReviewLike.getId());
    }

    @Test
    public void testGetMovieReviewLikeWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MovieReviewLike.class, wrongId);
        Mockito.when(movieReviewLikeService.getMovieReviewLike(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movie-review-likes/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchMovieReviewLike() throws Exception {
        MovieReviewLikePatchDTO patch = generateObject(MovieReviewLikePatchDTO.class);
        MovieReviewLikeReadDTO read = generateObject(MovieReviewLikeReadDTO.class);

        Mockito.when(movieReviewLikeService.patchMovieReviewLike(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movie-review-likes/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewLikeReadDTO actualMovieReviewLike = objectMapper.readValue(
                resultJson, MovieReviewLikeReadDTO.class);
        Assert.assertEquals(read, actualMovieReviewLike);
    }

    @Test
    public void testDeleteMovieReviewLike() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movie-review-likes/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(movieReviewLikeService).deleteMovieReviewLike(id);
    }

    @Test
    public void testGetMovieReviewLikes() throws Exception {
        MovieReviewLikeFilter filter = new MovieReviewLikeFilter();
        filter.setMovieReviewId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        MovieReviewLikeReadDTO readDTO = generateObject(MovieReviewLikeReadDTO.class);

        PageResult<MovieReviewLikeReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(movieReviewLikeService.getMovieReviewLike(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movie-review-likes")
                .param("movieReviewId", filter.getMovieReviewId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReviewLikeReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetMovieReviewLikeWithPagingAndSorting() throws Exception {
        MovieReviewLikeFilter movieReviewLikeFilter = new MovieReviewLikeFilter();
        MovieReviewLikeReadDTO readDTO = generateObject(MovieReviewLikeReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<MovieReviewLikeReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(movieReviewLikeService.getMovieReviewLike(
                movieReviewLikeFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movie-review-likes")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReviewLikeReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}