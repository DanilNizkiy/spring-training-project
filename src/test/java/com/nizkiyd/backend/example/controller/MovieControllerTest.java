package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Movie;
import com.nizkiyd.backend.example.domain.MovieStatus;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.movie.MovieCreateDTO;
import com.nizkiyd.backend.example.dto.movie.MoviePatchDTO;
import com.nizkiyd.backend.example.dto.movie.MoviePutDTO;
import com.nizkiyd.backend.example.dto.movie.MovieReadDTO;
import com.nizkiyd.backend.example.dto.movie.MovieFilter;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.MovieService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static net.javacrumbs.jsonunit.JsonAssert.when;
import static net.javacrumbs.jsonunit.core.Option.IGNORING_VALUES;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MovieController.class)
public class MovieControllerTest extends BaseControllerTest {

    @MockBean
    private MovieService movieService;

    @WithMockUser
    @Test
    public void testCreateMovieValidationFailed() throws Exception {
        MovieCreateDTO createDTO = new MovieCreateDTO();
        String resultJson = mvc.perform(
                post("/api/v1/movies")
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(movieService, Mockito.never()).createMovie(ArgumentMatchers.any());
    }

    @Test
    public void testGetMovie() throws Exception {
        MovieReadDTO movie = generateObject(MovieReadDTO.class);

        Mockito.when(movieService.getMovie(movie.getId())).thenReturn(movie);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", movie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(movie);

        Mockito.verify(movieService).getMovie(movie.getId());
    }

    @Test
    public void testGetMoviesNews() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID movieId = UUID.randomUUID();

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        read.setId(movieId);
        List<MovieReadDTO> expectedMovie = List.of(read);
        Mockito.when(movieService.getMoviesNews(newsId)).thenReturn(expectedMovie);

        String resultJson = mvc.perform(get("/api/v1/news/{newsId}/movies", newsId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReadDTO> actualMovies = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedMovie, actualMovies);
    }

    @WithMockUser
    @Test
    public void testGetMovieNewsWithUserId() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID movieId = UUID.randomUUID();

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        read.setId(movieId);
        List<MovieReadDTO> expectedMovie = List.of(read);
        Mockito.when(movieService.getMoviesNews(newsId)).thenReturn(expectedMovie);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/news/{newsId}/movies",
                UUID.randomUUID(), newsId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReadDTO> actualMovies = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedMovie, actualMovies);
    }

    @WithMockUser
    @Test
    public void testGetMovieWithUsersId() throws Exception {
        MovieReadDTO movie = generateObject(MovieReadDTO.class);
        UUID usersId = UUID.randomUUID();

        Mockito.when(movieService.getMovie(movie.getId())).thenReturn(movie);

        String resultJson = mvc.perform(get("/api/v1/users/{usersId}/movies/{id}", usersId, movie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(movie);

        Mockito.verify(movieService).getMovie(movie.getId());
    }

    @Test
    public void testGetMovieWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Movie.class, wrongId);
        Mockito.when(movieService.getMovie(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMovieWrongIdFormat() throws Exception {
        String wrongId = "123";

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", wrongId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo randomError = new ErrorInfo(HttpStatus.BAD_REQUEST, Movie.class, "error message");
        String errorJson = objectMapper.writeValueAsString(randomError);

        assertJsonEquals(resultJson, errorJson, when(IGNORING_VALUES));
    }

    @WithMockUser
    @Test
    public void testCreateMovie() throws Exception {
        MovieCreateDTO create = generateObject(MovieCreateDTO.class);
        MovieReadDTO read = generateObject(MovieReadDTO.class);

        Mockito.when(movieService.createMovie(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchMovie() throws Exception {
        MoviePatchDTO patch = generateObject(MoviePatchDTO.class);
        MovieReadDTO read = generateObject(MovieReadDTO.class);

        Mockito.when(movieService.patchMovie(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assert.assertEquals(read, actualMovie);
    }

    @WithMockUser
    @Test
    public void testDeleteMovie() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(movieService).deleteMovie(id);
    }

    @WithMockUser
    @Test
    public void testPutMovie() throws Exception {
        UUID id = UUID.randomUUID();

        MoviePutDTO put = generateObject(MoviePutDTO.class);

        mvc.perform(put("/api/v1/movies/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(movieService).updateMovie(id, put);
    }

    @Test
    public void testGetMovies() throws Exception {
        MovieFilter filter = new MovieFilter();
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());
        filter.setAverageMarkFrom(6.0);
        filter.setAverageMarkTo(10.0);
        filter.setMovieStatus(MovieStatus.RELEASED);
        filter.setNewsId(UUID.randomUUID());

        MovieReadDTO readDTO = generateObject(MovieReadDTO.class);

        PageResult<MovieReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(movieService.getMovie(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies")
                .param("movieStatus", filter.getMovieStatus().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString())
                .param("averageMarkFrom", filter.getAverageMarkFrom().toString())
                .param("newsId", filter.getNewsId().toString())
                .param("averageMarkTo", filter.getAverageMarkTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testGetMoviesWithUsersId() throws Exception {
        UUID userId = UUID.randomUUID();

        MovieFilter filter = new MovieFilter();
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());
        filter.setAverageMarkFrom(6.0);
        filter.setAverageMarkTo(10.0);
        filter.setMovieStatus(MovieStatus.RELEASED);
        filter.setNewsId(UUID.randomUUID());

        MovieReadDTO readDTO = generateObject(MovieReadDTO.class);

        PageResult<MovieReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(movieService.getMovie(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/movies", userId)
                .param("movieStatus", filter.getMovieStatus().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString())
                .param("averageMarkFrom", filter.getAverageMarkFrom().toString())
                .param("newsId", filter.getNewsId().toString())
                .param("averageMarkTo", filter.getAverageMarkTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }


    @Test
    public void testGetMovieWithPagingAndSorting() throws Exception {
        MovieFilter movieFilter = new MovieFilter();
        MovieReadDTO readDTO = generateObject(MovieReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<MovieReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "averageMark"));
        Mockito.when(movieService.getMovie(movieFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "averageMark,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testAddMovieToNews() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID movieId = UUID.randomUUID();

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        read.setId(movieId);
        List<MovieReadDTO> expectedMovies = List.of(read);
        Mockito.when(movieService.addMovieToNews(newsId, movieId)).thenReturn(expectedMovies);

        String resultJson = mvc.perform(post("/api/v1/news/{newsId}/movies/{movieId}", newsId, movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReadDTO> actualMovies = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedMovies, actualMovies);
    }

    @WithMockUser
    @Test
    public void testRemoveMovieFromNews() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID newsId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news/{newsId}/movies/{movieId}", newsId.toString(), movieId.toString()))
                .andExpect(status().isOk());

        Mockito.verify(movieService).removeMovieFromNews(newsId, movieId);
    }
}