package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;

import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserSignalToContentManagerService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserSignalToContentManagerController.class)
public class RegisteredUserSignalToContentManagerControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserSignalToContentManagerService service;

    @Test
    public void testCreateSignalToContentManagerWrongIndex() throws Exception {
        SignalToContentManagerCreateDTO signalCreate = generateObject(SignalToContentManagerCreateDTO.class);
        signalCreate.setFromIndex(20);
        signalCreate.setToIndex(10);

        UUID randomUUID = UUID.randomUUID();

        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/signals-to-content-manager", randomUUID)
                        .content(objectMapper.writeValueAsString(signalCreate))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assert.assertTrue(errorInfo.getMessage().contains("fromIndex"));
        Assert.assertTrue(errorInfo.getMessage().contains("toIndex"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());

        Mockito.verify(service, Mockito.never()).createSignalToContentManager(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testCreateSignalToContentManagerValidationFailed() throws Exception {
        SignalToContentManagerCreateDTO signalCreate = new SignalToContentManagerCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/signals-to-content-manager", randomUUID)
                        .content(objectMapper.writeValueAsString(signalCreate))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createSignalToContentManager(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetSignalToContentManager() throws Exception {
        SignalToContentManagerReadDTO signalToContentManager = generateObject(SignalToContentManagerReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getSignalToContentManager(
                randomUUID, signalToContentManager.getId())).thenReturn(signalToContentManager);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                "signals-to-content-manager/{signalToContentManagerId}", randomUUID, signalToContentManager.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToContentManager = objectMapper.readValue(
                resultJson, SignalToContentManagerReadDTO.class);
        Assertions.assertThat(actualSignalToContentManager).isEqualToComparingFieldByField(signalToContentManager);

        Mockito.verify(service).getSignalToContentManager(randomUUID, signalToContentManager.getId());
    }

    @Test
    public void testGetSignalToContentManagerWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(SignalToContentManager.class, wrongId);
        Mockito.when(service.getSignalToContentManager(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                        "signals-to-content-manager/{signalToContentManagerId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateSignalToContentManager() throws Exception {
        SignalToContentManagerCreateDTO create = generateObject(SignalToContentManagerCreateDTO.class);
        create.setFromIndex(10);
        create.setToIndex(20);
        UUID randomUUID = UUID.randomUUID();

        SignalToContentManagerReadDTO read = generateObject(SignalToContentManagerReadDTO.class);
        read.setFromIndex(10);
        read.setToIndex(20);

        Mockito.when(service.createSignalToContentManager(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/signals-to-content-manager", randomUUID)
                        .content(objectMapper.writeValueAsString(create))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToContentManager = objectMapper.readValue(
                resultJson, SignalToContentManagerReadDTO.class);
        Assertions.assertThat(actualSignalToContentManager).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchSignalToContentManager() throws Exception {
        SignalToContentManagerPatchDTO patch = generateObject(SignalToContentManagerPatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        SignalToContentManagerReadDTO read = generateObject(SignalToContentManagerReadDTO.class);

        Mockito.when(service.patchSignalToContentManager(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}" +
                "/signals-to-content-manager/{signalToContentManagerId}", randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToContentManager = objectMapper.readValue(
                resultJson, SignalToContentManagerReadDTO.class);
        Assert.assertEquals(read, actualSignalToContentManager);
    }

    @Test
    public void testDeleteSignalToContentManager() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/"
                        + "signals-to-content-manager/{signalToContentManagerId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteSignalToContentManager(randomUUID, id);
    }

    @Test
    public void testPutSignalToContentManager() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        SignalToContentManagerPutDTO put = generateObject(SignalToContentManagerPutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/"
                        + "signals-to-content-manager/{signalToContentManagerId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateSignalToContentManager(randomUUID, id, put);
    }

    @Test
    public void testGetRegisteredUserSignalToContentManagers() throws Exception {
        SignalToContentManagerReadDTO signalToContentManager1 = generateObject(SignalToContentManagerReadDTO.class);
        SignalToContentManagerReadDTO signalToContentManager2 = generateObject(SignalToContentManagerReadDTO.class);

        List<SignalToContentManagerReadDTO> signalToContentManagersReadDTO = new ArrayList<>();
        signalToContentManagersReadDTO.add(signalToContentManager1);
        signalToContentManagersReadDTO.add(signalToContentManager2);

        Mockito.when(service.getRegisteredUserSignalToContentManagers(signalToContentManager1.getRegisteredUserId()))
                .thenReturn(signalToContentManagersReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/signals-to-content-manager",
                signalToContentManager1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<SignalToContentManagerReadDTO> actualSignalToContentManagersReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualSignalToContentManagersReadDTO.size());
        Assertions.assertThat(actualSignalToContentManagersReadDTO).extracting("id")
                .containsExactlyInAnyOrder(signalToContentManager1.getId(), signalToContentManager2.getId());

        Mockito.verify(service).getRegisteredUserSignalToContentManagers(
                signalToContentManager1.getRegisteredUserId());
    }
}
