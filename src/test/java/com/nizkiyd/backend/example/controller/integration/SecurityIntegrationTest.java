package com.nizkiyd.backend.example.controller.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.movie.MovieReadDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.repository.ContentManagerRepository;
import com.nizkiyd.backend.example.repository.ModeratorRepository;
import com.nizkiyd.backend.example.repository.RegisteredUserRepository;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.apache.xmlbeans.impl.util.Base64;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@DirtiesContext
@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SecurityIntegrationTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Test
    public void testHealthNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Void> response = restTemplate.getForEntity("http://localhost:8080/health", Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetMoviesNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + registeredUser.getId() + "/movies", HttpMethod.GET,
                HttpEntity.EMPTY, new ParameterizedTypeReference<PageResult<MovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMovies() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setEmail(email);
        registeredUser.setEncodedPassword(passwordEncoder.encode(password));
        registeredUser.setName("dsds");
        registeredUser.setGender(Gender.MALE);
        registeredUser.setAge(22);
        registeredUserRepository.save(registeredUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<MovieReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + registeredUser.getId() + "/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetMoviesWrongPassword() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setEmail(email);
        registeredUser.setEncodedPassword(passwordEncoder.encode(password));
        registeredUser.setName("dsds");
        registeredUser.setGender(Gender.MALE);
        registeredUser.setAge(22);
        registeredUserRepository.save(registeredUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, "wrong pass"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<MovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMovieWrongRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setEmail(email);
        registeredUser.setEncodedPassword(passwordEncoder.encode(password));
        registeredUser.setName("dsds");
        registeredUser.setGender(Gender.MALE);
        registeredUser.setAge(22);
        registeredUserRepository.save(registeredUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue("wrong registered user", password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<MovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetMoviesNoSession() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setEmail(email);
        registeredUser.setEncodedPassword(passwordEncoder.encode(password));
        registeredUser.setName("dsds");
        registeredUser.setGender(Gender.MALE);
        registeredUser.setAge(22);
        registeredUserRepository.save(registeredUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<MovieReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/users/" + registeredUser.getId() + "/movies", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Assert.assertNull(response.getHeaders().get("Set-Cookie"));
    }

    @Test
    public void testGetSignalToContentManagersAuthorizationForbidden() {
        RegisteredUser registeredUser = createRegisteredUser("test@gmail.com", "abc");

        String moderatorEmail = "test123@gmail.com";
        String moderatorPassword = "qwe123";

        Moderator moderator = new Moderator();
        moderator.setEmail(moderatorEmail);
        moderator.setEncodedPassword(passwordEncoder.encode(moderatorPassword));
        moderator.setName("hello!");
        moderator = moderatorRepository.save(moderator);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(moderatorEmail, moderatorPassword));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/registered-users/" + registeredUser.getId() +
                        "/signals-to-content-manager", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<SignalToContentManagerReadDTO>>() {
                })).isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetSignalToContentManagersContentManager() {
        String emailContentManager = "test123@gmail.com";
        String passwordContentManager = "pass123";

        RegisteredUser registeredUser = createRegisteredUser("test@gmail.com", "pass");

        ContentManager contentManager = new ContentManager();
        contentManager.setEmail(emailContentManager);
        contentManager.setEncodedPassword(passwordEncoder.encode(passwordContentManager));
        contentManager.setName("hello!");
        contentManagerRepository.save(contentManager);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(emailContentManager,
                passwordContentManager));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<SignalToContentManagerReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/registered-users/" + registeredUser.getId() +
                        "/signals-to-content-manager", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetSignalToContentManagersRegisteredUser() {
        String password = "pass";

        RegisteredUser registeredUser = createRegisteredUser("test@gmail.com", password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(
                registeredUser.getEmail(), "pass"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<SignalToContentManagerReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/registered-users/" + registeredUser.getId() +
                        "/signals-to-content-manager", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetSignalToContentManagersWrongRegisteredUser() {
        String password = "pass123";

        RegisteredUser ru1 = createRegisteredUser("test1@gmail.com", "abc");
        RegisteredUser ru2 = createRegisteredUser("test2@gmail.com", password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(ru2.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/registered-users/" + ru1.getId() + "/signals-to-content-manager",
                HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<SignalToContentManagerReadDTO>>() {
                })).isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testCreateSignalToContentManager() throws JsonProcessingException {
        String email = "test@gmail.com";
        String password = "pass";

        RegisteredUser registeredUser = createRegisteredUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        Movie movie = testObjectsFactory.createMovie();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        SignalToContentManagerCreateDTO create = new SignalToContentManagerCreateDTO();
        create.setReplacement("sds");
        create.setFromIndex(4);
        create.setToIndex(8);
        create.setSignalToContentManagerObjectId(movie.getId());
        create.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.MOVIE);
        create.setContentManagerId(contentManager.getId());
        create.setWrongText("qweqwe");

        HttpEntity<?> httpEntity = new HttpEntity<>(create, headers);

        ResponseEntity<SignalToContentManagerReadDTO> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/registered-users/" + registeredUser.getId() +
                        "/signals-to-content-manager",
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", userName, password).getBytes()));
    }

    private RegisteredUser createRegisteredUser(String email, String password) {
        RegisteredUser ru = new RegisteredUser();
        ru.setEmail(email);
        ru.setEncodedPassword(passwordEncoder.encode(password));
        ru.setName("dsds");
        ru.setGender(Gender.MALE);
        ru.setAge(22);
        return ru = registeredUserRepository.save(ru);
    }
}