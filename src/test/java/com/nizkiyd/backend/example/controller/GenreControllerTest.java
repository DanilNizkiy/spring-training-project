package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.dto.genre.GenreReadDTO;
import com.nizkiyd.backend.example.dto.genre.GenreTypeDTO;
import com.nizkiyd.backend.example.service.GenreService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = GenreController.class)
public class GenreControllerTest extends BaseControllerTest {

    @MockBean
    private GenreService genreService;

    @WithMockUser
    @Test
    public void testAddGenreToMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = generateObject(GenreReadDTO.class);
        read.setId(genreId);
        List<GenreReadDTO> expectedGenre = List.of(read);
        Mockito.when(genreService.addGenreToMovie(movieId, genreId)).thenReturn(expectedGenre);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/genres/{id}", movieId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedGenre, actualGenres);
    }

    @Test
    public void testGetMovieGenres() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = generateObject(GenreReadDTO.class);
        read.setId(genreId);
        List<GenreReadDTO> expectedGenre = List.of(read);
        Mockito.when(genreService.getMovieGenres(movieId)).thenReturn(expectedGenre);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/genres", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedGenre, actualGenres);
    }

    @WithMockUser
    @Test
    public void testGetMovieGenresWithUserId() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = generateObject(GenreReadDTO.class);
        read.setId(genreId);
        List<GenreReadDTO> expectedGenre = List.of(read);
        Mockito.when(genreService.getMovieGenres(movieId)).thenReturn(expectedGenre);

        String resultJson = mvc.perform(get("/api/v1/users/{usersId}/movies/{movieId}/genres",
                UUID.randomUUID(), movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedGenre, actualGenres);
    }

    @WithMockUser
    @Test
    public void testGetStaticGenreIdByType() throws Exception {
        GenreTypeDTO genreTypeDTO = generateObject(GenreTypeDTO.class);
        GenreReadDTO read = generateObject(GenreReadDTO.class);

        Mockito.when(genreService.getStaticGenreIdByType(genreTypeDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/genres/type", read.getId().toString())
                .content(objectMapper.writeValueAsString(genreTypeDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assert.assertEquals(read, actualGenre);
    }

    @WithMockUser
    @Test
    public void testRemoveGenreFromMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{movieId}/genres/{id}", movieId.toString(), id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(genreService).removeGenreFromMovie(movieId, id);
    }
}