package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Moderator;
import com.nizkiyd.backend.example.dto.moderator.ModeratorCreateDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorPatchDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.ModeratorService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ModeratorController.class)
public class ModeratorControllerTest extends BaseControllerTest {

    @MockBean
    private ModeratorService moderatorService;

    @Test
    public void testGetModerator() throws Exception {
        ModeratorReadDTO moderator = generateObject(ModeratorReadDTO.class);

        Mockito.when(moderatorService.getModerator(moderator.getId())).thenReturn(moderator);

        String resultJson = mvc.perform(get("/api/v1/moderators/{id}", moderator.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assertions.assertThat(actualModerator).isEqualToComparingFieldByField(moderator);

        Mockito.verify(moderatorService).getModerator(moderator.getId());
    }

    @Test
    public void testGetModeratorWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Moderator.class, wrongId);
        Mockito.when(moderatorService.getModerator(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/moderators/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }


    @Test
    public void testCreateModerator() throws Exception {
        ModeratorCreateDTO create = generateObject(ModeratorCreateDTO.class);

        ModeratorReadDTO read = generateObject(ModeratorReadDTO.class);

        Mockito.when(moderatorService.createModerator(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/moderators")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assertions.assertThat(actualModerator).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchModerator() throws Exception {
        ModeratorPatchDTO patch = generateObject(ModeratorPatchDTO.class);

        ModeratorReadDTO read = generateObject(ModeratorReadDTO.class);

        Mockito.when(moderatorService.patchModerator(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/moderators/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assert.assertEquals(read, actualModerator);
    }

    @WithMockUser
    @Test
    public void testDeleteModerator() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/moderators/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(moderatorService).deleteModerator(id);
    }

    @WithMockUser
    @Test
    public void testGetInvalidModerators() throws Exception {
        ModeratorReadDTO moderator1 = generateObject(ModeratorReadDTO.class);
        ModeratorReadDTO moderator2 = generateObject(ModeratorReadDTO.class);

        List<ModeratorReadDTO> moderatorsReadDTO = new ArrayList<>();
        moderatorsReadDTO.add(moderator1);
        moderatorsReadDTO.add(moderator2);

        Mockito.when(moderatorService.getInvalidModerators()).thenReturn(moderatorsReadDTO);

        String resultJson = mvc.perform(get("/api/v1/moderators/invalid-account"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ModeratorReadDTO> actualModeratorsReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualModeratorsReadDTO.size());
        Assertions.assertThat(actualModeratorsReadDTO).extracting("id")
                .containsExactlyInAnyOrder(moderator1.getId(), moderator2.getId());

        Mockito.verify(moderatorService).getInvalidModerators();
    }
}