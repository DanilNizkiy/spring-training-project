package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.role.RoleFilter;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.RoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RoleController.class)
public class RoleControllerTest extends BaseControllerTest {

    @MockBean
    private RoleService roleService;

    @Test
    public void testGetRole() throws Exception {
        RoleReadDTO role = generateObject(RoleReadDTO.class);

        Mockito.when(roleService.getRole(role.getId())).thenReturn(role);

        String resultJson = mvc.perform(get("/api/v1/roles/{id}", role.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(role);

        Mockito.verify(roleService).getRole(role.getId());
    }

    @Test
    public void testGetRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Role.class, wrongId);
        Mockito.when(roleService.getRole(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchRole() throws Exception {
        RolePatchDTO patch = generateObject(RolePatchDTO.class);
        ;

        RoleReadDTO read = generateObject(RoleReadDTO.class);

        Mockito.when(roleService.patchRole(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualRole);
    }

    @Test
    public void testDeleteRole() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/roles/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(roleService).deleteRole(id);
    }

    @Test
    public void testPutRole() throws Exception {
        UUID id = UUID.randomUUID();

        RolePutDTO put = generateObject(RolePutDTO.class);

        mvc.perform(put("/api/v1/roles/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(roleService).updateRole(id, put);
    }

    @Test
    public void testGetRoles() throws Exception {
        RoleFilter filter = new RoleFilter();
        filter.setActorId(UUID.randomUUID());
        filter.setAverageMarkFrom(6.0);
        filter.setAverageMarkTo(10.0);

        RoleReadDTO readDTO = generateObject(RoleReadDTO.class);

        PageResult<RoleReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(roleService.getRole(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/roles")
                .param("actorId", filter.getActorId().toString())
                .param("averageMarkFrom", filter.getAverageMarkFrom().toString())
                .param("averageMarkTo", filter.getAverageMarkTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetRoleWithPagingAndSorting() throws Exception {
        RoleFilter roleFilter = new RoleFilter();
        RoleReadDTO readDTO = generateObject(RoleReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<RoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "averageMark"));
        Mockito.when(roleService.getRole(roleFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/roles")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "averageMark,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}