package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.domain.Admin;
import com.nizkiyd.backend.example.dto.admin.AdminCreateDTO;
import com.nizkiyd.backend.example.dto.admin.AdminPatchDTO;
import com.nizkiyd.backend.example.dto.admin.AdminReadDTO;
import com.nizkiyd.backend.example.dto.admin.AdminStatusDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.AdminService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = AdminController.class)
public class AdminControllerTest extends BaseControllerTest {

    @MockBean
    private AdminService adminService;

    @Test
    public void testGetAdmin() throws Exception {
        AdminReadDTO admin = generateObject(AdminReadDTO.class);

        Mockito.when(adminService.getAdmin(admin.getId())).thenReturn(admin);

        String resultJson = mvc.perform(get("/api/v1/admins/{id}", admin.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assertions.assertThat(actualAdmin).isEqualToComparingFieldByField(admin);

        Mockito.verify(adminService).getAdmin(admin.getId());
    }

    @Test
    public void testGetAdminWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Admin.class, wrongId);
        Mockito.when(adminService.getAdmin(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/admins/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }


    @Test
    public void testCreateAdmin() throws Exception {
        AdminCreateDTO create = generateObject(AdminCreateDTO.class);

        AdminReadDTO read = generateObject(AdminReadDTO.class);

        Mockito.when(adminService.createAdmin(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/admins")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assertions.assertThat(actualAdmin).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchAdmin() throws Exception {
        AdminPatchDTO patch = generateObject(AdminPatchDTO.class);

        AdminReadDTO read = generateObject(AdminReadDTO.class);

        Mockito.when(adminService.patchAdmin(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/admins/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testChangeAdminStatus() throws Exception {
        AdminStatusDTO statusDTO = generateObject(AdminStatusDTO.class);

        AdminReadDTO read = generateObject(AdminReadDTO.class);

        Mockito.when(adminService.changeAdminStatus(read.getId(), statusDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/admins/{id}/account-status", read.getId().toString())
                .content(objectMapper.writeValueAsString(statusDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteAdmin() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/admins/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(adminService).deleteAdmin(id);
    }
}