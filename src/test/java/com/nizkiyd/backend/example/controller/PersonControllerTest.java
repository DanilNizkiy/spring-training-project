package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.domain.Person;
import com.nizkiyd.backend.example.dto.person.PersonCreateDTO;
import com.nizkiyd.backend.example.dto.person.PersonPatchDTO;
import com.nizkiyd.backend.example.dto.person.PersonPutDTO;
import com.nizkiyd.backend.example.dto.person.PersonReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.PersonService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PersonController.class)
public class PersonControllerTest extends BaseControllerTest {

    @MockBean
    private PersonService personService;

    @Test
    public void testGetPerson() throws Exception {
        PersonReadDTO person = generateObject(PersonReadDTO.class);

        Mockito.when(personService.getPerson(person.getId())).thenReturn(person);

        String resultJson = mvc.perform(get("/api/v1/person/{id}", person.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(person);

        Mockito.verify(personService).getPerson(person.getId());
    }

    @Test
    public void testGetPersonWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Person.class, wrongId);
        Mockito.when(personService.getPerson(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/person/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testCreatePerson() throws Exception {
        PersonCreateDTO create = generateObject(PersonCreateDTO.class);

        PersonReadDTO read = generateObject(PersonReadDTO.class);

        Mockito.when(personService.createPerson(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/person")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchPerson() throws Exception {
        PersonPatchDTO patch = generateObject(PersonPatchDTO.class);

        PersonReadDTO read = generateObject(PersonReadDTO.class);

        Mockito.when(personService.patchPerson(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/person/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualPerson);
    }

    @WithMockUser
    @Test
    public void testDeletePerson() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/person/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(personService).deletePerson(id);
    }

    @WithMockUser
    @Test
    public void testPutPerson() throws Exception {
        UUID id = UUID.randomUUID();

        PersonPutDTO put = generateObject(PersonPutDTO.class);

        mvc.perform(put("/api/v1/person/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(personService).updatePerson(id, put);
    }
}