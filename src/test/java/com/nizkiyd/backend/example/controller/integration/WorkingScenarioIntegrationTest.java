package com.nizkiyd.backend.example.controller.integration;

import com.nizkiyd.backend.example.ProjectConstants;
import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.actor.ActorCreateDTO;
import com.nizkiyd.backend.example.dto.actor.ActorReadDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintCreateDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintProcessingDTO;
import com.nizkiyd.backend.example.dto.complaint.ComplaintReadDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerStatusDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerReadDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberCreateDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberReadDTO;
import com.nizkiyd.backend.example.dto.genre.GenreReadDTO;
import com.nizkiyd.backend.example.dto.genre.GenreTypeDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorStatusDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorCreateDTO;
import com.nizkiyd.backend.example.dto.moderator.ModeratorReadDTO;
import com.nizkiyd.backend.example.dto.movie.MovieCreateDTO;
import com.nizkiyd.backend.example.dto.movie.MovieReadDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkCreateDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewCreateDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.dto.news.NewsCreateDTO;
import com.nizkiyd.backend.example.dto.news.NewsReadDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeCreateDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePutDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.dto.person.PersonCreateDTO;
import com.nizkiyd.backend.example.dto.person.PersonReadDTO;
import com.nizkiyd.backend.example.dto.registereduser.RegisteredUserCreateDTO;
import com.nizkiyd.backend.example.dto.registereduser.RegisteredUserPatchDTO;
import com.nizkiyd.backend.example.dto.registereduser.RegisteredUserReadDTO;
import com.nizkiyd.backend.example.dto.registereduser.RegisteredUserTrustPatchDTO;
import com.nizkiyd.backend.example.dto.role.RoleCreateDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkCreateDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerFixContent;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.nizkiyd.backend.example.service.BaseTest;
import com.nizkiyd.backend.example.util.TestObjectsFactory;
import org.apache.xmlbeans.impl.util.Base64;
import org.assertj.core.api.Assertions;
import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.util.*;

@DirtiesContext
@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Import(WorkingScenarioIntegrationTest.ScheduledTestConfig.class)
public class WorkingScenarioIntegrationTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper objectMapper;

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    private RestTemplate restTemplate;

    private List<HttpMessageConverter<?>> oldConverters;

    private final String urlApi = "http://localhost:8080/api/v1";

    {
        Configuration c = new Configuration();
        Configuration.Bean bean = c.beanOfClass(RegisteredUserCreateDTO.class);
        Configuration.Bean.Property p = bean.property("age");
        p.setMinValue(0);
        p.setMaxValue(100);
        generator = new RandomObjectGenerator(c);
    }

    public WorkingScenarioIntegrationTest() {
        config();
    }

    @EnableScheduling
    static class ScheduledTestConfig {
    }

    @Test
    public void testWorkingScenarioIntegration() throws InterruptedException {
        String a1Password = "a1password123";
        String m1Password = "m1password123";
        String c1Password = "c1Password";
        String u1Password = "u1password123";
        String u2Password = "u2password123";
        String u3Password = "u3password123";
        Admin a1 = testObjectsFactory.creteAdmin("adminEmail@gmail.com", passwordEncoder.encode(a1Password));

        //FINAL_1
        ModeratorCreateDTO m1CreateDTO = generator.generateRandomObject(ModeratorCreateDTO.class);
        m1CreateDTO.setEmail("moderatorEmail@gmail.com");
        m1CreateDTO.setPassword(m1Password);

        ModeratorReadDTO m1ReadDTO = performRequest(
                "/moderators",
                HttpMethod.POST,
                null,
                null,
                m1CreateDTO,
                ModeratorReadDTO.class);
        Assertions.assertThat(m1CreateDTO).isEqualToIgnoringGivenFields(m1ReadDTO, "password");
        Assert.assertEquals(AccountStatus.INVALID, m1ReadDTO.getStatus());


        //FINAL_2
        List<ModeratorReadDTO> invalidModerators = performRequestCollection(
                "/moderators/invalid-account",
                HttpMethod.GET,
                a1.getEmail(),
                a1Password,
                null,
                ModeratorReadDTO.class);
        Assert.assertEquals(1, invalidModerators.size());
        Assert.assertEquals(m1CreateDTO.getEmail(), invalidModerators.get(0).getEmail());


        UUID m1Id = invalidModerators.get(0).getId();
        ModeratorStatusDTO m1statusDTO = new ModeratorStatusDTO();
        m1statusDTO.setStatus(AccountStatus.VALID);

        ModeratorReadDTO m1AfterChangeAccountStatus = performRequest(
                "/moderators/" + m1Id + "/account-status",
                HttpMethod.PATCH,
                a1.getEmail(),
                a1Password,
                m1statusDTO,
                ModeratorReadDTO.class);
        Assert.assertEquals(m1statusDTO.getStatus(), m1AfterChangeAccountStatus.getStatus());


        //FINAL_3
        ContentManagerCreateDTO c1CreateDTO = generator.generateRandomObject(ContentManagerCreateDTO.class);
        c1CreateDTO.setEmail("c1Email@gmail.com");
        c1CreateDTO.setPassword(c1Password);

        ContentManagerReadDTO c1ReadDTO = performRequest(
                "/content-managers",
                HttpMethod.POST,
                null,
                null,
                c1CreateDTO,
                ContentManagerReadDTO.class);
        Assertions.assertThat(c1CreateDTO).isEqualToIgnoringGivenFields(c1ReadDTO, "password");
        Assert.assertEquals(AccountStatus.INVALID, c1ReadDTO.getStatus());

        //FINAL_4
        List<ContentManagerReadDTO> invalidContentManagers = performRequestCollection(
                "/content-managers/invalid-account",
                HttpMethod.GET,
                a1.getEmail(),
                a1Password,
                null,
                ContentManagerReadDTO.class
        );
        Assert.assertEquals(1, invalidContentManagers.size());
        Assert.assertEquals(c1CreateDTO.getEmail(), invalidContentManagers.get(0).getEmail());


        UUID c1Id = invalidContentManagers.get(0).getId();
        ContentManagerStatusDTO c1StatusDTO = new ContentManagerStatusDTO();
        c1StatusDTO.setStatus(AccountStatus.VALID);

        ContentManagerReadDTO c1AfterChangeAccountStatus = performRequest(
                "/content-managers/" + c1Id + "/account-status",
                HttpMethod.PATCH,
                a1.getEmail(),
                a1Password,
                c1StatusDTO,
                ContentManagerReadDTO.class
        );
        Assert.assertEquals(c1StatusDTO.getStatus(), c1AfterChangeAccountStatus.getStatus());


        //FINAL_5
        RegisteredUserCreateDTO u1CreateDTO = generator.generateRandomObject(RegisteredUserCreateDTO.class);
        u1CreateDTO.setEmail("u1email@gmail.com");
        u1CreateDTO.setPassword(u1Password);
        u1CreateDTO.setGender(Gender.MALE);

        RegisteredUserReadDTO u1ReadDTO = performRequest(
                "/registered-users",
                HttpMethod.POST,
                null,
                null,
                u1CreateDTO,
                RegisteredUserReadDTO.class
        );
        Assertions.assertThat(u1CreateDTO).isEqualToIgnoringGivenFields(u1ReadDTO, "password");
        Assert.assertEquals(AccountStatus.VALID, u1ReadDTO.getStatus());


        RegisteredUserCreateDTO u2CreateDTO = generator.generateRandomObject(RegisteredUserCreateDTO.class);
        u2CreateDTO.setEmail("u2email@gmail.com");
        u2CreateDTO.setPassword(u2Password);
        u2CreateDTO.setGender(Gender.MALE);

        RegisteredUserReadDTO u2ReadDTO = performRequest(
                "/registered-users",
                HttpMethod.POST,
                null,
                null,
                u2CreateDTO,
                RegisteredUserReadDTO.class
        );
        Assertions.assertThat(u2CreateDTO).isEqualToIgnoringGivenFields(u2ReadDTO, "password");
        Assert.assertEquals(AccountStatus.VALID, u2ReadDTO.getStatus());


        RegisteredUserCreateDTO u3CreateDTO = generator.generateRandomObject(RegisteredUserCreateDTO.class);
        u3CreateDTO.setEmail("u3email@gmail.com");
        u3CreateDTO.setPassword(u3Password);
        u3CreateDTO.setGender(Gender.FEMALE);

        RegisteredUserReadDTO u3ReadDTO = performRequest(
                "/registered-users",
                HttpMethod.POST,
                null,
                null,
                u3CreateDTO,
                RegisteredUserReadDTO.class
        );
        Assertions.assertThat(u3CreateDTO).isEqualToIgnoringGivenFields(u3ReadDTO, "password");
        Assert.assertEquals(AccountStatus.VALID, u3ReadDTO.getStatus());


        //FINAL_6
        RegisteredUserPatchDTO u2PathDTO = new RegisteredUserPatchDTO();
        u2PathDTO.setName("new name");

        RegisteredUserReadDTO u2AfterChangeName = performRequest(
                "/registered-users/" + u2ReadDTO.getId(),
                HttpMethod.PATCH,
                u2ReadDTO.getEmail(),
                u2Password,
                u2PathDTO,
                RegisteredUserReadDTO.class
        );
        Assert.assertNotEquals(u2AfterChangeName, u2ReadDTO.getName());
        Assert.assertEquals(u2PathDTO.getName(), u2AfterChangeName.getName());


        //FINAL_7
        MovieCreateDTO movieCreateDTO = new MovieCreateDTO();
        movieCreateDTO.setTitle("The Castle");
        movieCreateDTO.setAdult(false);
        movieCreateDTO.setBudget(10000);
        movieCreateDTO.setOriginalLanguage("ru");
        movieCreateDTO.setDescription(ProjectConstants.DESCRIPTION_THE_CASTLE);
        movieCreateDTO.setReleaseDate(LocalDate.of(1994, 1, 1));
        movieCreateDTO.setDuration("120");
        movieCreateDTO.setCountry("France, Russia, Germany");
        movieCreateDTO.setRevenue(100000);
        movieCreateDTO.setMovieStatus(MovieStatus.RELEASED);

        MovieReadDTO movieReadDTO = performRequest(
                "/movies",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                movieCreateDTO,
                MovieReadDTO.class
        );
        Assertions.assertThat(movieCreateDTO).isEqualToComparingFieldByField(movieReadDTO);


        GenreTypeDTO genreTypeDTO = new GenreTypeDTO();
        genreTypeDTO.setType(GenreType.DRAMA);

        GenreReadDTO genreReadDTO = performRequest(
                "/genres/type",
                HttpMethod.PATCH,
                c1ReadDTO.getEmail(),
                c1Password,
                genreTypeDTO,
                GenreReadDTO.class);
        Assert.assertEquals(genreTypeDTO.getType(), genreReadDTO.getType());


        List<GenreReadDTO> movieGenres = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/genres/" + genreReadDTO.getId(),
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                genreTypeDTO,
                GenreReadDTO.class
        );
        Assert.assertEquals(1, movieGenres.size());
        Assert.assertEquals(genreReadDTO.getId(), movieGenres.get(0).getId());


        PersonCreateDTO person1CreateDTO = new PersonCreateDTO();
        person1CreateDTO.setName("Nikolay Stotskiy");
        person1CreateDTO.setGender(Gender.MALE);
        person1CreateDTO.setPlaceOfBirth("Novosibirsk");
        person1CreateDTO.setBirthday(LocalDate.of(1964, 1, 10));
        person1CreateDTO.setAdult(false);
        person1CreateDTO.setBiography(ProjectConstants.BIOGRAPHY_STOTSKIY);

        PersonReadDTO person1ReadDTO = performRequest(
                "/person",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                person1CreateDTO,
                PersonReadDTO.class
        );
        Assertions.assertThat(person1CreateDTO).isEqualToComparingFieldByField(person1ReadDTO);


        ActorCreateDTO actor1CreateDTO = new ActorCreateDTO();
        actor1CreateDTO.setPersonId(person1ReadDTO.getId());

        ActorReadDTO actor1ReadDTO = performRequest(
                "/actors",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                actor1CreateDTO,
                ActorReadDTO.class);
        Assertions.assertThat(actor1CreateDTO).isEqualToComparingFieldByField(actor1ReadDTO);


        RoleCreateDTO role1CreateDTO = new RoleCreateDTO();
        role1CreateDTO.setRoleName("Land Surveyor");
        role1CreateDTO.setGender(Gender.MALE);
        role1CreateDTO.setActorId(actor1ReadDTO.getId());

        RoleReadDTO role1ReadDTO = performRequest(
                "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                role1CreateDTO,
                RoleReadDTO.class
        );
        Assertions.assertThat(role1CreateDTO).isEqualToComparingFieldByField(role1ReadDTO);


        PersonCreateDTO person2CreateDTO = new PersonCreateDTO();
        person2CreateDTO.setName("Svetlana Pismichenko");
        person2CreateDTO.setGender(Gender.FEMALE);
        person2CreateDTO.setPlaceOfBirth("Burli, Komsomolsky district, Kustanai region, Tselinny region");
        person2CreateDTO.setBirthday(LocalDate.of(1964, 11, 28));
        person2CreateDTO.setAdult(false);
        person2CreateDTO.setBiography(ProjectConstants.BIOGRAPHY_PISMICHENKO);

        PersonReadDTO person2ReadDTO = performRequest(
                "/person",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password, person2CreateDTO,
                PersonReadDTO.class
        );
        Assertions.assertThat(person2CreateDTO).isEqualToComparingFieldByField(person2ReadDTO);


        ActorCreateDTO actor2CreateDTO = new ActorCreateDTO();
        actor2CreateDTO.setPersonId(person2ReadDTO.getId());

        ActorReadDTO actor2ReadDTO = performRequest(
                "/actors",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                actor2CreateDTO,
                ActorReadDTO.class);
        Assertions.assertThat(actor2CreateDTO).isEqualToComparingFieldByField(actor2ReadDTO);


        RoleCreateDTO role2CreateDTO = new RoleCreateDTO();
        role2CreateDTO.setRoleName("Frida");
        role2CreateDTO.setGender(Gender.FEMALE);
        role2CreateDTO.setActorId(actor2ReadDTO.getId());

        RoleReadDTO role2ReadDTO = performRequest(
                "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                role2CreateDTO,
                RoleReadDTO.class
        );
        Assertions.assertThat(role2CreateDTO).isEqualToComparingFieldByField(role2ReadDTO);


        PersonCreateDTO person3CreateDTO = new PersonCreateDTO();
        person3CreateDTO.setName("Viktor Sukhorukov");
        person3CreateDTO.setGender(Gender.MALE);
        person3CreateDTO.setPlaceOfBirth("Orekhovo-Zuevo, USSR (Russia)");
        person3CreateDTO.setBirthday(LocalDate.of(1951, 11, 10));
        person3CreateDTO.setAdult(false);
        person3CreateDTO.setBiography(ProjectConstants.BIOGRAPHY_SUKHORUKOV);

        PersonReadDTO person3ReadDTO = performRequest(
                "/person",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                person3CreateDTO,
                PersonReadDTO.class
        );
        Assertions.assertThat(person3CreateDTO).isEqualToComparingFieldByField(person3ReadDTO);


        ActorCreateDTO actor3CreateDTO = new ActorCreateDTO();
        actor3CreateDTO.setPersonId(person3ReadDTO.getId());

        ActorReadDTO actor3ReadDTO = performRequest(
                "/actors",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                actor3CreateDTO,
                ActorReadDTO.class);
        Assertions.assertThat(actor3CreateDTO).isEqualToComparingFieldByField(actor3ReadDTO);


        RoleCreateDTO role3CreateDTO = new RoleCreateDTO();
        role3CreateDTO.setRoleName("Helper");
        role3CreateDTO.setGender(Gender.MALE);
        role3CreateDTO.setActorId(actor3ReadDTO.getId());

        RoleReadDTO role3ReadDTO = performRequest(
                "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                role3CreateDTO,
                RoleReadDTO.class
        );
        Assertions.assertThat(role3CreateDTO).isEqualToComparingFieldByField(role3ReadDTO);


        PersonCreateDTO person4CreateDTO = new PersonCreateDTO();
        person4CreateDTO.setName("Aleksei Balabanov");
        person4CreateDTO.setGender(Gender.MALE);
        person4CreateDTO.setPlaceOfBirth("Sverdlovsk, USSR (Ekaterinburg, Russia)");
        person4CreateDTO.setBirthday(LocalDate.of(1959, 2, 25));
        person4CreateDTO.setAdult(false);
        person4CreateDTO.setBiography(ProjectConstants.BIOGRAPHY_BALABANOV);

        PersonReadDTO person4ReadDTO = performRequest(
                "/person",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                person4CreateDTO,
                PersonReadDTO.class
        );
        Assertions.assertThat(person4CreateDTO).isEqualToComparingFieldByField(person4ReadDTO);


        CrewMemberCreateDTO crewMember1CreateDTO = new CrewMemberCreateDTO();
        crewMember1CreateDTO.setCrewMemberType(CrewMemberType.DIRECTOR);
        crewMember1CreateDTO.setPersonId(person4ReadDTO.getId());

        CrewMemberReadDTO crewMember1ReadDTO = performRequest(
                "/crew-members",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                crewMember1CreateDTO,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMember1CreateDTO).isEqualToComparingFieldByField(crewMember1ReadDTO);


        List<CrewMemberReadDTO> crewMembersToMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/crew-members/" + crewMember1ReadDTO.getId(),
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMembersToMovie).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(crewMember1ReadDTO.getId());


        PersonCreateDTO person5CreateDTO = new PersonCreateDTO();
        person5CreateDTO.setName("Andrei Zhegalov");
        person5CreateDTO.setGender(Gender.MALE);
        person5CreateDTO.setPlaceOfBirth("Tashkent, Uzbek SSR, USSR [now Uzbekistan]");
        person5CreateDTO.setBirthday(LocalDate.of(1964, 5, 27));
        person5CreateDTO.setAdult(false);
        person5CreateDTO.setBiography(ProjectConstants.BIOGRAPHY_SHEGALOV);


        PersonReadDTO person5ReadDTO = performRequest(
                "/person",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                person5CreateDTO,
                PersonReadDTO.class
        );
        Assertions.assertThat(person5CreateDTO).isEqualToComparingFieldByField(person5ReadDTO);


        CrewMemberCreateDTO crewMember2CreateDTO = new CrewMemberCreateDTO();
        crewMember2CreateDTO.setCrewMemberType(CrewMemberType.CAMERA);
        crewMember2CreateDTO.setPersonId(person5ReadDTO.getId());

        CrewMemberReadDTO crewMember2ReadDTO = performRequest(
                "/crew-members",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                crewMember2CreateDTO,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMember2CreateDTO).isEqualToComparingFieldByField(crewMember2ReadDTO);


        crewMembersToMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/crew-members/" + crewMember2ReadDTO.getId(),
                HttpMethod.POST, c1ReadDTO.getEmail(), c1Password, null, CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMembersToMovie).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(crewMember2ReadDTO.getId(), crewMember1ReadDTO.getId());


        PersonCreateDTO person6CreateDTO = new PersonCreateDTO();
        person6CreateDTO.setName("Sergei Kuryokhin");
        person6CreateDTO.setGender(Gender.MALE);
        person6CreateDTO.setPlaceOfBirth("Murmansk, Russia");
        person6CreateDTO.setBirthday(LocalDate.of(1954, 6, 16));
        person6CreateDTO.setAdult(false);
        person6CreateDTO.setBiography(ProjectConstants.BIOGRAPHY_KURYOKHIN);

        PersonReadDTO person6ReadDTO = performRequest(
                "/person", HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                person6CreateDTO,
                PersonReadDTO.class
        );
        Assertions.assertThat(person6CreateDTO).isEqualToComparingFieldByField(person6ReadDTO);


        CrewMemberCreateDTO crewMember3CreateDTO = new CrewMemberCreateDTO();
        crewMember3CreateDTO.setCrewMemberType(CrewMemberType.MUSIC);
        crewMember3CreateDTO.setPersonId(person6ReadDTO.getId());

        CrewMemberReadDTO crewMember3ReadDTO = performRequest(
                "/crew-members",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                crewMember3CreateDTO,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMember3CreateDTO).isEqualToComparingFieldByField(crewMember3ReadDTO);


        crewMembersToMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/crew-members/" + crewMember3ReadDTO.getId(),
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMembersToMovie).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(crewMember2ReadDTO.getId(), crewMember1ReadDTO.getId(),
                        crewMember3ReadDTO.getId());


        //FINAL_8
        NewsCreateDTO newsCreateDTO = new NewsCreateDTO();
        newsCreateDTO.setText(ProjectConstants.NEWS_TEXT);

        NewsReadDTO newsReadDTO = performRequest(
                "/news",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                newsCreateDTO,
                NewsReadDTO.class
        );
        Assertions.assertThat(newsCreateDTO).isEqualToComparingFieldByField(newsReadDTO);


        String movieTitle = "The Castle";
        PageResult<MovieReadDTO> moviePageResult = performRequestPageResult(
                "/users/" + c1ReadDTO.getId() + "/movies?title=" + movieTitle,
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                MovieReadDTO.class);
        List<MovieReadDTO> moviesSearchResults = moviePageResult.getData();
        Assert.assertEquals(1, moviesSearchResults.size());

        MovieReadDTO movieSearchResults = moviesSearchResults.get(0);
        Assert.assertEquals(movieTitle, movieSearchResults.getTitle());


        List<MovieReadDTO> moviesNews = performRequestCollection(
                "/news/" + newsReadDTO.getId() + "/movies/" + movieSearchResults.getId(),
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                MovieReadDTO.class
        );
        Assertions.assertThat(moviesNews).extracting(MovieReadDTO::getId)
                .containsExactlyInAnyOrder(movieReadDTO.getId());


        String actorName = "Svetlana Pismichenko";
        PageResult<ActorReadDTO> actorPageResult = performRequestPageResult(
                "/users/" + c1ReadDTO.getId() + "/actors?name=" + actorName,
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                ActorReadDTO.class
        );
        List<ActorReadDTO> actorsSearchResults = actorPageResult.getData();
        Assert.assertEquals(1, actorsSearchResults.size());

        ActorReadDTO actorSearchResults = actorsSearchResults.get(0);
        Assert.assertNotNull(actorSearchResults.getPersonId());


        List<ActorReadDTO> actorsNews = performRequestCollection(
                "/news/" + newsReadDTO.getId() + "/actors/" + actorSearchResults.getId(),
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                ActorReadDTO.class
        );
        Assertions.assertThat(actorsNews).extracting(ActorReadDTO::getId)
                .containsExactlyInAnyOrder(actorSearchResults.getId());


        String crewMemberName = "Aleksei Balabanov";
        PageResult<CrewMemberReadDTO> crewMemberPageResult = performRequestPageResult(
                "/users/" + c1ReadDTO.getId() + "/crew-members?name=" + crewMemberName,
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                CrewMemberReadDTO.class
        );

        List<CrewMemberReadDTO> crewMembersSearchResults = crewMemberPageResult.getData();
        Assert.assertEquals(1, crewMembersSearchResults.size());

        CrewMemberReadDTO crewMemberSearchResults = crewMembersSearchResults.get(0);
        Assert.assertNotNull(crewMemberSearchResults.getPersonId());


        List<CrewMemberReadDTO> crewMembersNews = performRequestCollection(
                "/news/" + newsReadDTO.getId() + "/crew-members/" + crewMemberSearchResults.getId(),
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMembersNews).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(crewMemberSearchResults.getId());


        //FINAL_9
        NewsReadDTO actualNews = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                NewsReadDTO.class
        );
        Assertions.assertThat(newsCreateDTO).isEqualToComparingFieldByField(actualNews);


        moviesNews = performRequestCollection(
                "/users/" + u1ReadDTO.getId() + "/news/" + actualNews.getId() + "/movies",
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                MovieReadDTO.class
        );
        Assertions.assertThat(moviesNews).extracting(MovieReadDTO::getId)
                .containsExactlyInAnyOrder(movieReadDTO.getId());
        Assert.assertEquals(1, moviesNews.size());


        actorsNews = performRequestCollection(
                "/users/" + u1ReadDTO.getId() + "/news/" + actualNews.getId() + "/actors",
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                ActorReadDTO.class
        );
        Assertions.assertThat(actorsNews).extracting(ActorReadDTO::getId)
                .containsExactlyInAnyOrder(actor2ReadDTO.getId());
        Assert.assertEquals(1, actorsNews.size());


        crewMembersNews = performRequestCollection(
                "/users/" + u1ReadDTO.getId() + "/news/" + actualNews.getId() + "/crew-members",
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMembersNews).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(crewMember1ReadDTO.getId());
        Assert.assertEquals(1, crewMembersNews.size());


        //FINAL_10
        NewsLikeCreateDTO newsLikeU1CreateDTO = new NewsLikeCreateDTO();
        newsLikeU1CreateDTO.setLike(true);
        newsLikeU1CreateDTO.setNewsId(newsReadDTO.getId());

        NewsLikeReadDTO newsLikeU1ReadDTO = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/news-likes",
                HttpMethod.POST,
                u1ReadDTO.getEmail(),
                u1Password,
                newsLikeU1CreateDTO,
                NewsLikeReadDTO.class
        );
        Assertions.assertThat(newsLikeU1CreateDTO).isEqualToComparingFieldByField(newsLikeU1ReadDTO);


        NewsReadDTO newsAfterAddingLike = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                NewsReadDTO.class
        );
        Assert.assertEquals(1, newsAfterAddingLike.getLike());


        NewsLikeCreateDTO newsLikeU2CreateDTO = new NewsLikeCreateDTO();
        newsLikeU2CreateDTO.setLike(true);
        newsLikeU2CreateDTO.setNewsId(newsReadDTO.getId());

        NewsLikeReadDTO newsLikeU2ReadDTO = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/news-likes",
                HttpMethod.POST,
                u2ReadDTO.getEmail(),
                u2Password,
                newsLikeU1CreateDTO,
                NewsLikeReadDTO.class
        );
        Assertions.assertThat(newsLikeU2CreateDTO).isEqualToComparingFieldByField(newsLikeU2ReadDTO);


        newsAfterAddingLike = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                u2ReadDTO.getEmail(),
                u2Password,
                null,
                NewsReadDTO.class
        );
        Assert.assertEquals(2, newsAfterAddingLike.getLike());


        //FINAL_11
        NewsLikeCreateDTO newsLikeU3CreateDTO = new NewsLikeCreateDTO();
        newsLikeU3CreateDTO.setLike(true);
        newsLikeU3CreateDTO.setNewsId(newsReadDTO.getId());

        NewsLikeReadDTO newsLikeU3ReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/news-likes",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                newsLikeU1CreateDTO,
                NewsLikeReadDTO.class
        );
        Assertions.assertThat(newsLikeU3CreateDTO).isEqualToComparingFieldByField(newsLikeU3ReadDTO);


        newsAfterAddingLike = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                NewsReadDTO.class
        );
        Assert.assertEquals(3, newsAfterAddingLike.getLike());


        NewsLikePutDTO newsLikePutDTO = new NewsLikePutDTO();
        newsLikePutDTO.setLike(null);

        performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/news-likes/" + newsLikeU3ReadDTO.getId(),
                HttpMethod.PUT,
                u3ReadDTO.getEmail(),
                u3Password,
                newsLikePutDTO,
                null
        );


        newsAfterAddingLike = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                NewsReadDTO.class
        );
        Assert.assertEquals(2, newsAfterAddingLike.getLike());


        //FINAL_12
        NewsLikePatchDTO newsLikePatchDTO = new NewsLikePatchDTO();
        newsLikePatchDTO.setLike(false);

        NewsLikeReadDTO actualNewsLikeU3ReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/news-likes/" + newsLikeU3ReadDTO.getId(),
                HttpMethod.PATCH,
                u3ReadDTO.getEmail(),
                u3Password,
                newsLikePatchDTO,
                NewsLikeReadDTO.class
        );
        Assertions.assertThat(newsLikePatchDTO).isEqualToComparingFieldByField(actualNewsLikeU3ReadDTO);


        newsAfterAddingLike = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                NewsReadDTO.class
        );
        Assert.assertEquals(2, newsAfterAddingLike.getLike());
        Assert.assertEquals(1, newsAfterAddingLike.getDislike());


        //FINAL_13
        SignalToContentManagerCreateDTO signalCreateDTO = new SignalToContentManagerCreateDTO();
        signalCreateDTO.setWrongText("Castle");
        signalCreateDTO.setFromIndex(69);
        signalCreateDTO.setToIndex(75);
        signalCreateDTO.setSignalToContentManagerObjectType(SignalToContentManagerObjectType.NEWS);
        signalCreateDTO.setSignalToContentManagerObjectId(newsReadDTO.getId());

        SignalToContentManagerReadDTO signalReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/signals-to-content-manager",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                signalCreateDTO,
                SignalToContentManagerReadDTO.class
        );
        Assertions.assertThat(signalCreateDTO).isEqualToComparingFieldByField(signalReadDTO);


        //FINAL_14
        SignalToContentManagerStatus signalStatusNeedToFix = SignalToContentManagerStatus.NEED_TO_FIX;
        PageResult<SignalToContentManagerReadDTO> signalPageResult = performRequestPageResult(
                "/signals-to-content-manager?status=" + signalStatusNeedToFix,
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                SignalToContentManagerReadDTO.class
        );
        List<SignalToContentManagerReadDTO> signalsSearchResults = signalPageResult.getData();
        Assertions.assertThat(signalsSearchResults).extracting(SignalToContentManagerReadDTO::getId)
                .containsExactlyInAnyOrder(signalReadDTO.getId());
        Assert.assertEquals(1, signalsSearchResults.size());

        SignalToContentManagerReadDTO signalForFix = signalsSearchResults.get(0);


        //FINAL_15
        SignalToContentManagerFixContent signalFix = new SignalToContentManagerFixContent();
        signalFix.setContentManagerId(c1ReadDTO.getId());
        signalFix.setReplacement("The Castle");

        SignalToContentManagerReadDTO signalAfterChange = performRequest(
                "/signals-to-content-manager/" + signalForFix.getId() + "/fix-content",
                HttpMethod.POST,
                c1ReadDTO.getEmail(),
                c1Password,
                signalFix,
                SignalToContentManagerReadDTO.class
        );
        Assert.assertEquals(SignalToContentManagerStatus.FIXED, signalAfterChange.getSignalToContentManagerStatus());
        Assert.assertEquals(c1ReadDTO.getId(), signalAfterChange.getContentManagerId());


        //FINAL_16
        signalStatusNeedToFix = SignalToContentManagerStatus.NEED_TO_FIX;
        signalPageResult = performRequestPageResult(
                "/signals-to-content-manager?status=" + signalStatusNeedToFix,
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                SignalToContentManagerReadDTO.class
        );
        signalsSearchResults = signalPageResult.getData();
        Assert.assertEquals(0, signalsSearchResults.size());


        //FINAL_17
        NewsReadDTO updatedNews = performRequest(
                "/news/" + newsReadDTO.getId(),
                HttpMethod.GET,
                null,
                null,
                null,
                NewsReadDTO.class
        );
        Assert.assertNotEquals(updatedNews.getText(), newsReadDTO.getText());
        Assert.assertEquals(2, updatedNews.getLike());
        Assert.assertEquals(1, updatedNews.getDislike());


        moviesNews = performRequestCollection(
                "/news/" + actualNews.getId() + "/movies",
                HttpMethod.GET,
                null,
                null,
                null,
                MovieReadDTO.class
        );
        Assertions.assertThat(moviesNews).extracting(MovieReadDTO::getId)
                .containsExactlyInAnyOrder(movieReadDTO.getId());
        Assert.assertEquals(1, moviesNews.size());


        actorsNews = performRequestCollection(
                "/news/" + actualNews.getId() + "/actors",
                HttpMethod.GET,
                null,
                null,
                null,
                ActorReadDTO.class
        );
        Assertions.assertThat(actorsNews).extracting(ActorReadDTO::getId)
                .containsExactlyInAnyOrder(actor2ReadDTO.getId());
        Assert.assertEquals(1, actorsNews.size());


        crewMembersNews = performRequestCollection(
                "/news/" + actualNews.getId() + "/crew-members",
                HttpMethod.GET,
                null,
                null,
                null,
                CrewMemberReadDTO.class
        );
        Assertions.assertThat(crewMembersNews).extracting(CrewMemberReadDTO::getId)
                .containsExactlyInAnyOrder(crewMember1ReadDTO.getId());
        Assert.assertEquals(1, crewMembersNews.size());


        //FINAL_18
        movieReadDTO = performRequest(
                "/users/" + u1ReadDTO.getId() + "/movies/" + movieReadDTO.getId(),
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                MovieReadDTO.class
        );
        Assertions.assertThat(movieCreateDTO).isEqualToComparingFieldByField(movieReadDTO);


        List<RoleReadDTO> rolesMovie = performRequestCollection(
                "/users/" + u1ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                RoleReadDTO.class
        );
        Assert.assertEquals(3, rolesMovie.size());


        List<CrewMemberReadDTO> crewMembersMovie = performRequestCollection(
                "/users/" + u1ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/crew-members",
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                CrewMemberReadDTO.class
        );
        Assert.assertEquals(3, crewMembersMovie.size());


        List<GenreReadDTO> genresMovie = performRequestCollection(
                "/users/" + u1ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/genres",
                HttpMethod.GET,
                u1ReadDTO.getEmail(),
                u1Password,
                null,
                GenreReadDTO.class
        );
        Assert.assertEquals(1, genresMovie.size());
        Assert.assertEquals(GenreType.DRAMA, genresMovie.get(0).getType());


        //FINAL_19
        MovieReviewCreateDTO movieReviewCreateDTO = new MovieReviewCreateDTO();
        movieReviewCreateDTO.setMovieId(movieReadDTO.getId());
        movieReviewCreateDTO.setText("Good film. <FONT style=\"BACKGROUND-COLOR: #000000\" color=#000000>It " +
                "is a pity that the main character did not get what he wanted</FONT style>");

        MovieReviewReadDTO movieReviewReadDTO = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/movie-reviews",
                HttpMethod.POST,
                u1ReadDTO.getEmail(),
                u1Password,
                movieReviewCreateDTO,
                MovieReviewReadDTO.class
        );
        Assertions.assertThat(movieReviewCreateDTO).isEqualToComparingFieldByField(movieReviewReadDTO);


        MovieMarkCreateDTO movieMarkCreateDTO = new MovieMarkCreateDTO();
        movieMarkCreateDTO.setMark(8);
        movieMarkCreateDTO.setMovieId(movieReadDTO.getId());

        MovieMarkReadDTO movieMarkReadDTO = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/movie-marks",
                HttpMethod.POST,
                u1ReadDTO.getEmail(),
                u1Password,
                movieMarkCreateDTO,
                MovieMarkReadDTO.class
        );
        Assertions.assertThat(movieMarkCreateDTO).isEqualToComparingFieldByField(movieMarkReadDTO);
        Thread.sleep(2000);

        //FINAL_20
        movieReadDTO = performRequest(
                "/users/" + u2ReadDTO.getId() + "/movies/" + movieReadDTO.getId(),
                HttpMethod.GET,
                u2ReadDTO.getEmail(),
                u2Password,
                null,
                MovieReadDTO.class
        );
        Assertions.assertThat(movieCreateDTO).isEqualToComparingFieldByField(movieReadDTO);


        List<MovieReviewReadDTO> reviewsMovie = performRequestCollection(
                "/users/" + u2ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/movie-reviews",
                HttpMethod.GET,
                u2ReadDTO.getEmail(),
                u2Password,
                null,
                MovieReviewReadDTO.class
        );
        Assert.assertEquals(0, reviewsMovie.size());


        rolesMovie = performRequestCollection(
                "/users/" + u2ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.GET,
                u2ReadDTO.getEmail(),
                u2Password,
                null,
                RoleReadDTO.class
        );
        Assert.assertEquals(3, rolesMovie.size());


        crewMembersMovie = performRequestCollection(
                "/users/" + u2ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/crew-members",
                HttpMethod.GET,
                u2ReadDTO.getEmail(),
                u2Password,
                null,
                CrewMemberReadDTO.class
        );
        Assert.assertEquals(3, crewMembersMovie.size());


        genresMovie = performRequestCollection(
                "/users/" + u2ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/genres",
                HttpMethod.GET,
                u2ReadDTO.getEmail(),
                u2Password,
                null,
                GenreReadDTO.class
        );
        Assert.assertEquals(1, genresMovie.size());
        Assert.assertEquals(GenreType.DRAMA, genresMovie.get(0).getType());


        //FINAL_21
        movieMarkCreateDTO = new MovieMarkCreateDTO();
        movieMarkCreateDTO.setMark(4);
        movieMarkCreateDTO.setMovieId(movieReadDTO.getId());

        movieMarkReadDTO = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/movie-marks",
                HttpMethod.POST,
                u2ReadDTO.getEmail(),
                u2Password,
                movieMarkCreateDTO,
                MovieMarkReadDTO.class
        );
        Assertions.assertThat(movieMarkCreateDTO).isEqualToComparingFieldByField(movieMarkReadDTO);
        Thread.sleep(2000);

        //FINAL_22
        UUID movieId = movieReadDTO.getId();
        ReviewStatus reviewStatus = ReviewStatus.MODERATION;
        PageResult<MovieReviewReadDTO> movieReviewPageResult = performRequestPageResult(
                "/users/" + m1ReadDTO.getId() + "/movie-reviews?movieId=" + movieId + "&reviewStatus=" + reviewStatus,
                HttpMethod.GET,
                m1ReadDTO.getEmail(),
                m1Password,
                null,
                MovieReviewReadDTO.class
        );
        List<MovieReviewReadDTO> movieReviewsSearchResults = movieReviewPageResult.getData();
        Assert.assertEquals(1, movieReviewsSearchResults.size());

        movieReviewReadDTO = movieReviewsSearchResults.get(0);
        Assertions.assertThat(movieReviewCreateDTO).isEqualToComparingFieldByField(movieReviewReadDTO);


        //FINAL_23
        MovieReviewPatchDTO movieReviewPatchDTO = new MovieReviewPatchDTO();
        movieReviewPatchDTO.setReviewStatus(ReviewStatus.APPROVED);

        MovieReviewReadDTO actualMovieReviewReadDTO = performRequest(
                "/movie-reviews/" + movieReviewReadDTO.getId(),
                HttpMethod.PATCH,
                m1ReadDTO.getEmail(),
                m1Password,
                movieReviewPatchDTO,
                MovieReviewReadDTO.class
        );
        Assert.assertEquals(ReviewStatus.APPROVED, actualMovieReviewReadDTO.getReviewStatus());


        RegisteredUserTrustPatchDTO trustPatchDTO = new RegisteredUserTrustPatchDTO();
        trustPatchDTO.setTrust(true);

        RegisteredUserReadDTO actualRegisteredUserReadDTO = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/trust",
                HttpMethod.PATCH,
                m1ReadDTO.getEmail(),
                m1Password,
                trustPatchDTO,
                RegisteredUserReadDTO.class
        );
        Assert.assertEquals(trustPatchDTO.getTrust(), actualRegisteredUserReadDTO.getTrust());


        //FINAL_24
        movieReadDTO = performRequest(
                "/users/" + u3ReadDTO.getId() + "/movies/" + movieReadDTO.getId(),
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                MovieReadDTO.class
        );
        Assert.assertEquals(Double.valueOf(6.0), movieReadDTO.getAverageMark());
        Assertions.assertThat(movieCreateDTO).isEqualToComparingFieldByField(movieReadDTO);

        rolesMovie = performRequestCollection(
                "/users/" + u3ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                RoleReadDTO.class
        );
        Assert.assertEquals(3, rolesMovie.size());


        crewMembersMovie = performRequestCollection(
                "/users/" + u3ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/crew-members",
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                CrewMemberReadDTO.class
        );
        Assert.assertEquals(3, crewMembersMovie.size());


        genresMovie = performRequestCollection(
                "/users/" + u3ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/genres",
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                GenreReadDTO.class
        );
        Assert.assertEquals(1, genresMovie.size());
        Assert.assertEquals(GenreType.DRAMA, genresMovie.get(0).getType());


        movieMarkCreateDTO = new MovieMarkCreateDTO();
        movieMarkCreateDTO.setMark(9);
        movieMarkCreateDTO.setMovieId(movieReadDTO.getId());

        movieMarkReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/movie-marks",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                movieMarkCreateDTO,
                MovieMarkReadDTO.class
        );
        Assertions.assertThat(movieMarkCreateDTO).isEqualToComparingFieldByField(movieMarkReadDTO);
        Thread.sleep(2000);


        reviewsMovie = performRequestCollection(
                "/users/" + u3ReadDTO.getId() + "/movies/" + movieReadDTO.getId() + "/movie-reviews",
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                MovieReviewReadDTO.class
        );
        Assert.assertEquals(1, reviewsMovie.size());


        //FINAL_25
        MovieReviewLikeCreateDTO movieReviewLikeCreateDTO = new MovieReviewLikeCreateDTO();
        movieReviewLikeCreateDTO.setLike(true);
        movieReviewLikeCreateDTO.setMovieReviewId(movieReviewReadDTO.getId());

        MovieReviewLikeReadDTO movieReviewLikeReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/movie-review-likes",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                movieReviewLikeCreateDTO,
                MovieReviewLikeReadDTO.class
        );
        Assertions.assertThat(movieReviewLikeCreateDTO).isEqualToComparingFieldByField(movieReviewLikeReadDTO);


        //FINAL_26
        MovieReadDTO actualMovieReadDTO = performRequest(
                "/movies/" + movieReadDTO.getId(),
                HttpMethod.GET,
                null,
                null,
                null,
                MovieReadDTO.class
        );
        Assert.assertEquals(Double.valueOf(7.0), actualMovieReadDTO.getAverageMark());
        Assertions.assertThat(movieCreateDTO).isEqualToComparingFieldByField(actualMovieReadDTO);


        reviewsMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/movie-reviews",
                HttpMethod.GET,
                null,
                null,
                null,
                MovieReviewReadDTO.class
        );
        Assert.assertEquals(1, reviewsMovie.size());


        rolesMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/roles",
                HttpMethod.GET,
                null,
                null,
                null,
                RoleReadDTO.class
        );
        Assert.assertEquals(3, rolesMovie.size());


        crewMembersMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/crew-members",
                HttpMethod.GET,
                null,
                null,
                null,
                CrewMemberReadDTO.class
        );
        Assert.assertEquals(3, crewMembersMovie.size());


        genresMovie = performRequestCollection(
                "/movies/" + movieReadDTO.getId() + "/genres",
                HttpMethod.GET,
                null,
                null,
                null,
                GenreReadDTO.class
        );
        Assert.assertEquals(1, genresMovie.size());
        Assert.assertEquals(GenreType.DRAMA, genresMovie.get(0).getType());


        //FINAL_27
        String movieExternalId = "20992";
        MovieReadDTO importedMovieReadDTO = performRequest(
                "/movies/" + movieExternalId + "/import",
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                MovieReadDTO.class
        );
        Assert.assertEquals("Brother", importedMovieReadDTO.getTitle());
        Assert.assertEquals(MovieStatus.RELEASED, importedMovieReadDTO.getMovieStatus());
        Assert.assertEquals("ru", importedMovieReadDTO.getOriginalLanguage());
        Assert.assertEquals(false, importedMovieReadDTO.getAdult());


        List<Genre> importedMovieGenres = performRequestCollection(
                "/users/" + c1ReadDTO.getId() + "/movies/" + importedMovieReadDTO.getId() + "/genres",
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                Genre.class
        );
        Assert.assertEquals(4, importedMovieGenres.size());
        Assertions.assertThat(importedMovieGenres).extracting(Genre::getType)
                .containsExactlyInAnyOrder(GenreType.DRAMA, GenreType.ACTION, GenreType.CRIME, GenreType.MUSIC);


        List<CrewMemberReadDTO> importedCrewMemberReadDTO = performRequestCollection(
                "/users/" + c1ReadDTO.getId() + "/movies/" + importedMovieReadDTO.getId() + "/crew-members",
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null,
                CrewMemberReadDTO.class
        );
        Assert.assertEquals(6, importedCrewMemberReadDTO.size());


        List<RoleReadDTO> importedMovieRoles = performRequestCollection(
                "/users/" + c1ReadDTO.getId() + "/movies/" + importedMovieReadDTO.getId() + "/roles",
                HttpMethod.GET,
                c1ReadDTO.getEmail(),
                c1Password,
                null, RoleReadDTO.class
        );
        Assert.assertEquals(15, importedMovieRoles.size());


        //FINAL_28
        movieMarkCreateDTO = new MovieMarkCreateDTO();
        movieMarkCreateDTO.setMark(2);
        movieMarkCreateDTO.setMovieId(importedMovieReadDTO.getId());

        movieMarkReadDTO = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/movie-marks",
                HttpMethod.POST,
                u1ReadDTO.getEmail(),
                u1Password,
                movieMarkCreateDTO,
                MovieMarkReadDTO.class
        );
        Assertions.assertThat(movieMarkCreateDTO).isEqualToComparingFieldByField(movieMarkReadDTO);
        Thread.sleep(2000);


        movieReviewCreateDTO = new MovieReviewCreateDTO();
        movieReviewCreateDTO.setMovieId(importedMovieReadDTO.getId());
        movieReviewCreateDTO.setText("*******");

        movieReviewReadDTO = performRequest(
                "/registered-users/" + u1ReadDTO.getId() + "/movie-reviews",
                HttpMethod.POST,
                u1ReadDTO.getEmail(),
                u1Password,
                movieReviewCreateDTO,
                MovieReviewReadDTO.class
        );
        Assertions.assertThat(movieReviewCreateDTO).isEqualToComparingFieldByField(movieReviewReadDTO);


        //FINAL_29
        movieMarkCreateDTO = new MovieMarkCreateDTO();
        movieMarkCreateDTO.setMark(10);
        movieMarkCreateDTO.setMovieId(importedMovieReadDTO.getId());

        movieMarkReadDTO = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/movie-marks",
                HttpMethod.POST,
                u2ReadDTO.getEmail(),
                u2Password,
                movieMarkCreateDTO,
                MovieMarkReadDTO.class
        );
        Assertions.assertThat(movieMarkCreateDTO).isEqualToComparingFieldByField(movieMarkReadDTO);
        Thread.sleep(2000);


        movieMarkCreateDTO = new MovieMarkCreateDTO();
        movieMarkCreateDTO.setMark(9);
        movieMarkCreateDTO.setMovieId(importedMovieReadDTO.getId());

        movieMarkReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/movie-marks",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                movieMarkCreateDTO,
                MovieMarkReadDTO.class
        );
        Assertions.assertThat(movieMarkCreateDTO).isEqualToComparingFieldByField(movieMarkReadDTO);
        Thread.sleep(2000);


        reviewsMovie = performRequestCollection(
                "/users/" + u3ReadDTO.getId() + "/movies/" + importedMovieReadDTO.getId() + "/movie-reviews",
                HttpMethod.GET,
                u3ReadDTO.getEmail(),
                u3Password,
                null,
                MovieReviewReadDTO.class
        );
        Assert.assertEquals(1, reviewsMovie.size());


        UUID movieReviewId = reviewsMovie.get(0).getId();
        ComplaintCreateDTO complaintU2CreateDTO = new ComplaintCreateDTO();
        complaintU2CreateDTO.setType(ComplaintType.SWEARING);
        complaintU2CreateDTO.setComplaintObjectType(ComplaintObjectType.MOVIE_REVIEW);
        complaintU2CreateDTO.setComplaintObjectId(movieReviewId);

        ComplaintReadDTO complaintU2ReadDTO = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/complaints",
                HttpMethod.POST,
                u2ReadDTO.getEmail(),
                u2Password,
                complaintU2CreateDTO,
                ComplaintReadDTO.class
        );
        Assertions.assertThat(complaintU2CreateDTO).isEqualToComparingFieldByField(complaintU2ReadDTO);


        movieReviewId = reviewsMovie.get(0).getId();
        ComplaintCreateDTO complaintU3CreateDTO = new ComplaintCreateDTO();
        complaintU3CreateDTO.setType(ComplaintType.SWEARING);
        complaintU3CreateDTO.setComplaintObjectType(ComplaintObjectType.MOVIE_REVIEW);
        complaintU3CreateDTO.setComplaintObjectId(movieReviewId);

        ComplaintReadDTO complaintU3ReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/complaints",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                complaintU3CreateDTO,
                ComplaintReadDTO.class
        );
        Assertions.assertThat(complaintU3CreateDTO).isEqualToComparingFieldByField(complaintU3ReadDTO);


        //FINAL_30
        ComplaintStatus complaintStatus = ComplaintStatus.MODERATION;
        PageResult<ComplaintReadDTO> complaintsPageResult = performRequestPageResult(
                "/complaints?complaintStatus=" + complaintStatus,
                HttpMethod.GET,
                m1ReadDTO.getEmail(),
                m1Password,
                null,
                ComplaintReadDTO.class
        );
        List<ComplaintReadDTO> complaints = complaintsPageResult.getData();
        Assertions.assertThat(complaints).extracting(ComplaintReadDTO::getId)
                .containsExactlyInAnyOrder(complaintU2ReadDTO.getId(), complaintU3ReadDTO.getId());
        Assert.assertEquals(2, complaints.size());


        //FINAL_31
        ComplaintReadDTO complaintReadDTO = performRequest(
                "/complaints/" + complaintU2ReadDTO.getId(),
                HttpMethod.GET,
                m1ReadDTO.getEmail(),
                m1Password,
                null,
                ComplaintReadDTO.class
        );
        Assertions.assertThat(complaintU2CreateDTO).isEqualToComparingFieldByField(complaintReadDTO);


        ComplaintProcessingDTO complaintProcessingDTO = new ComplaintProcessingDTO();
        complaintProcessingDTO.setNewStatus(ComplaintStatus.APPROVED);
        complaintProcessingDTO.setModeratorId(m1ReadDTO.getId());

        ComplaintReadDTO complaintAfterProcessing = performRequest(
                "/complaints/" + complaintReadDTO.getId() + "/processing",
                HttpMethod.POST,
                m1ReadDTO.getEmail(),
                m1Password,
                complaintProcessingDTO,
                ComplaintReadDTO.class
        );
        Assert.assertEquals(m1ReadDTO.getId(), complaintAfterProcessing.getModeratorId());
        Assert.assertEquals(ComplaintStatus.APPROVED, complaintAfterProcessing.getComplaintStatus());
        Assert.assertNotNull(complaintAfterProcessing.getProcessedAt());


        RegisteredUserReadDTO u1AfterSanctions = performRequest(
                "/registered-users/" + u1ReadDTO.getId(),
                HttpMethod.GET,
                null,
                null,
                null,
                RegisteredUserReadDTO.class
        );
        Assert.assertEquals(AccountStatus.BLOCKED, u1AfterSanctions.getStatus());
        Assert.assertEquals(false, u1AfterSanctions.getTrust());


        //FINAL_32
        complaintStatus = ComplaintStatus.MODERATION;
        complaintsPageResult = performRequestPageResult(
                "/complaints?complaintStatus=" + complaintStatus,
                HttpMethod.GET,
                m1ReadDTO.getEmail(),
                m1Password,
                null,
                ComplaintReadDTO.class
        );
        complaints = complaintsPageResult.getData();
        Assert.assertEquals(0, complaints.size());


        //FINAL_33
        RoleMarkCreateDTO roleMarkCreateDTO = new RoleMarkCreateDTO();
        roleMarkCreateDTO.setMark(5);
        roleMarkCreateDTO.setRoleId(role3ReadDTO.getId());

        RoleMarkReadDTO roleMarkU2ReadDTO = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/role-marks",
                HttpMethod.POST,
                u2ReadDTO.getEmail(),
                u2Password,
                roleMarkCreateDTO,
                RoleMarkReadDTO.class
        );
        Assertions.assertThat(roleMarkCreateDTO).isEqualToComparingFieldByField(roleMarkU2ReadDTO);
        Thread.sleep(2000);


        List<RoleReadDTO> commonRoles = new ArrayList<>();
        importedMovieRoles.stream().filter(r -> r.getActorId().equals(actor3ReadDTO.getId())).forEach(commonRoles::add);
        Assert.assertEquals(1, commonRoles.size());
        RoleReadDTO commonRole = commonRoles.get(0);

        roleMarkCreateDTO = new RoleMarkCreateDTO();
        roleMarkCreateDTO.setMark(6);
        roleMarkCreateDTO.setRoleId(commonRole.getId());

        roleMarkU2ReadDTO = performRequest(
                "/registered-users/" + u2ReadDTO.getId() + "/role-marks",
                HttpMethod.POST,
                u2ReadDTO.getEmail(),
                u2Password,
                roleMarkCreateDTO,
                RoleMarkReadDTO.class
        );
        Assertions.assertThat(roleMarkCreateDTO).isEqualToComparingFieldByField(roleMarkU2ReadDTO);
        Thread.sleep(2000);


        roleMarkCreateDTO = new RoleMarkCreateDTO();
        roleMarkCreateDTO.setMark(9);
        roleMarkCreateDTO.setRoleId(role3ReadDTO.getId());

        roleMarkU2ReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/role-marks",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                roleMarkCreateDTO,
                RoleMarkReadDTO.class
        );
        Assertions.assertThat(roleMarkCreateDTO).isEqualToComparingFieldByField(roleMarkU2ReadDTO);
        Thread.sleep(2000);


        roleMarkCreateDTO = new RoleMarkCreateDTO();
        roleMarkCreateDTO.setMark(4);
        roleMarkCreateDTO.setRoleId(commonRole.getId());

        roleMarkU2ReadDTO = performRequest(
                "/registered-users/" + u3ReadDTO.getId() + "/role-marks",
                HttpMethod.POST,
                u3ReadDTO.getEmail(),
                u3Password,
                roleMarkCreateDTO,
                RoleMarkReadDTO.class
        );
        Assertions.assertThat(roleMarkCreateDTO).isEqualToComparingFieldByField(roleMarkU2ReadDTO);
        Thread.sleep(2000);


        //FINAL_34
        roleMarkCreateDTO = new RoleMarkCreateDTO();
        roleMarkCreateDTO.setMark(2);
        roleMarkCreateDTO.setRoleId(role3ReadDTO.getId());

        HttpEntity<?> httpEntity = createHttpEntity(u1ReadDTO.getEmail(), u1Password, roleMarkCreateDTO);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                urlApi + "/registered-users/" + u1ReadDTO.getId() + "/role-marks",
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<RoleMarkReadDTO>() {
                })).isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);


        //FINAL_35
        ActorReadDTO actorReadDTO = performRequest(
                "/actors/" + actor3ReadDTO.getId(),
                HttpMethod.GET,
                null,
                null,
                null,
                ActorReadDTO.class
        );
        Assert.assertEquals(person3ReadDTO.getId(), actorReadDTO.getPersonId());
        Assert.assertEquals(Double.valueOf(6.0), actorReadDTO.getAverageRoleRating());
        Assert.assertEquals(Double.valueOf(7.0), actorReadDTO.getAverageMovieRating());
    }

    private void config() {
        restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        restTemplate.setRequestFactory(requestFactory);

        oldConverters = new ArrayList<HttpMessageConverter<?>>();
        oldConverters.addAll(restTemplate.getMessageConverters());

        List<HttpMessageConverter<?>> stringConverter = new ArrayList<HttpMessageConverter<?>>();
        stringConverter.add(new StringHttpMessageConverter());

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        stringConverter.add(converter);

        restTemplate.setMessageConverters(stringConverter);
    }

    private <T> T performRequest(String url, HttpMethod httpMethod, String email, String password,
                                 Object object, final Class<T> clazz) {
        HttpEntity<?> httpEntity = createHttpEntity(email, password, object);

        //put http
        if (clazz == null) {
            createResponseEntity(url, httpMethod, httpEntity);
            return null;
        }
        ResponseEntity<String> response = createResponseEntity(url, httpMethod, httpEntity);

        String body = null;
        T result = null;

        if (response.hasBody()) {
            body = response.getBody();
            try {
                result = objectMapper.readValue(body, clazz);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                restTemplate.setMessageConverters(oldConverters);
            }
        }
        return result;
    }

    private ResponseEntity<String> createResponseEntity(String url, HttpMethod httpMethod, HttpEntity<?> httpEntity) {
        return restTemplate.exchange(
                urlApi + url,
                httpMethod,
                httpEntity, String.class);
    }

    private <T> List<T> performRequestCollection(String url, HttpMethod httpMethod, String email, String password,
                                                 Object object, final Class<T> clazz) {
        HttpEntity<?> httpEntity = createHttpEntity(email, password, object);
        ResponseEntity<String> response = createResponseEntity(url, httpMethod, httpEntity);

        String body = null;
        List<T> result = new ArrayList<T>();

        if (response.hasBody()) {
            body = response.getBody();
            try {
                result = objectMapper.readValue(body, objectMapper.getTypeFactory().constructCollectionType(
                        List.class, clazz));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                restTemplate.setMessageConverters(oldConverters);
            }
        }
        return result;
    }

    private <T> PageResult<T> performRequestPageResult(String url, HttpMethod httpMethod, String email, String password,
                                                       Object object, final Class<T> clazz) {
        HttpEntity<?> httpEntity = createHttpEntity(email, password, object);
        ResponseEntity<String> response = createResponseEntity(url, httpMethod, httpEntity);

        String body = null;
        PageResult<T> result = new PageResult<T>();

        if (response.hasBody()) {
            body = response.getBody();
            try {
                result = objectMapper.readValue(body, objectMapper.getTypeFactory().constructParametricType(
                        PageResult.class, clazz));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                restTemplate.setMessageConverters(oldConverters);
            }
        }
        return result;
    }

    private HttpEntity<?> createHttpEntity(String email, String password,
                                           Object object) {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> httpEntity = null;
        if (email == null && password == null && object != null) {
            httpEntity = new HttpEntity<>(object);
        }
        if (email != null && password != null && object == null) {
            headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
            httpEntity = new HttpEntity<>(headers);
        }
        if (email == null && password == null && object == null) {
            headers = new HttpHeaders();
            httpEntity = new HttpEntity<>(headers);
        }
        if (email != null && password != null && object != null) {
            headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
            httpEntity = new HttpEntity<>(object, headers);
        }
        return httpEntity;
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", userName, password).getBytes()));
    }
}