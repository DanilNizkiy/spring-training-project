package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.ContentManager;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerCreateDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerPatchDTO;
import com.nizkiyd.backend.example.dto.contentmanager.ContentManagerReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.ContentManagerService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ContentManagerController.class)
public class ContentManagerControllerTest extends BaseControllerTest {

    @MockBean
    private ContentManagerService contentManagerService;

    @Test
    public void testGetContentManager() throws Exception {
        ContentManagerReadDTO contentManager = generateObject(ContentManagerReadDTO.class);

        Mockito.when(contentManagerService.getContentManager(contentManager.getId())).thenReturn(contentManager);

        String resultJson = mvc.perform(get("/api/v1/content-managers/{id}", contentManager.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager = objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assertions.assertThat(actualContentManager).isEqualToComparingFieldByField(contentManager);

        Mockito.verify(contentManagerService).getContentManager(contentManager.getId());
    }

    @Test
    public void testGetContentManagerWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ContentManager.class, wrongId);
        Mockito.when(contentManagerService.getContentManager(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/content-managers/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }


    @Test
    public void testCreateContentManager() throws Exception {
        ContentManagerCreateDTO create = generateObject(ContentManagerCreateDTO.class);

        ContentManagerReadDTO read = generateObject(ContentManagerReadDTO.class);

        Mockito.when(contentManagerService.createContentManager(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/content-managers")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager = objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assertions.assertThat(actualContentManager).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchContentManager() throws Exception {
        ContentManagerPatchDTO patch = generateObject(ContentManagerPatchDTO.class);

        ContentManagerReadDTO read = generateObject(ContentManagerReadDTO.class);

        Mockito.when(contentManagerService.patchContentManager(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/content-managers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager = objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assert.assertEquals(read, actualContentManager);
    }

    @WithMockUser
    @Test
    public void testDeleteContentManager() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/content-managers/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(contentManagerService).deleteContentManager(id);
    }

    @WithMockUser
    @Test
    public void testGetInvalidContentManagers() throws Exception {
        ContentManagerReadDTO contentManager1 = generateObject(ContentManagerReadDTO.class);
        ContentManagerReadDTO contentManager2 = generateObject(ContentManagerReadDTO.class);

        List<ContentManagerReadDTO> contentManagersReadDTO = new ArrayList<>();
        contentManagersReadDTO.add(contentManager1);
        contentManagersReadDTO.add(contentManager2);

        Mockito.when(contentManagerService.getInvalidContentManagers()).thenReturn(contentManagersReadDTO);

        String resultJson = mvc.perform(get("/api/v1/content-managers/invalid-account"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ContentManagerReadDTO> actualContentManagersReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualContentManagersReadDTO.size());
        Assertions.assertThat(actualContentManagersReadDTO).extracting("id")
                .containsExactlyInAnyOrder(contentManager1.getId(), contentManager2.getId());

        Mockito.verify(contentManagerService).getInvalidContentManagers();
    }
}