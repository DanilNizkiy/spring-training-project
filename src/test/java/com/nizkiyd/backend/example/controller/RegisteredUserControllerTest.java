package com.nizkiyd.backend.example.controller;

import com.nizkiyd.backend.example.domain.RegisteredUser;
import com.nizkiyd.backend.example.dto.registereduser.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.RegisteredUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RegisteredUserController.class)
public class RegisteredUserControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserService registeredUserService;

    @Test
    public void testGetRegisteredUser() throws Exception {
        RegisteredUserReadDTO registeredUser = generateObject(RegisteredUserReadDTO.class);

        Mockito.when(registeredUserService.getRegisteredUser(registeredUser.getId())).thenReturn(registeredUser);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{id}", registeredUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(actualRegisteredUser).isEqualToComparingFieldByField(registeredUser);

        Mockito.verify(registeredUserService).getRegisteredUser(registeredUser.getId());
    }

    @Test
    public void testGetRegisteredUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RegisteredUser.class, wrongId);
        Mockito.when(registeredUserService.getRegisteredUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredUser() throws Exception {
        RegisteredUserCreateDTO create = generateObject(RegisteredUserCreateDTO.class);

        RegisteredUserReadDTO read = generateObject(RegisteredUserReadDTO.class);

        Mockito.when(registeredUserService.createRegisteredUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(actualRegisteredUser).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchRegisteredUser() throws Exception {
        RegisteredUserPatchDTO patch = generateObject(RegisteredUserPatchDTO.class);

        RegisteredUserReadDTO read = generateObject(RegisteredUserReadDTO.class);

        Mockito.when(registeredUserService.patchRegisteredUser(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assert.assertEquals(read, actualRegisteredUser);
    }

    @WithMockUser
    @Test
    public void testChangeAccountStatusRegisteredUser() throws Exception {
        RegisteredUserAccountStatusPatchDTO patch = generateObject(RegisteredUserAccountStatusPatchDTO.class);

        RegisteredUserReadDTO read = generateObject(RegisteredUserReadDTO.class);

        Mockito.when(registeredUserService.changeAccountStatusRegisteredUser(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{id}/account-status", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assert.assertEquals(read, actualRegisteredUser);
    }

    @WithMockUser
    @Test
    public void testChangeTrustRegisteredUser() throws Exception {
        RegisteredUserTrustPatchDTO patch = generateObject(RegisteredUserTrustPatchDTO.class);

        RegisteredUserReadDTO read = generateObject(RegisteredUserReadDTO.class);

        Mockito.when(registeredUserService.changeTrustRegisteredUser(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{id}/trust", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assert.assertEquals(read, actualRegisteredUser);
    }

    @WithMockUser
    @Test
    public void testDeleteRegisteredUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(registeredUserService).deleteRegisteredUser(id);
    }

    @WithMockUser
    @Test
    public void testPutRegisteredUser() throws Exception {
        UUID id = UUID.randomUUID();

        RegisteredUserPutDTO put = generateObject(RegisteredUserPutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserService).updateRegisteredUser(id, put);
    }
}