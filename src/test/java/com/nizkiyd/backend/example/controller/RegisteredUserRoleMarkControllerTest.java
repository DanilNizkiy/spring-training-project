package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.RoleMark;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkCreateDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserRoleMarkService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserRoleMarkController.class)
public class RegisteredUserRoleMarkControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserRoleMarkService service;

    @Test
    public void testCreateRoleMarkValidationFailed() throws Exception {
        RoleMarkCreateDTO createDTO = new RoleMarkCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/role-marks", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createRoleMark(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetRoleMark() throws Exception {
        RoleMarkReadDTO roleMark = generateObject(RoleMarkReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getRoleMark(randomUUID, roleMark.getId())).thenReturn(roleMark);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-marks/{roleMarkId}",
                randomUUID, roleMark.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleMarkReadDTO actualRoleMark = objectMapper.readValue(resultJson, RoleMarkReadDTO.class);
        Assertions.assertThat(actualRoleMark).isEqualToComparingFieldByField(roleMark);

        Mockito.verify(service).getRoleMark(randomUUID, roleMark.getId());
    }

    @Test
    public void testGetRoleMarkWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RoleMark.class, wrongId);
        Mockito.when(service.getRoleMark(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-marks/{roleMarkId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRoleMark() throws Exception {
        RoleMarkCreateDTO create = generateObject(RoleMarkCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleMarkReadDTO read = generateObject(RoleMarkReadDTO.class);

        Mockito.when(service.createRoleMark(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/role-marks", randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleMarkReadDTO actualRoleMark = objectMapper.readValue(resultJson, RoleMarkReadDTO.class);
        Assertions.assertThat(actualRoleMark).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRoleMark() throws Exception {
        RoleMarkPatchDTO patch = generateObject(RoleMarkPatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleMarkReadDTO read = generateObject(RoleMarkReadDTO.class);

        Mockito.when(service.patchRoleMark(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}/role-marks/{roleMarkId}",
                randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleMarkReadDTO actualRoleMark = objectMapper.readValue(resultJson, RoleMarkReadDTO.class);
        Assert.assertEquals(read, actualRoleMark);
    }

    @Test
    public void testDeleteRoleMark() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/role-marks/{roleMarkId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteRoleMark(randomUUID, id);
    }

    @Test
    public void testGetRegisteredUserRoleMarks() throws Exception {
        RoleMarkReadDTO roleMark1 = generateObject(RoleMarkReadDTO.class);
        RoleMarkReadDTO roleMark2 = generateObject(RoleMarkReadDTO.class);

        List<RoleMarkReadDTO> roleMarksReadDTO = new ArrayList<>();
        roleMarksReadDTO.add(roleMark1);
        roleMarksReadDTO.add(roleMark2);

        Mockito.when(service.getRegisteredUserRoleMarks(roleMark1.getRegisteredUserId())).thenReturn(roleMarksReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-marks",
                roleMark1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleMarkReadDTO> actualRoleMarksReadDTO = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(2, actualRoleMarksReadDTO.size());
        Assertions.assertThat(actualRoleMarksReadDTO).extracting("id")
                .containsExactlyInAnyOrder(roleMark1.getId(), roleMark2.getId());

        Mockito.verify(service).getRegisteredUserRoleMarks(roleMark1.getRegisteredUserId());
    }
}
