package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewFilter;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPutDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.MovieReviewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MovieReviewController.class)
public class MovieReviewControllerTest extends BaseControllerTest {

    @MockBean
    private MovieReviewService movieReviewService;

    @WithMockUser
    @Test
    public void testGetMovieReview() throws Exception {
        MovieReviewReadDTO movieReview = generateObject(MovieReviewReadDTO.class);

        Mockito.when(movieReviewService.getMovieReview(movieReview.getId())).thenReturn(movieReview);

        String resultJson = mvc.perform(get("/api/v1/movie-reviews/{id}", movieReview.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewReadDTO actualMovieReview = objectMapper.readValue(resultJson, MovieReviewReadDTO.class);
        Assertions.assertThat(actualMovieReview).isEqualToComparingFieldByField(movieReview);

        Mockito.verify(movieReviewService).getMovieReview(movieReview.getId());
    }

    @Test
    public void testGetRegisteredUserMovieReviews() throws Exception {
        MovieReviewReadDTO movieReview1 = generateObject(MovieReviewReadDTO.class);
        MovieReviewReadDTO movieReview2 = generateObject(MovieReviewReadDTO.class);

        List<MovieReviewReadDTO> movieReviewsReadDTO = new ArrayList<>();
        movieReviewsReadDTO.add(movieReview1);
        movieReviewsReadDTO.add(movieReview2);

        UUID movieId = UUID.randomUUID();
        Mockito.when(movieReviewService.getReviewsMovie(movieId))
                .thenReturn(movieReviewsReadDTO);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/movie-reviews", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReviewReadDTO> actualMovieReviewsReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualMovieReviewsReadDTO.size());
        Assertions.assertThat(actualMovieReviewsReadDTO).extracting("id")
                .containsExactlyInAnyOrder(movieReview1.getId(), movieReview2.getId());

        Mockito.verify(movieReviewService).getReviewsMovie(movieId);
    }

    @WithMockUser
    @Test
    public void testGetRegisteredUserMovieReviewsWithUserId() throws Exception {
        MovieReviewReadDTO movieReview1 = generateObject(MovieReviewReadDTO.class);
        MovieReviewReadDTO movieReview2 = generateObject(MovieReviewReadDTO.class);

        List<MovieReviewReadDTO> movieReviewsReadDTO = new ArrayList<>();
        movieReviewsReadDTO.add(movieReview1);
        movieReviewsReadDTO.add(movieReview2);

        UUID movieId = UUID.randomUUID();
        Mockito.when(movieReviewService.getReviewsMovie(movieId)).thenReturn(movieReviewsReadDTO);

        String resultJson = mvc.perform(get("/api/v1/users/{usersId}/movies/{movieId}/movie-reviews",
                UUID.randomUUID(), movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReviewReadDTO> actualMovieReviewsReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualMovieReviewsReadDTO.size());
        Assertions.assertThat(actualMovieReviewsReadDTO).extracting("id")
                .containsExactlyInAnyOrder(movieReview1.getId(), movieReview2.getId());

        Mockito.verify(movieReviewService).getReviewsMovie(movieId);
    }

    @WithMockUser
    @Test
    public void testGetMovieReviewWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MovieReview.class, wrongId);
        Mockito.when(movieReviewService.getMovieReview(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movie-reviews/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testPatchMovieReview() throws Exception {
        MovieReviewPatchDTO patch = generateObject(MovieReviewPatchDTO.class);
        MovieReviewReadDTO read = generateObject(MovieReviewReadDTO.class);

        Mockito.when(movieReviewService.patchMovieReview(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movie-reviews/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewReadDTO actualMovieReview = objectMapper.readValue(resultJson, MovieReviewReadDTO.class);
        Assert.assertEquals(read, actualMovieReview);
    }

    @WithMockUser
    @Test
    public void testPutMovieReview() throws Exception {
        UUID id = UUID.randomUUID();

        MovieReviewPutDTO put = generateObject(MovieReviewPutDTO.class);

        mvc.perform(put("/api/v1/movie-reviews/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(movieReviewService).updateMovieReview(id, put);
    }

    @WithMockUser
    @Test
    public void testDeleteMovieReview() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movie-reviews/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(movieReviewService).deleteMovieReview(id);
    }

    @WithMockUser
    @Test
    public void testGetReviews() throws Exception {
        MovieReviewFilter filter = new MovieReviewFilter();
        filter.setMovieId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        MovieReviewReadDTO readDTO = generateObject(MovieReviewReadDTO.class);

        PageResult<MovieReviewReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(movieReviewService.getMovieReview(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movie-reviews")
                .param("movieId", filter.getMovieId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReviewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testGetMovieReviewWithPagingAndSorting() throws Exception {
        MovieReviewFilter movieReviewFilter = new MovieReviewFilter();
        MovieReviewReadDTO readDTO = generateObject(MovieReviewReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<MovieReviewReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(movieReviewService.getMovieReview(movieReviewFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movie-reviews")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<MovieReviewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}