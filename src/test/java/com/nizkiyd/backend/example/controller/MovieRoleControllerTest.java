package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Role;
import com.nizkiyd.backend.example.dto.role.RoleCreateDTO;
import com.nizkiyd.backend.example.dto.role.RolePatchDTO;
import com.nizkiyd.backend.example.dto.role.RolePutDTO;
import com.nizkiyd.backend.example.dto.role.RoleReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.MovieRoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MovieRoleController.class)
public class MovieRoleControllerTest extends BaseControllerTest {

    @MockBean
    private MovieRoleService service;

    @WithMockUser
    @Test
    public void testCreateRoleValidationFailed() throws Exception {
        RoleCreateDTO createDTO = new RoleCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/movies/{movieId}/roles", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createRole(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetRole() throws Exception {
        RoleReadDTO role = generateObject(RoleReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getRole(randomUUID, role.getId())).thenReturn(role);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/" +
                "roles/{roleId}", randomUUID, role.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(role);

        Mockito.verify(service).getRole(randomUUID, role.getId());
    }

    @Test
    public void testGetRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Role.class, wrongId);
        Mockito.when(service.getRole(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/" +
                        "roles/{roleId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testCreateRole() throws Exception {
        RoleCreateDTO create = generateObject(RoleCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleReadDTO read = generateObject(RoleReadDTO.class);

        Mockito.when(service.createRole(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/roles", randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchRole() throws Exception {
        RolePatchDTO patch = generateObject(RolePatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleReadDTO read = generateObject(RoleReadDTO.class);

        Mockito.when(service.patchRole(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{movieId}" +
                "/roles/{roleId}", randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualRole);
    }

    @WithMockUser
    @Test
    public void testDeleteRole() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{movieId}/roles/{roleId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteRole(randomUUID, id);
    }

    @WithMockUser
    @Test
    public void testPutRole() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        RolePutDTO put = generateObject(RolePutDTO.class);

        mvc.perform(put("/api/v1/movies/{movieId}/roles/{roleId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateRole(randomUUID, id, put);
    }

    @Test
    public void testGetMovieRoles() throws Exception {
        RoleReadDTO role1 = generateObject(RoleReadDTO.class);
        RoleReadDTO role2 = generateObject(RoleReadDTO.class);

        List<RoleReadDTO> rolesReadDTO = new ArrayList<>();
        rolesReadDTO.add(role1);
        rolesReadDTO.add(role2);

        Mockito.when(service.getMovieRoles(role1.getMovieId()))
                .thenReturn(rolesReadDTO);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/roles",
                role1.getMovieId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReadDTO> actualRolesReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualRolesReadDTO.size());
        Assertions.assertThat(actualRolesReadDTO).extracting("id")
                .containsExactlyInAnyOrder(role1.getId(), role2.getId());

        Mockito.verify(service).getMovieRoles(role1.getMovieId());
    }

    @WithMockUser
    @Test
    public void testGetMovieRolesWithUserId() throws Exception {
        RoleReadDTO role1 = generateObject(RoleReadDTO.class);
        RoleReadDTO role2 = generateObject(RoleReadDTO.class);

        List<RoleReadDTO> rolesReadDTO = new ArrayList<>();
        rolesReadDTO.add(role1);
        rolesReadDTO.add(role2);

        Mockito.when(service.getMovieRoles(role1.getMovieId()))
                .thenReturn(rolesReadDTO);

        String resultJson = mvc.perform(get("/api/v1/users/{usersId}/movies/{movieId}/roles",
                UUID.randomUUID(), role1.getMovieId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReadDTO> actualRolesReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualRolesReadDTO.size());
        Assertions.assertThat(actualRolesReadDTO).extracting("id")
                .containsExactlyInAnyOrder(role1.getId(), role2.getId());

        Mockito.verify(service).getMovieRoles(role1.getMovieId());
    }
}
