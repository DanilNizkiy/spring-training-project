package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.RoleReviewLike;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserRoleReviewLikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserRoleReviewLikeController.class)
public class RegisteredUserRoleReviewLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserRoleReviewLikeService service;

    @Test
    public void testCreateRoleReviewLikeValidationFailed() throws Exception {
        RoleReviewLikeCreateDTO createDTO = new RoleReviewLikeCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/role-review-likes", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createRoleReviewLike(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetRoleReviewLike() throws Exception {
        RoleReviewLikeReadDTO roleReviewLike = generateObject(RoleReviewLikeReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getRoleReviewLike(randomUUID, roleReviewLike.getId())).thenReturn(roleReviewLike);

        String resultJson = mvc.perform(
                get("/api/v1/registered-users/{registeredUserId}/role-review-likes/{roleReviewLikeId}",
                        randomUUID, roleReviewLike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewLikeReadDTO actualRoleReviewLike = objectMapper.readValue(resultJson, RoleReviewLikeReadDTO.class);
        Assertions.assertThat(actualRoleReviewLike).isEqualToComparingFieldByField(roleReviewLike);

        Mockito.verify(service).getRoleReviewLike(randomUUID, roleReviewLike.getId());
    }

    @Test
    public void testGetRoleReviewLikeWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RoleReviewLike.class, wrongId);
        Mockito.when(service.getRoleReviewLike(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                "role-review-likes/{roleReviewLikeId}", randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRoleReviewLike() throws Exception {
        RoleReviewLikeCreateDTO create = generateObject(RoleReviewLikeCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleReviewLikeReadDTO read = generateObject(RoleReviewLikeReadDTO.class);

        Mockito.when(service.createRoleReviewLike(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/role-review-likes",
                randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewLikeReadDTO actualRoleReviewLike = objectMapper.readValue(resultJson, RoleReviewLikeReadDTO.class);
        Assertions.assertThat(actualRoleReviewLike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRoleReviewLike() throws Exception {
        RoleReviewLikePatchDTO patch = generateObject(RoleReviewLikePatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleReviewLikeReadDTO read = generateObject(RoleReviewLikeReadDTO.class);

        Mockito.when(service.patchRoleReviewLike(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}/" +
                "role-review-likes/{roleReviewLikeId}", randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewLikeReadDTO actualRoleReviewLike = objectMapper.readValue(resultJson, RoleReviewLikeReadDTO.class);
        Assert.assertEquals(read, actualRoleReviewLike);
    }

    @Test
    public void testDeleteRoleReviewLike() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/" +
                "role-review-likes/{roleReviewLikeId}", randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteRoleReviewLike(randomUUID, id);
    }

    @Test
    public void testGetRegisteredUserRoleReviewLikes() throws Exception {
        RoleReviewLikeReadDTO roleReviewLike1 = generateObject(RoleReviewLikeReadDTO.class);
        RoleReviewLikeReadDTO roleReviewLike2 = generateObject(RoleReviewLikeReadDTO.class);

        List<RoleReviewLikeReadDTO> roleReviewLikesReadDTO = new ArrayList<>();
        roleReviewLikesReadDTO.add(roleReviewLike1);
        roleReviewLikesReadDTO.add(roleReviewLike2);

        Mockito.when(service.getRegisteredUserRoleReviewLikes(
                roleReviewLike1.getRegisteredUserId())).thenReturn(roleReviewLikesReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-review-likes",
                roleReviewLike1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReviewLikeReadDTO> actualRoleReviewLikesReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualRoleReviewLikesReadDTO.size());
        Assertions.assertThat(actualRoleReviewLikesReadDTO).extracting("id")
                .containsExactlyInAnyOrder(roleReviewLike1.getId(), roleReviewLike2.getId());

        Mockito.verify(service).getRegisteredUserRoleReviewLikes(roleReviewLike1.getRegisteredUserId());
    }

    @Test
    public void testPutRoleReviewLike() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        RoleReviewLikePutDTO put = generateObject(RoleReviewLikePutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/"
                        + "role-review-likes/{roleReviewLikeId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateRoleReviewLike(randomUUID, id, put);
    }
}