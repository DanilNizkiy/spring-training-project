package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.MovieMark;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkCreateDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkPatchDTO;
import com.nizkiyd.backend.example.dto.moviemark.MovieMarkReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserMovieMarkService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserMovieMarkController.class)
public class RegisteredUserMovieMarkControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserMovieMarkService service;

    @Test
    public void testCreateMovieMarkValidationFailed() throws Exception {
        MovieMarkCreateDTO createDTO = new MovieMarkCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/movie-marks", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createMovieMark(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetMovieMark() throws Exception {
        MovieMarkReadDTO movieMark = generateObject(MovieMarkReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getMovieMark(randomUUID, movieMark.getId())).thenReturn(movieMark);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/movie-marks/{movieMarkId}",
                randomUUID, movieMark.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieMarkReadDTO actualMovieMark = objectMapper.readValue(resultJson, MovieMarkReadDTO.class);
        Assertions.assertThat(actualMovieMark).isEqualToComparingFieldByField(movieMark);

        Mockito.verify(service).getMovieMark(randomUUID, movieMark.getId());
    }

    @Test
    public void testGetMovieMarkWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MovieMark.class, wrongId);
        Mockito.when(service.getMovieMark(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/movie-marks/{movieMarkId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateMovieMark() throws Exception {
        MovieMarkCreateDTO create = generateObject(MovieMarkCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        MovieMarkReadDTO read = generateObject(MovieMarkReadDTO.class);

        Mockito.when(service.createMovieMark(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/movie-marks", randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieMarkReadDTO actualMovieMark = objectMapper.readValue(resultJson, MovieMarkReadDTO.class);
        Assertions.assertThat(actualMovieMark).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchMovieMark() throws Exception {
        MovieMarkPatchDTO patch = generateObject(MovieMarkPatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        MovieMarkReadDTO read = generateObject(MovieMarkReadDTO.class);

        Mockito.when(service.patchMovieMark(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}/movie-marks/{movieMarkId}",
                randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieMarkReadDTO actualMovieMark = objectMapper.readValue(resultJson, MovieMarkReadDTO.class);
        Assert.assertEquals(read, actualMovieMark);
    }

    @Test
    public void testDeleteMovieMark() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/movie-marks/{movieMarkId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteMovieMark(randomUUID, id);
    }

    @Test
    public void testGetRegisteredUserMovieMarks() throws Exception {
        MovieMarkReadDTO movieMark1 = generateObject(MovieMarkReadDTO.class);
        MovieMarkReadDTO movieMark2 = generateObject(MovieMarkReadDTO.class);

        List<MovieMarkReadDTO> movieMarksReadDTO = new ArrayList<>();
        movieMarksReadDTO.add(movieMark1);
        movieMarksReadDTO.add(movieMark2);

        Mockito.when(service.getRegisteredUserMovieMarks(
                movieMark1.getRegisteredUserId())).thenReturn(movieMarksReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/movie-marks",
                movieMark1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieMarkReadDTO> actualMovieMarksReadDTO = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(2, actualMovieMarksReadDTO.size());
        Assertions.assertThat(actualMovieMarksReadDTO).extracting("id")
                .containsExactlyInAnyOrder(movieMark1.getId(), movieMark2.getId());

        Mockito.verify(service).getRegisteredUserMovieMarks(movieMark1.getRegisteredUserId());
    }
}
