package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.RoleMark;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkFilter;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkReadDTO;
import com.nizkiyd.backend.example.dto.rolemark.RoleMarkPatchDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.RoleMarkService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RoleMarkController.class)
public class RoleMarkControllerTest extends BaseControllerTest {

    @MockBean
    private RoleMarkService roleMarkService;

    @Test
    public void testGetRoleMark() throws Exception {
        RoleMarkReadDTO roleMark = generateObject(RoleMarkReadDTO.class);

        Mockito.when(roleMarkService.getRoleMark(roleMark.getId())).thenReturn(roleMark);

        String resultJson = mvc.perform(get("/api/v1/role-marks/{id}", roleMark.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleMarkReadDTO actualRoleMark = objectMapper.readValue(resultJson, RoleMarkReadDTO.class);
        Assertions.assertThat(actualRoleMark).isEqualToComparingFieldByField(roleMark);

        Mockito.verify(roleMarkService).getRoleMark(roleMark.getId());
    }

    @Test
    public void testGetRoleMarkWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RoleMark.class, wrongId);
        Mockito.when(roleMarkService.getRoleMark(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/role-marks/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchRoleMark() throws Exception {
        RoleMarkPatchDTO patch = generateObject(RoleMarkPatchDTO.class);

        RoleMarkReadDTO read = generateObject(RoleMarkReadDTO.class);

        Mockito.when(roleMarkService.patchRoleMark(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/role-marks/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleMarkReadDTO actualRoleMark = objectMapper.readValue(resultJson, RoleMarkReadDTO.class);
        Assert.assertEquals(read, actualRoleMark);
    }

    @Test
    public void testDeleteRoleMark() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/role-marks/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(roleMarkService).deleteRoleMark(id);
    }

    @Test
    public void testGetRoleMarks() throws Exception {
        RoleMarkFilter filter = new RoleMarkFilter();
        filter.setRoleId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        RoleMarkReadDTO readDTO = generateObject(RoleMarkReadDTO.class);

        PageResult<RoleMarkReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(roleMarkService.getRoleMark(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/role-marks")
                .param("roleId", filter.getRoleId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleMarkReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetRoleMarkWithPagingAndSorting() throws Exception {
        RoleMarkFilter roleMarkFilter = new RoleMarkFilter();
        RoleMarkReadDTO readDTO = generateObject(RoleMarkReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<RoleMarkReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "mark"));
        Mockito.when(roleMarkService.getRoleMark(roleMarkFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/role-marks")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "mark,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleMarkReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}