package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeFilter;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.NewsLikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = NewsLikeController.class)
public class NewsLikeControllerTest extends BaseControllerTest {

    @MockBean
    private NewsLikeService newsLikeService;

    @Test
    public void testGetNewsLike() throws Exception {
        NewsLikeReadDTO newsLike = generateObject(NewsLikeReadDTO.class);

        Mockito.when(newsLikeService.getNewsLike(newsLike.getId())).thenReturn(newsLike);

        String resultJson = mvc.perform(get("/api/v1/news-likes/{id}", newsLike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeReadDTO actualNewsLike = objectMapper.readValue(
                resultJson, NewsLikeReadDTO.class);
        Assertions.assertThat(actualNewsLike).isEqualToComparingFieldByField(newsLike);

        Mockito.verify(newsLikeService).getNewsLike(newsLike.getId());
    }

    @Test
    public void testGetNewsLikeWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(NewsLike.class, wrongId);
        Mockito.when(newsLikeService.getNewsLike(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/news-likes/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchNewsLike() throws Exception {
        NewsLikePatchDTO patch = generateObject(NewsLikePatchDTO.class);

        NewsLikeReadDTO read = generateObject(NewsLikeReadDTO.class);

        Mockito.when(newsLikeService.patchNewsLike(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/news-likes/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeReadDTO actualNewsLike = objectMapper.readValue(
                resultJson, NewsLikeReadDTO.class);
        Assert.assertEquals(read, actualNewsLike);
    }

    @Test
    public void testDeleteNewsLike() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news-likes/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(newsLikeService).deleteNewsLike(id);
    }

    @Test
    public void testGetNewsLikes() throws Exception {
        NewsLikeFilter filter = new NewsLikeFilter();
        filter.setNewsId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        NewsLikeReadDTO readDTO = generateObject(NewsLikeReadDTO.class);

        PageResult<NewsLikeReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(newsLikeService.getNewsLike(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/news-likes")
                .param("newsId", filter.getNewsId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<NewsLikeReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetNewsLikeWithPagingAndSorting() throws Exception {
        NewsLikeFilter newsLikeFilter = new NewsLikeFilter();
        NewsLikeReadDTO readDTO = generateObject(NewsLikeReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<NewsLikeReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(newsLikeService.getNewsLike(newsLikeFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/news-likes")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<NewsLikeReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}