package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.CrewMember;
import com.nizkiyd.backend.example.domain.CrewMemberType;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberCreateDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberFilter;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberPatchDTO;
import com.nizkiyd.backend.example.dto.crewmember.CrewMemberReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.CrewMemberService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CrewMemberController.class)
public class CrewMemberControllerTest extends BaseControllerTest {

    @MockBean
    private CrewMemberService crewMemberService;

    @Test
    public void testGetCrewMember() throws Exception {
        CrewMemberReadDTO crewMember = generateObject(CrewMemberReadDTO.class);

        Mockito.when(crewMemberService.getCrewMember(crewMember.getId())).thenReturn(crewMember);

        String resultJson = mvc.perform(get("/api/v1/crew-members/{id}", crewMember.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(crewMember);

        Mockito.verify(crewMemberService).getCrewMember(crewMember.getId());
    }

    @Test
    public void testGetCrewMemberWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(CrewMember.class, wrongId);
        Mockito.when(crewMemberService.getCrewMember(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/crew-members/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testAddCrewMemberToMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);
        read.setId(crewMemberId);
        List<CrewMemberReadDTO> expectedCrewMember = List.of(read);
        Mockito.when(crewMemberService.addCrewMemberToMovie(movieId, crewMemberId)).thenReturn(expectedCrewMember);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/crew-members/{id}", movieId, crewMemberId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedCrewMember, actualCrewMembers);
    }

    @Test
    public void testGetCrewMembersNews() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);
        read.setId(crewMemberId);
        List<CrewMemberReadDTO> expectedCrewMember = List.of(read);
        Mockito.when(crewMemberService.getCrewMembersNews(newsId)).thenReturn(expectedCrewMember);

        String resultJson = mvc.perform(get("/api/v1/news/{newsId}/crew-members", newsId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedCrewMember, actualCrewMembers);
    }

    @WithMockUser
    @Test
    public void testGetCrewMemberNewsWithUserId() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);
        read.setId(crewMemberId);
        List<CrewMemberReadDTO> expectedCrewMember = List.of(read);
        Mockito.when(crewMemberService.getCrewMembersNews(newsId)).thenReturn(expectedCrewMember);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/news/{newsId}/crew-members",
                UUID.randomUUID(), newsId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedCrewMember, actualCrewMembers);
    }

    @Test
    public void testGetCrewMemberMovies() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);
        read.setId(crewMemberId);
        List<CrewMemberReadDTO> expectedCrewMember = List.of(read);
        Mockito.when(crewMemberService.getCrewMemberMovies(movieId)).thenReturn(expectedCrewMember);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crew-members", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedCrewMember, actualCrewMembers);
    }

    @WithMockUser
    @Test
    public void testGetCrewMemberMoviesWithUserId() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);
        read.setId(crewMemberId);
        List<CrewMemberReadDTO> expectedCrewMember = List.of(read);
        Mockito.when(crewMemberService.getCrewMemberMovies(movieId)).thenReturn(expectedCrewMember);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/movies/{movieId}/crew-members",
                UUID.randomUUID(), movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedCrewMember, actualCrewMembers);
    }

    @WithMockUser
    @Test
    public void testCreateCrewMember() throws Exception {
        CrewMemberCreateDTO create = generateObject(CrewMemberCreateDTO.class);
        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);

        Mockito.when(crewMemberService.createCrewMember(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/crew-members")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchCrewMember() throws Exception {
        CrewMemberPatchDTO patch = generateObject(CrewMemberPatchDTO.class);
        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);

        Mockito.when(crewMemberService.patchCrewMember(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/crew-members/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assert.assertEquals(read, actualCrewMember);
    }


    @Test
    public void testGetCrewMembers() throws Exception {
        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setName("a");
        filter.setPersonId(UUID.randomUUID());
        filter.setCrewMemberType(CrewMemberType.PRODUCER);
        filter.setNewsId(UUID.randomUUID());

        CrewMemberReadDTO readDTO = generateObject(CrewMemberReadDTO.class);

        PageResult<CrewMemberReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(crewMemberService.getCrewMember(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/crew-members")
                .param("name", filter.getName())
                .param("crewMemberType", filter.getCrewMemberType().toString())
                .param("newsId", filter.getNewsId().toString())
                .param("personId", filter.getPersonId().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<CrewMemberReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testGetCrewMembersWithNewsId() throws Exception {
        UUID userId = UUID.randomUUID();

        CrewMemberFilter filter = new CrewMemberFilter();
        filter.setName("a");
        filter.setPersonId(UUID.randomUUID());
        filter.setCrewMemberType(CrewMemberType.PRODUCER);
        filter.setNewsId(UUID.randomUUID());

        CrewMemberReadDTO readDTO = generateObject(CrewMemberReadDTO.class);

        PageResult<CrewMemberReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(crewMemberService.getCrewMember(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/crew-members", userId)
                .param("name", filter.getName())
                .param("crewMemberType", filter.getCrewMemberType().toString())
                .param("newsId", filter.getNewsId().toString())
                .param("personId", filter.getPersonId().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<CrewMemberReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetCrewMemberWithPagingAndSorting() throws Exception {
        CrewMemberFilter crewMemberFilter = new CrewMemberFilter();
        CrewMemberReadDTO readDTO = generateObject(CrewMemberReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<CrewMemberReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size);
        Mockito.when(crewMemberService.getCrewMember(crewMemberFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/crew-members")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<CrewMemberReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }


    @WithMockUser
    @Test
    public void testAddCrewMemberToNews() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = generateObject(CrewMemberReadDTO.class);
        read.setId(crewMemberId);
        List<CrewMemberReadDTO> expectedCrewMembers = List.of(read);
        Mockito.when(crewMemberService.addCrewMemberToNews(newsId, crewMemberId)).thenReturn(expectedCrewMembers);

        String resultJson = mvc.perform(post("/api/v1/news/{newsId}/crew-members/{crewMemberId}", newsId, crewMemberId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedCrewMembers, actualCrewMembers);
    }

    @WithMockUser
    @Test
    public void testRemoveCrewMemberFromNews() throws Exception {
        UUID crewMemberId = UUID.randomUUID();
        UUID newsId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news/{newsId}/crew-members/{crewMemberId}", newsId.toString(), crewMemberId.toString()))
                .andExpect(status().isOk());

        Mockito.verify(crewMemberService).removeCrewMemberFromNews(newsId, crewMemberId);
    }

    @WithMockUser
    @Test
    public void testRemoveCrewMemberFromMovie() throws Exception {
        UUID crewMemberId = UUID.randomUUID();
        UUID movieId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{movieId}/crew-members/{id}",
                movieId.toString(), crewMemberId.toString())).andExpect(status().isOk());

        Mockito.verify(crewMemberService).removeCrewMemberFromMovie(movieId, crewMemberId);
    }
}