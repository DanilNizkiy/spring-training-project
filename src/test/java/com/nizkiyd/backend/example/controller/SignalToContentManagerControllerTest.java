package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.SignalToContentManager;
import com.nizkiyd.backend.example.domain.SignalToContentManagerStatus;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.signaltocontentmanager.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.SignalToContentManagerWrongStatusException;
import com.nizkiyd.backend.example.service.SignalToContentManagerService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = SignalToContentManagerController.class)
public class SignalToContentManagerControllerTest extends BaseControllerTest {

    @MockBean
    private SignalToContentManagerService signalToContentManagerService;

    @Test
    public void testGetSignalToContentManager() throws Exception {
        SignalToContentManagerReadDTO signalToContentManager = generateObject(SignalToContentManagerReadDTO.class);

        Mockito.when(signalToContentManagerService.getSignalToContentManager(
                signalToContentManager.getId())).thenReturn(signalToContentManager);

        String resultJson = mvc.perform(get("/api/v1/signals-to-content-manager/{id}", signalToContentManager.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToContentManager = objectMapper.readValue(
                resultJson, SignalToContentManagerReadDTO.class);
        Assertions.assertThat(actualSignalToContentManager).isEqualToComparingFieldByField(signalToContentManager);

        Mockito.verify(signalToContentManagerService).getSignalToContentManager(signalToContentManager.getId());
    }

    @Test
    public void testGetSignalToContentManagerWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(SignalToContentManager.class, wrongId);
        Mockito.when(signalToContentManagerService.getSignalToContentManager(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/signals-to-content-manager/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchSignalToContentManager() throws Exception {
        SignalToContentManagerPatchDTO patch = generateObject(SignalToContentManagerPatchDTO.class);

        SignalToContentManagerReadDTO read = generateObject(SignalToContentManagerReadDTO.class);

        Mockito.when(signalToContentManagerService.patchSignalToContentManager(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/signals-to-content-manager/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToContentManager = objectMapper.readValue(
                resultJson, SignalToContentManagerReadDTO.class);
        Assert.assertEquals(read, actualSignalToContentManager);
    }

    @Test
    public void testPatchSignalToContentManagerFix() throws Exception {
        SignalToContentManagerReadDTO read = generateObject(SignalToContentManagerReadDTO.class);
        SignalToContentManagerFixContent signalToContentManagerFixContent = generateObject(
                SignalToContentManagerFixContent.class);

        Mockito.when(signalToContentManagerService.fixContent(
                read.getId(), signalToContentManagerFixContent)).thenReturn(read);

        String resultJson = mvc.perform(post(
                "/api/v1/signals-to-content-manager/{id}/fix-content", read.getId().toString())
                .content(objectMapper.writeValueAsString(signalToContentManagerFixContent))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToContentManager = objectMapper.readValue(
                resultJson, SignalToContentManagerReadDTO.class);
        Assert.assertEquals(read, actualSignalToContentManager);
    }

    @Test
    public void testPatchSignalToContentManagerFixWrongStatus() throws Exception {
        UUID contentManagerId = UUID.randomUUID();

        SignalToContentManagerWrongStatusException exception = new SignalToContentManagerWrongStatusException(
                UUID.randomUUID(), SignalToContentManagerStatus.FIXED);

        SignalToContentManagerReadDTO read = generateObject(SignalToContentManagerReadDTO.class);
        SignalToContentManagerFixContent signalToContentManagerFixContent = generateObject(
                SignalToContentManagerFixContent.class);

        Mockito.when(signalToContentManagerService.fixContent(
                read.getId(), signalToContentManagerFixContent)).thenThrow(exception);

        String resultJson = mvc.perform(post(
                "/api/v1/signals-to-content-manager/{id}/fix-content", read.getId().toString())
                .content(objectMapper.writeValueAsString(signalToContentManagerFixContent))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPutSignalToContentManager() throws Exception {
        UUID id = UUID.randomUUID();

        SignalToContentManagerPutDTO put = new SignalToContentManagerPutDTO();
        put.setReplacement("asdbhtc");

        mvc.perform(put("/api/v1/signals-to-content-manager/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(signalToContentManagerService).updateSignalToContentManager(id, put);
    }

    @Test
    public void testDeleteSignalToContentManager() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/signals-to-content-manager/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(signalToContentManagerService).deleteSignalToContentManager(id);
    }

    @Test
    public void testGetSignalToContentManagers() throws Exception {
        SignalToContentManagerFilter filter = new SignalToContentManagerFilter();
        filter.setSignalToContentManagerObjectId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());
        filter.setContentManagerId(UUID.randomUUID());

        SignalToContentManagerReadDTO readDTO = generateObject(SignalToContentManagerReadDTO.class);

        PageResult<SignalToContentManagerReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(signalToContentManagerService.getSignalToContentManager(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/signals-to-content-manager")
                .param("signalToContentManagerObjectId", filter.getSignalToContentManagerObjectId().toString())
                .param("contentManagerId", filter.getContentManagerId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<SignalToContentManagerReadDTO> actualPage = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetSignalToContentManagerWithPagingAndSorting() throws Exception {
        SignalToContentManagerFilter signalToContentManagerFilter = new SignalToContentManagerFilter();
        SignalToContentManagerReadDTO readDTO = generateObject(SignalToContentManagerReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<SignalToContentManagerReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(signalToContentManagerService.getSignalToContentManager(
                signalToContentManagerFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/signals-to-content-manager")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<SignalToContentManagerReadDTO> actualPage = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetSignalToContentManagerWithBigPage() throws Exception {
        SignalToContentManagerFilter signalToContentManagerFilter = new SignalToContentManagerFilter();
        SignalToContentManagerReadDTO readDTO = generateObject(SignalToContentManagerReadDTO.class);

        int page = 8;
        int size = 99999;

        PageResult<SignalToContentManagerReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize);
        Mockito.when(signalToContentManagerService.getSignalToContentManager(
                signalToContentManagerFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/signals-to-content-manager")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<SignalToContentManagerReadDTO> actualPage = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(resultPage, actualPage);
    }
}