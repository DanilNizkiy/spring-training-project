package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.RoleReviewLike;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeFilter;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikeReadDTO;
import com.nizkiyd.backend.example.dto.rolereviewlike.RoleReviewLikePatchDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.RoleReviewLikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RoleReviewLikeController.class)
public class RoleReviewLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RoleReviewLikeService roleReviewLikeService;

    @Test
    public void testGetRoleReviewLike() throws Exception {
        RoleReviewLikeReadDTO roleReview = generateObject(RoleReviewLikeReadDTO.class);

        Mockito.when(roleReviewLikeService.getRoleReviewLike(roleReview.getId())).thenReturn(roleReview);

        String resultJson = mvc.perform(get("/api/v1/role-review-likes/{id}", roleReview.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewLikeReadDTO actualRoleReviewLike = objectMapper.readValue(resultJson, RoleReviewLikeReadDTO.class);
        Assertions.assertThat(actualRoleReviewLike).isEqualToComparingFieldByField(roleReview);

        Mockito.verify(roleReviewLikeService).getRoleReviewLike(roleReview.getId());
    }

    @Test
    public void testGetRoleReviewLikeWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RoleReviewLike.class, wrongId);
        Mockito.when(roleReviewLikeService.getRoleReviewLike(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/role-review-likes/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchRoleReviewLike() throws Exception {
        RoleReviewLikePatchDTO patch = generateObject(RoleReviewLikePatchDTO.class);

        RoleReviewLikeReadDTO read = generateObject(RoleReviewLikeReadDTO.class);

        Mockito.when(roleReviewLikeService.patchRoleReviewLike(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/role-review-likes/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewLikeReadDTO actualRoleReviewLike = objectMapper.readValue(resultJson, RoleReviewLikeReadDTO.class);
        Assert.assertEquals(read, actualRoleReviewLike);
    }

    @Test
    public void testDeleteRoleReviewLike() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/role-review-likes/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(roleReviewLikeService).deleteRoleReviewLike(id);
    }

    @Test
    public void testGetRoleReviewLikes() throws Exception {
        RoleReviewLikeFilter filter = new RoleReviewLikeFilter();
        filter.setRoleReviewId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());

        RoleReviewLikeReadDTO readDTO = generateObject(RoleReviewLikeReadDTO.class);

        PageResult<RoleReviewLikeReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(roleReviewLikeService.getRoleReviewLike(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/role-review-likes")
                .param("roleReviewId", filter.getRoleReviewId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleReviewLikeReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetRoleReviewLikeWithPagingAndSorting() throws Exception {
        RoleReviewLikeFilter roleReviewLikeFilter = new RoleReviewLikeFilter();
        RoleReviewLikeReadDTO readDTO = generateObject(RoleReviewLikeReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<RoleReviewLikeReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(roleReviewLikeService.getRoleReviewLike(roleReviewLikeFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/role-review-likes")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<RoleReviewLikeReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}