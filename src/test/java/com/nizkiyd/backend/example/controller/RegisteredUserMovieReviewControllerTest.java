package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.MovieReview;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewCreateDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPatchDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewPutDTO;
import com.nizkiyd.backend.example.dto.moviereview.MovieReviewReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserMovieReviewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserMovieReviewController.class)
public class RegisteredUserMovieReviewControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserMovieReviewService service;

    @Test
    public void testCreateMovieReviewValidationFailed() throws Exception {
        MovieReviewCreateDTO createDTO = new MovieReviewCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/movie-reviews", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createMovieReview(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetMovieReview() throws Exception {
        MovieReviewReadDTO movieReview = generateObject(MovieReviewReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getMovieReview(randomUUID, movieReview.getId())).thenReturn(movieReview);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                "movie-reviews/{movieReviewId}", randomUUID, movieReview.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewReadDTO actualMovieReview = objectMapper.readValue(resultJson, MovieReviewReadDTO.class);
        Assertions.assertThat(actualMovieReview).isEqualToComparingFieldByField(movieReview);

        Mockito.verify(service).getMovieReview(randomUUID, movieReview.getId());
    }

    @Test
    public void testGetMovieReviewWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MovieReview.class, wrongId);
        Mockito.when(service.getMovieReview(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/" +
                        "movie-reviews/{movieReviewId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateMovieReview() throws Exception {
        MovieReviewCreateDTO create = generateObject(MovieReviewCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        MovieReviewReadDTO read = generateObject(MovieReviewReadDTO.class);

        Mockito.when(service.createMovieReview(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/movie-reviews", randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewReadDTO actualMovieReview = objectMapper.readValue(resultJson, MovieReviewReadDTO.class);
        Assertions.assertThat(actualMovieReview).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchMovieReview() throws Exception {
        MovieReviewPatchDTO patch = generateObject(MovieReviewPatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        MovieReviewReadDTO read = generateObject(MovieReviewReadDTO.class);

        Mockito.when(service.patchMovieReview(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}" +
                "/movie-reviews/{movieReviewId}", randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewReadDTO actualMovieReview = objectMapper.readValue(resultJson, MovieReviewReadDTO.class);
        Assert.assertEquals(read, actualMovieReview);
    }

    @Test
    public void testDeleteMovieReview() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/movie-reviews/{movieReviewId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteMovieReview(randomUUID, id);
    }

    @Test
    public void testPutMovieReview() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        MovieReviewPutDTO put = generateObject(MovieReviewPutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/movie-reviews/{movieReviewId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateMovieReview(randomUUID, id, put);
    }

    @Test
    public void testGetRegisteredUserMovieReviews() throws Exception {
        MovieReviewReadDTO movieReview1 = generateObject(MovieReviewReadDTO.class);
        MovieReviewReadDTO movieReview2 = generateObject(MovieReviewReadDTO.class);

        List<MovieReviewReadDTO> movieReviewsReadDTO = new ArrayList<>();
        movieReviewsReadDTO.add(movieReview1);
        movieReviewsReadDTO.add(movieReview2);

        Mockito.when(service.getRegisteredUserMovieReviews(movieReview1.getRegisteredUserId()))
                .thenReturn(movieReviewsReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/movie-reviews",
                movieReview1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReviewReadDTO> actualMovieReviewsReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualMovieReviewsReadDTO.size());
        Assertions.assertThat(actualMovieReviewsReadDTO).extracting("id")
                .containsExactlyInAnyOrder(movieReview1.getId(), movieReview2.getId());

        Mockito.verify(service).getRegisteredUserMovieReviews(movieReview1.getRegisteredUserId());
    }
}
