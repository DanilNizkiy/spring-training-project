package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Actor;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.actor.ActorCreateDTO;
import com.nizkiyd.backend.example.dto.actor.ActorPatchDTO;
import com.nizkiyd.backend.example.dto.actor.ActorReadDTO;
import com.nizkiyd.backend.example.dto.actor.ActorFilter;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.ActorService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ActorController.class)
public class ActorControllerTest extends BaseControllerTest {

    @MockBean
    private ActorService actorService;

    @Test
    public void testGetActor() throws Exception {
        ActorReadDTO actor = generateObject(ActorReadDTO.class);

        Mockito.when(actorService.getActor(actor.getId())).thenReturn(actor);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", actor.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(actor);

        Mockito.verify(actorService).getActor(actor.getId());
    }

    @Test
    public void testGetActorWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Actor.class, wrongId);
        Mockito.when(actorService.getActor(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testCreateActor() throws Exception {
        ActorCreateDTO create = generateObject(ActorCreateDTO.class);

        ActorReadDTO read = generateObject(ActorReadDTO.class);

        Mockito.when(actorService.createActor(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/actors")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchActor() throws Exception {
        ActorPatchDTO patch = generateObject(ActorPatchDTO.class);

        ActorReadDTO read = generateObject(ActorReadDTO.class);

        Mockito.when(actorService.patchActor(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/actors/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assert.assertEquals(read, actualActor);
    }

    @WithMockUser
    @Test
    public void testDeleteActor() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/actors/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(actorService).deleteActor(id);
    }

    @Test
    public void testGetActors() throws Exception {
        ActorFilter filter = new ActorFilter();
        filter.setName("a");
        filter.setPersonId(UUID.randomUUID());
        filter.setAverageMovieRatingFrom(6.0);
        filter.setAverageMovieRatingTo(10.0);
        filter.setAverageRoleRatingFrom(6.0);
        filter.setAverageRoleRatingTo(10.0);
        filter.setNewsId(UUID.randomUUID());

        ActorReadDTO readDTO = generateObject(ActorReadDTO.class);

        PageResult<ActorReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(actorService.getActor(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/actors")
                .param("name", filter.getName())
                .param("personId", filter.getPersonId().toString())
                .param("newsId", filter.getNewsId().toString())
                .param("averageMovieRatingFrom", filter.getAverageMovieRatingFrom().toString())
                .param("averageMovieRatingTo", filter.getAverageMovieRatingTo().toString())
                .param("averageRoleRatingFrom", filter.getAverageRoleRatingFrom().toString())
                .param("averageRoleRatingTo", filter.getAverageRoleRatingTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ActorReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testGetActorsWithNewsId() throws Exception {
        UUID userId = UUID.randomUUID();

        ActorFilter filter = new ActorFilter();
        filter.setName("a");
        filter.setPersonId(UUID.randomUUID());
        filter.setAverageMovieRatingFrom(6.0);
        filter.setAverageMovieRatingTo(10.0);
        filter.setAverageRoleRatingFrom(6.0);
        filter.setAverageRoleRatingTo(10.0);
        filter.setNewsId(UUID.randomUUID());

        ActorReadDTO readDTO = generateObject(ActorReadDTO.class);

        PageResult<ActorReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(actorService.getActor(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/actors", userId)
                .param("name", filter.getName())
                .param("personId", filter.getPersonId().toString())
                .param("newsId", filter.getNewsId().toString())
                .param("averageMovieRatingFrom", filter.getAverageMovieRatingFrom().toString())
                .param("averageMovieRatingTo", filter.getAverageMovieRatingTo().toString())
                .param("averageRoleRatingFrom", filter.getAverageRoleRatingFrom().toString())
                .param("averageRoleRatingTo", filter.getAverageRoleRatingTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ActorReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetActorWithPagingAndSorting() throws Exception {
        ActorFilter actorFilter = new ActorFilter();
        ActorReadDTO readDTO = generateObject(ActorReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<ActorReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "averageRoleRating"));
        Mockito.when(actorService.getActor(actorFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/actors")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "averageRoleRating,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ActorReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @WithMockUser
    @Test
    public void testAddActorToNews() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID actorId = UUID.randomUUID();

        ActorReadDTO read = generateObject(ActorReadDTO.class);
        read.setId(actorId);
        List<ActorReadDTO> expectedActors = List.of(read);
        Mockito.when(actorService.addActorToNews(newsId, actorId)).thenReturn(expectedActors);

        String resultJson = mvc.perform(post("/api/v1/news/{newsId}/actors/{actorId}", newsId, actorId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ActorReadDTO> actualActors = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedActors, actualActors);
    }

    @WithMockUser
    @Test
    public void testRemoveActorFromNews() throws Exception {
        UUID actorId = UUID.randomUUID();
        UUID newsId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news/{newsId}/actors/{actorId}", newsId.toString(), actorId.toString()))
                .andExpect(status().isOk());

        Mockito.verify(actorService).removeActorFromNews(newsId, actorId);
    }

    @Test
    public void testGetActorsNews() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID actorId = UUID.randomUUID();

        ActorReadDTO read = generateObject(ActorReadDTO.class);
        read.setId(actorId);
        List<ActorReadDTO> expectedActor = List.of(read);
        Mockito.when(actorService.getActorsNews(newsId)).thenReturn(expectedActor);

        String resultJson = mvc.perform(get("/api/v1/news/{newsId}/actors", newsId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ActorReadDTO> actualActors = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedActor, actualActors);
    }

    @WithMockUser
    @Test
    public void testGetActorNewsWithUserId() throws Exception {
        UUID newsId = UUID.randomUUID();
        UUID actorId = UUID.randomUUID();

        ActorReadDTO read = generateObject(ActorReadDTO.class);
        read.setId(actorId);
        List<ActorReadDTO> expectedActor = List.of(read);
        Mockito.when(actorService.getActorsNews(newsId)).thenReturn(expectedActor);

        String resultJson = mvc.perform(get("/api/v1/users/{userId}/news/{newsId}/actors",
                UUID.randomUUID(), newsId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ActorReadDTO> actualActors = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedActor, actualActors);
    }
}