package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.MovieReviewLike;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeCreateDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePatchDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikePutDTO;
import com.nizkiyd.backend.example.dto.moviereviewlike.MovieReviewLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserMovieReviewLikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserMovieReviewLikeController.class)
public class RegisteredUserMovieReviewLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserMovieReviewLikeService service;

    @Test
    public void testCreateMovieReviewLikeValidationFailed() throws Exception {
        MovieReviewLikeCreateDTO createDTO = new MovieReviewLikeCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/movie-review-likes", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createMovieReviewLike(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetMovieReviewLike() throws Exception {
        MovieReviewLikeReadDTO movieReviewLike = generateObject(MovieReviewLikeReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getMovieReviewLike(randomUUID, movieReviewLike.getId())).thenReturn(movieReviewLike);

        String resultJson = mvc.perform(
                get("/api/v1/registered-users/{registeredUserId}/movie-review-likes/{movieReviewLikeId}",
                        randomUUID, movieReviewLike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewLikeReadDTO actualMovieReviewLike = objectMapper.readValue(
                resultJson, MovieReviewLikeReadDTO.class);
        Assertions.assertThat(actualMovieReviewLike).isEqualToComparingFieldByField(movieReviewLike);

        Mockito.verify(service).getMovieReviewLike(randomUUID, movieReviewLike.getId());
    }

    @Test
    public void testGetMovieReviewLikeWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MovieReviewLike.class, wrongId);
        Mockito.when(service.getMovieReviewLike(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(
                get("/api/v1/registered-users/{registeredUserId}/movie-review-likes/{movieReviewLikeId}",
                        randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateMovieReviewLike() throws Exception {
        MovieReviewLikeCreateDTO create = generateObject(MovieReviewLikeCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        MovieReviewLikeReadDTO read = generateObject(MovieReviewLikeReadDTO.class);

        Mockito.when(service.createMovieReviewLike(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/movie-review-likes",
                randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewLikeReadDTO actualMovieReviewLike = objectMapper.readValue(
                resultJson, MovieReviewLikeReadDTO.class);
        Assertions.assertThat(actualMovieReviewLike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchMovieReviewLike() throws Exception {
        MovieReviewLikePatchDTO patch = generateObject(MovieReviewLikePatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        MovieReviewLikeReadDTO read = generateObject(MovieReviewLikeReadDTO.class);

        Mockito.when(service.patchMovieReviewLike(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(
                patch("/api/v1/registered-users/{registeredUserId}" +
                        "/movie-review-likes/{movieReviewLikeId}", randomUUID, read.getId().toString())
                        .content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReviewLikeReadDTO actualMovieReviewLike = objectMapper.readValue(
                resultJson, MovieReviewLikeReadDTO.class);
        Assert.assertEquals(read, actualMovieReviewLike);
    }

    @Test
    public void testDeleteMovieReviewLike() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/movie-review-likes/{movieReviewLikeId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteMovieReviewLike(randomUUID, id);
    }

    @Test
    public void testGetRegisteredUserMovieReviewLikes() throws Exception {
        MovieReviewLikeReadDTO movieReviewLike1 = generateObject(MovieReviewLikeReadDTO.class);
        MovieReviewLikeReadDTO movieReviewLike2 = generateObject(MovieReviewLikeReadDTO.class);

        List<MovieReviewLikeReadDTO> movieReviewLikesReadDTO = new ArrayList<>();
        movieReviewLikesReadDTO.add(movieReviewLike1);
        movieReviewLikesReadDTO.add(movieReviewLike2);

        Mockito.when(service.getRegisteredUserMovieReviewLikes(
                movieReviewLike1.getRegisteredUserId())).thenReturn(movieReviewLikesReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/movie-review-likes",
                movieReviewLike1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReviewLikeReadDTO> actualMovieReviewLikesReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualMovieReviewLikesReadDTO.size());
        Assertions.assertThat(actualMovieReviewLikesReadDTO).extracting("id")
                .containsExactlyInAnyOrder(movieReviewLike1.getId(), movieReviewLike2.getId());

        Mockito.verify(service).getRegisteredUserMovieReviewLikes(movieReviewLike1.getRegisteredUserId());
    }

    @Test
    public void testPutMovieReviewLike() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        MovieReviewLikePutDTO put = generateObject(MovieReviewLikePutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/"
                        + "movie-review-likes/{movieReviewLikeId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateMovieReviewLike(randomUUID, id, put);
    }
}
