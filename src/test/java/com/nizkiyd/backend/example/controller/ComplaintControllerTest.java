package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.Complaint;
import com.nizkiyd.backend.example.domain.ComplaintType;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.complaint.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.service.ComplaintService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = ComplaintController.class)
public class ComplaintControllerTest extends BaseControllerTest {

    @MockBean
    private ComplaintService complaintService;

    @Test
    public void testGetComplaint() throws Exception {
        ComplaintReadDTO complaint = generateObject(ComplaintReadDTO.class);

        Mockito.when(complaintService.getComplaint(complaint.getId())).thenReturn(complaint);

        String resultJson = mvc.perform(get("/api/v1/complaints/{id}", complaint.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ComplaintReadDTO actualComplaint = objectMapper.readValue(resultJson, ComplaintReadDTO.class);
        Assertions.assertThat(actualComplaint).isEqualToComparingFieldByField(complaint);

        Mockito.verify(complaintService).getComplaint(complaint.getId());
    }

    @Test
    public void testGetComplaintWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Complaint.class, wrongId);
        Mockito.when(complaintService.getComplaint(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/complaints/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchComplaint() throws Exception {
        ComplaintPatchDTO patch = generateObject(ComplaintPatchDTO.class);
        ComplaintReadDTO read = generateObject(ComplaintReadDTO.class);

        Mockito.when(complaintService.patchComplaint(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/complaints/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ComplaintReadDTO actualComplaint = objectMapper.readValue(resultJson, ComplaintReadDTO.class);
        Assert.assertEquals(read, actualComplaint);
    }

    @Test
    public void testDeleteComplaint() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/complaints/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(complaintService).deleteComplaint(id);
    }

    @Test
    public void testPutComplaint() throws Exception {
        UUID id = UUID.randomUUID();

        ComplaintPutDTO put = generateObject(ComplaintPutDTO.class);

        mvc.perform(put("/api/v1/complaints/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(complaintService).updateComplaint(id, put);
    }

    @Test
    public void testGetComplaints() throws Exception {
        ComplaintFilter filter = new ComplaintFilter();
        filter.setModeratorId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());
        filter.setType(ComplaintType.SPAM);
        filter.setComplaintObjectId(UUID.randomUUID());

        ComplaintReadDTO readDTO = generateObject(ComplaintReadDTO.class);

        PageResult<ComplaintReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(complaintService.getComplaint(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/complaints")
                .param("moderatorId", filter.getModeratorId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("type", filter.getType().toString())
                .param("complaintObjectId", filter.getComplaintObjectId().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ComplaintReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetComplaintWithPagingAndSorting() throws Exception {
        ComplaintFilter complaintFilter = new ComplaintFilter();
        ComplaintReadDTO readDTO = generateObject(ComplaintReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<ComplaintReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(complaintService.getComplaint(complaintFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/complaints")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ComplaintReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testProcessingComplaint() throws Exception {
        ComplaintProcessingDTO processingDTO = generateObject(ComplaintProcessingDTO.class);
        UUID complaintId = UUID.randomUUID();

        ComplaintReadDTO read = generateObject(ComplaintReadDTO.class);

        Mockito.when(complaintService.processingComplaint(complaintId, processingDTO)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/complaints/{id}/processing", complaintId)
                .content(objectMapper.writeValueAsString(processingDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ComplaintReadDTO actualComplaint = objectMapper.readValue(resultJson, ComplaintReadDTO.class);
        Assertions.assertThat(actualComplaint).isEqualToComparingFieldByField(read);
    }
}