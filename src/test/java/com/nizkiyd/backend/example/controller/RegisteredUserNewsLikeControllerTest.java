package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.NewsLike;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeCreateDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePatchDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikePutDTO;
import com.nizkiyd.backend.example.dto.newslike.NewsLikeReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserNewsLikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserNewsLikeController.class)
public class RegisteredUserNewsLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserNewsLikeService service;

    @Test
    public void testCreateNewsLikeValidationFailed() throws Exception {
        NewsLikeCreateDTO createDTO = new NewsLikeCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/news-likes", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createNewsLike(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetNewsLike() throws Exception {
        NewsLikeReadDTO newsLike = generateObject(NewsLikeReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getNewsLike(randomUUID, newsLike.getId())).thenReturn(newsLike);

        String resultJson = mvc.perform(
                get("/api/v1/registered-users/{registeredUserId}/news-likes/{newsLikeId}",
                        randomUUID, newsLike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeReadDTO actualNewsLike = objectMapper.readValue(
                resultJson, NewsLikeReadDTO.class);
        Assertions.assertThat(actualNewsLike).isEqualToComparingFieldByField(newsLike);

        Mockito.verify(service).getNewsLike(randomUUID, newsLike.getId());
    }

    @Test
    public void testGetNewsLikeWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(NewsLike.class, wrongId);
        Mockito.when(service.getNewsLike(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(
                get("/api/v1/registered-users/{registeredUserId}/news-likes/{newsLikeId}",
                        randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateNewsLike() throws Exception {
        NewsLikeCreateDTO create = generateObject(NewsLikeCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        NewsLikeReadDTO read = generateObject(NewsLikeReadDTO.class);

        Mockito.when(service.createNewsLike(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/news-likes",
                randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeReadDTO actualNewsLike = objectMapper.readValue(
                resultJson, NewsLikeReadDTO.class);
        Assertions.assertThat(actualNewsLike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchNewsLike() throws Exception {
        NewsLikePatchDTO patch = generateObject(NewsLikePatchDTO.class);
        ;
        UUID randomUUID = UUID.randomUUID();

        NewsLikeReadDTO read = generateObject(NewsLikeReadDTO.class);

        Mockito.when(service.patchNewsLike(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(
                patch("/api/v1/registered-users/{registeredUserId}" +
                        "/news-likes/{newsLikeId}", randomUUID, read.getId().toString())
                        .content(objectMapper.writeValueAsString(patch))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeReadDTO actualNewsLike = objectMapper.readValue(
                resultJson, NewsLikeReadDTO.class);
        Assert.assertEquals(read, actualNewsLike);
    }

    @Test
    public void testDeleteNewsLike() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/news-likes/{newsLikeId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteNewsLike(randomUUID, id);
    }

    @Test
    public void testGetRegisteredUserNewsLikes() throws Exception {
        NewsLikeReadDTO newsLike1 = generateObject(NewsLikeReadDTO.class);
        NewsLikeReadDTO newsLike2 = generateObject(NewsLikeReadDTO.class);

        List<NewsLikeReadDTO> newsLikesReadDTO = new ArrayList<>();
        newsLikesReadDTO.add(newsLike1);
        newsLikesReadDTO.add(newsLike2);

        Mockito.when(service.getRegisteredUserNewsLikes(
                newsLike1.getRegisteredUserId())).thenReturn(newsLikesReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/news-likes",
                newsLike1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<NewsLikeReadDTO> actualNewsLikesReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualNewsLikesReadDTO.size());
        Assertions.assertThat(actualNewsLikesReadDTO).extracting("id")
                .containsExactlyInAnyOrder(newsLike1.getId(), newsLike2.getId());

        Mockito.verify(service).getRegisteredUserNewsLikes(newsLike1.getRegisteredUserId());
    }

    @Test
    public void testPutNewsLike() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        NewsLikePutDTO put = generateObject(NewsLikePutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/"
                        + "news-likes/{newsLikeId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateNewsLike(randomUUID, id, put);
    }
}
