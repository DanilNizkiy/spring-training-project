package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.News;
import com.nizkiyd.backend.example.dto.PageResult;
import com.nizkiyd.backend.example.dto.news.NewsReadDTO;
import com.nizkiyd.backend.example.dto.news.*;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.NewsService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = NewsController.class)
public class NewsControllerTest extends BaseControllerTest {

    @MockBean
    private NewsService newsService;

    @WithMockUser
    @Test
    public void testCreateNewsValidationFailed() throws Exception {
        NewsCreateDTO createDTO = new NewsCreateDTO();
        String resultJson = mvc.perform(
                post("/api/v1/news")
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(newsService, Mockito.never()).createNews(ArgumentMatchers.any());
    }

    @Test
    public void testGetNews() throws Exception {
        NewsReadDTO news = generateObject(NewsReadDTO.class);

        Mockito.when(newsService.getNews(news.getId())).thenReturn(news);

        String resultJson = mvc.perform(get("/api/v1/news/{id}", news.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualMovie = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(news);

        Mockito.verify(newsService).getNews(news.getId());
    }

    @WithMockUser
    @Test
    public void testGetNewsWithRegisteredUserId() throws Exception {
        NewsReadDTO news = generateObject(NewsReadDTO.class);
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(newsService.getNews(news.getId())).thenReturn(news);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/news/{id}",
                registeredUserId, news.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualMovie = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(news);

        Mockito.verify(newsService).getNews(news.getId());
    }

    @Test
    public void testGetNewsWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(News.class, wrongId);
        Mockito.when(newsService.getNews(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/news/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @WithMockUser
    @Test
    public void testCreateNews() throws Exception {

        NewsCreateDTO create = generateObject(NewsCreateDTO.class);
        NewsReadDTO read = generateObject(NewsReadDTO.class);

        Mockito.when(newsService.createNews(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/news")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(actualNews).isEqualToComparingFieldByField(read);
    }

    @WithMockUser
    @Test
    public void testPatchNews() throws Exception {
        NewsPatchDTO patch = generateObject(NewsPatchDTO.class);
        NewsReadDTO read = generateObject(NewsReadDTO.class);

        Mockito.when(newsService.patchNews(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/news/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assert.assertEquals(read, actualNews);
    }

    @WithMockUser
    @Test
    public void testDeleteNews() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(newsService).deleteNews(id);
    }

    @WithMockUser
    @Test
    public void testPutNews() throws Exception {
        UUID id = UUID.randomUUID();

        NewsPutDTO put = generateObject(NewsPutDTO.class);

        mvc.perform(put("/api/v1/news/{id}", id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(newsService).updateNews(id, put);
    }

    @Test
    public void testGetNewsFilter() throws Exception {
        NewsFilter filter = new NewsFilter();
        filter.setMovieId(UUID.randomUUID());
        filter.setCreatedAtFrom(Instant.now());
        filter.setCreatedAtTo(Instant.now());
        filter.setActorId(UUID.randomUUID());
        filter.setCrewMemberId(UUID.randomUUID());

        NewsReadDTO readDTO = generateObject(NewsReadDTO.class);

        PageResult<NewsReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(readDTO));
        Mockito.when(newsService.getNews(
                filter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/news")
                .param("movieId", filter.getMovieId().toString())
                .param("actorId", filter.getActorId().toString())
                .param("crewMemberId", filter.getCrewMemberId().toString())
                .param("createdAtFrom", filter.getCreatedAtFrom().toString())
                .param("createdAtTo", filter.getCreatedAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<NewsReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetNewsWithPagingAndSorting() throws Exception {
        NewsFilter newsFilter = new NewsFilter();
        NewsReadDTO readDTO = generateObject(NewsReadDTO.class);

        int page = 1;
        int size = 25;

        PageResult<NewsReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(readDTO));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
        Mockito.when(newsService.getNews(newsFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/news")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "createdAt,asc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<NewsReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }
}