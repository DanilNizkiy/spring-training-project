package com.nizkiyd.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nizkiyd.backend.example.domain.RoleReview;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewCreateDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPatchDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewPutDTO;
import com.nizkiyd.backend.example.dto.rolereview.RoleReviewReadDTO;
import com.nizkiyd.backend.example.exception.EntityNotFoundException;
import com.nizkiyd.backend.example.exception.handler.ErrorInfo;
import com.nizkiyd.backend.example.service.RegisteredUserRoleReviewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = RegisteredUserRoleReviewController.class)
public class RegisteredUserRoleReviewControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserRoleReviewService service;

    @Test
    public void testCreateRoleReviewValidationFailed() throws Exception {
        RoleReviewCreateDTO createDTO = new RoleReviewCreateDTO();
        UUID randomUUID = UUID.randomUUID();
        String resultJson = mvc.perform(
                post("/api/v1/registered-users/{registeredUserId}/role-reviews", randomUUID)
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(service, Mockito.never()).createRoleReview(eq(randomUUID), ArgumentMatchers.any());
    }

    @Test
    public void testGetRoleReview() throws Exception {
        RoleReviewReadDTO roleReview = generateObject(RoleReviewReadDTO.class);
        UUID randomUUID = UUID.randomUUID();

        Mockito.when(service.getRoleReview(randomUUID, roleReview.getId())).thenReturn(roleReview);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-reviews/{roleReviewId}",
                randomUUID, roleReview.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewReadDTO actualRoleReview = objectMapper.readValue(resultJson, RoleReviewReadDTO.class);
        Assertions.assertThat(actualRoleReview).isEqualToComparingFieldByField(roleReview);

        Mockito.verify(service).getRoleReview(randomUUID, roleReview.getId());
    }

    @Test
    public void testGetRoleReviewWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RoleReview.class, wrongId);
        Mockito.when(service.getRoleReview(randomUUID, wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-reviews/{roleReviewId}",
                randomUUID, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRoleReview() throws Exception {
        RoleReviewCreateDTO create = generateObject(RoleReviewCreateDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleReviewReadDTO read = generateObject(RoleReviewReadDTO.class);

        Mockito.when(service.createRoleReview(randomUUID, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/{registeredUserId}/role-reviews", randomUUID)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewReadDTO actualRoleReview = objectMapper.readValue(resultJson, RoleReviewReadDTO.class);
        Assertions.assertThat(actualRoleReview).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRoleReview() throws Exception {
        RoleReviewPatchDTO patch = generateObject(RoleReviewPatchDTO.class);
        UUID randomUUID = UUID.randomUUID();

        RoleReviewReadDTO read = generateObject(RoleReviewReadDTO.class);

        Mockito.when(service.patchRoleReview(randomUUID, read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{registeredUserId}" +
                        "/role-reviews/{roleReviewId}",
                randomUUID, read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReviewReadDTO actualRoleReview = objectMapper.readValue(resultJson, RoleReviewReadDTO.class);
        Assert.assertEquals(read, actualRoleReview);
    }

    @Test
    public void testDeleteRoleReview() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{registeredUserId}/role-reviews/{roleReviewId}",
                randomUUID, id.toString())).andExpect(status().isOk());

        Mockito.verify(service).deleteRoleReview(randomUUID, id);
    }

    @Test
    public void testPutRoleReview() throws Exception {
        UUID id = UUID.randomUUID();
        UUID randomUUID = UUID.randomUUID();

        RoleReviewPutDTO put = generateObject(RoleReviewPutDTO.class);

        mvc.perform(put("/api/v1/registered-users/{registeredUserId}/role-reviews/{roleReviewId}",
                randomUUID, id.toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(service).updateRoleReview(randomUUID, id, put);
    }

    @Test
    public void testGetRegisteredUserRoleReviews() throws Exception {
        RoleReviewReadDTO roleReview1 = generateObject(RoleReviewReadDTO.class);
        RoleReviewReadDTO roleReview2 = generateObject(RoleReviewReadDTO.class);

        List<RoleReviewReadDTO> roleReviewsReadDTO = new ArrayList<>();
        roleReviewsReadDTO.add(roleReview1);
        roleReviewsReadDTO.add(roleReview2);

        Mockito.when(service.getRegisteredUserRoleReviews(
                roleReview1.getRegisteredUserId())).thenReturn(roleReviewsReadDTO);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{registeredUserId}/role-reviews",
                roleReview1.getRegisteredUserId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReviewReadDTO> actualRoleReviewsReadDTO = objectMapper.readValue(
                resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(2, actualRoleReviewsReadDTO.size());
        Assertions.assertThat(actualRoleReviewsReadDTO).extracting("id")
                .containsExactlyInAnyOrder(roleReview1.getId(), roleReview2.getId());

        Mockito.verify(service).getRegisteredUserRoleReviews(roleReview1.getRegisteredUserId());
    }
}
