package com.nizkiyd.backend.example.util;

import com.nizkiyd.backend.example.domain.*;
import com.nizkiyd.backend.example.repository.*;
import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

@Service
public class TestObjectsFactory {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleReviewRepository roleReviewRepository;

    @Autowired
    private MovieReviewLikeRepository movieReviewLikeRepository;

    @Autowired
    private RoleReviewLikeRepository roleReviewLikeRepository;

    @Autowired
    private MovieMarkRepository movieMarkRepository;

    @Autowired
    private RoleMarkRepository roleMarkRepository;

    @Autowired
    private NewsLikeRepository newsLikeRepository;

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AdminRepository adminRepository;

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    private RandomObjectGenerator flatGenerator;

    {
        Configuration c = new Configuration();
        c.setFlatMode(true);
        flatGenerator = new RandomObjectGenerator(c);
    }

    public CrewMember createCrewMember() {
        CrewMember crewMember = generateFlatEntityWithoutId(CrewMember.class);
        return crewMember = crewMemberRepository.save(crewMember);
    }

    public CrewMember createCrewMember(Person person, CrewMemberType crewMemberType) {
        CrewMember crewMember = generateFlatEntityWithoutId(CrewMember.class);
        crewMember.setPerson(person);
        crewMember.setCrewMemberType(crewMemberType);
        return crewMember = crewMemberRepository.save(crewMember);
    }

    public CrewMember createCrewMember(Person person) {
        CrewMember crewMember = generateFlatEntityWithoutId(CrewMember.class);
        crewMember.setPerson(person);
        return crewMember = crewMemberRepository.save(crewMember);
    }

    public NewsLike createNewsLike(RegisteredUser registeredUser) {
        NewsLike newsLike = generateFlatEntityWithoutId(NewsLike.class);
        newsLike.setNews(createNews());
        newsLike.setRegisteredUser(registeredUser);
        return newsLike = newsLikeRepository.save(newsLike);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser) {
        SignalToContentManager signal = generateFlatEntityWithoutId(SignalToContentManager.class);
        signal.setContentManager(createContentManager());
        signal.setRegisteredUser(registeredUser);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerStatus status) {
        SignalToContentManager signal = generateFlatEntityWithoutId(SignalToContentManager.class);
        signal.setContentManager(createContentManager());
        signal.setSignalToContentManagerStatus(status);
        signal.setRegisteredUser(registeredUser);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public ExternalSystemImport createExternalSystemImport() {
        ExternalSystemImport externalSystemImport = generateFlatEntityWithoutId(ExternalSystemImport.class);
        return externalSystemImport = externalSystemImportRepository.save(externalSystemImport);
    }

    public Complaint createComplaint(
            RegisteredUser registeredUser, ComplaintObjectType complaintObjectType,
            UUID complaintObjectId, Moderator moderator) {
        Complaint complaint = generateFlatEntityWithoutId(Complaint.class);
        complaint.setComplaintObjectType(complaintObjectType);
        complaint.setComplaintObjectId(complaintObjectId);
        complaint.setModerator(moderator);
        complaint.setRegisteredUser(registeredUser);
        return complaint = complaintRepository.save(complaint);
    }

    public Complaint createComplaint(RegisteredUser registeredUser, ComplaintObjectType complaintObjectType,
                                     UUID complaintObjectId, ComplaintType complaintType) {
        Complaint complaint = new Complaint();
        complaint.setComplaintObjectType(complaintObjectType);
        complaint.setComplaintObjectId(complaintObjectId);
        complaint.setType(complaintType);
        complaint.setRegisteredUser(registeredUser);
        return complaint = complaintRepository.save(complaint);
    }

    public Complaint createComplaint(
            RegisteredUser registeredUser, ComplaintObjectType complaintObjectType,
            UUID complaintObjectId, Moderator moderator, ComplaintType complaintType) {
        Complaint complaint = generateFlatEntityWithoutId(Complaint.class);
        complaint.setType(complaintType);
        complaint.setComplaintObjectType(complaintObjectType);
        complaint.setComplaintObjectId(complaintObjectId);
        complaint.setModerator(moderator);
        complaint.setRegisteredUser(registeredUser);
        return complaint = complaintRepository.save(complaint);
    }

    public Complaint createComplaint(
            RegisteredUser registeredUser, ComplaintObjectType complaintObjectType,
            UUID complaintObjectId, Moderator moderator, ComplaintStatus complaintStatus) {
        Complaint complaint = generateFlatEntityWithoutId(Complaint.class);
        complaint.setComplaintStatus(complaintStatus);
        complaint.setComplaintObjectType(complaintObjectType);
        complaint.setComplaintObjectId(complaintObjectId);
        complaint.setModerator(moderator);
        complaint.setRegisteredUser(registeredUser);
        return complaint = complaintRepository.save(complaint);
    }

    public Complaint createComplaint(RegisteredUser registeredUser) {
        Complaint complaint = generateFlatEntityWithoutId(Complaint.class);
        MovieReview movieReview = movieReviewRepository.save(createMovieReview(registeredUser));
        complaint.setComplaintObjectId(movieReview.getId());
        complaint.setRegisteredUser(registeredUser);
        return complaint = complaintRepository.save(complaint);
    }

    public RoleMark createRoleMark(RegisteredUser registeredUser, Role role) {
        RoleMark roleMark = generateFlatEntityWithoutId(RoleMark.class);
        roleMark.setRegisteredUser(registeredUser);
        roleMark.setRole(role);
        return roleMark = roleMarkRepository.save(roleMark);
    }

    public RoleMark createRoleMark(RegisteredUser registeredUser, Role role, Integer mark) {
        RoleMark roleMark = generateFlatEntityWithoutId(RoleMark.class);
        roleMark.setMark(mark);
        roleMark.setRegisteredUser(registeredUser);
        roleMark.setRole(role);
        return roleMark = roleMarkRepository.save(roleMark);
    }

    public RoleMark createRoleMark(RegisteredUser registeredUser) {
        RoleMark roleMark = generateFlatEntityWithoutId(RoleMark.class);
        roleMark.setRole(createRole());
        roleMark.setRegisteredUser(registeredUser);
        return roleMark = roleMarkRepository.save(roleMark);
    }

    public MovieMark createMovieMark(RegisteredUser registeredUser) {
        MovieMark movieMark = generateFlatEntityWithoutId(MovieMark.class);
        movieMark.setMovie(createMovie());
        movieMark.setRegisteredUser(registeredUser);
        return movieMark = movieMarkRepository.save(movieMark);
    }

    public MovieMark createMovieMark(RegisteredUser registeredUser, Movie movie) {
        MovieMark movieMark = generateFlatEntityWithoutId(MovieMark.class);
        movieMark.setRegisteredUser(registeredUser);
        movieMark.setMovie(movie);
        return movieMark = movieMarkRepository.save(movieMark);
    }

    public MovieMark createMovieMark(RegisteredUser registeredUser, Movie movie, Integer mark) {
        MovieMark movieMark = generateFlatEntityWithoutId(MovieMark.class);
        movieMark.setMark(mark);
        movieMark.setRegisteredUser(registeredUser);
        movieMark.setMovie(movie);
        return movieMark = movieMarkRepository.save(movieMark);
    }

    public MovieMark createMovieMark(RegisteredUser registeredUser, Movie movie, boolean withMark) {
        MovieMark movieMark = generateFlatEntityWithoutId(MovieMark.class);
        if (!withMark) {
            movieMark.setMark(null);
        }
        movieMark.setRegisteredUser(registeredUser);
        movieMark.setMovie(movie);
        return movieMark = movieMarkRepository.save(movieMark);
    }

    public RoleReviewLike createRoleReviewLike(RegisteredUser registeredUser, RoleReview roleReview) {
        RoleReviewLike roleReviewLike = generateFlatEntityWithoutId(RoleReviewLike.class);
        roleReviewLike.setRegisteredUser(registeredUser);
        roleReviewLike.setRoleReview(roleReview);
        return roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);
    }

    public RoleReviewLike createRoleReviewLike(RegisteredUser registeredUser) {
        RoleReviewLike roleReviewLike = generateFlatEntityWithoutId(RoleReviewLike.class);
        roleReviewLike.setRoleReview(createRoleReview(registeredUser));
        roleReviewLike.setRegisteredUser(registeredUser);
        return roleReviewLike = roleReviewLikeRepository.save(roleReviewLike);
    }

    public RoleReview createRoleReview(RegisteredUser registeredUser) {
        RoleReview roleReview = generateFlatEntityWithoutId(RoleReview.class);
        roleReview.setRole(createRole());
        roleReview.setRegisteredUser(registeredUser);
        return roleReview = roleReviewRepository.save(roleReview);
    }

    public MovieReviewLike createMovieReviewLike(RegisteredUser registeredUser, MovieReview movieReview) {
        MovieReviewLike movieReviewLike = generateFlatEntityWithoutId(MovieReviewLike.class);
        movieReviewLike.setRegisteredUser(registeredUser);
        movieReviewLike.setMovieReview(movieReview);
        return movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);
    }

    public NewsLike createNewsLike(RegisteredUser registeredUser, News news) {
        NewsLike newsLike = generateFlatEntityWithoutId(NewsLike.class);
        newsLike.setRegisteredUser(registeredUser);
        newsLike.setNews(news);
        return newsLike = newsLikeRepository.save(newsLike);
    }

    public MovieReviewLike createMovieReviewLike(RegisteredUser registeredUser) {
        MovieReviewLike movieReviewLike = generateFlatEntityWithoutId(MovieReviewLike.class);
        movieReviewLike.setMovieReview(createMovieReview(registeredUser));
        movieReviewLike.setRegisteredUser(registeredUser);
        return movieReviewLike = movieReviewLikeRepository.save(movieReviewLike);
    }


    public RoleReview createRoleReview(RegisteredUser registeredUser, Role role, Moderator moderator) {
        RoleReview roleReview = generateFlatEntityWithoutId(RoleReview.class);
        roleReview.setRegisteredUser(registeredUser);
        roleReview.setRole(role);
        roleReview.setModerator(moderator);
        return roleReview = roleReviewRepository.save(roleReview);
    }

    public Role createRole() {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setAverageMark(null);
        role.setMovie(createMovie());
        return role = roleRepository.save(role);
    }

    public RoleReview createRoleReview(
            RegisteredUser registeredUser, Role role, Moderator moderator, ReviewStatus reviewStatus) {
        RoleReview roleReview = generateFlatEntityWithoutId(RoleReview.class);
        roleReview.setRegisteredUser(registeredUser);
        roleReview.setRole(role);
        roleReview.setModerator(moderator);
        roleReview.setReviewStatus(reviewStatus);
        return roleReview = roleReviewRepository.save(roleReview);
    }

    public MovieReview createMovieReview(RegisteredUser registeredUser, Movie movie, Moderator moderator) {
        MovieReview movieReview = generateFlatEntityWithoutId(MovieReview.class);
        movieReview.setRegisteredUser(registeredUser);
        movieReview.setMovie(movie);
        movieReview.setModerator(moderator);
        return movieReview = movieReviewRepository.save(movieReview);
    }

    public MovieReview createMovieReview(
            RegisteredUser registeredUser, Movie movie, Moderator moderator, ReviewStatus reviewStatus) {
        MovieReview movieReview = generateFlatEntityWithoutId(MovieReview.class);
        movieReview.setRegisteredUser(registeredUser);
        movieReview.setMovie(movie);
        movieReview.setModerator(moderator);
        movieReview.setReviewStatus(reviewStatus);
        return movieReview = movieReviewRepository.save(movieReview);
    }

    public MovieReview createMovieReview(RegisteredUser registeredUser) {
        MovieReview movieReview = generateFlatEntityWithoutId(MovieReview.class);
        movieReview.setMovie(createMovie());
        movieReview.setRegisteredUser(registeredUser);
        return movieReview = movieReviewRepository.save(movieReview);
    }

    public RegisteredUser createRegisteredUser() {
        RegisteredUser registeredUser = generateFlatEntityWithoutId(RegisteredUser.class);
        registeredUser.setAge(55);
        return registeredUser = registeredUserRepository.save(registeredUser);
    }

    public RegisteredUser createRegisteredUser(AccountStatus accountStatus, Boolean trust) {
        RegisteredUser registeredUser = generateFlatEntityWithoutId(RegisteredUser.class);
        registeredUser.setStatus(accountStatus);
        registeredUser.setTrust(trust);
        registeredUser.setAge(55);
        return registeredUser = registeredUserRepository.save(registeredUser);
    }

    public RegisteredUser createRegisteredUser(Boolean trust) {
        RegisteredUser registeredUser = generateFlatEntityWithoutId(RegisteredUser.class);
        registeredUser.setTrust(false);
        registeredUser.setAge(55);
        return registeredUser = registeredUserRepository.save(registeredUser);
    }

    public RegisteredUser createRegisteredUser(AccountStatus status) {
        RegisteredUser registeredUser = generateFlatEntityWithoutId(RegisteredUser.class);
        registeredUser.setAge(55);
        registeredUser.setStatus(status);
        return registeredUser = registeredUserRepository.save(registeredUser);
    }

    public Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movie = movieRepository.save(movie);
    }

    public Movie createMovie(Double averageMark) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setAverageMark(averageMark);
        return movie = movieRepository.save(movie);
    }

    public Movie createMovie(String title, MovieStatus movieStatus) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setTitle(title);
        movie.setMovieStatus(movieStatus);
        return movie = movieRepository.save(movie);
    }

    public Movie createMovieWithEmptyAverageMark() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setAverageMark(null);
        return movie = movieRepository.save(movie);
    }

    public Movie createMovie(UUID genreId, MovieStatus movieStatus, Double averageMark) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.getGenres().add(genreRepository.findById(genreId).get());
        movie.setMovieStatus(movieStatus);
        movie.setAverageMark(averageMark);
        return movie = movieRepository.save(movie);
    }

    public Movie createMovie(MovieStatus movieStatus, Double averageMark) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setMovieStatus(movieStatus);
        movie.setAverageMark(averageMark);
        return movie = movieRepository.save(movie);
    }


    public Movie createMovie(MovieStatus movieStatus) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setMovieStatus(movieStatus);
        return movie = movieRepository.save(movie);
    }

    public Movie createMovie(String description) {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setDescription(description);
        return movie = movieRepository.save(movie);
    }

    public Moderator createModerator() {
        Moderator moderator = generateFlatEntityWithoutId(Moderator.class);
        return moderator = moderatorRepository.save(moderator);
    }

    public Moderator createModerator(AccountStatus status) {
        Moderator moderator = generateFlatEntityWithoutId(Moderator.class);
        moderator.setStatus(status);
        return moderator = moderatorRepository.save(moderator);
    }

    public Instant createInstant(int hour) {
        return LocalDateTime.of(2020, 2, 22, hour, 0).toInstant(ZoneOffset.UTC);
    }

    public Actor createActor() {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setAverageRoleRating(null);
        actor.setAverageMovieRating(null);
        return actor = actorRepository.save(actor);
    }

    public Actor createActor(Person person, Double averageMovieRating) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        actor.setAverageMovieRating(averageMovieRating);
        return actor = actorRepository.save(actor);
    }

    public Actor createActor(Double averageRoleRating) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setAverageRoleRating(averageRoleRating);
        return actor = actorRepository.save(actor);
    }

    public Actor createActor(Double averageRoleRating, Double averageMovieRating, Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setAverageMovieRating(averageMovieRating);
        actor.setAverageRoleRating(averageRoleRating);
        actor.setPerson(person);
        return actor = actorRepository.save(actor);
    }

    public Actor createActor(String biography) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        return actor = actorRepository.save(actor);
    }

    public Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        return actor = actorRepository.save(actor);
    }

    public Role createRole(Movie movie, Actor actor) {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setMovie(movie);
        role.setActor(actor);
        return role = roleRepository.save(role);
    }

    public Role createRole(Movie movie, Actor actor, Double averageMark) {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setMovie(movie);
        role.setActor(actor);
        role.setAverageMark(averageMark);
        return role = roleRepository.save(role);
    }

    public News createNews(Actor actor, Movie movie) {
        News news = generateFlatEntityWithoutId(News.class);
        return news = newsRepository.save(news);
    }

    public News createNews() {
        News news = generateFlatEntityWithoutId(News.class);
        news.setLike(0);
        news.setDislike(0);
        return news = newsRepository.save(news);
    }

    public News createNews(String text) {
        News news = generateFlatEntityWithoutId(News.class);
        news.setText(text);
        return news = newsRepository.save(news);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerObjectType signalObjectType,
                                                               UUID signalObjectId,
                                                               ContentManager contentManager,
                                                               SignalToContentManagerStatus status) {
        SignalToContentManager signal = generateFlatEntityWithoutId(SignalToContentManager.class);
        signal.setRegisteredUser(registeredUser);
        signal.setContentManager(contentManager);
        signal.setSignalToContentManagerObjectId(signalObjectId);
        signal.setSignalToContentManagerObjectType(signalObjectType);
        signal.setSignalToContentManagerStatus(status);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerObjectType signalObjectType,
                                                               UUID signalObjectId,
                                                               ContentManager contentManager) {
        SignalToContentManager signal = generateFlatEntityWithoutId(SignalToContentManager.class);
        signal.setRegisteredUser(registeredUser);
        signal.setContentManager(contentManager);
        signal.setSignalToContentManagerObjectId(signalObjectId);
        signal.setSignalToContentManagerObjectType(signalObjectType);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerObjectType signalObjectType,
                                                               UUID signalObjectId,
                                                               Integer fromIndex,
                                                               Integer toIndex,
                                                               String replacement) {
        SignalToContentManager signal = generateFlatEntityWithoutId(SignalToContentManager.class);
        signal.setReplacement(replacement);
        signal.setFromIndex(fromIndex);
        signal.setToIndex(toIndex);
        signal.setRegisteredUser(registeredUser);
        signal.setSignalToContentManagerObjectId(signalObjectId);
        signal.setSignalToContentManagerObjectType(signalObjectType);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerObjectType signalObjectType,
                                                               UUID signalObjectId,
                                                               Integer fromIndex,
                                                               Integer toIndex,
                                                               String replacement,
                                                               String wrongText) {
        SignalToContentManager signal = generateFlatEntityWithoutId(SignalToContentManager.class);
        signal.setReplacement(replacement);
        signal.setWrongText(wrongText);
        signal.setFromIndex(fromIndex);
        signal.setToIndex(toIndex);
        signal.setRegisteredUser(registeredUser);
        signal.setSignalToContentManagerObjectId(signalObjectId);
        signal.setSignalToContentManagerObjectType(signalObjectType);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerObjectType signalObjectType,
                                                               UUID signalObjectId,
                                                               Integer fromIndex,
                                                               Integer toIndex,
                                                               SignalToContentManagerStatus status) {
        SignalToContentManager signal = new SignalToContentManager();
        signal.setReplacement("java");
        signal.setWrongText("world");
        signal.setFromIndex(fromIndex);
        signal.setToIndex(toIndex);
        signal.setFixedAt(createInstant(8));
        signal.setSignalToContentManagerStatus(status);
        signal.setRegisteredUser(registeredUser);
        signal.setSignalToContentManagerObjectId(signalObjectId);
        signal.setSignalToContentManagerObjectType(signalObjectType);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public SignalToContentManager createSignalToContentManager(RegisteredUser registeredUser,
                                                               SignalToContentManagerObjectType signalObjectType,
                                                               UUID signalObjectId,
                                                               String replacement,
                                                               String wrongText) {
        SignalToContentManager signal = new SignalToContentManager();
        signal.setReplacement(replacement);
        signal.setWrongText(wrongText);
        signal.setFromIndex(6);
        signal.setToIndex(11);
        signal.setFixedAt(createInstant(8));
        signal.setSignalToContentManagerStatus(SignalToContentManagerStatus.NEED_TO_FIX);
        signal.setRegisteredUser(registeredUser);
        signal.setSignalToContentManagerObjectId(signalObjectId);
        signal.setSignalToContentManagerObjectType(signalObjectType);
        return signal = signalToContentManagerRepository.save(signal);
    }

    public Person cretePerson() {
        Person person = generateEntityWithoutId(Person.class);
        return person = personRepository.save(person);
    }

    public Person cretePerson(String name, Gender gender) {
        Person person = generateEntityWithoutId(Person.class);
        person.setName(name);
        person.setGender(gender);
        return person = personRepository.save(person);
    }

    public Person cretePerson(String biography) {
        Person person = generateEntityWithoutId(Person.class);
        person.setBiography(biography);
        return person = personRepository.save(person);
    }

    public Admin creteAdmin() {
        Admin admin = generateFlatEntityWithoutId(Admin.class);
        return admin = adminRepository.save(admin);
    }

    public Admin creteAdmin(AccountStatus status) {
        Admin admin = generateFlatEntityWithoutId(Admin.class);
        admin.setStatus(status);
        return admin = adminRepository.save(admin);
    }

    public Admin creteAdmin(String email, String password) {
        Admin admin = generateFlatEntityWithoutId(Admin.class);
        admin.setEmail(email);
        admin.setEncodedPassword(password);
        return admin = adminRepository.save(admin);
    }

    public ContentManager creteContentManager() {
        ContentManager contentManager = generateFlatEntityWithoutId(ContentManager.class);
        return contentManager = contentManagerRepository.save(contentManager);
    }

    public Moderator creteModerator() {
        Moderator moderator = generateFlatEntityWithoutId(Moderator.class);
        return moderator = moderatorRepository.save(moderator);
    }

    public ContentManager createContentManager() {
        ContentManager contentManager = generateFlatEntityWithoutId(ContentManager.class);
        return contentManagerRepository.save(contentManager);
    }

    public ContentManager createContentManager(AccountStatus status) {
        ContentManager contentManager = generateFlatEntityWithoutId(ContentManager.class);
        contentManager.setStatus(status);
        return contentManagerRepository.save(contentManager);
    }

    private <T extends AbstractEntity> T generateEntityWithoutId(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    private <T extends AbstractEntity> T generateFlatEntityWithoutId(Class<T> entityClass) {
        T entity = flatGenerator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }
}
